﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSConcatenateFields : DTStep
    {
        private PrmsConcatenateFields prms;
        public OSConcatenateFields(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsConcatenateFields;
            if (prms == null)
            {
                throw new MemberAccessException("Prms_OSConcatenateFields parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            if (string.IsNullOrEmpty(prms.InputFieldNames) || string.IsNullOrEmpty(prms.OutputFieldName) || string.IsNullOrEmpty(prms.FieldSeparator))
            {
                throw new MemberAccessException("Prms_OSConcatenateFields - " + ftdt.DTAction.ActionName + " not configured correctly, please update the settings and try again.");
            }
        }
        public override bool Execute()
        {
            //Add the note column if it doesn't exist
            if (!ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(prms.OutputFieldName))
            {
                ParentFTDT.FileTemplate.ProcessedDataTable.AddColumn(prms.OutputFieldName);
            }

            var rows = !string.IsNullOrEmpty(prms.FieldValues) ? ParentFTDT.FileTemplate.ProcessedDataTable.Select(prms.FieldValues) : ParentFTDT.FileTemplate.ProcessedDataTable.Select();
            foreach (DataRow row in rows)
            {
                AppendFields(row);
                ClearInputFields(row);
            }

            DCPostDT(rows);
            RemoveInputFields();
            return true;
        }

        private string[] conFields = null;

        public string[] ConFields
        {
            get
            {
                if (conFields == null)
                {
                    conFields = prms.InputFieldNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray());
                }
                return conFields;
            }
        }

        private string GetText(DataRow row, string fieldName)
        {
            fieldName = fieldName.Trim();
            string conStr = string.Empty;
            DataColumn dc = (DataColumn)this.ParentFTDT.FileTemplate.BUDataTable.Columns[fieldName];
            try
            {
                if (!string.IsNullOrEmpty(row[fieldName].ToString()))
                {
                    if (prms.RegularExpressionId.HasValue)
                    {
                        Regex regex = new Regex(prms.RegularExpression.RegExText);
                        Match match = null;
                        match = regex.Match(row[fieldName].ToString());
                        if (match.Length <= 0)
                        {
                            return conStr;
                        }
                    }

                    if (dc != null)
                    {
                        conStr = row[fieldName].ObjToString(dc) + prms.FieldSeparator;
                    }
                    else
                    {
                        conStr = row[fieldName].ToString() + prms.FieldSeparator;
                    }

                    if (prms.IncludeFieldName)
                        conStr = fieldName + ":" + conStr;
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            if (prms.RegExRefListId.HasValue)
                conStr = RemoveTextsByRegEx(conStr);

            if (!string.IsNullOrEmpty(conStr) && prms.FixedWidthFieldLength > 0)
            {
                conStr = string.Format("{0,-" + prms.FixedWidthFieldLength.ToString() + "}", conStr);
            }


            return conStr;
        }


        private void AppendFields(DataRow row)
        {
            string conStr = string.Empty;
            int chunkLength = prms.SubStrMaxLength == 0 ? Utility.ONESTEP_MAX_LENGTH : prms.SubStrMaxLength;
            foreach (var fieldName in ConFields)
            {
                if (fieldName.EndsWith("*"))
                {
                    string[] columnNames = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
                                .Where (y=> y.ColumnName.StartsWith(fieldName.Substring(0,fieldName.Length-1)))
                                 .Select(x => x.ColumnName)
                                 .ToArray();
                    if (columnNames != null && columnNames.Length > 0)
                    {
                        foreach (var cn in columnNames)
                        {
                            conStr = conStr + GetText(row, cn);
                        }
                    }
                }
                else
                {
                    conStr = conStr + GetText(row, fieldName);
                }
            }

            conStr = conStr.Trim(prms.FieldSeparator.ToCharArray());

            if (prms.SplitOutputString && conStr.Length > chunkLength)
                conStr = string.Join(Utility.ONESTEP_STRING_SEPARATOR, conStr.SmartSplit(chunkLength).ToArray());
            
            if (prms.OverwriteOutputField)
                row[prms.OutputFieldName] = conStr;
            else
                row[prms.OutputFieldName] = string.IsNullOrEmpty( row[prms.OutputFieldName].ToString()) ? conStr: row[prms.OutputFieldName].ToString() + prms.FieldSeparator + conStr;
        }

        private void RemoveInputFields()
        {
            if (prms.HideInputField == true)
            {
                foreach (var fieldName in ConFields)
                {
                    if (fieldName.EndsWith("*"))
                    {
                        string[] columnNames = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
                                    .Where(y => y.ColumnName.StartsWith(fieldName.Substring(0, fieldName.Length - 1)))
                                     .Select(x => x.ColumnName)
                                     .ToArray();

                        if (columnNames != null && columnNames.Length > 0)
                        {
                            foreach (var cn in columnNames)
                            {
                                //Delete wildcard columns here
                                ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Remove(cn);
                            }
                        }
                    }
                    else
                    {
                        //Delete single column here
                        ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Remove(fieldName);
                    }
                }
            }
        }

        private void ClearInputFields(DataRow row)
        {
            if (prms.ClearInputFields == true)
            {
                foreach (var fieldName in ConFields)
                {
                    if (fieldName.EndsWith("*"))
                    {
                        //Clear multi columns with wildcard
                        string[] columnNames = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
                                    .Where(y => y.ColumnName.StartsWith(fieldName.Substring(0, fieldName.Length - 1)))
                                     .Select(x => x.ColumnName)
                                     .ToArray();

                        if (columnNames != null && columnNames.Length > 0)
                        {
                            foreach (var cn in columnNames)
                            {
                                row[cn] = null;
                            }
                        }
                    }
                    else
                    {
                        //Clear single column here
                        row[fieldName]=null;
                    }
                }
            }
        }

        private void DCPostDT(DataRow[] rows)
        {
            DataField df = this.ParentFTDT.FileTemplate.BodyCols.Where(x => x.Name == prms.OutputFieldName).FirstOrDefault();
            if (df != null)
            {
                if (prms.DCPostDT == true && ((EnumFieldType)df.FieldType) == EnumFieldType.String)
                {
                    foreach (DataRow row in rows)
                    {
                        df.OriginalValue = row[prms.OutputFieldName].ToString();
                        foreach (var dc in df.DataFieldDataCleansings)
                        {
                            dc.DCAction.ExecuteAction();
                        }
                        row[prms.OutputFieldName] = df.OriginalValue;
                    }
                }

            }

        }

        private string RemoveTextsByRegEx(string inputStr)
        {
            foreach (var ptn in prms.RegExRefList.ReferenceListItems)
            {
                inputStr = Regex.Replace(inputStr, ptn.Description.Trim(), string.Empty, RegexOptions.IgnoreCase);
            }
            return inputStr;
        }
    }
}