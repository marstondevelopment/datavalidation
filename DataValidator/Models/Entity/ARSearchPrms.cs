﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class ARSearchPrms<T>:SearchPrms<T>
    {
        public ARSearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults,int? arActionTypeId):base( sortOrder,  searchString,  page,  pageSize,  defaultPageSize, allResults)
        {
            ARActionTypeId = arActionTypeId;
        }

        [Display(Name = "AR Action Type")]
        public int? ARActionTypeId { get; set; }

    }
}