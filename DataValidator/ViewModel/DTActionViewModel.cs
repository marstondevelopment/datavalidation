﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mgl.DataValidator.Models;

namespace Mgl.DataValidator.ViewModel
{
    public class DTActionViewModel
    {
        public DTActionViewModel(DTActionRepository dtr, DTAction dTAction)
        {
            _dtr = dtr;
            DTAction = dTAction;
            var regexs = RegexsListItems;
        }
        private DTActionRepository _dtr;

        public bool IsDetails;

        public DTAction DTAction { get; set; }
        public IEnumerable<DTAction> DTActions { get; set; }       
        public DTASearchPrms<DTAction> DTSearchPrms { get; set; }

        private List<SelectListItem> dtActionListItems = null;
        public List<SelectListItem> SelectDTActions
        {
            get
            {
                if (dtActionListItems == null)
                {
                    dtActionListItems = new List<SelectListItem>();
                    dtActionListItems.AddRange(_dtr.GetAll()
                        .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.ActionName }));
                }
                return dtActionListItems;
            }
        }

        private List<SelectListItem> spListItems = null;
        public List<SelectListItem> SelectSPs
        {
            get
            {
                if (spListItems == null)
                {
                    spListItems = new List<SelectListItem>();
                    spListItems.AddRange(_dtr.GetSPs()
                        .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.SPName }));
                }
                return spListItems;
            }
        }

        private List<SelectListItem> ctListItems = null;
        public List<SelectListItem> CTListItems
        {
            get
            {
                if (ctListItems == null)
                {
                    ctListItems = new List<SelectListItem>();
                    ctListItems.AddRange(_dtr.GetCmdTxts()
                        .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
                }
                return ctListItems;
            }
        }

        private List<SelectListItem> regexsListItems = null;
        public List<SelectListItem> RegexsListItems
        {
            get
            {
                if (regexsListItems == null)
                {
                    regexsListItems = _dtr.GetRegexs().Select(x => new SelectListItem() { Text = x.Description, Value = x.Id.ToString() }).ToList();
                }

                return regexsListItems;
            }
        }
        private List<SelectListItem> refLists = null;
        public List<SelectListItem> RefLists
        {
            get
            {
                if (refLists == null)
                {
                    refLists = _dtr.GetRefLists().Select(x => new SelectListItem() { Text = x.Name, Value = x.Id.ToString() }).ToList();
                }

                return refLists;
            }
        }
        private List<SelectListItem> lMPLListItems = null;
        public List<SelectListItem> LMPLListItems
        {
            get
            {
                if (lMPLListItems == null)
                {
                    int[] numbers = new int[9] { 2,3,4,5,6,7,8,9,10};
                    lMPLListItems = numbers.Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
                }

                return lMPLListItems;
            }
        }
    }
}