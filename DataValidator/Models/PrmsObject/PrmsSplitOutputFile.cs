﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitOutputFile : PrmsBase
    {
        public PrmsSplitOutputFile() { }
        public PrmsSplitOutputFile(DTAction dt) : base(dt) { }

        [Display(Name = "Filters")]
        public string FieldValues { get; set; }
        [Display(Name = "Scheme Names")]
        public string Scheme { get; set; }
    }
}