﻿CREATE TABLE [dbo].[DataField] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (250) NULL,
    [FieldType]   INT            NOT NULL,
    [CreatedBy]   INT            NOT NULL,
    [DateCreated] DATETIME       NOT NULL,
    [UpdatedBy]   INT            NOT NULL,
    [DateUpdated] DATETIME       NOT NULL,
    [Disabled]    BIT            CONSTRAINT [DF_DataField_Disabled] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DataField] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DataField_User] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_DataField_User1] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);

