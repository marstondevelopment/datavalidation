﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsExtractLinkedCasesAfterGroupBy : PrmsBase
    {
        public PrmsExtractLinkedCasesAfterGroupBy() { }
        public PrmsExtractLinkedCasesAfterGroupBy(DTAction dt) : base(dt) { }

        [Display(Name = "Group By")]
        public string GBFieldNames { get; set; }

        [Display(Name = "Filter Column Name")]
        public string FilterFieldName { get; set; }

        [Display(Name = "Threshold Value")]
        public decimal ThresholdValue { get; set; }

        [Display(Name = "Comparison Operator")]
        public int CompOpId { get; set; }
        [JsonIgnore]
        public EnumComparisonOperator ComparisonOperator => (EnumComparisonOperator)CompOpId;

        [Display(Name = "Scheme Name")]
        public string SchemeName { get; set; }

        [Display(Name = "Include Single Cases")]
        public bool IncludeSingleCases{ get; set; }

        [Display(Name = "DB Value Column Name")]
        public string DBValueColumnName { get; set; }
    }
}