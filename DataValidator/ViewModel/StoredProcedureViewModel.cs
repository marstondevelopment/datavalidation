﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.ViewModel
{
    public class StoredProcedureViewModel
    {
        private StoredProcedureRepository _spr;
        public StoredProcedureViewModel(StoredProcedureRepository spr, StoredProcedure sp)
        {
            _spr = spr;
            StoredProcedure = sp;
        }
        public StoredProcedure StoredProcedure { get; set; }
        public IEnumerable<StoredProcedure> StoredProcedures { get; set; }        

        private List<SelectListItem> selectListItems = null;
        public List<SelectListItem> DBConnectionListItems
        {
            get
            {
                if (selectListItems == null)
                {
                    selectListItems = new List<SelectListItem>();
                    foreach (var connection in _spr.GetConnections().OrderBy(x => x.Name))
                    {
                        selectListItems.Add(new SelectListItem { Text = connection.Name, Value = connection.Id.ToString() });
                    }
                }
                return selectListItems;
            }
        }
    }
}