﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebGrease.Css.Extensions;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class SplitAndReshapeOutputFiles : DTStep
    {
        private PrmsSplitAndReshapeOutputFiles prms;
        List<string> filters;
        List<string> schemes;
        List<List<string>> excludedCols;

        public SplitAndReshapeOutputFiles(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitAndReshapeOutputFiles;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsSplitAndReshapeOutputFiles  has not been set for DTAction - " + ParentFTDT.DTAction.ActionName);
            }
            filters = prms.FieldValues.Split(",".ToCharArray()).ToList();
            schemes = prms.Scheme.Split(",".ToCharArray()).ToList();

            if (filters.Count != schemes.Count)
            {
                throw new Exception("Filters and schemes are not set up correctly (i.e Not in sync) for DTAction - " + ParentFTDT.DTAction.ActionName);
            }

            schemes.ForEach( x => x = x.Trim());

            if (!string.IsNullOrEmpty(prms.ColumnExcluded))
            {
                List<string> reshapedOFs = prms.ColumnExcluded.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();
                if (reshapedOFs.Count != schemes.Count)
                {
                    throw new Exception("Column Names not configure correctly (i.e Not in sync with Scheme Names) for DTAction - " + ParentFTDT.DTAction.ActionName);
                }
                reshapedOFs.ForEach(x => x.Trim());
                excludedCols = new List<List<string>>();
                foreach (var cols in reshapedOFs)
                {
                    List<string> columns = cols.Split(Utility.PIPE_DELIMITER.ToCharArray()).ToList();
                    columns.ForEach(x => x = x.Trim());
                    excludedCols.Add(columns);
                }
            }


        }
        public override bool Execute()
        {

            for (int i = 0; i < filters.Count; i++)
            {
                var rows = ParentFTDT.FileTemplate.ProcessedDataTable.Select(filters[i]);
                if (rows.Length > 0)
                {
                    OutputFile outputFile = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(schemes[i].Trim(),EnumOutputFileCategory.Output);
                    foreach (DataRow rw in rows)
                    {
                        outputFile.Data.ImportRow(rw);
                    }

                    if (excludedCols != null && excludedCols.Count > 0)
                    {
                        List<string> wildcardCNs = new List<string>();
                        foreach (var cn in excludedCols[i].Where(x => x.Trim().EndsWith("*")))
                        {
                            wildcardCNs.AddRange(Utility.GetColumns(cn, outputFile.Data));
                        }

                        excludedCols[i].RemoveAll(x => x.Trim().EndsWith("*"));
                        excludedCols[i].AddRange(wildcardCNs);

                        foreach (var colName in excludedCols[i])
                        {
                            if (!string.IsNullOrEmpty(colName) && outputFile.Data.Columns[colName] != null)
                                outputFile.Data.Columns.Remove(colName);
                        }

                        outputFile.Data.AcceptChanges();
                    }

                    if (!prms.KeepSelectedRow)
                    {
                        rows.ForEach(x => x.Delete());
                        ParentFTDT.FileTemplate.ProcessedDataTable.AcceptChanges();
                    }
                }


            }

            return true;
        }
    }
}