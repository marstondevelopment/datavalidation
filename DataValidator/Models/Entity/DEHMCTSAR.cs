﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class DEHMCTSAR : DEARBase
    {
        [Display(Name = "Case Number")]
        public string CaseNumber { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Surname")]
        public string LastName { get; set; }
        [Display(Name = "Client Id")]
        public int ClientId { get; set; }
        [Display(Name = "Client")]
        public string CLientName { get; set; }
        [Display(Name = "Batch Date")]
        public DateTime BatchDate { get; set; }
        [Display(Name = "Case Id")]
        public int? CaseId { get; set; }
    }
}