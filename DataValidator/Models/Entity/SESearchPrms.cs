﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class SESearchPrms<T>:SearchPrms<T>
    {
        public SESearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults,DateTime? fromDate, DateTime? toDate, int? dbConnectionId, List<DbConnection> dbConnections) :base( sortOrder,  searchString,  page,  pageSize,  defaultPageSize, allResults)
        {
            FromDate = fromDate;
            ToDate = toDate;
            DBConnectionId = dbConnectionId;
            DBConnections = dbConnections;
        }

        [Display(Name = "From Date")]
        [DataType(DataType.Date)]
        public DateTime? FromDate { get; set; }
        [Display(Name = "To Date")]
        [DataType(DataType.Date)]
        public DateTime? ToDate { get; set; }
        [Display(Name = "DB Connection")]
        public int? DBConnectionId { get; set; }

        public List<DbConnection> DBConnections;

    }
}