﻿CREATE TABLE [dbo].[ReferenceListItem] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [ReferenceListId] INT            NOT NULL,
    [Value]           NVARCHAR (250) NOT NULL,
    [Description]     NVARCHAR (250) NULL,
    CONSTRAINT [PK_ReferenceListItem] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ReferenceListItem_ReferenceList] FOREIGN KEY ([ReferenceListId]) REFERENCES [dbo].[ReferenceList] ([Id])
);

