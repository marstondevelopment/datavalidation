﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  Mgl.DataValidator.Models.ValidationRules;
using  Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class TestProfile
    {
        public string Error { get; set; }

        public bool ResultStatus { get; set; }

        public string TestResult
        {
            get
            {
                return ResultStatus ? "Passed" : "Failed";
            }
        }

        public int TotalNoRows { get; set; }
        public int TotalNoNewCases { get; set; }
        public decimal TotalBalanceForNewCases { get; set; }
        public int TotalNoIgnoredRows { get; set; }

        public void UpdateStatus(bool status, string error)
        {
            ResultStatus = status;
            Error = error;
        }

        public class Metadata
        {            

            [Required(ErrorMessage = "FT is required.")]
            [Display(Name = "Test FT")]
            public int FileTemplateId;
            [Required(ErrorMessage = "Total Rows number is required.")]
            [Display(Name = "Total Rows Exp.")]
            public int TotalNoOfRowsExpected;
            [Required(ErrorMessage = "Total Cases number is required.")]
            [Display(Name = "Total Cases Exp.")]
            public int TotalNoNewCasesExpected;
            [Required(ErrorMessage = "Total Bal. Cases is required.")]
            [Display(Name = "Total Bal. Cases Exp.")]
            public decimal TotalBalanceForNewCasesExpected;
            [Required(ErrorMessage = "Total Ignored Rows is required.")]
            [Display(Name = "Total Ignored Rows Exp.")]
            public int TotalNoIgnoredRowsExpected;            
            [Display(Name = "Input File Name")]
            public string InputFileName;
            [Display(Name = "Created By")]
            public int CreatedBy;
            [Display(Name = "Date Created")]
            public DateTime DateCreated;
            [Display(Name = "Updated By")]
            public int UpdatedBy;
            [Display(Name = "Date Updated")]
            public DateTime DateUpdated;
            [Display(Name = "Enabled")]
            public bool Disabled;
            [Display(Name = "Debt Column Name")]
            public string DebtColumnName;
        }
    }
}