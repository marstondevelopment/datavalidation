﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class ARPrmsGetOutputFileRowCount : ARPrmsBase
    {
        public ARPrmsGetOutputFileRowCount() { }
        public ARPrmsGetOutputFileRowCount(ARAction ar) : base(ar) { }

        [Display(Name = "File Name Prefixs")]
        public string FileNamePrefixs { get; set; }
    }
}