﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class DTError
    {
        private static Dictionary<EnumDTActionType, string> _dTErrors;

        public static Dictionary<EnumDTActionType, string> DTErrors
        {
            get
            {
                if (_dTErrors == null)
                {
                    _dTErrors = new Dictionary<EnumDTActionType, string>
                    {
                        { EnumDTActionType.OSExcludeCasesAlreadyImported, "DT action, OSExcludeCasesAlreadyImported, failed." },
                        { EnumDTActionType.OSExcludeCasesLessThanThreadholdVal, "DT action, OSExcludeCasesLessThanThreadholdVal, failed " },
                        { EnumDTActionType.OSExtractComments, "DT action, OSExtractComments, failed " },
                        { EnumDTActionType.OSGroupBy, "DT action, OSGroupBy, failed. " },
                        { EnumDTActionType.OSMergeDebts, "DT action, OSMergeDebts, failed. " },
                        { EnumDTActionType.OSSplitOutputFileBasedOnFields, "DT action, OSSplitOutputFileBasedOnFields, failed " },
                        { EnumDTActionType.OSSplitColumn, "DT action, OSSplitColumn, failed " },
                        { EnumDTActionType.OSConcatenateFields, "DT action, OSConcatenateFields, failed " },
                        { EnumDTActionType.OSAddProcessInfo, "DT action, OSAddProcessInfo, failed " },
                        { EnumDTActionType.OSGetSumOfColumns, "DT action, OSGetSumOfColumns, failed " },
                        { EnumDTActionType.OSExtractLinkedCasesAfterGroupBy, "DT action, OSExtractLinkedCasesAfterGroupBy, failed " },
                        { EnumDTActionType.OSSplitNameColumnIntoThree, "DT action, OSSplitNameColumnIntoThree, failed " },
                        { EnumDTActionType.GetDataFromDB, "DT action, GetDataFromDB, failed " },
                        { EnumDTActionType.XMappingDataFromDB, "DT action, XMappingDataFromDB, failed " },
                        { EnumDTActionType.GetTotalOfAColumn, "DT action, GetTotalOfAColumn, failed " },
                        { EnumDTActionType.SplitOutputAfterGrouping, "DT action, SplitOutputAfterGrouping, failed " },
                        { EnumDTActionType.SplitOneColIntoMultipleCols, "DT action, SplitOneColIntoMultipleCols, failed " },
                        { EnumDTActionType.ExcludeSelectedRows, "DT action, ExcludeSelctedRows, failed " },
                        { EnumDTActionType.OSTransformDefaulterAddresses, "DT action, OSTransformDefaulterAddresses, failed " },
                        { EnumDTActionType.OSNCTransformDefaulterAddresses, "DT action, OSNCTransformDefaulterAddresses, failed " },
                        { EnumDTActionType.SetDuplicatedRowsWithUniqueValues, "DT action, SetDuplicatedRowsWithUniqueValues, failed " },
                        { EnumDTActionType.TruncateFieldsExceedingMaxLength, "DT action, TruncateFieldsExceedingMaxLength, failed " },
                        { EnumDTActionType.ComputeAggregateFunction, "DT action, ComputeAggregateFunction, failed " },
                        { EnumDTActionType.SplitFullAddressColIntoSepLines, "DT action, SplitFullAddressColIntoSepLines, failed " },
                        { EnumDTActionType.OSAssignDefaultPostcode, "DT action, OSAssignDefaultPostcode, failed " },
                        { EnumDTActionType.SplitAndReshapeOutputFiles, "DT action, SplitAndReshapeOutputFiles, failed " },
                        { EnumDTActionType.ExcludeRowsNotMatchReferenceList, "DT action, ExcludeRowsNotMatchReferenceList, failed " },
                        { EnumDTActionType.ConcatenateMultiNodesDFs, "DT action, ConcatenateMultiNodesDFs, failed " },
                        { EnumDTActionType.SplitAndTransposeOutputFile, "DT action, SplitAndTransposeOutputFile, failed " },
                        { EnumDTActionType.ConcatenateXMLListItemsDFs, "DT action, ConcatenateXmlListItemsDFs, failed " }
                    };
                }

                return _dTErrors;

            }
        }
    }
}