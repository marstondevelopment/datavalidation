﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Web.Administration;
using System.Web.Hosting;
using System.Net;
using System;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class UserController : ControllerBase
    {
        private UserRepository _ur;
        private ClientRepository _cr;

        public UserController(UserRepository ur, ClientRepository cr)
        {
            _ur = ur;
            _cr = cr;
        }

        public ActionResult Index()
        {
            var vm = new UserViewModel(_ur, null, null)
            {
                Users = _ur.GetAll().OrderBy(u => u.UserId)
            };
            return View(vm);
        }

        //public ActionResult RecycleAppPool()
        //{
        //    using (ServerManager iisManager = new ServerManager())
        //    {
        //        SiteCollection sites = iisManager.Sites;
        //        foreach (Site site in sites)
        //        {
        //            if (site.Name == HostingEnvironment.ApplicationHost.GetSiteName())
        //            {
        //                iisManager.ApplicationPools[site.Applications["/"].ApplicationPoolName].Recycle();
        //                break;
        //            }
        //        }
        //    }
        //    return RedirectToAction("Index").Information(RecycleApplicationPool(null));
        //    //return RedirectToAction("Index").Information("Application Pool reclying process has been initiated. This might causes few seconds of unresponsiveness. If the problem persists please consult system administrator for technical assistants.");
        //}

        private string RecycleApplicationPool(string siteName = null)
        {
            string message = string.Empty;
            if (siteName == null) siteName = HostingEnvironment.ApplicationHost.GetSiteName();
            using (ServerManager iisManager = new ServerManager())
            {
                SiteCollection sites = iisManager.Sites;
                foreach (Site site in sites)
                {
                    if (site.Name == siteName)
                    {
                        message = message + site.Name + "|";
                        iisManager.ApplicationPools[site.Applications["/"].ApplicationPoolName].Recycle();
                        return "Application pool for " + site.Name + " has been recycled.";
                    }
                }
            }
            return "Failed to recycle application pool.";
        }

        //public ActionResult Create()
        //{
        //    var vm = new UserViewModel(_ur, _cr, new User());
        //    vm.User.RoleId = (int)EnumRole.ReadOnly;
        //    vm.User.Disabled = false;
        //    return View(vm);
        //}

        public ActionResult Details(int id)
        {
            User user = _ur.GetUserById(id);
            var vm = new UserViewModel(_ur, _cr, user);
            vm.User.UserName = user.UserName ?? "N/A";            
            return View(vm);
        }

        public ActionResult Edit(int id)
        {
            var vm = new UserViewModel(_ur, _cr, _ur.GetUserById(id));
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var vm = new UserViewModel(_ur, _cr, _ur.GetUserById(id));
            TryUpdateModel(vm);
            if (ModelState.IsValid)
            {
                vm.User.Disabled = collection["Enabled"] == "false" ? true : false;
                _ur.Update(vm.User);
                return RedirectToAction("Index", vm).Success("User profile updated successfully.");
            }
            return RedirectToAction("Edit");
        }

        [HttpPost]
        public ActionResult UpdateClient(int? UserId, int? ClientId, bool IsDefault)
        {
            if ((!UserId.HasValue || !ClientId.HasValue))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            UserClient userClient = new UserClient()
            {
                ClientId = ClientId.Value,
                UserId = UserId.Value,
                IsDefault = IsDefault
            };


            var ucList = _ur.UpdateClient(userClient);

            return PartialView("_ClientsList", ucList).Success("User profile updated successfully");

        }

        [HttpPost]
        public ActionResult DeleteClient(int? Id)
        {
            if (!Id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserClient uc = _ur.GetUserClient(Id.Value);
            if (uc == null)
            {
                throw new Exception("Failed to find UserClient with id - " + Id.ToString());
            }
            int uId = uc.UserId;

            var ucList = _ur.DeleteClient(uc);

            return PartialView("_ClientsList", ucList);

        }

    }
}
