﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class ELSearchPrms<T>:SearchPrms<T>
    {
        public ELSearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults,DateTime? fromDate, DateTime? toDate, int? eventTypeId, int? userId, List<SelectListItem> users):base( sortOrder,  searchString,  page,  pageSize,  defaultPageSize, allResults)
        {
            FromDate = fromDate;
            ToDate = toDate;
            EventTypeId = eventTypeId;
            UserId = userId;
            Users = users;
        }
        [Display(Name = "From Date")]
        [DataType(DataType.Date)]
        public DateTime? FromDate { get; set; }
        [Display(Name = "To Date")]
        [DataType(DataType.Date)]
        public DateTime? ToDate { get; set; }
        [Display(Name = "Event Type")]
        public int? EventTypeId { get; set; }
        [Display(Name = "User Name")]
        public int? UserId { get; set; }

        public List<SelectListItem> Users;

    }
}