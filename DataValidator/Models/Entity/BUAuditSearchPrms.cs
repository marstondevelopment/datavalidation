﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class BUAuditSearchPrms<T>:SearchPrms<T>
    {
        public BUAuditSearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults,DateTime? fromDate, DateTime? toDate,  int? clientId, List<SelectListItem> clients, int fileTemplateId, List<User> users, int importedCasesTotal, int appendingCasesTotal, int withErrorCasesTotal) :base( sortOrder,  searchString,  page,  pageSize,  defaultPageSize, allResults)
        {
            FromDate = fromDate;
            ToDate = toDate;
            ClientId = clientId;
            Clients = clients;
            Users = users;
            casesImported = importedCasesTotal;
            appendingCases = appendingCasesTotal;
            casesWithError = withErrorCasesTotal;
            SelectedFileTemplateId = fileTemplateId;
        }
        [Display(Name = "From Date")]
        [DataType(DataType.Date)]
        public DateTime? FromDate { get; set; }
        [Display(Name = "To Date")]
        [DataType(DataType.Date)]
        public DateTime? ToDate { get; set; }

        [Display(Name = "Client Name")]
        public int? ClientId { get; set; }

        public List<SelectListItem> Clients;

        [Display(Name = "File Template")]
        public int SelectedFileTemplateId { get; set; }

        public List<User> Users;

        public int CasesImported { get => casesImported; set => casesImported = value; }
        public int AppendingCases { get => appendingCases; set => appendingCases = value; }
        public int CasesWithError { get => casesWithError; set => casesWithError = value; }

        private int casesImported;

        private int appendingCases;

        private int casesWithError;
    }
}