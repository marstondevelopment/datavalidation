﻿using Mgl.DataValidator.Models;
using Security.Authorize;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class ReferenceListController : ControllerBase
    {
        ReferenceListRepository _rlr;
        public ReferenceListController(ReferenceListRepository rlr)
        {
            _rlr = rlr;
        }
        
        public ActionResult Index()
        {          
            return View(_rlr.GetAll().Where(x => x.Disabled == false).ToList());
        }
        
        public ActionResult Details(int id)
        {

            return View(_rlr.GetById(id));
        }
        
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Create(ReferenceList referenceList)
        {
            if (ModelState.IsValid)
            {
                _rlr.Create(referenceList);
                return RedirectToAction("Edit", new { id = referenceList.Id }).Information("Please add items for the new list.");
            }

            return RedirectToAction("Create").Danger("Failed to create the reference list");
        }
        
        public ActionResult Edit(int id)
        {
            ReferenceList rl = _rlr.GetById(id);
            if (rl == null)
            {
                return RedirectToAction("Index").Danger("Failed to find the reference list ("  + id.ToString() +  ")");
            }
            return View(rl);
        }
        
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            ReferenceList rl = _rlr.GetById(id);
            TryUpdateModel(rl);
            if (ModelState.IsValid)
            {                
                _rlr.Update(rl);
                 return RedirectToAction("Index").Success(rl.Name + " has been updated successfully.");
            }
            return RedirectToAction("Index").Danger("Failing to update the reference list");
           
        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            ReferenceList rlList = _rlr.GetById(Id);
            _rlr.Delete(rlList);
            var lists = _rlr.GetAll(x => x.Disabled == false).OrderBy(x => x.Name).ToList();
            return RedirectToAction("Index", lists).Success(rlList.Name + " has been deleted successfully");
        }

        [HttpPost]
        [AdminOnlyAuthorizeUser]
        public ActionResult AddListItem(int? RLId, string ItemValue, string ItemDescription)
        {
            if ( !RLId.HasValue || string.IsNullOrEmpty(ItemValue))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ReferenceListItem item = new ReferenceListItem()
            {
                ReferenceListId = RLId.Value,
                Value = ItemValue,
                Description = ItemDescription
            };
            _rlr.AddListItem(item);
            
            List<ReferenceListItem> items = _rlr.GetById(RLId.Value).ReferenceListItems.OrderBy(x => x.Value).ToList();
            return PartialView("_ListItems", items);

        }

        [HttpPost]
        [AdminOnlyAuthorizeUser]
        public ActionResult DeleteListItem(int? Id)
        {
            if (!Id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int rlId = _rlr.DeleteReferenceListItem(Id.Value);
            List<ReferenceListItem> items = _rlr.GetById(rlId).ReferenceListItems.OrderBy(x => x.Value).ToList();
            return PartialView("_ListItems", items);
        }
    }
}
