﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.ViewModel
{
    public class ClientViewModel
    {
        public Client Client { get; set; }
        public IEnumerable<Client> Clients { get; set; }        
    }
}