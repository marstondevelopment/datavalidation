﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsConcatenateFields : PrmsBase
    {
        public PrmsConcatenateFields() { }
        public PrmsConcatenateFields(DTAction dt) : base(dt) { }

        [Display(Name = "Input Field Names")]
        public string InputFieldNames { get; set; }
        [Display(Name = "Hide Input Fields")]
        public bool HideInputField { get; set; }

        [Display(Name = "Clear Input Fields")]
        public bool ClearInputFields { get; set; }

        [Display(Name = "Output Field Name")]
        public string OutputFieldName { get; set; }
        [Display(Name = "Field Separator")]
        public string FieldSeparator { get; set; }

        [Display(Name = "Split Output String")]
        public bool SplitOutputString { get; set; }

        [Display(Name = "Include Field Name")]
        public bool IncludeFieldName { get; set; }

        [Display(Name = "Filters")]
        public string FieldValues { get; set; }

        [Display(Name = "Sub Str Max Length")]
        public int SubStrMaxLength{ get; set; }

        [Display(Name = "Overwrite Output Field")]
        public bool OverwriteOutputField { get; set; }

        [Display(Name = "DC Post DT")]
        public bool DCPostDT { get; set; }

        public string DisplayFieldSeparator
        {
            get
            {
                return !string.IsNullOrEmpty(FieldSeparator) ? FieldSeparator : "N/A";
            }
        }

        [Display(Name = "Regular Expression")]
        public int? RegularExpressionId { get; set; }
        [JsonIgnore]
        public RegularExpression RegularExpression { get; set; }

        [Display(Name = "Fixed Width Field Length")]
        public int FixedWidthFieldLength { get; set; }

        [Display(Name = "Remove Text By Regex List")]
        public int? RegExRefListId { get; set; }

        [JsonIgnore]
        public ReferenceList RegExRefList { get; set; }
    }
}