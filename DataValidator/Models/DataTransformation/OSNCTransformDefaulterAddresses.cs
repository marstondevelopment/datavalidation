﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSNCTransformDefaulterAddresses : DTStep
    {

        private const string DEFAULTER_NAMES = "DefaultersNames";
        private const string WARRANT_ADDRESS_PREFIX = "WarrantAddressLine";
        private const string WARRANT_POSTCODE = "WarrantPostcode";
        private const string LEAD_DEFAULTER_ADDRESS_PREFIX = "DefaulterAddressLine";
        private const string LEAD_DEFAULTER_POSTCODE = "DefaulterPostcode";
        private const string OTHER_DEFAULTER_ADDRESS_PATTERN = "Add{ADDLINENO}Liable{LIABLENO}";
        private const string ADD_LINE_NO_TAG = "{ADDLINENO}";
        private const string LIABLE_NO_TAG = "{LIABLENO}";
        private const string SECONDARY_DEFAULTER_POSTCODE_PREFIX = "AddPostCodeLiable";


        

        string[] warrantAddressColNames;
        string[] LeadDefaulterAddressColNames;
        private PrmsNCTransformDefaultersAddresses prms;


        public OSNCTransformDefaulterAddresses(FileTemplateDTAction ftdt) : base(ftdt)
        {
            warrantAddressColNames = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
            .Where(y => y.ColumnName.StartsWith(WARRANT_ADDRESS_PREFIX.Substring(0, WARRANT_ADDRESS_PREFIX.Length - 1)))
             .Select(x => x.ColumnName)
             .ToArray();

            LeadDefaulterAddressColNames = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
            .Where(y => y.ColumnName.StartsWith(LEAD_DEFAULTER_ADDRESS_PREFIX.Substring(0, LEAD_DEFAULTER_ADDRESS_PREFIX.Length - 1)))
             .Select(x => x.ColumnName)
             .ToArray();

            if (warrantAddressColNames == null || warrantAddressColNames.Length <= 0 || LeadDefaulterAddressColNames == null || LeadDefaulterAddressColNames.Length <= 0 || !ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(DEFAULTER_NAMES))
            {
                throw new MemberAccessException("File format is not in the right context for DTAction: " + ftdt.DTAction.ActionName + ", please review the file template and try again.");
            }

            prms = ftdt.DTAction.PrmsNCTransformDefaultersAddresses;
        }
        public override bool Execute()
        {

            var rows = ParentFTDT.FileTemplate.ProcessedDataTable.Select();

            foreach (DataRow row in rows)
            {
                int defaulterCount = 1;
                string leadDefaulterAddLine1 = GetText(row, LEAD_DEFAULTER_ADDRESS_PREFIX + defaulterCount.ToString());
                string warrantAddressLine1 = GetText(row, WARRANT_ADDRESS_PREFIX + defaulterCount.ToString());

                string defaulterNames = GetText(row, DEFAULTER_NAMES);
                
                //If Defaulter Names is not empty, then copy Warrant address to each of the defaulter's address
                if (!string.IsNullOrEmpty(defaulterNames) && !string.IsNullOrEmpty(warrantAddressLine1))
                {
                    string[] arrNames = defaulterNames.Split(string.IsNullOrEmpty(prms.StringSeparator) ? Utility.DEFAULT_DELIMITER.ToCharArray():prms.StringSeparator.ToCharArray());

                    if (string.IsNullOrEmpty(row[LEAD_DEFAULTER_ADDRESS_PREFIX + "1"].ToString()))
                    {
                        for (int i = 1; i <= 5; i++)
                        {

                            row[LEAD_DEFAULTER_ADDRESS_PREFIX + i.ToString()] = GetText(row, WARRANT_ADDRESS_PREFIX + i.ToString());
                        }
                        row[LEAD_DEFAULTER_POSTCODE] = GetText(row, WARRANT_POSTCODE);
                    }

                    if (arrNames.Length>1)
                    {
                        for (int liableNo = 2; liableNo <= arrNames.Length; liableNo++)
                        {
                            if (string.IsNullOrEmpty(row[OTHER_DEFAULTER_ADDRESS_PATTERN.Replace(ADD_LINE_NO_TAG,"1").Replace(LIABLE_NO_TAG, liableNo.ToString())].ToString()))
                            {
                                for (int i = 1; i <= 5; i++)
                                {
                                    string secondaryDefAddressColName = OTHER_DEFAULTER_ADDRESS_PATTERN.Replace(ADD_LINE_NO_TAG, i.ToString()).Replace(LIABLE_NO_TAG, liableNo.ToString());
                                    row[secondaryDefAddressColName] = GetText(row, WARRANT_ADDRESS_PREFIX + i.ToString());
                                }
                                row[SECONDARY_DEFAULTER_POSTCODE_PREFIX + liableNo.ToString()] = GetText(row, WARRANT_POSTCODE);
                            }
                        }


                    }

                }
            }

            return true;
        }

        private string GetText(DataRow row, string fieldName)
        {
            fieldName = fieldName.Trim();
            string conStr = string.Empty;
            DataColumn dc = (DataColumn)this.ParentFTDT.FileTemplate.BUDataTable.Columns[fieldName];
            if (!string.IsNullOrEmpty(row[fieldName].ToString()))
            {
                conStr = dc != null ? row[fieldName].ObjToString(dc) : row[fieldName].ToString();
            }

            return conStr;
        }
    }
}