﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class ARPrmsGetDataFromGeneratedFile : ARPrmsBase
    {
        public ARPrmsGetDataFromGeneratedFile() { }
        public ARPrmsGetDataFromGeneratedFile(ARAction ar) : base(ar) { }

        [Display(Name = "Error Tag Name")]
        public string ExclusionReasonTagName { get; set; }

        [Display(Name = "File Name Prefixs")]
        public string FileNamePrefixs { get; set; }
    }
}