﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class GetDataFromDB : DTStep
    {
        private PrmsGetDataFromDB prms;
        private List<string> fieldsToBePopulated;
        public GetDataFromDB(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsGetDataFromDB;
            if (prms == null || (prms.GetMethod==EnumValidationMethod.SP && prms.StoredProcedure == null) || (prms.GetMethod== EnumValidationMethod.CmdTxt && prms.CmdText == null))
            {
                throw new MemberAccessException("PrmsGetDataFromDB parameters are missing/configured incorrectly for " + ftdt.DTAction.ActionName + ", please update the configuration and try again.");
            }

            fieldsToBePopulated = prms.FieldsToPopulate.Split(",".ToCharArray()).Select(x=> x.Trim()).ToList();
        }
        public override bool Execute()
        {
            List<DataRow> exRows = new List<DataRow>();

            foreach (DataRow  rw in ParentFTDT.FileTemplate.ProcessedDataTable.Rows)
            {
                foreach (var df in ParentFTDT.FileTemplate.BodyCols)
                {
                    df.OriginalValue = rw[df.Name]?.ToString();
                }
                DataTable dt = null;

                switch (prms.GetMethod)
                {
                    case EnumValidationMethod.SP:

                        dt = prms.StoredProcedure.GetData();
                        break;
                    case EnumValidationMethod.CmdTxt:
                        dt = prms.CmdText.GetData();
                        break;
                    default:
                        break;
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    foreach (var fieldName in fieldsToBePopulated)
                    {
                        string inputColName = fieldName;
                        if (!ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(fieldName))
                        {
                            DataField df = ParentFTDT.FileTemplate.Columns.Where(x => x.OutputColumnName == fieldName).FirstOrDefault();
                            if (df==null)
                                throw new Exception(this.ParentFTDT.DTAction.ActionName + " failed to find column, " + fieldName + " from staging table, please update the configuration and try again.");
                            inputColName = df.Name;
                        }

                        rw[inputColName] = dr[fieldName];
                    }
                }
                else
                {
                    exRows.Add(rw);
                }
            }

            if (prms.ExcludeNotFound && exRows.Count > 0)
            {
                    for (int i = 0; i < exRows.Count; i++)
                    {
                    ParentFTDT.FileTemplate.ExcludedDataTable.ImportRow(exRows[i]);
                        ParentFTDT.FileTemplate.ProcessedDataTable.Rows.Remove(exRows[i]);
                    }
            }



            return true;
        }
    }
}