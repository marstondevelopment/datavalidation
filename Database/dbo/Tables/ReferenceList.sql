﻿CREATE TABLE [dbo].[ReferenceList] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (250) NULL,
    [Disabled]    BIT            CONSTRAINT [DF_ReferenceList_Disabled] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_FieldReference] PRIMARY KEY CLUSTERED ([Id] ASC)
);

