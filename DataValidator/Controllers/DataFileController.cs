﻿using Mgl.DataValidator.Models;
using Security.Authorize;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Mgl.DataValidator.Controllers
{
    [DisabledAuthorizeUser]
    public class DataFileController : ApiController
    {

        public DataFileController(FileTemplateRepository ftr)
        {
            _ftr = ftr;
        }

        private FileTemplateRepository _ftr;

        [Route("api/datafile/validate"), HttpPost]
        [ActionName("validate")]
        public async Task<HttpResponseMessage> Validate()
        {

            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                Task<HttpResponseMessage> mytask = new Task<HttpResponseMessage>(delegate ()
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent("Invalid file & request content type!")

                    };
                });
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }


            var streamProvider = new CustomMultipartFileStreamProvider();
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            var fileStream = await streamProvider.Contents[CustomMultipartFileStreamProvider.FileIndex].ReadAsStreamAsync();
            if (!streamProvider.FileTemplateId.HasValue || string.IsNullOrEmpty(streamProvider.FullFileName) )
            {
                Task<HttpResponseMessage> mytask = new Task<HttpResponseMessage>(delegate ()
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent("File template and data file are required for this validation process.")
                    };
                });
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            FileTemplate ft = _ftr.GetById(streamProvider.FileTemplateId.Value);

            FileParser fileParser = FileParser.New(ft, fileStream, streamProvider.FullFileName, EnumDataFileScreen.Validate);
            FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

            switch ((EnumParserResult) frm.ProcessFile())
            {
                case EnumParserResult.ValiationSucceed:
                    return Request.CreateResponse(HttpStatusCode.OK, "Data is valid.");
                case EnumParserResult.ValidationFailed:
                    return SetCSVFile(HttpStatusCode.OK, fileParser.ValidationSummary.ToString());
                default:
                    throw new Exception("Unexpected ParserResult from ProcessFile()");
            }
        }


        [Route("api/datafile/upload"), HttpPost]
        [ActionName("upload")]
        public async Task<HttpResponseMessage> Upload()
        {

            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                Task<HttpResponseMessage> mytask = new Task<HttpResponseMessage>(delegate ()
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent("Invalid file & request content type!")

                    };
                });
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }


            var streamProvider = new CustomMultipartFileStreamProvider();
            await Request.Content.ReadAsMultipartAsync(streamProvider);

            var fileStream = await streamProvider.Contents[CustomMultipartFileStreamProvider.FileIndex].ReadAsStreamAsync();
            if (!streamProvider.FileTemplateId.HasValue || string.IsNullOrEmpty(streamProvider.FullFileName))
            {
                Task<HttpResponseMessage> mytask = new Task<HttpResponseMessage>(delegate ()
                {
                    return new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest,
                        Content = new StringContent("File template and data file are required for this validation process.")
                    };
                });
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            FileTemplate ft = _ftr.GetById(streamProvider.FileTemplateId.Value);

            FileParser fileParser = FileParser.New(ft, fileStream, streamProvider.FullFileName, EnumDataFileScreen.Upload);
            FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

            switch ((EnumParserResult)frm.ProcessFile())
            {
                case EnumParserResult.UploadSucceed:
                    return Request.CreateResponse(HttpStatusCode.OK, "Data has been uploaded successfully.");
                case EnumParserResult.UploadFailed:
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Bulk copy process failed due to some technical issue, please consult system administrator for further assistance.");
                case EnumParserResult.ConfigNotSet:
                    return SetCSVFile(HttpStatusCode.OK, "Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
                case EnumParserResult.ValidationFailed:
                    return SetCSVFile(HttpStatusCode.OK, fileParser.ValidationSummary.ToString());
                default:
                    throw new Exception("Unexpected ParserResult from ProcessFile()");
            }

        }


        private HttpResponseMessage SetCSVFile(HttpStatusCode code, string message)
        {
            var res = Request.CreateResponse(code);
            res.Content = new StringContent(message, Encoding.UTF8, "text/csv");
            string contentDisposition = "inline; filename=ValidationResult_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv";
            res.Content.Headers.ContentDisposition = ContentDispositionHeaderValue.Parse(contentDisposition);
            return res;
        }

        [Route("api/datafile/transform"), HttpPost]
        [ActionName("transform")]
        [AdminOnlyAuthorizeUser]
        public HttpResponseMessage Transform(int ftId)
        {
            FileTemplate fileTemplate = _ftr.GetById(ftId);
            if (fileTemplate.TransformData == false || fileTemplate.StoredProcedure == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Configuration for data transformation has not been set. Please update the file template and try again.");
            }

            FileReaderManager frm = new FileReaderManager(_ftr);


            if (frm.TransformData(fileTemplate))
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Data transformation process completed successfully.");
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Data transformation process failed due to technical issue, please consult system administrator for further assistance.");
            }
        }

    }

    class CustomMultipartFileStreamProvider : MultipartMemoryStreamProvider
    {
        public int? FileTemplateId { get; set; }
        public int? ClientId { get; set; }
        public FileStream FileStream { get; set; }

        public string FileNameNoExt { get; set; }
        public string FullFileName { get; set; }

        public const int FileIndex = 3;

        private const string SelectedFileTemplateId = "SelectedFileTemplateId";
        private const string SelectedClientId = "SelectedClientId";

        private const string UploadedFile = "FileName";


        public CustomMultipartFileStreamProvider()
        {
        }

        public override Task ExecutePostProcessingAsync()
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form[SelectedFileTemplateId]))
                FileTemplateId = int.Parse(HttpContext.Current.Request.Form[SelectedFileTemplateId]);

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form[SelectedClientId]))
                ClientId = int.Parse(HttpContext.Current.Request.Form[SelectedClientId]);

            foreach (var file in Contents)
            {

                var parameters = file.Headers.ContentDisposition.Parameters;
                string fileName = GetNameHeaderValue(parameters, UploadedFile);
                if (fileName != null)
                {
                    fileName = fileName.Trim("\"".ToCharArray());
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        FullFileName = fileName;
                        FileNameNoExt = FullFileName.Substring(0, FullFileName.LastIndexOf("."));
                    }
                }
            }

            return base.ExecutePostProcessingAsync();
        }

        private static string GetNameHeaderValue(ICollection<NameValueHeaderValue> headerValues, string name)
        {
            var nameValueHeader = headerValues.FirstOrDefault(
                x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            return nameValueHeader != null ? nameValueHeader.Value : null;
        }
    }

}