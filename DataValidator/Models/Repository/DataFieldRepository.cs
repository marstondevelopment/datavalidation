﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Mgl.DataValidator.Models;

namespace Mgl.DataValidator.Models
{
    public class DataFieldRepository : RepositoryBase<DataIntegrationEntities, DataField>
    {
        private UserRepository _ur;
        public DataFieldRepository(DataIntegrationEntities entity, UserRepository ur) : base(entity)
        {
            _ur = ur;
        }

        public override IQueryable<DataField> GetAll()
        {
            return base.GetAll(x => x.Disabled == false);
        }

        public DataField GetById(int id)
        {
            return GetAll(ft => ft.Id == id).FirstOrDefault();
        }

        public List<DataFieldRule> GetVRByDataFieldId(int id)
        {
            return _entities.DataFieldRules
                .Include(x => x.RegularExpression)
                .Include(x => x.ReferenceList)
                .Include(x => x.StoredProcedure)
                .Where(x => x.DataFieldId == id).ToList();
        }

        public void Create(DataField df)
        {
            df.Name = df.Name.Trim();
            df.OutputColumnName = string.IsNullOrEmpty(df.OutputColumnName) ? df.OutputColumnName : df.OutputColumnName.Trim();
            df.CreatedBy = _ur.CurrentUser.Id;
            df.DateCreated = DateTime.Now;
            df.UpdatedBy = _ur.CurrentUser.Id;
            df.DateUpdated = DateTime.Now;
            Add(df);
            SubmitChanges();
        }

        public void Update(DataField df)
        {
            df.Name = df.Name.Trim();
            df.OutputColumnName = string.IsNullOrEmpty(df.OutputColumnName) ? df.OutputColumnName : df.OutputColumnName.Trim();
            df.UpdatedBy = _ur.CurrentUser.Id;
            df.DateUpdated = DateTime.Now;
            SubmitChanges();

        }

        public void Delete(DataField df)
        {
            df.Disabled = true;
            Update(df);
        }



        public void AddDataFieldAction(DataFieldDataCleansing dc)
        {
            base._entities.DataFieldDataCleansings.Add(dc);
            DataField df = GetById(dc.DataFieldId);
            Update(df);
        }

        public void DeleteDataFieldAction(DataFieldDataCleansing dc)
        {

            _entities.DataFieldDataCleansings.Remove(dc);
            DataField df = GetById(dc.DataFieldId);
            Update(df);
        }

        public DataFieldDataCleansing GetDataFieldAction(int id)
        {
            return _entities.DataFieldDataCleansings.Where(x => x.Id == id).FirstOrDefault();
        }

        public DataFieldRule GetVR(int id)
        {
            return _entities.DataFieldRules.Where(x => x.Id == id).FirstOrDefault();
        }

        public void AddVR(DataFieldRule dr)
        {
            base._entities.DataFieldRules.Add(dr);
            DataField df = GetById(dr.DataFieldId);
            Update(df);
        }

        public void DeleteVR(DataFieldRule dr)
        {

            _entities.DataFieldRules.Remove(dr);
            DataField df = GetById(dr.DataFieldId);
            Update(df);
        }

        public List<StoredProcedure> GetSPs()
        {
            return _entities.StoredProcedures.ToList();
        }
        public List<RegularExpression> GetRegExs()
        {
            return _entities.RegularExpressions.Where(x=> x.Disabled == false).ToList();
        }

        public List<ReferenceList> GetReferenceLists()
        {
            return _entities.ReferenceLists.ToList();
        }

        public IEnumerable<DataFieldRule> GetDFRules()
        {
            return _entities.DataFieldRules;
        }

        public DataField ReorderDataFieldDataCleasning(DataField df, List<DataFieldDataCleansing> dfdcs)
        {
            _entities.DataFieldDataCleansings.RemoveRange(df.DataFieldDataCleansings);
            _entities.DataFieldDataCleansings.AddRange(dfdcs);
            SubmitChanges();
            return GetById(df.Id);
        }
    }
}