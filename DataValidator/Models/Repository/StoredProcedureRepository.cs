﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    public class StoredProcedureRepository : RepositoryBase<DataIntegrationEntities, StoredProcedure>
    {
        public StoredProcedureRepository(DataIntegrationEntities entity) : base(entity)
        {
        }

        public override IQueryable<StoredProcedure> GetAll()
        {
            return base.GetAll();
        }

        public void GetAssociatedDTACtions(IEnumerable<StoredProcedure> sps)
        {
            foreach (var item in sps)
            {
                var dtActions = _entities.DTActions.Where(x => x.PrmsObject.Contains("\"StoredProcedureId\":" + item.Id.ToString() + ",")).ToList();
                item.AssociatedDTActions = dtActions.ToDictionary(x => x.Id, x => x.ActionName);
            }
        }

        public StoredProcedure GetById(int id)
        {
            var sp = _entities.StoredProcedures.Where(x => x.Id == id).FirstOrDefault();
            if (sp != null)
            {
                var dtActions = _entities.DTActions.Where(x => x.PrmsObject.Contains("\"StoredProcedureId\":" + sp.Id.ToString() + ",")).ToList();
                sp.AssociatedDTActions = dtActions.ToDictionary(x => x.Id, x => x.ActionName);

            }

            return sp;
        }

        public void Create(StoredProcedure sp)
        {           
            this.Add(sp);
            this.SubmitChanges();
        }

        public IEnumerable<DbConnection> GetConnections()
        {
            return _entities.DbConnections.Where(x => x.Disabled == false);
        }       

        public void Update(StoredProcedure sp)
        {
            this.SubmitChanges();
        }
    }
}