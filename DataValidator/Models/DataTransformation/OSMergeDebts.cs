﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSMergeDebts : DTStep
    {
        private string debtField;
        public OSMergeDebts(FileTemplateDTAction ftdt) : base(ftdt)
        {
            PrmsMergeDebts prms = ftdt.DTAction.PrmsMergeDebts;
            if (prms == null)
            {
                throw new MemberAccessException("Merge Debt parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            debtField = prms.DebtFieldName;
            if (!string.IsNullOrEmpty(prms.GroupByName))
            {

                var prmsGroupBy = ParentFTDT.FileTemplate.FileTemplateDTActions.Where(x => (x.DTAction.DTActionTypeId == (int)EnumDTActionType.OSGroupBy) && (x.DTAction.ActionName.Trim() == prms.GroupByName.Trim())).FirstOrDefault();
                GroupByFields = prmsGroupBy.DTAction.PrmsGroupBy.FieldNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x => x.Trim()).ToArray();
            }

        }
        public override bool Execute()
        {
            DataField dataField = ParentFTDT.FileTemplate.BodyCols.Where(x => x.Name.ToUpper() == debtField.ToUpper()).FirstOrDefault();

            if (GroupByFields != null)
            {
                foreach (DataRow row in ParentFTDT.FileTemplate.ProcessedDataTable.Rows)
                {
                    string filter = string.Empty;
                    foreach (var item in GroupByFields)
                    {
                        if (row[item] == DBNull.Value)
                            filter = filter + "[" + item + "] is null AND ";
                        else
                            filter = filter + "[" + item + "]" + "='" + row[item] + "' AND ";
                    }
                    filter = filter.Trim().Remove(filter.LastIndexOf("AND"));
                    List<DataRow> rows = ParentFTDT.FileTemplate.BUDataTable.Select(filter).ToList();
                    if (rows.Count > 1)
                    {
                        switch ((EnumFieldType)dataField.FieldType)
                        {
                            case EnumFieldType.Integer:
                                row[debtField] = rows.Sum(rw => rw.IsNull(debtField) ? 0 : (int)rw[debtField]);
                                break;
                            case EnumFieldType.Decimal:
                                row[debtField] = rows.Sum(rw => rw.IsNull(debtField) ? 0 : (decimal)rw[debtField]);
                                break;
                            case EnumFieldType.Float:
                                row[debtField] = rows.Sum(rw => rw.IsNull(debtField) ? 0 : (float)rw[debtField]);
                                break;
                            default:
                                throw new Exception("The field " + debtField + " is type of " + ((EnumFieldType)dataField.FieldType).ToString() + ", it is not valid for the OSMergeDebts operation.");
                        }
                    }
                }
            }
            else
            {
                throw new Exception("OSMergeDebts needs the OSGroupBy action to be setup for the opertation.");
            }

            return true;
                 
        }
    }
}