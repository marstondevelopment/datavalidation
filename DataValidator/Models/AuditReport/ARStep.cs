﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Text;
using Mgl.DataValidator.Models;

namespace Mgl.DataValidator.Models.AuditReport
{
    public abstract class ARStep
    {
        public ARStep(FileTemplateARAction ftar)
        {
            ParentFTAR = ftar;
            ErrorDescription = ARError.DTErrors[(EnumARActionType)ftar.ARAction.ARActionTypeId] ?? "Unexpected error occurred during the DT process, please consult system administrator for further assistance.";

            var prmsGroupBy = ParentFTAR.FileTemplate.FileTemplateDTActions.Where(x => x.DTAction.DTActionTypeId == (int)EnumDTActionType.OSGroupBy).FirstOrDefault();

            if (prmsGroupBy != null)
                GroupByFields = prmsGroupBy.DTAction.PrmsGroupBy.FieldNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x => x.Trim()).ToArray();
        }

        public FileTemplateARAction ParentFTAR { get; set; }
        protected string ErrorDescription { get; set; }
        protected StringBuilder StrBuilder = new StringBuilder();

        protected string[] GroupByFields;
        public abstract bool Execute(ref string arScript);

        public static ARStep New(FileTemplateARAction ftar)
        {

            switch ((EnumARActionType)ftar.ARAction.ARActionTypeId)
            {
                case EnumARActionType.DisplayMnCrBalance:
                    return new ARDisplayMnCrBalance(ftar);
                case EnumARActionType.GetUserName:
                    return new ARGetUserName(ftar);
                case EnumARActionType.GetInputFileName:
                    return new ARGetInputFileName(ftar);
                case EnumARActionType.GetDateTime:
                    return new ARGetDateTime(ftar);

                case EnumARActionType.GetClientScheme:
                    return new ARGetClientScheme(ftar);
                case EnumARActionType.GetNoOfNewCases:
                    return new ARGetNoOfNewCases(ftar);
                case EnumARActionType.GetTotalBalanceOfNewCases:
                    return new ARGetTotalBalanceOfNewCases(ftar);

                case EnumARActionType.GetInputFileRowCount:
                    return new ARGetInputFileRowCount(ftar);
                case EnumARActionType.GetOutputFileRowCount:
                    return new ARGetOutputFileRowCount(ftar);
                case EnumARActionType.GetIgnoredFileRowCount:
                    return new ARGetIgnoredFileRowCount(ftar);
                case EnumARActionType.GetOutputFileTotalBalance:
                    return new ARGetOutputFileTotalBalance(ftar);
                case EnumARActionType.GetDataFromGeneratedFile:
                    return new ARGetDataFromGeneratedFile(ftar);

                default:
                    throw new NotSupportedException();
            }

        }


        public void AppendError()
        {
            ParentFTAR.FileTemplate.ParentDataFile.ValidationSummary.AppendLine(ErrorDescription + " Please consult system administrator for further assistance.");
        }

    }
}