﻿CREATE TABLE [dbo].[DataFieldRule] (
    [Id]                  INT IDENTITY (1, 1) NOT NULL,
    [DataFieldId]         INT NOT NULL,
    [VRId]                INT NOT NULL,
    [StoredProcedureId]   INT NULL,
    [RegularExpressionId] INT NULL,
    [MaxFieldLength]      INT NULL,
    [MinFieldLength]      INT NULL,
    [ReferenceListId]     INT NULL,
    CONSTRAINT [PK_DataFieldRule] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DataFieldRule_DataField] FOREIGN KEY ([DataFieldId]) REFERENCES [dbo].[DataField] ([Id]),
    CONSTRAINT [FK_DataFieldRule_ReferenceList] FOREIGN KEY ([ReferenceListId]) REFERENCES [dbo].[ReferenceList] ([Id]),
    CONSTRAINT [FK_DataFieldRule_RegularExpression] FOREIGN KEY ([RegularExpressionId]) REFERENCES [dbo].[RegularExpression] ([Id]),
    CONSTRAINT [FK_DataFieldRule_StoredProcedure] FOREIGN KEY ([StoredProcedureId]) REFERENCES [dbo].[StoredProcedure] ([Id])
);





