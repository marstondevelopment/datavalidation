﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsExtractComments : PrmsBase
    {
        public PrmsExtractComments() { }
        public PrmsExtractComments(DTAction dt) : base(dt) { }

        [Display(Name = "Comment Field Names")]
        public string CommentFieldName { get; set; }
        [Display(Name = "Output Field Name")]
        public string OutputFieldName { get; set; }
        [Display(Name = "Split Output String")]
        public bool SplitOutputString { get; set; }
        [Display(Name = "Exclude Parent Row")]
        public bool ExcludeParentRow { get; set; }
        [Display(Name = "Exclude Field Name")]
        public bool ExcludeFieldName { get; set; }
        [Display(Name = "Sub Str Max Length")]
        public int SubStrMaxLength { get; set; }
    }
}