﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsNCTransformDefaultersAddresses : PrmsBase
    {
        public PrmsNCTransformDefaultersAddresses() { }
        public PrmsNCTransformDefaultersAddresses(DTAction dt) : base(dt) { }

        [Display(Name = "String Separator")]
        public string StringSeparator { get; set; }
    }
}