﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSGetSumOfColumns : DTStep
    {
        private PrmsGetSumOfColumns prms;
        public OSGetSumOfColumns(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsGetSumOfColumns;
            if (prms == null)
            {
                throw new MemberAccessException("OSGetSumOfColumns parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
        }

        private List<string> columns = null;

        public List<string> Columns
        {
            get
            {
                if (columns == null)
                {
                    columns = prms.ColumnNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();
                }
                return columns;
            }
            set
            {
                columns = value;
            }

        }


        public override bool Execute()
        {
            List<string> wildCardNames = new List<string>();
            foreach (var column in Columns)
            {
                if (column.EndsWith("*"))
                {
                    string[] names = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
                                .Where(y => y.ColumnName.StartsWith(column.Substring(0, column.Length - 1)))
                                 .Select(x => x.ColumnName)
                                 .ToArray();
                    if (names != null && names.Length > 0)
                    {
                        wildCardNames.AddRange(names);
                    }
                }

            }

            Columns = Columns.Where(x => x.EndsWith("*") == false).ToList();
            if (wildCardNames.Count > 0)
            {
                Columns.AddRange(wildCardNames);
            }

            ParentFTDT.FileTemplate.ProcessedDataTable.AddColumn(prms.TotalFieldName);


            foreach (DataRow dr in ParentFTDT.FileTemplate.ProcessedDataTable.Rows)
            {
                decimal total = 0;
                foreach (var col in Columns)
                {
                    decimal colVal = dr.IsNull(col) ? 0 : (decimal)dr[col];
                    total = total + colVal;
                }

                dr[prms.TotalFieldName] = total;
            }

            return true;
                 
        }
    }
}