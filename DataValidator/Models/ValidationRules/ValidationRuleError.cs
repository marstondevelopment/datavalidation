﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public class ValidationRuleError
    {
        private static Dictionary<EnumValidationRule, string> _vRErrors;

        public static Dictionary<EnumValidationRule, string> VRErrors
        {
            get {
                if (_vRErrors == null)
                {
                    _vRErrors = new Dictionary<EnumValidationRule, string>();
                    _vRErrors.Add(EnumValidationRule.MaxFieldLength, "Exceeds maximum field length");
                    _vRErrors.Add(EnumValidationRule.MinFieldLength, "Minimum field length required");
                    _vRErrors.Add(EnumValidationRule.MandatoryField, "Mandatory field required");
                    _vRErrors.Add(EnumValidationRule.FieldType, "Failed to convert to pre-defined type");
                    _vRErrors.Add(EnumValidationRule.SP_Validation, "SP validation failed");
                    _vRErrors.Add(EnumValidationRule.VRM_VehicleReg, "Invalid format for vehicle registration No.");
                    _vRErrors.Add(EnumValidationRule.RE_Validation, "RegEx validation failed.");
                    _vRErrors.Add(EnumValidationRule.RL_Validation, "Reference list validation failed.");
                }

                return _vRErrors;

            }
                
                
                
                
                }
    }
}