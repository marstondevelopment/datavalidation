﻿using System;
using System.Collections.Generic;
using System.Text;
using Marston.MessageExchange.Common.BusinessLayer.PostcodeWS;
using System.Configuration;
using MarstonGroupLtd.Shared.Postcodes;

namespace Mgl.DataValidator.Models.QAS
{
    class AddressFinder
    {
        private const string aLINE_SEPARATOR = " | ";
        private static PostcodeWS.ProWeb m_Server;

        //Engine searching configuration settings (optional to override server defaults) 
        private static PostcodeWS.EngineType m_Engine;

        // Configuration file settings (optional to override defaults) 
        private static PostcodeWS.QAConfigType m_Config;

        private static string _CountryDB = ConfigurationManager.AppSettings["PostcodeWS.CountryDB"].ToString();

        private static string sLayout = ConfigurationManager.AppSettings["PostcodeWS.Layout"].ToString();

        private static List<Address> m_AddList;

        public AddressFinder()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        private string VerifyPostcode(string Postcode)
        {
            string PC = "";

            if (!AddressFinder.ShouldQueryAddressFinder(String.Empty, String.Empty, Postcode))
            {
                return PC;
            }


            List<Address> lstAddressOptions = null;

            lstAddressOptions = AddressFinder.FindAddressOptions(String.Empty, String.Empty, Postcode);


            if ((lstAddressOptions != null) && (lstAddressOptions.Count > 0))
            {
                foreach (Address a in lstAddressOptions)
                {
                    if ((a.Postcode.ToUpper().Trim() == Postcode.ToUpper().Trim()) || (a.Postcode.ToUpper().Trim().Replace(" ", "") == Postcode.ToUpper().Trim().Replace(" ", "")))
                    {
                        PC = a.Postcode;
                        break;
                    }
                }
            }
            return PC;
        }


        /// <summary>
        /// This procedure provides address verification system by QAS. 
        /// It takes in the address elements provided  by the user and 
        /// returns matching address, if any.
        /// </summary>
        /// <param name="AddLine1"></param>
        /// <param name="AddLine2"></param>
        /// <param name="pc"></param>
        /// <returns></returns>
        //public static Address FindAddressByQAS(string AddLine1, string AddLine2, string pc)
        //{
        //    Address Add = new Address();
        //    QAAddressType QAdd = VerifyAddress(AddLine1, AddLine2, pc);

        //    if (QAdd != null)
        //    {
        //        StringBuilder sb = new StringBuilder();

        //        if (!string.IsNullOrEmpty(QAdd.AddressLine[0].Line))
        //        {
        //            Add.AddressLine1 = QAdd.AddressLine[0].Line;
        //        }
        //        if (!string.IsNullOrEmpty(QAdd.AddressLine[1].Line))
        //        {
        //            sb.AppendLine(QAdd.AddressLine[1].Line);
        //        }
        //        if (!string.IsNullOrEmpty(QAdd.AddressLine[2].Line))
        //        {
        //            sb.AppendLine(QAdd.AddressLine[2].Line);
        //        }
        //        if (!string.IsNullOrEmpty(QAdd.AddressLine[3].Line))
        //        {
        //            sb.AppendLine(QAdd.AddressLine[3].Line);
        //        }
        //        Add.RemAddress = sb.ToString();
        //        Add.AddressLine2 = Add.RemAddress;
        //        if (!string.IsNullOrEmpty(QAdd.AddressLine[4].Line)) Add.Postcode = QAdd.AddressLine[4].Line;
        //        return Add;
        //    }
        //    else
        //    {
        //        // introduce some mechanism to capture the address errors

        //        return null;
        //    }

        //}

        public static List<Address> FindAddressOptions(string Address1, string Address2, string Postcode)
        {
            try
            {
                m_Server = new PostcodeWS.ProWeb();
                m_Engine = new PostcodeWS.EngineType();
                m_Config = new PostcodeWS.QAConfigType();

                m_AddList = new List<Address>();

                Address Add = null;

                QASearchResult SR = GetAddressOptions(Address1, Address2, Postcode);

                if (SR != null)
                {
                    QAAddressType QAdd = SR.QAAddress;

                    if (QAdd != null)
                    {
                        Add = GetColumbusAddress(QAdd);
                        if (Add != null) m_AddList.Add(Add);
                    }

                    if ((SR.QAPicklist != null) && (SR.QAPicklist.Total != null))
                    {
                        foreach (PicklistEntryType p in SR.QAPicklist.PicklistEntry)
                        {
                            if (p.CanStep == true)
                            {
                                if (!string.IsNullOrEmpty(p.Moniker)) RefineSearch(p.Moniker);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(p.Moniker)) Add = GetFinalAddress(p.Moniker);
                                if (Add != null) m_AddList.Add(Add);
                            }
                        } // end foreach
                    } // end  SR.QApicklist if           
                } // end SR if

                if (m_AddList != null)
                {
                    //AddressList = m_AddList.ToArray();
                    //m_AddList = null;
                    return m_AddList;
                }
                else
                {
                    return null;
                }
            } // END TRY
            catch (Exception ex)
            {
                MapException(ex);
                return null;
            }
            finally
            {
                m_Server = null;
                m_Engine = null;
                m_Config = null;
            }

        }

        /// <summary>
        /// Test whether a search can be performed using a data set/layout/engine combination
        /// </summary>
        /// <param name="sDataID">Three-letter data identifier</param>
        /// <param name="sLayout">Name of the layout; optional</param>
        /// <returns>If the country and layout combination available, it returns true else false</returns>
        /// <throws>SoapException</throws>
        private static bool CanSearch()
        {
            PostcodeWS.QACanSearch param = new PostcodeWS.QACanSearch();
            param.Country = _CountryDB;
            param.Engine = m_Engine;
            param.Layout = sLayout;
            param.QAConfig = m_Config;

            try
            {
                PostcodeWS.QASearchOk cansearchResult = m_Server.DoCanSearch(param);
                return cansearchResult.IsOk;
            }
            catch (Exception x)
            {
                MapException(x);
                return false;
            }
        }


        /// <summary>
        /// Rethrow a remote SoapException exception, with details parsed and exposed
        /// </summary>
        /// <param name="e"></param>
        private static void MapException(Exception x)
        {
            System.Diagnostics.Debugger.Log(0, "Error", x.ToString() + "\n");

            if (x is System.Web.Services.Protocols.SoapHeaderException)
            {
                System.Web.Services.Protocols.SoapHeaderException xHeader = x as System.Web.Services.Protocols.SoapHeaderException;
                throw x;
            }
            else if (x is System.Web.Services.Protocols.SoapException)
            {
                // Parse out qas:QAFault string
                System.Web.Services.Protocols.SoapException xSoap = x as System.Web.Services.Protocols.SoapException;
                System.Xml.XmlNode xmlDetail = xSoap.Detail;

                string sDetail = xmlDetail.InnerText.Trim();
                string[] asDetail = sDetail.Split('\n');

                string sMessage = asDetail[1].Trim() + " [" + asDetail[0].Trim() + "]";

                // If there is detail available, add it to the message
                // Do this in reverse order - the most relevant detail is the last one
                if (asDetail.Length > 2)
                {
                    for (int i = (asDetail.Length - 1); i > 1; --i)
                    {
                        sMessage += '\n' + asDetail[i].Trim();
                    }
                }
                //QAServerException xThrow = new QAServerException(sMessage, xSoap);
                //throw xThrow;
                //x.InnerException = xSoap;
                //x.Message = sMessage; 
                throw x;
            }
            else
            {
                throw x;
            }
        }

        /// <summary>
        /// This is the main method that verifies Address from the given address elements 
        /// It returns a QAAddressType object, from which is is possible to derive Columbus 
        /// compatible address.
        /// </summary>
        /// <param name="AddLine1"></param>
        /// <param name="AddLine2"></param>
        /// <param name="pc"></param>
        /// <returns></returns>
        private static QAAddressType VerifyAddress(string AddLine1, string AddLine2, string pc)
        {
            // Declare private variables 
            //_CountryDB = ConfigurationManager.AppSettings["PostcodeWS.CountryDB"].ToString(); 
            m_Server = new PostcodeWS.ProWeb();
            //m_Server.Url = ConfigurationManager.AppSettings["PostcodeWS.QAS"].ToString();

            m_Engine = new PostcodeWS.EngineType();
            m_Config = new PostcodeWS.QAConfigType();
            //string sLayout = ConfigurationManager.AppSettings["PostcodeWS.Layout"].ToString();

            PostcodeWS.QASearch param = new PostcodeWS.QASearch();
            param.Country = _CountryDB;

            m_Engine.Value = PostcodeWS.EngineEnumType.Verification;
            param.Engine = m_Engine;
            param.Engine.PromptSet = PostcodeWS.PromptSetType.Default;
            param.Engine.PromptSetSpecified = true;
            param.Layout = sLayout;
            param.QAConfig = m_Config;
            param.Search = GetSearchString(AddLine1, AddLine2, pc);

            //bool bPreVerificationFailed = false;

            try
            {
                if (!CanSearch())
                {
                    // bPreVerificationFailed = true;
                    return null;
                }
                else
                {
                    //Make the call to the server 
                    PostcodeWS.QASearchResult SR = m_Server.DoSearch(param);

                    //if ((SR.VerifyLevel == PostcodeWS.VerifyLevelType.Verified)|| (SR.VerifyLevel == PostcodeWS.VerifyLevelType.InteractionRequired)

                    //if (SR.QAAddress != null)
                    if (SR.VerifyLevel == PostcodeWS.VerifyLevelType.Verified)
                    {
                        return SR.QAAddress;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception x)
            {

                MapException(x);
                return null;
            }

            finally
            {
                //Destroy private variables 
                m_Server = null;
                m_Engine = null;
                m_Config = null;
            }
        }

        /// <summary>
        /// This takes in the address elements and returns a search string 
        /// combing all the input parameters with a separator. Address 
        /// verification seach is performed agaiist this  string. 
        /// </summary>
        /// <param name="AddLine1"></param>
        /// <param name="AddLine2"></param>
        /// <param name="pc"></param>
        /// <returns></returns>
        private static string GetSearchString(string AddLine1, string AddLine2, string pc)
        {

            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(AddLine1)) sb.Append(AddLine1.Trim() + aLINE_SEPARATOR);

            if (!string.IsNullOrEmpty(AddLine2)) sb.Append(AddLine2.Trim() + aLINE_SEPARATOR);

            if (!string.IsNullOrEmpty(pc)) sb.Append(pc.Trim());

            return sb.ToString();
        }


        // end FindAddressOptions


        /// <summary>
        /// When a pcklist is provided that matches user inputs, this picklist is drilled down 
        /// to the lowest level possible so that a complete address is provided by the Address 
        /// verification System.
        /// </summary>
        /// <param name="sMoniker"></param>
        private static void RefineSearch(string sMoniker)
        {

            System.Diagnostics.Debug.Assert(sMoniker != null);

            //Set up the parameter for the SOAP call 
            PostcodeWS.QARefine param = new PostcodeWS.QARefine();
            param.Moniker = sMoniker;
            //param.Refinement = sRefinementText; 
            param.QAConfig = m_Config;
            param.Threshold = m_Engine.Threshold;
            param.Timeout = m_Engine.Timeout;

            Address Add = null;

            try
            {
                //Make the call to the server 
                PostcodeWS.QAPicklistType pl = m_Server.DoRefine(param).QAPicklist;

                foreach (PicklistEntryType p in pl.PicklistEntry)
                {
                    if (p.CanStep == true)
                    {
                        if (!string.IsNullOrEmpty(p.Moniker)) RefineSearch(p.Moniker);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(p.Moniker)) Add = GetFinalAddress(p.Moniker);
                        if (Add != null) m_AddList.Add(Add);
                    }
                }
            }

            catch (Exception x)
            {
                MapException(x);
            }
        } // end Refine Search

        /// <summary>
        /// Passing the moniker, Address verification ssytem provides the 
        /// correct adress, if it is the final addresss.
        /// </summary>
        /// <param name="moniker"></param>
        /// <returns></returns>
        private static Address GetFinalAddress(string moniker)
        {
            System.Diagnostics.Debug.Assert(moniker != null);

            //Set up the parameter for the SOAP call 
            PostcodeWS.QAGetAddress param = new PostcodeWS.QAGetAddress();
            param.Layout = sLayout;
            param.Moniker = moniker;
            param.QAConfig = m_Config;

            try
            {
                //Make the call to the server 
                PostcodeWS.QAAddressType AddressType = m_Server.DoGetAddress(param).QAAddress;
                Address add = GetColumbusAddress(AddressType);
                return add;
            }

            catch (Exception x)
            {
                MapException(x);
                return null;
            }

        } // end  GetFinalAddress method

        /// <summary>
        /// The method takes in the QAS address type object and constructs  
        /// a columbus Address Type object.
        /// <param name="QAdd"></param>
        /// <returns></returns>
        private static Address GetColumbusAddress(QAAddressType QAdd)
        {
            Address Add = new Address();
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(QAdd.AddressLine[0].Line)) Add.AddressLine1 = QAdd.AddressLine[0].Line;
            if (!string.IsNullOrEmpty(QAdd.AddressLine[1].Line))
            {
                sb.AppendLine(QAdd.AddressLine[1].Line);
            }
            if (!string.IsNullOrEmpty(QAdd.AddressLine[2].Line))
            {
                sb.AppendLine(QAdd.AddressLine[2].Line);
            }
            if (!string.IsNullOrEmpty(QAdd.AddressLine[3].Line))
            {
                sb.AppendLine(QAdd.AddressLine[3].Line);
            }
            sb = sb.Replace("\r\n", ", ");
            // remove the last comma
            sb = sb.Remove(sb.Length - 2, 2);
            Add.RemAddress = sb.ToString();
            Add.AddressLine2 = Add.RemAddress;
            if (!string.IsNullOrEmpty(QAdd.AddressLine[4].Line)) Add.Postcode = QAdd.AddressLine[4].Line;
            return Add;

        }
        /// <summary>
        /// This private method provides the search result from QAS address 
        /// verification system, when provided with the address elements. the search result contains 
        /// various optional address elements.
        /// </summary>
        /// <param name="Address1"></param>
        /// <param name="Address2"></param>
        /// <param name="Postcode"></param>
        /// <returns></returns>
        private static QASearchResult GetAddressOptions(string Address1, string Address2, string Postcode)
        {
            // Declare private variables 
            PostcodeWS.QASearch param = new PostcodeWS.QASearch();
            param.Country = _CountryDB;
            m_Engine.Value = PostcodeWS.EngineEnumType.Singleline;
            param.Engine = m_Engine;
            param.Engine.PromptSet = PostcodeWS.PromptSetType.Default;
            param.Engine.PromptSetSpecified = true;
            param.Layout = sLayout;
            param.QAConfig = m_Config;
            param.Search = GetSearchString(Address1, Address2, Postcode);

            // bool bPreVerificationFailed = false;

            try
            {
                if (!CanSearch())
                {
                    //bPreVerificationFailed = true;
                    return null;
                }
                else
                {
                    //Make the call to the server 
                    PostcodeWS.QASearchResult SR = m_Server.DoSearch(param);

                    return SR;
                }
            }
            catch (Exception x)
            {

                MapException(x);
                return null;
            }

        }

        public static bool ShouldQueryAddressFinder(string address1, string address2, string postcode)
        {
            if (!string.IsNullOrWhiteSpace(address1) ||
                !string.IsNullOrWhiteSpace(address2))
            {
                // If any address data is available, querying PAF is desired.
                return true;
            }

            // There is only postcode, query PAF only if Postcode has Full completeness.
            // Partial postcode PAF requests are very inefficient (by attempting to download all potential matches). The response may take many minutes.
            // If any results are received, they are useless to the user, as there are too many possibilities.

            var decomposedPostcode = new Postcode(postcode).Normalize().TryDecompose();

            return decomposedPostcode?.Completeness == PostcodeCompleteness.Full;
        }

    }
}
