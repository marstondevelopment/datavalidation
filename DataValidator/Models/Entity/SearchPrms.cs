﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class SearchPrms<T>
    {
        public SearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults)
        {
            SortOrder = sortOrder;
            SearchString = searchString;
            PageNo = page ?? 1;
            PageSize = pageSize ?? defaultPageSize;
            if (allResults != null)
            {
                Results = allResults.ToPagedList(PageNo, PageSize);
                RowsCount = allResults.Count;
            }
        }

        public int RowsCount { get; set; }
        public int PageNo { get; set; }
        [Display(Name = "Search Criteria")]
        public string SearchString { get; set; }
        public string SortOrder { get; set; }
        [Display(Name = "Page Size")]
        public int PageSize { get; set; }
        public IPagedList<T> Results { get; set; }

        private SelectList pageSizeList;
        public SelectList PageSizeList
        {
            get
            {

                if (pageSizeList == null)
                {
                    pageSizeList = new SelectList(new int[] { 10, 25, 50, 100, 200 });
                }

                return pageSizeList;
            }

            set => pageSizeList = value;
        }

    }
}