﻿CREATE TABLE [dbo].[Title] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (25) NOT NULL,
    [Description] NVARCHAR (50) NULL,
    CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED ([Id] ASC)
);

