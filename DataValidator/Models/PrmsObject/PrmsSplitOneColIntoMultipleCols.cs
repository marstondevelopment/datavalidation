﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitOneColIntoMultipleCols : PrmsBase
    {
        public PrmsSplitOneColIntoMultipleCols() { }
        public PrmsSplitOneColIntoMultipleCols(DTAction dt) : base(dt) { }

        [Display(Name = "Input Column Name")]
        public string InputColumnName { get; set; }
        [Display(Name = "Output Column Names")]
        public string OutputColumnNames { get; set; }

        [Display(Name = "String Separator")]
        public string StringSeparator { get; set; }
        [Display(Name = "Filter")]
        public string Filter { get; set; }
        [Display(Name = "Notes Column Name")]
        public string NotesColumnName { get; set; }
    }
}