﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitAndTransposeOutputFile : PrmsBase
    {
        public PrmsSplitAndTransposeOutputFile() { }
        public PrmsSplitAndTransposeOutputFile(DTAction dt) : base(dt) { }

        [Display(Name = "ID Col. Name")]
        public string IDColName { get; set; }

        [Display(Name = "Other Col. Names")]
        public string OtherColNames { get; set; }

        [Display(Name = "Filter")]
        public string Filter { get; set; }
        [Display(Name = "Scheme Name")]
        public string Scheme { get; set; }
        [Display(Name = "2 Output Col. Names")]
        public string OutputColNames { get; set; }
        [Display(Name = "Keep Selected Row")]
        public bool KeepSelectedRow{ get; set; }

        [Display(Name = "Remove Text By Regex List")]
        public int? RegexRefListId { get; set; }

        [JsonIgnore]
        public ReferenceList RegexRefList { get; set; }
    }
}