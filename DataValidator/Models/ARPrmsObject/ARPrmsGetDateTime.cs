﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class ARPrmsGetDateTime : ARPrmsBase
    {
        public ARPrmsGetDateTime() { }
        public ARPrmsGetDateTime(ARAction ar) : base(ar) { }

        [Display(Name = "Date Format")]
        public string DateFormat { get; set; }
    }
}