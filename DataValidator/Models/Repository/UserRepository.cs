﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mgl.DataValidator.Models;
using WebGrease.Css.Extensions;

namespace Mgl.DataValidator.Models
{
    public class UserRepository : RepositoryBase<DataIntegrationEntities, User>
    {
        //public const string CURRENT_USER = "CurrentUser";

        public UserRepository(DataIntegrationEntities context) : base(context)
        {
        }


        public override IQueryable<User> GetAll()
        {
            return base.GetAll();
        }

        public User GetUserById(int id)
        {
            return _entities.Users.Include("UserClients").Include("UserClients.Client").Include("UserClients.Client.FileTemplates").Where(x=> x.Id == id).FirstOrDefault();
        }

        public User GetUser(string logonId)
        {
            return _entities.Users.Include("UserClients").Include("UserClients.Client").Include("UserClients.Client.FileTemplates").Where(u => u.UserId == logonId).FirstOrDefault();
        }

        public User GetUser(string domainName, string logonId)
        {
            return _entities.Users.Include("UserClients").Include("UserClients.Client").Include("UserClients.Client.FileTemplates").Where(u => u.UserId == logonId && u.DomainName == domainName).FirstOrDefault();
        }

        public User CurrentUser
        {
            get
            {
                 string[] domainName = HttpContext.Current.User.Identity.Name.Split("\\".ToCharArray());
                User user = (User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                if (user == null || (user.UserId.ToUpper() != domainName[1].ToUpper() || user.DomainName.ToUpper() != domainName[0].ToUpper())  )
                {
                    user = GetUser(domainName[0], domainName[1]);
                    HttpContext.Current.Session[HttpContext.Current.Session.SessionID] = user;
                }

                return user;
            }

            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID] = value;
            }
        }

        public void RefreshCurrentUser()
        {
            if (HttpContext.Current.Session[HttpContext.Current.Session.SessionID] == null)
            {
                string[] domainName = HttpContext.Current.User.Identity.Name.Split("\\".ToCharArray());
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID] = GetUser(domainName[0], domainName[1]);
            }
            else
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID] = GetUserById(((User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]).Id);
            }

            return;

        }

        public void AddUser(User user)
        {
            user.CreatedDate = DateTime.Now;            
            user.VisitsCount++;
            user.LastLoggedOnDate = DateTime.Now;
            this.Add(user);
            this.SubmitChanges();
        }

        public void UpdateUser(User user)
        {
            user.VisitsCount++;
            user.LastLoggedOnDate = DateTime.Now;
            this.SubmitChanges();
        }

        public void Update(User user)
        {
            this.SubmitChanges();
        }

        internal IEnumerable<Client> GetCurrentUserAssociatedClients()
        {            
            return CurrentUser.AssociatedClients ?? _entities.UserClients.Include("Client.User").Where(x => x.UserId == CurrentUser.Id).Select(y => y.Client).OrderBy(z => z.Name).ToList();
        }


        internal IEnumerable<UserClient> UpdateClient(UserClient userClient)
        {
            var ucList = _entities.UserClients.Where(x => x.UserId == userClient.UserId);
            UserClient uc = ucList.Where(x => x.ClientId == userClient.ClientId).FirstOrDefault();
            if (userClient.IsDefault == true)
            {
                ucList.ForEach(x => x.IsDefault = false);
            }
            if (uc != null)
            {
                uc.IsDefault = userClient.IsDefault;
            }
            else
            {
                uc = _entities.UserClients.Add(userClient);
            }
            SubmitChanges();

            return _entities.UserClients.Include("Client.User").Where(x => x.UserId == userClient.UserId).OrderBy(y=> y.Client.Name).ToList();
        }

        internal UserClient GetUserClient(int id)
        {
            return _entities.UserClients.Include("Client.User").Where(x => x.Id == id).FirstOrDefault();
        }

        internal IEnumerable<UserClient> DeleteClient(UserClient uc)
        {
            int uId = uc.UserId;
            _entities.UserClients.Remove(uc);
            SubmitChanges();
            return _entities.UserClients.Include("Client.User").Where(x => x.UserId == uId).OrderBy(y => y.Client.Name).ToList();
        }
    }
}