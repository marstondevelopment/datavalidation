-- ---------------------------------------------Client ------------------------------------------
INSERT INTO Client ([Name]
      ,[Description]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[Disabled]) 
	  
	  VALUES
	  ('London Collectica', 'London Collectica',1, '2018-Jul-10 16:46:10', 1, '2018-Jul-10 16:46:10', 0);



DECLARE @id int;
SET @id = @@IDENTITY;

----------------------------------------------File Template -----------------------------------------

INSERT INTO FileTemplate ([Name]
      ,[Description]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[DBConnectionId]
      ,[Disabled]
      ,[MimeTypeId]
      ,[ClientId])
VALUES('VRM','Collectica VRM File Format',1, '2018-Jul-10 16:46:10', 1, '2018-Jul-10 16:46:10',null,0,1,@id)

GO

--------------------------------------  Data Field ---------------------------------------------------------------

INSERT INTO DataField ([Name]
      ,[Description]
      ,[FieldType]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[MaxFieldLength]
      ,[MinFieldLength])
	  VALUES
	  (
	  'WARRANTNO',
	  'Warranty Number',
	  5,
	  1,
	  '2018-Jul-10 16:46:10',
	  1,
	  '2018-Jul-10 16:46:10',
		null,
		null	
	  )

GO

INSERT INTO DataField ([Name]
      ,[Description]
      ,[FieldType]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[MaxFieldLength]
      ,[MinFieldLength])
	  VALUES
	  (
	  'ACCOUNTNO',
	  'VRM Account Number',
	  5,
	  1,
	  '2018-Jul-10 16:46:10',
	  1,
	  '2018-Jul-10 16:46:10',
		null,
		null	
	  )

	  GO




INSERT INTO DataField ([Name]
      ,[Description]
      ,[FieldType]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[MaxFieldLength]
      ,[MinFieldLength])
	  VALUES
	  (
	  'SURNAME',
	  'VRM Surname',
	  5,
	  1,
	  '2018-Jul-10 16:46:10',
	  1,
	  '2018-Jul-10 16:46:10',
		null,
		null	
	  )

	  GO


INSERT INTO DataField ([Name]
      ,[Description]
      ,[FieldType]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[MaxFieldLength]
      ,[MinFieldLength])
	  VALUES
	  (
	  'FORENAMES',
	  'VRM Forename',
	  5,
	  1,
	  '2018-Jul-10 16:46:10',
	  1,
	  '2018-Jul-10 16:46:10',
		null,
		null	
	  )

	  GO

INSERT INTO DataField ([Name]
      ,[Description]
      ,[FieldType]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[MaxFieldLength]
      ,[MinFieldLength])
	  VALUES
	  (
	  'TITLE',
	  'VRM Title',
	  5,
	  1,
	  '2018-Jul-10 16:46:10',
	  1,
	  '2018-Jul-10 16:46:10',
		null,
		null	
	  )

	  GO

INSERT INTO DataField ([Name]
      ,[Description]
      ,[FieldType]
      ,[CreatedBy]
      ,[DateCreated]
      ,[UpdatedBy]
      ,[DateUpdated]
      ,[MaxFieldLength]
      ,[MinFieldLength])
	  VALUES
	  (
	  'VEHICLEREG',
	  'VRM Vehicle Registration No.',
	  5,
	  1,
	  '2018-Jul-10 16:46:10',
	  1,
	  '2018-Jul-10 16:46:10',
		7,
		7	
	  )

	  GO




	  -------------------------------------------- File Template - Data Field ------------------------------------------------

	  INSERT INTO FileTemplateDataField 
	  (
	  [FileTemplateId]
      ,[DataFieldId]
	  )
	  VALUES
	  (
	  (SELECT Id FROM [dbo].[FileTemplate] WHERE Name='VRM'),
	  1
	  )

	  GO

	  INSERT INTO FileTemplateDataField 
	  (
	  [FileTemplateId]
      ,[DataFieldId]
	  )
	  VALUES
	  (
	  (SELECT Id FROM [dbo].[FileTemplate] WHERE Name='VRM'),
	  2
	  )

	  GO
	  INSERT INTO FileTemplateDataField 
	  (
	  [FileTemplateId]
      ,[DataFieldId]
	  )
	  VALUES
	  (
	  (SELECT Id FROM [dbo].[FileTemplate] WHERE Name='VRM'),
	  3
	  )

	  GO
	  INSERT INTO FileTemplateDataField 
	  (
	  [FileTemplateId]
      ,[DataFieldId]
	  )
	  VALUES
	  (
	  (SELECT Id FROM [dbo].[FileTemplate] WHERE Name='VRM'),
	  4
	  )

	  GO
	  INSERT INTO FileTemplateDataField 
	  (
	  [FileTemplateId]
      ,[DataFieldId]
	  )
	  VALUES
	  (
	  (SELECT Id FROM [dbo].[FileTemplate] WHERE Name='VRM'),
	  5
	  )

	  GO
	  INSERT INTO FileTemplateDataField 
	  (
	  [FileTemplateId]
      ,[DataFieldId]
	  )
	  VALUES
	  (
	  (SELECT Id FROM [dbo].[FileTemplate] WHERE Name='VRM'),
	  6
	  )

	  GO

	  ----------------------------------- Data Field Rules ---------------------------------------------
	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  1,
	  3,
	  null
	  )

	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  2,
	  3,
	  null
	  )

	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  3,
	  3,
	  null
	  )

	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  4,
	  3,
	  null
	  )

	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  5,
	  3,
	  null
	  )

	INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  5,
	  6,
	  null
	  )

	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  6,
	  7,
	  null
	  )

	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  6,
	  3,
	  null
	  )

	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  6,
	  1,
	  null
	  )

	  --------------------------------------------- Data Field Data Cleansing ----------------------------------------

	  INSERT INTO DataFieldDataCleansing
	  (
      [DataFieldId]
      ,[DCId]
      ,[OldText]
      ,[NewText]	  
	  )
	  VALUES
	  (
		6,
		1,
		null,
		null
	  )

	  	  INSERT INTO DataFieldDataCleansing
	  (
      [DataFieldId]
      ,[DCId]
      ,[OldText]
      ,[NewText]	  
	  )
	  VALUES
	  (
		6,
		2,
		null,
		null
	  )


	  ------------------------------------------ DB Connection String ------------------------------------

	  INSERT INTO DBConnection
	  (
      [Name]
      ,[ConnectionString]
      ,[Disabled]
	  )
	  VALUES
	  (
		'DataValidationDB',
		'Data Source=mh-da-dev-sql.database.windows.net;Initial Catalog=mh-dev-dataIntegration;Persist Security Info=True;User ID=dwadmin;Password=dw@dm1n23;MultipleActiveResultSets=True;',
		0
	  )


	  ------------------------------------------ StoredProcedure ------------------------------

	  	  INSERT INTO DBConnection
	  (
      [SPName]
      ,[Description]
      ,[Params]
	  ,[DBConnectionId]

	  )
	  VALUES
	  (
		'CheckWarrantNo',
		'Check if Warrant Number is exist on database ',
		'WarrantNo,AccountNo',
		3
	  )

-- =======================================================
-- Stored Procedure -- CheckWarrantNo
-- =======================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CheckWarrantNo] 
@WarrantNo varchar (50),
@AccountNo varchar (50),
@status BIT OUTPUT
AS
    BEGIN
    SET @status = 1
    END


---------------------------------------------- Add SP Validation ------------------
	  INSERT INTO DataFieldRule (
	  [DataFieldId]
      ,[VRId]
      ,[StoredProcedureId])
	  VALUES
	  (
	  1,
	  5,
	  1
	  )

	  Update FileTemplate 
	  SET
      [DBConnectionId] = 3
	  WHERE [Name] = 'VRM'
