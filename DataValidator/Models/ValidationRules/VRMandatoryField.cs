﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public class VRMandatoryField : VRBase
    {
        public VRMandatoryField(DataFieldRule df) : base(df)
        {
        }

        public override bool ValidateField()
        {
            if (string.IsNullOrEmpty(this.ParentFieldRule.DataField.OriginalValue))
            {
                AppendError();
                return false;
            }
            return true;
        }
    }
}