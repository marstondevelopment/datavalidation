﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARError
    {
        private static Dictionary<EnumARActionType, string> _arErrors;

        public static Dictionary<EnumARActionType, string> DTErrors
        {
            get
            {
                if (_arErrors == null)
                {
                    _arErrors = new Dictionary<EnumARActionType, string>
                    {
                        { EnumARActionType.DisplayMnCrBalance, "AR action, ARDisplayMnCrBalance, failed." },
                        { EnumARActionType.GetIgnoredFileRowCount, "AR action, ARGetIgnoredFileRowCount, failed." },
                        { EnumARActionType.GetInputFileName, "AR action, ARGetInputFileName, failed." },
                        { EnumARActionType.GetInputFileRowCount, "AR action, ARGetInputFileRowCount, failed." },
                        { EnumARActionType.GetOutputFileRowCount, "AR action, ARGetOutputFileRowCount, failed." },
                        { EnumARActionType.GetOutputFileTotalBalance, "AR action, ARGetOutputFileTotalBalance, failed." },
                        { EnumARActionType.GetDateTime, "AR action, ARGetDateTime, failed." },
                        { EnumARActionType.GetUserName, "AR action, ARGetUserName, failed." },
                        { EnumARActionType.GetClientScheme, "AR action, ARGetClientScheme, failed." },
                        { EnumARActionType.GetNoOfNewCases, "AR action, ARGetNoOfNewCases, failed." },
                        { EnumARActionType.GetTotalBalanceOfNewCases, "AR action, ARGetTotalBalanceOfNewCases, failed." },
                        { EnumARActionType.GetDataFromGeneratedFile, "AR action, ARGetDataFromGeneratedFile, failed." }
                    };
                }

                return _arErrors;

            }
        }
    }
}