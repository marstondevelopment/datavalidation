﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebGrease.Css.Extensions;
using System.Text;
using System.Text.RegularExpressions;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class SplitAndTransposeOutputFile : DTStep
    {
        private PrmsSplitAndTransposeOutputFile prms;
        List<string> otherColNames;
        List<string> outputColNames;

        public SplitAndTransposeOutputFile(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitAndTransposeOutputFile;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsSplitAndTransposeOutputFile  has not been set for DTAction - " + ParentFTDT.DTAction.ActionName);
            }

            otherColNames = prms.OtherColNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();
            outputColNames = prms.OutputColNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();




            if ((otherColNames == null || otherColNames.Count <=0) || (outputColNames == null || outputColNames.Count != 2) || string.IsNullOrEmpty(prms.IDColName) ||  string.IsNullOrEmpty(prms.Scheme)) 
            {
                throw new Exception("Settings are not correct for DTAction - " + ParentFTDT.DTAction.ActionName);
            }

            otherColNames.ForEach( x => x = x.Trim());
            outputColNames.ForEach(x => x = x.Trim());

            List<string> wildcardColNames = new List<string>();

            foreach (var cn in otherColNames.Where(x => x.EndsWith("*")).ToArray())
            {
                var dfGroups = Utility.GetColumns(cn, ParentFTDT.FileTemplate.ProcessedDataTable);
                dfGroups.Remove(cn.TrimEnd("*".ToCharArray()));
                wildcardColNames.AddRange(dfGroups);
            }
            otherColNames.RemoveAll(x => x.Trim().EndsWith("*"));
            if (wildcardColNames.Count >0)
                otherColNames.AddRange(wildcardColNames);

        }
        public override bool Execute()
        {
            OutputFile outputFile = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(prms.Scheme.Trim(), EnumOutputFileCategory.Output);
            outputFile.Data = new DataTable();
            outputColNames.ForEach(x => outputFile.Data.AddColumn(x));
            outputFile.Data.AcceptChanges();
            DataRow[] rows = ParentFTDT.FileTemplate.ProcessedDataTable.Select(prms.Filter);
            if (rows.Length > 0)
            {
                rows.ForEach(x => PopulateOutputTable(outputFile.Data, x));
                outputFile.Data.AcceptChanges();

                if (!prms.KeepSelectedRow)
                {
                    rows.ForEach(x => x.Delete());
                    ParentFTDT.FileTemplate.ProcessedDataTable.AcceptChanges();
                }
            }

            return true;
        }

        private void PopulateOutputTable(DataTable dt,DataRow row)
        {
            string idFieldVal = row[prms.IDColName.Trim()].ObjToString(ParentFTDT.FileTemplate.ProcessedDataTable.Columns[prms.IDColName.Trim()]);

            foreach (var cn in otherColNames)
            {
                string otherFieldVal = row[cn].ObjToString(ParentFTDT.FileTemplate.ProcessedDataTable.Columns[cn]);
                if (!string.IsNullOrEmpty(idFieldVal) && !string.IsNullOrEmpty(otherFieldVal))
                {
                    otherFieldVal = cn + ":" + otherFieldVal;

                    if (prms.RegexRefListId.HasValue)
                        otherFieldVal = RemoveTextsByRegEx(otherFieldVal);

                    dt.Rows.Add(idFieldVal, otherFieldVal);
                }
            }
        }

        private string RemoveTextsByRegEx(string inputStr)
        {            
            foreach (var ptn in prms.RegexRefList.ReferenceListItems)
            {
                inputStr = Regex.Replace(inputStr, ptn.Description.Trim(),string.Empty, RegexOptions.IgnoreCase);
            }
            return inputStr;
        }

    }
}