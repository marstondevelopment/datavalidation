﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class DTActionRepository : RepositoryBase<DataIntegrationEntities, DTAction>
    {
        private StoredProcedureRepository _spr;
        private UserRepository _ur;
        public DTActionRepository(DataIntegrationEntities entities, StoredProcedureRepository spr, UserRepository ur) : base(entities)
        {
            _spr = spr;
            _ur = ur;
        }

        public override IQueryable<DTAction> GetAll()
        {
            return base.GetAll();
        }

        private void PresetDBCmd(IDBCmd cmd)
        {
            if (cmd.StoredProcedureId.HasValue)
            {
                cmd.StoredProcedure = _spr.GetById(cmd.StoredProcedureId.Value);
            }
            if (cmd.CmdTextId.HasValue)
            {
                cmd.CmdText = _entities.CmdTexts.Where(x => x.Id == cmd.CmdTextId.Value).FirstOrDefault();
            }
        }

        public DTAction GetDTActionById(int id)
        {
            DTAction dtaction = GetAll(x => x.Id == id).FirstOrDefault();

            switch (dtaction.DTActionType)
            {
                case EnumDTActionType.OSExcludeCasesAlreadyImported:
                    PresetDBCmd((IDBCmd)dtaction.PrmsExcludeCasesAI);
                    break;

                case EnumDTActionType.GetDataFromDB:
                    PresetDBCmd((IDBCmd)dtaction.PrmsGetDataFromDB);
                    break;

                case EnumDTActionType.XMappingDataFromDB:
                    PresetDBCmd((IDBCmd)dtaction.PrmsXMappingDataFromDB);
                    break;

                case EnumDTActionType.SplitFullAddressColIntoSepLines:
                    dtaction.PrmsSplitFullAddressColIntoSepLines.RegularExpression =
                        _entities.RegularExpressions.Where(x => x.Id == dtaction.PrmsSplitFullAddressColIntoSepLines.RegularExpressionId).FirstOrDefault();
                    if (dtaction.PrmsSplitFullAddressColIntoSepLines.ReferenceListId.HasValue)
                        dtaction.PrmsSplitFullAddressColIntoSepLines.ReferenceList =
                            _entities.ReferenceLists.Where(x => x.Id == dtaction.PrmsSplitFullAddressColIntoSepLines.ReferenceListId.Value).FirstOrDefault();
                    break;
                case EnumDTActionType.ExcludeRowsNotMatchReferenceList:
                    if (dtaction.PrmsExcludeRowsNotMatchReferenceList.ReferenceListId.HasValue)
                        dtaction.PrmsExcludeRowsNotMatchReferenceList.ReferenceList =
                            _entities.ReferenceLists.Where(x => x.Id == dtaction.PrmsExcludeRowsNotMatchReferenceList.ReferenceListId.Value).FirstOrDefault();
                    break;
                case EnumDTActionType.SplitAndTransposeOutputFile:
                    if (dtaction.PrmsSplitAndTransposeOutputFile.RegexRefListId.HasValue)
                        dtaction.PrmsSplitAndTransposeOutputFile.RegexRefList =
                            _entities.ReferenceLists.Where(x => x.Id == dtaction.PrmsSplitAndTransposeOutputFile.RegexRefListId.Value).FirstOrDefault();
                    break;
                case EnumDTActionType.OSConcatenateFields:
                    if (dtaction.PrmsConcatenateFields.RegularExpressionId.HasValue)
                        dtaction.PrmsConcatenateFields.RegularExpression =
                            _entities.RegularExpressions.Where(x => x.Id == dtaction.PrmsConcatenateFields.RegularExpressionId.Value).FirstOrDefault();
                    if (dtaction.PrmsConcatenateFields.RegExRefListId.HasValue)
                        dtaction.PrmsConcatenateFields.RegExRefList =
                            _entities.ReferenceLists.Where(x => x.Id == dtaction.PrmsConcatenateFields.RegExRefListId.Value).FirstOrDefault();
                    break;
                case EnumDTActionType.ConcatenateMultiNodesDFs:
                    if (dtaction.PrmsConcatenateMultiNodesDFs.RegularExpressionId.HasValue)
                        dtaction.PrmsConcatenateMultiNodesDFs.RegularExpression =
                            _entities.RegularExpressions.Where(x => x.Id == dtaction.PrmsConcatenateMultiNodesDFs.RegularExpressionId.Value).FirstOrDefault();
                    break;
                default:
                    break;
            }

            return dtaction;
        }

        public void Create(DTAction dtAction)
        {
            dtAction.PrmsObject = dtAction.SerializePrmsObject();
            dtAction.CreatedBy = _ur.CurrentUser.Id;
            dtAction.UpdatedBy = _ur.CurrentUser.Id;
            dtAction.DateCreated = DateTime.Now;
            dtAction.DateUpdated = DateTime.Now;
            Add(dtAction);
            SubmitChanges();
        }

        public void Update(DTAction dtAction)
        {
            dtAction.PrmsObject = dtAction.SerializePrmsObject();
            dtAction.UpdatedBy = _ur.CurrentUser.Id;
            dtAction.DateUpdated = DateTime.Now;
            SubmitChanges();
        }

        public void Delete(DTAction dtAction)
        {
            Remove(dtAction);
            SubmitChanges();
        }

        public IEnumerable<StoredProcedure> GetSPs()
        {
            return _entities.StoredProcedures.Where(x => x.SPType == (int)EnumSPType.VRSP).ToList();
        }

        public IEnumerable<RegularExpression> GetRegexs()
        {
            return _entities.RegularExpressions.Where(x => x.Disabled == false).ToList();
        }

        public IEnumerable<ReferenceList> GetRefLists()
        {
            return _entities.ReferenceLists.Where(x => x.Disabled == false).ToList();
        }
        public IEnumerable<CmdText> GetCmdTxts()
        {
            return _entities.CmdTexts.ToList();
        }
    }
}