﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class FileScheme
    {
        public FileScheme(string prefix)
        {
            FilePrefix = prefix;
            OutputDataTables = new Dictionary<EnumOutputFileCategory, DataTable>();
        }
        public string FilePrefix { get; set; }
        public Dictionary<EnumOutputFileCategory, DataTable> OutputDataTables { get; set; }
        public string Audit { get; set; }
    }
}