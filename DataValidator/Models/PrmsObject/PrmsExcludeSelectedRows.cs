﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsExcludeSelectedRows : PrmsBase
    {
        public PrmsExcludeSelectedRows() { }
        public PrmsExcludeSelectedRows(DTAction dt) : base(dt) { }

        [Display(Name = "Filters")]
        public string FieldValues { get; set; }
        [Display(Name = "Scheme Names")]
        public string Scheme { get; set; }
        [Display(Name = "Exclusion Reasons")]
        public string ExclusionReasons { get; set; }
    }
}