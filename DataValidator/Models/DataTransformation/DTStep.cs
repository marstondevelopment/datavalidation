﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Mgl.DataValidator.Models;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public abstract class DTStep
    {
        public DTStep(FileTemplateDTAction ftdt)
        {
            ParentFTDT = ftdt;
            ErrorDescription = DTError.DTErrors[(EnumDTActionType)ftdt.DTAction.DTActionTypeId] ?? "Unexpected error occurred during the DT process, please consult system administrator for further assistance.";

            var prmsGroupBy = ParentFTDT.FileTemplate.FileTemplateDTActions.Where(x => x.DTAction.DTActionTypeId == (int)EnumDTActionType.OSGroupBy).FirstOrDefault();

            if (prmsGroupBy != null)
                GroupByFields = prmsGroupBy.DTAction.PrmsGroupBy.FieldNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x => x.Trim()).ToArray();
        }

        public FileTemplateDTAction ParentFTDT { get; set; }
        protected string ErrorDescription { get; set; }

        protected string[] GroupByFields;
        public abstract bool Execute();

        public static DTStep New(FileTemplateDTAction ftdt)
        {
            switch ((EnumDTActionType)ftdt.DTAction.DTActionTypeId)
            {
                case EnumDTActionType.OSSplitOutputFileBasedOnFields:
                    return new OSSplitOutputFileBasedOnFields(ftdt);
                case EnumDTActionType.ExcludeSelectedRows:
                    return new ExcludeSelectedRows(ftdt);
                case EnumDTActionType.OSExcludeCasesAlreadyImported:
                    return new OSExcludeCasesAlreadyImported(ftdt);
                case EnumDTActionType.OSExcludeCasesLessThanThreadholdVal:
                    return new OSExcludeCasesLessThanThreadholdVal(ftdt);
                case EnumDTActionType.OSExtractComments:
                    return new OSExtractComments(ftdt);
                case EnumDTActionType.OSMergeDebts:
                    return new OSMergeDebts(ftdt);
                case EnumDTActionType.OSGroupBy:
                    return new OSGroupBy(ftdt);
                case EnumDTActionType.OSSplitColumn:
                    return new OSSplitColumn(ftdt);
                case EnumDTActionType.OSConcatenateFields:
                    return new OSConcatenateFields(ftdt);
                case EnumDTActionType.OSAddProcessInfo:
                    return new OSAddProcessInfo(ftdt);
                case EnumDTActionType.OSGetSumOfColumns:
                    return new OSGetSumOfColumns(ftdt);
                case EnumDTActionType.OSExtractLinkedCasesAfterGroupBy:
                    return new OSExtractLinkedCaseAfterGroupBy(ftdt);
                case EnumDTActionType.OSSplitNameColumnIntoThree:
                    return new OSSplitNameColumnIntoThree(ftdt);
                case EnumDTActionType.GetDataFromDB:
                    return new GetDataFromDB(ftdt);
                case EnumDTActionType.XMappingDataFromDB:
                    return new XMappingDataFromDB(ftdt);
                case EnumDTActionType.GetTotalOfAColumn:
                    return new GetTotalOfAColumn(ftdt);
                case EnumDTActionType.SplitOutputAfterGrouping:
                    return new SplitOutputAfterGrouping(ftdt);
                case EnumDTActionType.SplitOneColIntoMultipleCols:
                    return new SplitOneColIntoMultipleCols(ftdt);
                case EnumDTActionType.OSTransformDefaulterAddresses:
                    return new OSTransformDefaulterAddresses(ftdt);
                case EnumDTActionType.OSNCTransformDefaulterAddresses:
                    return new OSNCTransformDefaulterAddresses(ftdt);
                case EnumDTActionType.SetDuplicatedRowsWithUniqueValues:
                    return new SetDuplicatedRowsWithUniqueValues(ftdt);
                case EnumDTActionType.TruncateFieldsExceedingMaxLength:
                    return new TruncateFieldsExceedingMaxLength(ftdt);
                case EnumDTActionType.SplitFullAddressColIntoSepLines:
                    return new SplitFullAddressColIntoSepLines(ftdt);
                case EnumDTActionType.ComputeAggregateFunction:
                    return new ComputeAggregateFunction(ftdt);
                case EnumDTActionType.OSAssignDefaultPostcode:
                    return new OSAssignDefaultPostcode(ftdt);
                case EnumDTActionType.SplitAndReshapeOutputFiles:
                    return new SplitAndReshapeOutputFiles(ftdt);
                case EnumDTActionType.ExcludeRowsNotMatchReferenceList:
                    return new ExcludeRowsNotMatchReferenceList(ftdt);
                case EnumDTActionType.ConcatenateMultiNodesDFs:
                    return new ConcatenateMultiNodesDFs(ftdt);
                case EnumDTActionType.SplitAndTransposeOutputFile:
                    return new SplitAndTransposeOutputFile(ftdt);
                case EnumDTActionType.ConcatenateXMLListItemsDFs:
                    return new ConcatenateXMLListItemsDFs(ftdt);
                default:
                    throw new NotSupportedException();
            }
        }


        protected void AppendExcludedRows(DataRow row, DataTable excludedDT)
        {

            if (GroupByFields != null)
            {

                string filter = string.Empty;
                foreach (var item in GroupByFields)
                {
                    filter = filter + item + "='" + row[item] + "' AND ";
                }
                filter = filter.Trim().Remove(filter.LastIndexOf("AND"));
                List<DataRow> rows = ParentFTDT.FileTemplate.BUDataTable.Select(filter).ToList();
                foreach (var rw in rows)
                {
                    excludedDT.ImportRow(rw);
                }

            }
            else
            {
                excludedDT.ImportRow(row);
            }

        }

        public void AppendError()
        {
            ParentFTDT.FileTemplate.ParentDataFile.ValidationSummary.AppendLine(ErrorDescription + " Please consult system administrator for further assistance.");
        }

        protected void UpdateColumn(DataTable dt, string colName, object val)
        {
            foreach (DataRow rw in dt.Rows)
            {
                rw[colName] = val;
            }

        }

    }
}