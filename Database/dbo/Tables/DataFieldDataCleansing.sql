﻿CREATE TABLE [dbo].[DataFieldDataCleansing] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [DataFieldId] INT           NOT NULL,
    [DCId]        INT           NOT NULL,
    [OldText]     NVARCHAR (50) NULL,
    [NewText]     NVARCHAR (50) NULL,
    CONSTRAINT [PK_DataFieldDataCleasing] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DataFieldDataCleansing_DataField] FOREIGN KEY ([DataFieldId]) REFERENCES [dbo].[DataField] ([Id])
);

