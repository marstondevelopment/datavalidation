﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class ARPrmsGetTotalBalanceOfNewCases : ARPrmsBase
    {
        public ARPrmsGetTotalBalanceOfNewCases() { }
        public ARPrmsGetTotalBalanceOfNewCases(ARAction ar) : base(ar) { }

        [Display(Name = "Aggre. Column Name")]
        public string ColumnName { get; set; }

        [Display(Name = "Output Format")]
        public string OutputFormat { get; set; }
    }
}