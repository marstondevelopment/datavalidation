﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.ViewModel
{
    public class UserViewModel
    {
        private UserRepository _ur;
        private ClientRepository _cr;
        public UserViewModel(UserRepository ur, ClientRepository cr, User u)
        {
            _ur = ur;
            _cr = cr;
            User = u;
        }
        public User User { get; set; }

        public IEnumerable<User> Users { get; set; }

        public List<SelectListItem> DomainsList { get; set; }

        public List<SelectListItem> GetDomains
        {
            get
            {
                var domains = new List<SelectListItem>()
                {
                    new SelectListItem { Text = "MGL" , Value = (EnumDomain.MGL).ToString() },
                    new SelectListItem { Text = "ITSERVICES", Value = (EnumDomain.ITSERVICES).ToString() }
                };
                return domains;
            }
        }

        private List<SelectListItem> selectClientList = null;

        public List<SelectListItem> AllClients
        {
            get
            {
                if(selectClientList == null)
                {
                    selectClientList = new List<SelectListItem>();
                    foreach(Client c in _cr.GetAll().Where(x=> x.Disabled == false).OrderBy(x => x.Name))
                    {
                        selectClientList.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
                    }
                }
                return selectClientList;
            }
        }


    }
}