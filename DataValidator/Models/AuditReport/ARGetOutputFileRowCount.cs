﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetOutputFileRowCount : ARStep
    {
        private ARPrmsGetOutputFileRowCount prms;
        private List<string> fileNamePrefixs;

        public ARGetOutputFileRowCount(FileTemplateARAction ftar) : base(ftar)
        {
            prms = ftar.ARAction.ARPrmsGetOutputFileRowCount;
            if (prms != null && !string.IsNullOrEmpty(prms.FileNamePrefixs))
                fileNamePrefixs = prms.FileNamePrefixs.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();

        }
        public override bool Execute(ref string arScript)
        {
            int rowCount = 0;
            var outputFiles = ParentFTAR.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output && (fileNamePrefixs == null || fileNamePrefixs.Contains(x.FileNamePrefix)));
            foreach (OutputFile item in outputFiles)
            {
                rowCount = rowCount + item.Data.Rows.Count;
            }
            
            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, rowCount.ToString());
            return true;
        }
    }
}