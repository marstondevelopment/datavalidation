﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class ExcludeRowsNotMatchReferenceList : DTStep
    {
        private PrmsExcludeRowsNotMatchReferenceList prms;
        private List<string> inputColumnNames;
        public ExcludeRowsNotMatchReferenceList(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsExcludeRowsNotMatchReferenceList;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsExcludeRowsNotMatchReferenceList parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            if (!prms.ReferenceListId.HasValue)
            {
                throw new MemberAccessException("ReferenceList missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            if (string.IsNullOrEmpty(prms.InputColumnNames) )
            {
                throw new MemberAccessException("InputColumnNames missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

            if  ((!string.IsNullOrEmpty(prms.FromColumn) && string.IsNullOrEmpty(prms.ToColumn)) || (string.IsNullOrEmpty(prms.FromColumn) && !string.IsNullOrEmpty(prms.ToColumn)))
            {
                throw new MemberAccessException("FromColumn and ToColumn missing in " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

            inputColumnNames = prms.InputColumnNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();
            List<string> wildcardCNs = new List<string>(); 
            foreach (var cn in inputColumnNames.Where(x=>x.Trim().EndsWith("*")))
            {
                wildcardCNs.AddRange(Utility.GetColumns(cn, ParentFTDT.FileTemplate.ProcessedDataTable)); 
            }

            inputColumnNames.RemoveAll(x => x.Trim().EndsWith("*"));
            inputColumnNames.AddRange(wildcardCNs);
        }


        public override bool Execute()
        {
            ExecuteDTRule(ParentFTDT.FileTemplate.ProcessedDataTable);
            return true;
        }

        private void ExecuteDTRule(DataTable dt)
        {
            var rows = dt.Select(prms.Filter);
            List<DataRow> invalidRows = new List<DataRow>();
            foreach (var rw in rows)
            {
                bool match;
                if (prms.CheckMatchedRows)
                {
                    match = false;
                    foreach (var cn in inputColumnNames)
                    {
                        if (!string.IsNullOrEmpty(rw[cn].ToString()))
                        {
                            if (prms.ReferenceList.ReferenceListItems.Where(x => x.Value.Trim().ToUpper() == rw[cn].ToString().Trim().ToUpper()).FirstOrDefault() != null)
                            {
                                match = true;
                                break;
                            }
                        }

                    }
                }
                else
                {
                    match = true;
                    foreach (var cn in inputColumnNames)
                    {
                        if (!string.IsNullOrEmpty(rw[cn].ToString()))
                        {
                            if (prms.ReferenceList.ReferenceListItems.Where(x => x.Value.Trim().ToUpper() == rw[cn].ToString().Trim().ToUpper()).FirstOrDefault() == null)
                            {
                                match = false;
                                break;
                            }
                        }

                    }
                }
                

                if ((!match && !prms.CheckMatchedRows) || (match && prms.CheckMatchedRows))
                {
                    if (!string.IsNullOrEmpty(prms.FromColumn) && !string.IsNullOrEmpty(prms.ToColumn))
                    {
                        rw[prms.ToColumn] = rw[prms.FromColumn];
                    }

                    if (!prms.KeepInvalidRow)
                    {
                        invalidRows.Add(rw);
                    }

                }
            }

            if (!prms.KeepInvalidRow && invalidRows.Count>0)
            {
                OutputFile outputFile = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(prms.FNPrefix.Trim(), EnumOutputFileCategory.Ignored);
                for (int j = 0; j < invalidRows.Count; j++)
                {
                    outputFile.Data.ImportRow(invalidRows[j]);
                    dt.Rows.Remove(invalidRows[j]);
                }

                if (!string.IsNullOrEmpty( prms.ExclusionReason))
                {
                    outputFile.DTActionMessage = prms.ExclusionReason;

                }

                dt.AcceptChanges();
                outputFile.Data.AcceptChanges();
            }

        }
    }
}