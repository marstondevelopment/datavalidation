﻿using Mgl.DataValidator;
using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Security.Authorize
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DisabledAuthorizeUserAttribute: AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);

            if (!isAuthorized) return false;

            User currentUser = (User) HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
            if (currentUser.Disabled)
            {
                httpContext.Response.Redirect("/Error/UserDisabled");
            }

            return true;
        }
    }
}