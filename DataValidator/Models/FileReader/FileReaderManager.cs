﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace Mgl.DataValidator.Models
{
    public class FileReaderManager
    {
        private FileTemplateRepository _ftr;
        private FileParser _fp;
        public EventLog EventLog { get; set; }
        public FileReaderManager(FileTemplateRepository ftr)
        {
            _ftr = ftr;
        }

        public FileReaderManager(FileTemplateRepository ftr, FileParser fileParser)
        {
            _ftr = ftr;
            _fp = fileParser;
            EventLog = new EventLog()
            {
                FileTemplateId = _fp.FileTemplate.Id,
                IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress,
                ProcessStartedOn = DateTime.Now,
                Action = (int)_fp.UserAction,
                FileName = _fp.FileName
            };
        }



        private void LogEvent(EventLog eventLog, string note, bool succeed, int? userAction)
        {
            eventLog.Note = note;
            eventLog.Succeeded = succeed;
            eventLog.ProcessEndedOn = DateTime.Now;
            if (userAction.HasValue)
            {
                eventLog.Action = userAction;
            }
            _ftr.UpdateEventLog(eventLog);
        }

        public EnumParserResult ProcessFile()
        {

            if (_fp.Validate())
            {
                if (_fp.FileTemplate.BUDataTable != null && _fp.FileTemplate.BUDataTable.Rows.Count == 0)
                {
                    LogEvent(EventLog, _fp.FileName + " is empty. Process terminated.", true, null);
                    return EnumParserResult.NoData;
                }
                if (_fp.UserAction == EnumDataFileScreen.Upload)
                {
                    _fp.FileTemplate.NoInputRows = _fp.FileTemplate.BUDataTable.Rows.Count;
                    if (_fp.FileTemplate.BulkUpload && !string.IsNullOrEmpty(_fp.FileTemplate.BUStagingTable))
                    {
                        // For bulk upload the process should merge DTA stages and execute it in one go as there is no output file.
                        if (!_fp.FileTemplate.ExecuteAllDTActions())
                        {
                                LogEvent(EventLog, "Failed to complete DTACtions due to technical issue.", false, null);
                            return EnumParserResult.DTACtionsFailed;
                        }

                        if (_fp.FileTemplate.DataProvider.UploadData())
                        {
                            if (_fp.FileTemplate.GenerateAuditFile)
                            {

                                if (!_fp.FileTemplate.FileWriter.GenerateFiles())
                                {
                                    LogEvent(EventLog, "Failed to generate output files due to technical issue.", false, null);
                                    return EnumParserResult.UploadFailed;
                                }


                            }

                            LogEvent(EventLog, "Number of row uploaded to " +  _fp.FileTemplate.BUStagingTable + ": " + (_fp.FileTemplate.UseProcessedTbl? _fp.FileTemplate.ProcessedDataTable.Rows.Count.ToString():_fp.FileTemplate.BUDataTable.Rows.Count.ToString()), true, null);
                            return EnumParserResult.UploadSucceed;
                        }
                        else
                        {
                            LogEvent(EventLog, "Failed to upload data due to technical issue.", false, null);
                            return EnumParserResult.UploadFailed;
                        }
                    }
                    else
                    {
                        LogEvent(EventLog, "Configuration has not been setup correctly to initiate the bulk copy process.", false, null);
                        return EnumParserResult.ConfigNotSet;
                    }


                }
                else
                {
                    // The parser is set to validate data only, so return true for successful
                    LogEvent(EventLog, "Data validation successful.", true, null);
                    return EnumParserResult.ValiationSucceed;
                }

            }
            else
            {
                LogEvent(EventLog, _fp.ValidationSummary.ToString(), false, null);
                return EnumParserResult.ValidationFailed;
            }

        }

        public EnumParserResult DEUpload(FileTemplate ft)
        {

            if (_fp.Validate())
            {
                if (_fp.FileTemplate.BUDataTable != null && _fp.FileTemplate.BUDataTable.Rows.Count == 0)
                {
                    LogEvent(EventLog, "No staging data for uploading. Process terminated.", true, null);
                    return EnumParserResult.NoData;
                }
                if (_fp.UserAction == EnumDataFileScreen.DEUpload)
                {
                    _fp.FileTemplate.NoInputRows = _fp.FileTemplate.BUDataTable.Rows.Count;
                    if (_fp.FileTemplate.DataEntry && !string.IsNullOrEmpty(_fp.FileTemplate.DEStagingTable) && _fp.FileTemplate.DataEntryFormId.HasValue)
                    {
                        // For bulk upload the process should merge DTA stages and execute it in one go as there is no output file.
                        if (!_fp.FileTemplate.ExecuteAllDTActions())
                        {
                            LogEvent(EventLog, "Failed to complete DTACtions due to technical issue.", false, null);
                            return EnumParserResult.DTACtionsFailed;
                        }

                        if (_fp.FileTemplate.DataProvider.UploadData())
                        {
                            if (_fp.FileTemplate.GenerateAuditFile)
                            {

                                if (!_fp.FileTemplate.FileWriter.GenerateFiles())
                                {
                                    LogEvent(EventLog, "Failed to generate output files due to technical issue.", false, null);
                                    return EnumParserResult.UploadFailed;
                                }


                            }

                            LogEvent(EventLog, "Number of row uploaded to " + _fp.FileTemplate.BUStagingTable + ": " + (_fp.FileTemplate.UseProcessedTbl ? _fp.FileTemplate.ProcessedDataTable.Rows.Count.ToString() : _fp.FileTemplate.BUDataTable.Rows.Count.ToString()), true, null);
                            return EnumParserResult.UploadSucceed;
                        }
                        else
                        {
                            LogEvent(EventLog, "Failed to upload data due to technical issue.", false, null);
                            return EnumParserResult.UploadFailed;
                        }
                    }
                    else
                    {
                        LogEvent(EventLog, "Configuration has not been setup correctly to initiate the bulk copy process.", false, null);
                        return EnumParserResult.ConfigNotSet;
                    }


                }
                else
                {
                    // The parser is set to validate data only, so return true for successful
                    LogEvent(EventLog, "Data validation successful.", true, null);
                    return EnumParserResult.ValiationSucceed;
                }

            }
            else
            {
                LogEvent(EventLog, _fp.ValidationSummary.ToString(), false, null);
                return EnumParserResult.ValidationFailed;
            }

        }

        public bool TransformData(FileTemplate fileTemplate)
        {
            EventLog = new EventLog()
            {
                FileTemplateId = fileTemplate.Id,
                IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress,
                ProcessStartedOn = DateTime.Now,
            };

            bool ret = true;
            string msg = string.Empty;
            if (fileTemplate.DataProvider.TransformData())
            {
                msg = fileTemplate.StoredProcedure.SPName + " executed successfully";

            }
            else
            {
                ret = false;
                msg = fileTemplate.StoredProcedure.SPName + " executed unsuccessfully. See application error log for further details.";
            }

            LogEvent(EventLog, msg, ret, (int)EnumDataFileScreen.Transform);
            return ret;
        }

        public EnumParserResult GenerateOutputFiles()
        {

            if (!_fp.Validate())
            {
                LogEvent(EventLog, _fp.ValidationSummary.ToString(), false, null);
                return EnumParserResult.ValidationFailed;
            }

            if (_fp.FileTemplate.BUDataTable.Rows.Count == 0)
            {
                LogEvent(EventLog, _fp.FileName + " is empty. Process terminated.", true, null);
                return EnumParserResult.NoData;
            }

            _fp.FileTemplate.NoInputRows = _fp.FileTemplate.BUDataTable.Rows.Count;


            //if (_fp.UserAction != EnumDataFileScreen.GenerateFiles)
            //{
            //    // The parser is set to validate data only, so return true for successful
            //    LogEvent(EventLog, "Data validation successful.", true, null);
            //    return EnumParserResult.ValiationSucceed;
            //}

            if (!_fp.FileTemplate.GenerateOutputFile && !_fp.FileTemplate.DataEntry)
            {
                LogEvent(EventLog, "Configuration has not been setup correctly to initiate the output files generation process.", false, null);
                return EnumParserResult.ConfigNotSet;
            }
            if (_fp.FileTemplate.MergeDTAStages)
            {
                if (!_fp.FileTemplate.ExecuteAllDTActions())
                {
                    LogEvent(EventLog, "Failed to complete DTACtions due to technical issue.", false, null);
                    return EnumParserResult.DTACtionsFailed;
                }
            }
            else
            {
                if (!_fp.FileTemplate.ExecuteDTActions())
                {
                    LogEvent(EventLog, "Failed to complete DTACtions due to technical issue.", false, null);
                    return EnumParserResult.DTACtionsFailed;
                }

                if (!_fp.FileTemplate.ExecutePreGenActions())
                {
                    LogEvent(EventLog, "Failed to complete PreGenActions due to technical issue.", false, null);
                    return EnumParserResult.PreGenActionsFailed;
                }
            }



            if (_fp.FileTemplate.FileWriter.GenerateFiles())
            {
                LogEvent(EventLog, "Number of row selected for output file: " + _fp.FileTemplate.ProcessedDataTable.Rows.Count.ToString(), true, null);
                return EnumParserResult.FileGenerationSucceed;
            }
            else
            {
                LogEvent(EventLog, "Failed to generate output files due to technical issue.", false, null);
                return EnumParserResult.FileGenerationFailed;
            }

        }

        public EnumParserResult UploadFilesToRepository()
        {
            EnumParserResult genFileStatus = GenerateOutputFiles();
            if (genFileStatus != EnumParserResult.FileGenerationSucceed)
                return genFileStatus;

            if (!_fp.FileTemplate.UploadToFileRepository)
            {
                LogEvent(EventLog, "Configuration has not been setup correctly to upload files to server repository.", false, null);
                return EnumParserResult.ConfigNotSet;
            }

            DirectoryInfo dirInfo = new DirectoryInfo(_fp.FileTemplate.RepositoryFilePath);
            if (!dirInfo.Exists)
                return EnumParserResult.RepositoryNotFound;

            var oFiles = _fp.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output).ToList();

            if (_fp.FileTemplate.FNPAsSubfolder)
            {
                var subDirs = dirInfo.GetDirectories();
                var disNames = subDirs.Select(x => x.Name).ToList();
                bool failed = false;
                _fp.ValidationSummary.Clear();

                foreach (var oFile in oFiles)
                {
                    if (!disNames.Contains(oFile.FileNamePrefix))
                    {
                        _fp.ValidationSummary.AppendLine(_fp.FileTemplate.RepositoryFilePath + "\\" + oFile.FileNamePrefix + "<br/>");
                        LogEvent(EventLog, "Failed to find subfolder " + oFile.FileNamePrefix + " in file repository,  " + _fp.FileTemplate.RepositoryFilePath, false, null);
                        failed = true;
                        //return EnumParserResult.SubfolderNotFound;
                    }
                }

                if (failed)
                    return EnumParserResult.SubfolderNotFound;
            }

            foreach (var oFile in oFiles)
            { 
                CreateFile(oFile);
            }

            return EnumParserResult.UploadToRepositorySucceed;
        }

        private bool CreateFile( OutputFile oFile)
        {
            string fullPath = oFile.FileWriter.FileTemplate.RepositoryFilePath.TrimEnd(new[] { '/', '\\' });
            if (oFile.FileWriter.FileTemplate.FNPAsSubfolder)
                fullPath = fullPath + "\\" + oFile.FileNamePrefix;
            
            //remove suffix from file name
            fullPath = fullPath + "\\" + oFile.FileName.Replace("_preprocess", string.Empty);

            // Create the file.
            Stream ms = oFile.Generate();

            using (var fileStream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
            {
                ms.Position = 0;
                ms.CopyTo(fileStream);
            }

            return true;
        }
    }
}