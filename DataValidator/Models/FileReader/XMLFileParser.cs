﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.XPath;
using Mgl.DataValidator.Models;

namespace Mgl.DataValidator.Models
{
    public class XMLFileParser : FileParser
    {
        public XMLFileParser(FileTemplate ft, Stream fs, string fileName, EnumDataFileScreen userAction) : base(ft, fs, fileName, userAction)
        {
            //User StreamReader to prevent file loading issue due to incompatible file encoding.
            StreamReader = new StreamReader(fs);
        }    

        private StreamReader StreamReader { get; set; }

        private void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    ValidationSummary.AppendLine("XSD Validation Error: " + e.Message);
                    break;
                    //case XmlSeverityType.Warning:
                    //    Console.WriteLine("Warning {0}", e.Message);
                    //    break;
            }

        }

        public override bool Validate()
        {
            bool valid = true;

            try
            {
                using (StreamReader)
                {
                    if (FileTemplate.UseXsd)
                        valid = ProcessXsdXml();
                    else
                        valid = ProcessNonXsdXml();
                }

            }
            catch (Exception ex)
            {
                if (ex is MissingFieldException)
                {
                    ValidationSummary.Clear();
                    ValidationSummary.AppendLine(ex.Message);
                    return false;
                }
                else
                    throw;
            }


            return valid;
        }

        private bool ProcessNonXsdXml()
        {
            bool valid = true;
            XmlDocument xdoc = new XmlDocument();
            try
            {
                xdoc.Load(StreamReader);
            }
            catch (Exception ex)
            {
                logger.Error("ProcessNonXsdXml failed", ex);
                ValidationSummary.Clear();
                ValidationSummary.AppendLine("The system failed to load the xml document. The file can be empty, or its data format is invalid.");

                return false;
            }

            XmlElement root = xdoc.DocumentElement;
            XmlNodeList rootNodes;

            // This section is for Variables
            foreach (DataField item in FileTemplate.Variables.Where(x => !string.IsNullOrEmpty(x.XMLChildNodeXPath) && x.OutputFieldOnly != true).ToList())
            {
                XmlNode node = root.SelectSingleNode("//" + item.XMLChildNodeXPath.Trim().Trim("/".ToCharArray()));
                if (node != null)
                {
                    item.OriginalValue = node.InnerText;
                }
            }

            //This section is for Header
                foreach (DataField item in FileTemplate.HeaderCols.Where(x => x.OutputFieldOnly != true).ToList())
            {
                if (item.OutputFieldOnly == false && !string.IsNullOrEmpty(item.XMLChildNodeXPath))
                {
                    XmlNode node = root.SelectSingleNode("//" + item.XMLChildNodeXPath.Trim().Trim("/".ToCharArray()));
                    if (node != null)
                    {
                        item.OriginalValue = node.InnerText;
                    }

                }

            }

            //Footer
            foreach (DataField item in FileTemplate.TrailerCols.Where(x => x.OutputFieldOnly != true).ToList())
            {
                XmlNode node = root.SelectSingleNode("//" + item.XMLChildNodeXPath.Trim().Trim("/".ToCharArray()));
                if (node != null)
                {
                    item.OriginalValue = node.InnerText;
                }
            }

            // Body

            rootNodes = root.SelectNodes("//" + FileTemplate.XMLRootNodeXPath.Trim().Trim("/".ToCharArray()));

            if (rootNodes != null)
            {
                int rowIndex = 0;
                foreach (XmlNode item in rootNodes)
                {
                    FileTemplate.ResetFieldValues();
                    FileTemplate.RowId = rowIndex + 1;

                    try
                    {
                        foreach (DataField col in FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList())
                        {
                            string xPath = string.IsNullOrEmpty(col.XMLChildNodeXPath) ? col.Name : col.XMLChildNodeXPath;
                            XmlNode node = item.SelectSingleNode(xPath.Trim().Trim("/".ToCharArray()));
                            if (node != null)
                            {
                                if (col.XMLListElementId.HasValue && ((EnumXmlListElement)col.XMLListElementId.Value) == EnumXmlListElement.XMLList   && node.HasChildNodes)
                                {
                                    ExtractChildNodes(node, col, string.Empty);
                                }
                                else
                                {
                                    col.OriginalValue = node.InnerText;
                                    if (col.HasMultipleNodes)
                                    {
                                        XmlNodeList nodes = item.SelectNodes(xPath.Trim().Trim("/".ToCharArray()));
                                        foreach (XmlNode nodeItem in nodes)
                                        {
                                            col.OriginalValues.Add(nodeItem.InnerText);
                                        }
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error occurred during parsing data file at row " + FileTemplate.RowId.ToString() + ": " + ex.Message + " \n Stack Trace: " + ex.StackTrace);
                    }

                    //Validate each row
                    if (!FileTemplate.ValidateRows())
                    {
                        if (FileTemplate.TerminateProcess)
                            return false;
                        valid = false;
                    }
                }
            }
            else
            {
                throw new Exception("Failed to find specified xml root node (" + FileTemplate.XMLRootNodeXPath + ")" + ", make sure the configuration for XML file is correct, and try again.");
            }

            return valid;
        }

        private void ExtractChildNodes(XmlNode parentNode, DataField parentCol, string cascadedPrefix)
        {
            if (parentNode.HasChildNodes)
            {
                for (int i = 0; i < parentNode.ChildNodes.Count; i++)
                {
                    var childNode = parentNode.ChildNodes[i];
                    DataField df = parentCol.ChildNodesColumns.Where(x => x.XMLChildNodeXPath == childNode.Name).FirstOrDefault();
                    if (df != null)
                    {
                        if (df.XMLListElementId.HasValue && ((EnumXmlListElement)df.XMLListElementId.Value) == EnumXmlListElement.XMLListItem && childNode.HasChildNodes)
                        {
                            string parentPrefix = (cascadedPrefix +  "_" + df.Name + (i + 1).ToString("00")).Trim("_".ToCharArray());

                            ExtractChildNodes(childNode, df, parentPrefix);
                        }
                        else if (df.XMLListElementId.HasValue && (((EnumXmlListElement)df.XMLListElementId.Value) == EnumXmlListElement.XMLGroupNode || ((EnumXmlListElement)df.XMLListElementId.Value) == EnumXmlListElement.XMLList) && childNode.HasChildNodes && childNode.ChildNodes.Count > 1)
                        {
                            string parentPrefix = cascadedPrefix;
                            ExtractChildNodes(childNode, df, parentPrefix);
                        }
                        else
                        {
                            string derivedColName = cascadedPrefix + "_" + df.Name;
                            if (df.DerivedSubNodes == null)
                                df.DerivedSubNodes = new Dictionary<string, string>();
                            try
                            {
                                df.DerivedSubNodes.Add(derivedColName, childNode.InnerText);
                            }
                            catch (Exception ex)
                            {

                                throw new Exception("Failed to add sub nodes to dictionary DerivedSubNodes for " + derivedColName + ":" + childNode.InnerText, ex);
                            }

                        }
                    }
                }
                
            }
        }


        private bool ProcessXsdXml()
        {
            bool valid = true;
            XmlDocument xdoc = new XmlDocument();
            string xmlNs = null;
            try
            {
                string xsdFullPath = (ConfigurationManager.AppSettings["XSDFilePath"]).TrimEnd("\\".ToCharArray()) + "\\" + FileTemplate.XsdFile.LocalFileName;

                XmlReaderSettings settings = new XmlReaderSettings();
                 xmlNs = string.IsNullOrEmpty(FileTemplate.XsdFile.XmlNameSpace) ? null : FileTemplate.XsdFile.XmlNameSpace;
                
                settings.Schemas.Add(xmlNs, xsdFullPath);
                settings.ValidationType = ValidationType.Schema;
                XmlReader reader = XmlReader.Create(StreamReader, settings);
                xdoc.Load(reader);
                ValidationEventHandler eventHandler = new ValidationEventHandler(ValidationEventHandler);
                // the following call to Validate succeeds.
                ValidationSummary.Clear();
                xdoc.Validate(eventHandler);
                if (ValidationSummary.Length > 0)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Error("ProcessXsdXml failed.", ex);
                ValidationSummary.Clear();
                if (ex is XmlSchemaValidationException)
                {
                    ValidationSummary.AppendLine("XSD Validation Error: " + ex.Message);
                }
                else
                {
                    ValidationSummary.AppendLine("The system failed to load the xml document. The file can be empty, or its data format is invalid.");
                }

                return false;
            }

            if (string.IsNullOrEmpty(xmlNs))
            {
                xdoc = null;
                //Reset file pointer so that next function can read the file.
                StreamReader.BaseStream.Position = 0;
                return ProcessNonXsdXml();
            }

            List<XElement> rootNodes = null;

            XElement doc = XElement.Parse(xdoc.InnerXml);

            // This section is for Variables
            foreach (DataField item in FileTemplate.Variables.Where(x => !string.IsNullOrEmpty(x.XMLChildNodeXPath) && x.OutputFieldOnly != true).ToList())
            {
                    string childNodeName = item.XMLChildNodeXPath.Trim().Trim("/".ToCharArray());
                    var node = doc.Elements().FirstOrDefault(x => x.Name.LocalName.Equals(childNodeName));
                    if (node != null)
                        item.OriginalValue = node.Value;
            }

            //This section is for Header
            foreach (DataField item in FileTemplate.HeaderCols.Where(x => x.OutputFieldOnly != true && !string.IsNullOrEmpty(x.XMLChildNodeXPath)).ToList())
            {
                    string childNodeName = item.XMLChildNodeXPath.Trim().Trim("/".ToCharArray());
                    var node = doc.Elements().FirstOrDefault(x => x.Name.LocalName.Equals(childNodeName));
                    if (node != null)
                        item.OriginalValue = node.Value;
            }

            //Footer
            foreach (DataField item in FileTemplate.TrailerCols.Where(x => x.OutputFieldOnly != true).ToList())
            {
                string childNodeName = item.XMLChildNodeXPath.Trim().Trim("/".ToCharArray());
                var node = doc.Elements().FirstOrDefault(x => x.Name.LocalName.Equals(childNodeName));
                if (node != null)
                    item.OriginalValue = node.Value;
            }

            // Body
            string rootNodeName = FileTemplate.XMLRootNodeXPath.Trim().Trim("/".ToCharArray());
            rootNodes = doc.Elements().Where(x => x.Name.LocalName.Equals(rootNodeName)).ToList();

            if (rootNodes != null)
            {
                int rowIndex = 0;
                foreach (XElement item in rootNodes)
                {
                    FileTemplate.ResetFieldValues();
                    FileTemplate.RowId = rowIndex + 1;
                    try
                    {
                        foreach (DataField col in FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList())
                        {
                            string childNodeName = (string.IsNullOrEmpty(col.XMLChildNodeXPath) ? col.Name : col.XMLChildNodeXPath).Trim().Trim("/".ToCharArray());
                            var node = item.Descendants().Where(x => x.Name.LocalName.Equals(childNodeName)).FirstOrDefault();

                            if (node != null)
                            {
                                col.OriginalValue = node.Value;
                                if (col.HasMultipleNodes)
                                {
                                    List<XElement> childNodes = item.Descendants().Where(x => x.Name.LocalName.Equals(col.XMLChildNodeXPath)).ToList();
                                    foreach (XElement nodeItem in childNodes)
                                    {
                                        col.OriginalValues.Add(nodeItem.Value);
                                    }
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error occurred during parsing data file at row " + FileTemplate.RowId.ToString() + ": " + ex.Message + " \n Stack Trace: " + ex.StackTrace);
                    }

                    //Validate each row
                    if (!FileTemplate.ValidateRows())
                    {
                        if (FileTemplate.TerminateProcess)
                            return false;
                        valid = false;
                    }
                }
            }
            else
            {
                throw new Exception("Failed to find specified xml root node (" + FileTemplate.XMLRootNodeXPath + ")" + ", make sure the configuration for XML file is correct, and try again.");
            }

            return valid;
        }


    }
}