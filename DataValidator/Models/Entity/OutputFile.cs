﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class OutputFile
    {

        public string FileNamePrefix;
        public EnumOutputFileCategory FileCategory;
        public DataTable Data;
        public DataTable Header;
        public DataTable Trailer;
        public string Context;
        public FileDelimiter FileDelimiter;
        public string DTActionMessage;
        private FileWriter fileWriter;
  
        private string fileName;

        public OutputFile(FileWriter fw)
        {
            fileWriter = fw;
        }


        public string FileName
        {
            get
            {
                if (string.IsNullOrEmpty(fileName))
                {
                    FileNamePrefix = FileNamePrefix.RenderTag("OFN", this.fileWriter.FileTemplate.ParentDataFile.FileNameNoExt);
                    FileNamePrefix = FileNamePrefix.RenderTag("TS", DateTime.Now.ToString("ddMMyyyyHHmmss"));

                    fileName = FileNamePrefix;
                    if (FileWriter.FileTemplate.FNTimestamp)
                        fileName = fileName + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss");

                    switch (FileCategory)
                    {
                        case EnumOutputFileCategory.Output:
                            fileName = fileName + "_preprocess." + (FileDelimiter != null ? FileDelimiter.FileExtenstion : "csv");
                            break;
                        case EnumOutputFileCategory.Ignored:
                            fileName = fileName + "_ignored." + (FileDelimiter != null ? FileDelimiter.FileExtenstion : "csv");
                            break;
                        case EnumOutputFileCategory.Exception:
                            fileName = fileName + "_exception.txt";
                            break;
                        case EnumOutputFileCategory.Report:
                            if (!string.IsNullOrEmpty(FileWriter.FileTemplate.ARFileNamePrefix))
                            {
                                fileName = this.fileWriter.FileTemplate.ARFileNamePrefix.RenderTag("OFN", this.fileWriter.FileTemplate.ParentDataFile.FileNameNoExt);
                                fileName = fileName.RenderTag("TS", DateTime.Now.ToString("yyyyMMddHHmmss"));
                            }
                            fileName = fileName + (FileWriter.FileTemplate.ARFileNameNoSuffix == true ? ".":"_audit.") + (fileWriter.FileTemplate.AuditFileExtenstion.HasValue ? ((EnumARMimeType)fileWriter.FileTemplate.AuditFileExtenstion.Value).ToString() : "txt");
                            break;
                        default:
                            break;
                    }

                }

                fileName = ZipEntry.CleanName(fileName);

                return fileName;
            }
        }

        public FileWriter FileWriter { get => fileWriter; set => fileWriter = value; }

        public Stream Generate()
        {

            switch (FileCategory)
            {
                case EnumOutputFileCategory.Output:
                    if (FileWriter.FileTemplate.OutputFileDelimiter.Delimiter == "XML")
                    {
                        Context = Data.ToXml(fileWriter.FileTemplate.BUDataTable, "ReferredRecords", "ReferredRecord");
                        
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        if (Header != null)
                        {
                            sb.AppendLine(Header.ToCsv(FileDelimiter.Delimiter, fileWriter.FileTemplate.HeaderDataTable, !fileWriter.FileTemplate.OFExcludeColumnHeader));
                        }

                        sb.AppendLine(Data.ToCsv(FileDelimiter.Delimiter, fileWriter.FileTemplate.BUDataTable, !fileWriter.FileTemplate.OFExcludeColumnHeader));

                        if (Trailer != null)
                        {
                            sb.AppendLine(Trailer.ToCsv(FileDelimiter.Delimiter, fileWriter.FileTemplate.TrailerDataTable, !fileWriter.FileTemplate.OFExcludeColumnHeader));
                        }

                        string retVal = sb.ToString();
                        if (retVal.Length >= 2)
                            //To remove the extra new line characters (\r\n) in the output file
                            retVal = retVal.Substring(0, retVal.Length - Environment.NewLine.Length);
                        Context = retVal;
                    }

                    break;
                case EnumOutputFileCategory.Ignored:
                    if (FileWriter.FileTemplate.OutputFileDelimiter.Delimiter == "XML")
                    {
                        Context = Data.ToXml(fileWriter.FileTemplate.BUDataTable, "ReferredRecords", "ReferredRecord");
                    }
                    else
                    {
                        Context = Data.ToCsv(FileDelimiter.Delimiter, !fileWriter.FileTemplate.OFExcludeColumnHeader);
                    }
                    break;
                case EnumOutputFileCategory.Report:
                    break;
                default:
                    break;
            }

            byte[] binData = null;
            switch ((EnumOutputFileEncoding)(fileWriter.FileTemplate.OutputFileEncoding ?? (int)EnumOutputFileEncoding.UTF8))
            {
                case EnumOutputFileEncoding.UTF8:
                    UTF8Encoding uTF8Encoding = new UTF8Encoding();
                    // Create the data to write to the stream.
                    binData = uTF8Encoding.GetBytes(Context);
                    break;
                case EnumOutputFileEncoding.Unicode:
                    UnicodeEncoding uniEncoding = new UnicodeEncoding();
                    binData = uniEncoding.GetBytes(Context);
                    break;
                case EnumOutputFileEncoding.ASSCII:
                    ASCIIEncoding aSCIIEncoding = new ASCIIEncoding();
                    binData = aSCIIEncoding.GetBytes(Context);
                    break;
                case EnumOutputFileEncoding.UTF32:
                    UTF32Encoding uTF32Encoding = new UTF32Encoding();
                    binData = uTF32Encoding.GetBytes(Context);
                    break;
                case EnumOutputFileEncoding.UTF7:
                    UTF7Encoding uTF7Encoding = new UTF7Encoding();
                    binData = uTF7Encoding.GetBytes(Context);
                    break;
                default:
                    throw new NotImplementedException();
            }

            MemoryStream memStream = new MemoryStream(100);
                // Write the first string to the stream.
            memStream.Write(binData, 0, binData.Length);

            return memStream;
        }



    }
}