﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  Mgl.DataValidator.Models.ValidationRules;
using  Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class XsdFile
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [Display(Name = "Referenced By")]
        public string AssociatedFileTemplates
        {
            get
            {
                string fileNames = string.Empty;
                List<FileTemplate> fileTemplates = FileTemplates.Where(x => x.Disabled == false).ToList();
                string appPath = System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/FileTemplate/Details/";
                if (fileTemplates.Count > 0)
                {
                    foreach (var fileTemplate in fileTemplates)
                    {
                        fileNames = fileNames + "| <a href='" + appPath + fileTemplate.Id.ToString() + "'>" + fileTemplate.Name + "</a>";
                    }
                    fileNames = fileNames.Trim().Trim("|".ToCharArray());
                }
                return fileNames;
            }
        }

        public class Metadata
        {
            [Display(Name = "File Name")]
//            [Required(ErrorMessage = "RegEx required.")]
            public string OriginalFileName;

            [Display(Name = "Local File Name")]
            [Required(ErrorMessage = "Local File Name required.")]
            public string LocalFileName;

            [Display(Name = "XML Namespace")]
            public string XmlNameSpace;

        }
    }

}