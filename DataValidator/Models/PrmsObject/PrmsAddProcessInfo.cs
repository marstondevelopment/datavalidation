﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    public class PrmsAddProcessInfo: PrmsBase
    {
        public PrmsAddProcessInfo() { }
        public PrmsAddProcessInfo(DTAction dt) : base(dt) { }

        [Display(Name = "Process Info")]
        public int ProcessInfoId { get; set; }
        [JsonIgnore]
        public EnumDTInfoType ProcessInfo
        {
            get
            {
                return (EnumDTInfoType)ProcessInfoId;
            }
        }
        [Display(Name = "Field Name")]
        public string FieldName { get; set; }

    }
}