﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetTotalBalanceOfNewCases : ARStep
    {
        private OutputFile prms;
        public ARGetTotalBalanceOfNewCases(FileTemplateARAction ftar) : base(ftar)
        {
        }
        public override bool Execute(ref string arScript)
        {
            prms = ParentFTAR.FileTemplate.FileWriter.ProcessingFile;

            if ( prms == null)
                throw new MissingMemberException("ProcessingFile is missing");

            object totalBalance = prms.Data.Compute("Sum([" + this.ParentFTAR.ARAction.ARPrmsGetTotalBalanceOfNewCases.ColumnName +  "])", "");
            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, totalBalance.ObjToString(prms.Data.Columns[this.ParentFTAR.ARAction.ARPrmsGetTotalBalanceOfNewCases.ColumnName]));
            return true;
        }
    }
}