﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  Mgl.DataValidator.Models.ValidationRules;
using  Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class RegularExpression
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string DisplayDisabled
        {
            get { return !Disabled ? "Yes" : "No"; }
        }

        public class Metadata
        {
            [Display(Name = "RegEx")]
            [Required(ErrorMessage = "RegEx required.")]
            public string RegExText;
            [Display(Name = "Disabled")]
            public string DisplayDisabled;
        }
    }

}