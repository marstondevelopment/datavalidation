﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    //[Serializable]
    [MetadataType(typeof(Metadata))]
    public partial class Title
    {
        public class Metadata
        {
            [Required(ErrorMessage = "Title Name is required.")]
            public string Name;
        }
    }
}