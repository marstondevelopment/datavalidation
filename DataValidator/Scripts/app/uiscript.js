﻿
var uiscriptControls = (function () {

    this.uiscript = this.uiscript || {};
    var ui = uiscript;

    ui.displayMessage = function (divId, message, isError) {

        var div = $("#" + divId);
        var successClass = 'alert alert-success Msg';
        var errorClass = 'alert alert-warning Msg';
        div.removeClass();

        div.addClass((isError) ? errorClass : successClass);
        div.text(message);
        div.fadeToggle(400).delay(10000).fadeOut(400);
        return false;
    };

    ui.executeValidation = function (elementId, validationDivClass, isRequired) {
        elemId = $('#' + elementId);
        vdClass = $('.' + validationDivClass);

        if (!isRequired) {
            vdClass.hide();
            elemId.prop('required', false);
        }
        else {
            vdClass.show();
            elemId.prop('required', true);
        }
    };

    ui.toggle = function (className) {
        $("." + className).toggle('slow');
        return false;
    };

    ui.sortTable = function getTableSorted(tableClass) {

        $('.' + tableClass + ' tbody').sortable({
            items: 'tr:not(:first-child)'
        });
    };

    ui.dialogbox = function (id, dialogId, urlAction, panelId, divMessageId, cardClass, sortTableClass, actionMessage) {

        var dialogid = $('#' + dialogId);
        var panelid = $('#' + panelId);

        dialogid.dialog({
            width: 600,
            modal: true,
            dialogClass: 'success-dialog',
            close: function (event, ui) {
                dialogid.remove();
            },
            buttons: {
                "Confirm": function () {
                    $.ajax({
                        url: urlAction,
                        type: 'POST',
                        dataType: 'html',
                        data:
                        {
                            Id: id
                        },
                        success: function (data) {
                            panelid.html(data);
                            if (sortTableClass != null) ui.sortTable(sortTableClass);
                            if (cardClass != null) {
                                if ($('.' + cardClass).is(":hidden")) ui.toggle(cardClass);
                            }
                            ui.displayMessage(divMessageId, actionMessage, false);
                        },
                        error: function (xhr, status, errorThrown) {
                            ui.displayMessage(divMessageId, xhr.status + ' - ' +
                                xhr.responseText, true);
                            if (sortTableClass != null) ui.sortTable(sortTableClass);
                        }
                    });
                    dialogid.remove();
                },
                "Cancel": function () {
                    dialogid.remove();
                }
            }
        });
    };




})();