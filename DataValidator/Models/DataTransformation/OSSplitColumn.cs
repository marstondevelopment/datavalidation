﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSSplitColumn : DTStep
    {
        private PrmsSplitColumn prms;
        public OSSplitColumn(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitColumn;
            if (prms == null)
            {
                throw new MemberAccessException("OSSplitColumn parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

        }


        public override bool Execute()
        {
            ParentFTDT.FileTemplate.FileWriter.AddDefaultOutputFiles();

            var fields = prms.OutputColumnNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x => x.Trim()).ToArray();
            var dts = ParentFTDT.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output).ToArray();
            for (int i = 0; i < dts.Length; i++)
            {
                DataTable dt = dts[i].Data;
                int colIndex = dt.Columns.IndexOf(prms.InputColumnName);

                if (colIndex >= 0)
                {
                    foreach (var field in fields)
                    {
                        DataColumn col = dt.AddColumn(field);
                        col.SetOrdinal(colIndex);
                        colIndex++;
                    }

                    dt.AcceptChanges();
                    foreach (DataRow rw in dt.Rows)
                    {
                        if (rw[prms.InputColumnName].ToString().StartsWith(prms.Prefix))
                        {
                            rw[prms.OutputColumnHasPrefix] = rw[prms.InputColumnName];
                        }
                        else
                        {
                            string colName = fields.Where(x => x != prms.OutputColumnHasPrefix).FirstOrDefault();
                            rw[colName] = rw[prms.InputColumnName];
                        }

                    }

                }
            }

            return true;
        }
    }
}