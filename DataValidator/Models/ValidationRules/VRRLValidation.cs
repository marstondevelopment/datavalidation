﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public class VRRLValidation : VRBase
    {
        public VRRLValidation(DataFieldRule df) : base(df)
        {
        }

        public override bool ValidateField()
        {

            if (!string.IsNullOrEmpty(ParentFieldRule.DataField.OriginalValue))
            {
                if (this.ParentFieldRule.ReferenceList == null || this.ParentFieldRule.ReferenceList.ReferenceListItems.Count <=0)
                {
                    throw new MemberAccessException("Reference list is not set correctly for validation rule - " + this.ParentFieldRule.DataField.Name);
                }

                if (ParentFieldRule.ReferenceList.CaseSensitive)
                {
                    if (!this.ParentFieldRule.ReferenceList.ReferenceListItems.Any(x => x.Value == ParentFieldRule.DataField.OriginalValue))
                    {
                        AppendError();
                        return false;
                    }
                }
                else
                {
                    if (!this.ParentFieldRule.ReferenceList.ReferenceListItems.Any(x => x.Value.ToUpper() == ParentFieldRule.DataField.OriginalValue.ToUpper()))
                    {
                        AppendError();
                        return false;
                    }
                }
            }
            return true;
        }
    }
}