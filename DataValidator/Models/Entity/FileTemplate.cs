﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    //[Serializable]
    [MetadataType(typeof(Metadata))]
    public partial class FileTemplate
    {
        private FileParser _parentDataFile;

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int RowId { get; set; }
        public int NoInputRows { get; set; }

        public string CurrentFileName { get; set; }

        public FileParser ParentDataFile { get => _parentDataFile; set => _parentDataFile = value; }

        private bool terminateProcess;
        protected internal bool TerminateProcess { get => terminateProcess; private set => terminateProcess = value; }

        public FileDelimiter OutputFileDelimiter
        {
            get
            {
                return FileDelimiter1;
            }
        }

        private List<DataField> _columns;
        public List<DataField> Columns
        {
            get
            {
                if (_columns == null)
                {
                    _columns = new List<DataField>();
                    foreach (var item in FileTemplateDataFields)
                    {
                        item.DataField.FileTemplate = item.FileTemplate;
                        item.DataField.Name = item.DataField.Name.Trim();
                        _columns.Add(item.DataField);
                    }

                }
                return _columns;
            }

        }

        public bool UseProcessedTbl;

        public List<DataField> HeaderCols
        {
            get
            {
                return Columns.Where(x => x.FileSection == (int)EnumFileSection.Header).ToList();
            }
        }

        public List<DataField> BodyCols
        {
            get
            {
                return Columns.Where(x => x.FileSection == (int)EnumFileSection.Body).ToList();
            }
        }

        public List<DataField> ParameterizedDataFields
        {
            get
            {
                return Columns.Where(x => x.Parameterize == true).ToList();
            }
        }

        public List<DataField> TrailerCols
        {
            get
            {
                return Columns.Where(x => x.FileSection == (int)EnumFileSection.Footer).ToList();
            }
        }

        public List<DataField> Variables
        {
            get
            {
                return Columns.Where(x => x.FileSection == (int)EnumFileSection.Variable).ToList();
            }
        }

        public void PresetParameterizedDataFields(FormCollection fc)
        {
            if (ParameterizedDataFields.Count>0)
            {
                foreach (var df in ParameterizedDataFields)
                {

                    if (fc[df.Name] != null)
                    {
                        if ((EnumFieldType)df.FieldType == EnumFieldType.Boolean)
                        {
                            df.PresetValue = (fc[df.Name] == "false" ? "FALSE":"TRUE");
                        }
                        else
                        {
                            df.PresetValue = fc[df.Name];
                        }
                    }
                }
            }
        }

        public void ResetFieldValues()
        {
            BodyCols.ForEach(a => {
                a.OriginalValue = null;
                a.OriginalValues = null;
                a.DerivedSubNodes = null;
            });            
        }

        private DataProvider _dataProvider;
        public DataTable HeaderDataTable;
        public DataTable TrailerDataTable;

        public DataTable BUDataTable;
        public IEnumerable<IGrouping<Object,DataRow>> GroupedDataTable { get; set; }
        private DataTable processedDataTable;
        public DataTable ProcessedDataTable
        {
            get
            {
                if (processedDataTable == null)
                {
                    processedDataTable = BUDataTable.Copy();
                }

                return processedDataTable;
            }
            set
            {
                processedDataTable = value;
            }
        }

        private DataTable excludedDataTable;
        public DataTable ExcludedDataTable
        {
            get
            {
                if (excludedDataTable == null)
                {
                    excludedDataTable = BUDataTable.Clone();
                }

                return excludedDataTable;
            }
            set
            {
                excludedDataTable = value;
            }
        }

        private FileWriter fileWriter;

        public FileWriter FileWriter
        {
            get
            {

                if (fileWriter == null)
                {
                    fileWriter = FileWriter.New(this);
                }
                return fileWriter;
            }

        }




        private List<FileScheme> outputFileSchemes;
        public List<FileScheme> OutputFileSchemes
        {
            get
            {
                if (outputFileSchemes == null)
                {
                    outputFileSchemes = new List<FileScheme>();
                }
                return outputFileSchemes;
            }
            set
            {
                outputFileSchemes = value;
            }
        }

        
        public void CreateDataTables()
        {
            CreateDataTable(ref HeaderDataTable, EnumFileSection.Header);
            CreateDataTable(ref BUDataTable, EnumFileSection.Body);
            CreateDataTable(ref TrailerDataTable, EnumFileSection.Footer);
        }

        private void CreateDataTable(ref DataTable dt, EnumFileSection fs)
        {
            var cols = Columns.Where(x => x.FileSection == (int)fs).ToList();
            if (cols.Count > 0)
            {
                dt = new DataTable();
                foreach (var col in cols)
                {
                    dt.AddColumn(col);
                }
            }
        }


        public void AppendToDataTable()
        {
            if (BUDataTable.Rows.Count == 0 && HeaderCols.Count > 0)
            {
                HeaderDataTable.AddRow(HeaderCols);
            }
            if (BUDataTable.Rows.Count == 0 && TrailerCols.Count > 0)
            {
                TrailerDataTable.AddRow(TrailerCols);
            }

            BUDataTable.AddRow(BodyCols);


        }

        public bool ValidateRows()
        {
            bool valid = true;
            _columns.ForEach(x => x.PopulateOVWithDVOrPV());
            foreach (var col in _columns)
            {

                foreach (var action in col.DataFieldDataCleansings)
                {
                    action.DCAction.ExecuteAction();
                }

                foreach (var rule in col.DataFieldRules)
                {
                    if (!rule.ValidationRule.IsValid())
                    {
                        if (rule.TerminateIfInvalid)
                        {
                            terminateProcess = true;
                            return false;
                        }

                        valid = false;
                    }
                }
            }
            if (valid && ParentDataFile.PersistingInputFile)
            {
                AppendToDataTable();
            }

            return valid;
        }

        public string DescriptionDisplay
        {
            get
            {
                return !string.IsNullOrEmpty(Description) ? Description : "N/A";
            }
        }

        public string BUStagingTblDisplay
        {
            get
            {
                return !string.IsNullOrEmpty(BUStagingTable) ? BUStagingTable : "N/A";
            }
        }
               
        public string XMLRootNodeXPathDisplay
        {
            get
            {
                return !string.IsNullOrEmpty(XMLRootNodeXPath) ? XMLRootNodeXPath : "N/A";
            }
        }
        private List<SelectListItem> selectDTSPItems = null;
        public List<SelectListItem> SelectDTSPItems
        {
            get
            {
                if (this.DBConnectionId != null && selectDTSPItems == null)
                {
                    selectDTSPItems = DbConnection.StoredProcedures
                        .Where(x => x.SPType == (int)EnumSPType.DTSP)
                        .Select(x => new SelectListItem { Text = x.SPName, Value = x.Id.ToString() })
                        .ToList();                
                }
                return selectDTSPItems;
            }
        }


        private List<SelectListItem> selectBUCmdTextItems = null;
        public List<SelectListItem> SelectBUCmdTextItems
        {
            get
            {
                if (this.DBConnectionId != null && selectBUCmdTextItems == null)
                {
                    selectBUCmdTextItems = DbConnection.CmdTexts
                        .Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() })
                        .ToList();
                }
                return selectBUCmdTextItems;
            }
        }

        public DataProvider DataProvider
        {
            get
            {
                if (_dataProvider == null)
                {
                    _dataProvider = DataProvider.New(this);
                }
                return _dataProvider;
            }
        }

        internal bool ExecuteAllDTActions()
        {
            try
            {

                //Override default DTAction execution order if the flag is true
                var fileTemplateDTActions = OverrideDefaultDTActionSequence ? FileTemplateDTActions.ToList() : FileTemplateDTActions.OrderBy(x => x.DTAction.ExecutionOrder).ToList();

                if (fileTemplateDTActions.Count > 0)
                {
                    foreach (var dtAction in fileTemplateDTActions)
                    {
                        //execute each action here
                        if (dtAction.DTStep.Execute() == false)
                        {
                            return false;
                        }
                    }

                    UseProcessedTbl = true;
                }



                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }

        }

        internal bool ExecuteDTActions()
        {
            try
            {

                //Override default DTAction execution order if the flag is true
                var fileTemplateDTActions = OverrideDefaultDTActionSequence?  FileTemplateDTActions.Where(z => z.DTAction.ActionGroup == EnumDTActionGroup.DT).ToList() : FileTemplateDTActions.Where(z => z.DTAction.ActionGroup == EnumDTActionGroup.DT).OrderBy(x => x.DTAction.ExecutionOrder).ToList();

                if (fileTemplateDTActions.Count > 0)
                {
                    foreach (var dtAction in fileTemplateDTActions)
                    {
                        //execute each action here
                        if (dtAction.DTStep.Execute() == false)
                        {
                            return false;
                        }
                    }

                    UseProcessedTbl = true;
                }



                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }

        }

        internal bool ExecutePreGenActions()
        {
            try
            {
                //Sort the actions into logical order as in EnumDTAction
                var fileTemplateDTActions = OverrideDefaultDTActionSequence ? FileTemplateDTActions.Where(z => z.DTAction.ActionGroup == EnumDTActionGroup.FW).ToList() : FileTemplateDTActions.Where(z => z.DTAction.ActionGroup == EnumDTActionGroup.FW).OrderBy(x => x.DTAction.ExecutionOrder).ToList();

                foreach (var dtAction in fileTemplateDTActions)
                {
                    //execute each action here
                    if (dtAction.DTStep.Execute() == false)
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("GenerateFiles failed due to error occurred in DTAction.DTStep.Execution()", ex);

                return false;
            }

            return true;

        }

        public FileTemplate Clone()
        {
            FileTemplate ft =  new FileTemplate()
            {
                Name = string.Format("Copy of " + Name),
                Description = Description,               
                DBConnectionId = DBConnectionId,
                Disabled = Disabled,
                MimeTypeId = MimeTypeId,
                ClientId = ClientId,
                BulkUpload = BulkUpload,
                BUStagingTable = BUStagingTable,
                TransformData = TransformData,
                DTStoredProcedureId = DTStoredProcedureId,
                DTFileWriter = DTFileWriter,
                RemoveHeader = RemoveHeader,
                RemoveFooter = RemoveFooter,
                NoColumnName = NoColumnName,
                DelimiterId = DelimiterId,
                GenerateOutputFile = GenerateOutputFile,
                OutputFileDelimiterId = OutputFileDelimiterId,
                XMLRootNodeXPath = XMLRootNodeXPath,
                HeaderRowsToSkip = HeaderRowsToSkip,
                FooterRowsToSkip = FooterRowsToSkip,
                GenerateAuditFile = GenerateAuditFile,
                ARTemplateScript = ARTemplateScript,
                AuditFileExtenstion = AuditFileExtenstion,
                OutputFileEncoding = OutputFileEncoding,
                OFExcludeColumnHeader = OFExcludeColumnHeader,
                UploadToFileRepository = UploadToFileRepository,
                RepositoryFilePath = RepositoryFilePath,
                FNPAsSubfolder = FNPAsSubfolder,
                OverrideDefaultDTActionSequence = OverrideDefaultDTActionSequence,
                DataEntry = DataEntry,
                DataEntryFormId = DataEntryFormId,
                MergeDTAStages = MergeDTAStages,
                DEStagingTable = DEStagingTable,
                BUReport = BUReport,
                BUReportClientIds = BUReportClientIds,
                BUReportCmdTextId = BUReportCmdTextId,
                ARFileNamePrefix = ARFileNamePrefix,
                ARFileNameNoSuffix = ARFileNameNoSuffix
            };
            foreach (var item in this.FileTemplateDataFields)
            {
                ft.FileTemplateDataFields.Add(new FileTemplateDataField() { DataFieldId = item.DataFieldId, FileTemplateId = item.FileTemplateId });
            }

            foreach (var item in FileTemplateDTActions)
            {
                ft.FileTemplateDTActions.Add(new FileTemplateDTAction { DTActionId = item.DTActionId, FileTemplateId = item.FileTemplateId });
            }

            foreach (var item in FileTemplateARActions)
            {
                ft.FileTemplateARActions.Add(new FileTemplateARAction { ARActionId = item.ARActionId, FileTemplateId = item.FileTemplateId });
            }
            return ft;
        }

        public bool RefreshGroupedByTable()
        {
            var groupBy = FileTemplateDTActions.Where(x => x.DTAction.DTActionTypeId == (int)EnumDTActionType.OSGroupBy).FirstOrDefault();
            if (groupBy != null)
            {
                return groupBy.DTStep.Execute();
            }
            return true;
        }

        public void RenderAuditFile(string tagName, string context)
        {
            if (!string.IsNullOrEmpty(ARTemplateScript))
            {
                string tag = "<" + tagName + ">";
                ARTemplateScript = ARTemplateScript.Replace(tag, context);
            }
        }

        public bool Editable
        {
            get
            {
                User user = (User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                if (user.IsAdmin)
                    return true;

                if (Id > 0)
                {
                    return (user.AssociatedFileTemplates.Any(x => x.Id == Id));
                }
                else
                {
                    return (user.RoleId > (int)EnumRole.ReadOnly);
                }
            }

        }

        public bool BUReportConfigured
        {
            get
            {
                return (BUReport == true && !string.IsNullOrEmpty(BUReportClientIds) && BUReportCmdTextId.HasValue);
            }
        }

        public CmdText BUSReportCmdText => CmdText;

        public List<DataEntry> DEForUpload
        {
            get
            {
                User user = (User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];

                if (user.RoleId > (int)EnumRole.ReadOnly)
                {
                    return DataEntries.Where(x => x.Uploaded == false && x.Deleted == false).ToList();
                }
                else
                {
                    return DataEntries.Where(x => x.UserId == user.Id && x.Uploaded == false && x.Deleted == false).ToList();
                }
               
            }
        }


    }


    public class Metadata
    {
        [Required(ErrorMessage = "Template name is required.")]
        public string Name;
        [Display(Name = "Created By")]
        public string CreatedBy;
        [Display(Name = "Date Created")]
        public DateTime DateCreated;
        [Display(Name = "Updated By")]
        public string UpdatedBy;
        [Display(Name = "Date Updated")]
        public DateTime DateUpdated;
        [Required(ErrorMessage = "File Type is required.")]
        [Display(Name = "Input File Type")]
        public int MimeTypeId;
        [Display(Name = "Client Group")]
        public int ClientId;
        [Required(ErrorMessage = "Connection type is required.")]
        [Display(Name = "DB connection")]
        public int DBConnectionId;
        [Display(Name = "Bulk Upload")]
        public bool BulkUpload;
        [Display(Name = "Transform Data")]
        public bool TransformData;
        [Display(Name = "BU Staging Table")]
        public string BUStagingTable;
        [Display(Name = "DT Stored Procedure")]
        public string DTStoredProcedureId;
        [Display(Name = "Remove Header")]
        public bool RemoveHeader;
        [Display(Name = "Remove Footer")]
        public bool RemoveFooter;
        [Display(Name = "No Column Name")]
        public bool NoColumnName;
        [Display(Name = "Delimiters")]
        public int DelimiterId;
        [Display(Name = "Generate Output File")]
        public bool GenerateOutputFile;
        [Display(Name = "DT File Writer")]
        public int DTFileWriter;
        [Display(Name = "Output File Delimiter")]
        public int OutputFileDelimiterId;
        [Display(Name = "XPath")]
        public string XMLRootNodeXPath;
        [Display(Name = "Header Rows To Skip")]
        public int HeaderRowsToSkip;
        [Display(Name = "Footer Rows To Skip")]
        public int FooterRowsToSkip;
        [Display(Name = "Generate Audit File")]
        public bool GenerateAuditFile;
        [Display(Name = "Override DDTA Order")]
        public bool OverrideDefaultDTActionSequence;
        [Display(Name = "Merge DTA Stages")]
        public bool MergeDTAStages;
        [Display(Name = "AR Template Script")]
        public string ARTemplateScript;
        [Display(Name = "AR File Extension")]
        public int AuditFileExtenstion;
        [Display(Name = "Exclude Column Header")]
        public bool OFExcludeColumnHeader;
        [Display(Name = "Output File Encoding")]
        public int? OutputFileEncoding;
        [Display(Name = "Upload To Repository")]
        public bool UploadToFileRepository;
        [Display(Name = "Full File Path")]
        public string RepositoryFilePath;
        [Display(Name = "FNP As Sub-folder")]
        public bool FNPAsSubfolder;
        [Display(Name = "Use Xsd")]
        public bool UseXsd;
        [Display(Name = "Xsd File")]
        //[Required(ErrorMessage = "Xsd File required.")]
        public int? XsdFileId;
        [Display(Name = "Data Entry")]
        public bool DataEntry;
        [Display(Name = "Data Entry Form")]
        //[Required(ErrorMessage = "Xsd File required.")]
        public int? DataEntryFormId;
        [Display(Name = "DE Staging Table")]
        public string DEStagingTable;

        [Display(Name = "BU Report")]
        public bool BUReport;
        [Display(Name = "Rep SQL Script")]
        public int? BUReportCmdTextId;
        [Display(Name = "FT Clients")]
        public string BUReportClientIds;
        [Display(Name = "FN Prefix")]
        public string ARFileNamePrefix;
        [Display(Name = "FN No Suffix")]
        public bool ARFileNameNoSuffix;

    }
}