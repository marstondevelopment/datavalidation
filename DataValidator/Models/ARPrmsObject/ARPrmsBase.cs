﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class ARPrmsBase
    {
        public ARPrmsBase() { }
        public ARPrmsBase(ARAction dt)
        {
            parent = dt;
        }

        [JsonIgnore]
        public ARAction Parent { get => parent; set => parent = value; }

        private ARAction parent;

    }
}