﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    public class XsdFileRepository : RepositoryBase<DataIntegrationEntities, XsdFile>
    {
        public XsdFileRepository(DataIntegrationEntities entity) : base(entity)
        {
        }

        public override IQueryable<XsdFile> GetAll()
        {
            return base.GetAll();
        }

        public XsdFile GetById(int id)
        {
            return GetAll(x => x.Id == id).FirstOrDefault();
        }

        public void Create(XsdFile xf)
        {           
            this.Add(xf);
            this.SubmitChanges();
            xf.LocalFileName = xf.Id.ToString("0000") + ".xsd";
            this.SubmitChanges();
        }

        public IEnumerable<XsdFile> GetActiveXsdFiles()
        {
            return _entities.XsdFiles.Where(x => x.Disabled == false);
        }       

        public void Update(XsdFile xf)
        {
            this.SubmitChanges();
        }
        public void Delete(XsdFile xf)
        {
            xf.Disabled = true;
            Update(xf);
        }
    }
}