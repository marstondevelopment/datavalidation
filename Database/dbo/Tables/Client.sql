﻿CREATE TABLE [dbo].[Client] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [Description] NVARCHAR (250) NULL,
    [CreatedBy]   INT            NOT NULL,
    [DateCreated] DATETIME       NOT NULL,
    [UpdatedBy]   INT            NOT NULL,
    [DateUpdated] DATETIME       NOT NULL,
    [Disabled]    BIT            CONSTRAINT [DF_Client_Disabled] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Client_User] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Client_User1] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);

