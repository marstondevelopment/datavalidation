﻿using ICSharpCode.SharpZipLib.Zip;
using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [ReadOnlyAuthorizeUser]
    public class FileController : ControllerBase
    {
        public FileController(ClientRepository cr, UserRepository ur, FileTemplateRepository ftr)
        {
            _cr = cr;
            _ur = ur;
            _ftr = ftr;
        }

        private ClientRepository _cr;
        private UserRepository _ur;
        private FileTemplateRepository _ftr;

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Upload()
        {
            VMFileUpload vm= new VMFileUpload();
            
            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
            vm.Clients = _ur.CurrentUser.IsAdmin? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }

        private const string SEARCH_BUAudit_PRMS = "BUAuditSearchPrms";
        protected BUAuditSearchPrms<DataRow> CurrentBUAuditSearchPrms
        {
            get
            {
                return Session[SEARCH_BUAudit_PRMS] == null ? null : (BUAuditSearchPrms<DataRow>)this.Session[SEARCH_BUAudit_PRMS];
            }
            set
            {
                Session.Add(SEARCH_BUAudit_PRMS, value);
            }
        }

        public ActionResult BUReport(int SelectedFileTemplateId, string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, DateTime? fromDate, DateTime? toDate, int? clientId)
        {
            //FileTemplate ft = GetSelectedFileTemplate(SelectedFileTemplateId, false);
            FileTemplate ft = _ftr.GetById(SelectedFileTemplateId);
            if (ft.BUReport != true || ft.BUReportCmdTextId == null || string.IsNullOrEmpty(ft.BUReportClientIds))
            {
                return RedirectToAction("Upload", "File").Warning("BU report configuration not been set up correctly.Please update the file template - (" + ft.Name + ") and try again.");
            }
            BUAuditSearchPrms<DataRow> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentBUAuditSearchPrms != null && ft.Id == CurrentBUAuditSearchPrms.SelectedFileTemplateId)
            {
                searchPrms = CurrentBUAuditSearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }


                var clients = _ftr.GetBUReportClients(ft);
                searchPrms = new BUAuditSearchPrms<DataRow>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, fromDate.HasValue ? fromDate.Value : DateTime.Today, toDate.HasValue ? toDate.Value : DateTime.Today, clientId, clients, SelectedFileTemplateId, _ur.GetAll().ToList(), 0, 0, 0);

            }

            IEnumerable<DataRow> results = null;

            bool succeed = _ftr.GetBUAuditReport(searchPrms, ft, ref results);

            CurrentBUAuditSearchPrms = new BUAuditSearchPrms<DataRow>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, results.ToList(), searchPrms.FromDate, searchPrms.ToDate, searchPrms.ClientId, searchPrms.Clients, ft.Id, searchPrms.Users, searchPrms.CasesImported, searchPrms.AppendingCases, searchPrms.CasesWithError);
            if (succeed)
                return View(CurrentBUAuditSearchPrms).Information("No. rows found: " + CurrentBUAuditSearchPrms.RowsCount.ToString());

            return View(CurrentBUAuditSearchPrms).Warning("Failed to retrieve audit information from the selected file template. Please see System Error Log for further details.");
        }


        [HttpPost]
        public ActionResult Upload(FormCollection fc)
        {
            VMFileUpload vm = new VMFileUpload();
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            TryUpdateModel(vm);
            FileTemplate ft = _ftr.GetById(vm.SelectedFileTemplateId.Value);
            vm.SelectedFT = ft;
            ft.PresetParameterizedDataFields(fc);
            var action = fc["action"];

            if (action == "BUReport")
            {
                if (ft.BUReportConfigured)
                {
                    return RedirectToAction("BUReport", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Configuration for data entry has not been set. Please update the file template and try again.");
                }

                return View(vm).Warning("The selected file template has not been configured correctly to genarate BU report. Update the template and try again.");
            }
            else

            {
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];
                    if (file.ContentLength > 0)
                    {
                        string fileName = file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1);
                        using (HttpClient client = new HttpClient())
                        {
                            using (var content = new MultipartFormDataContent())
                            {
                                if (!Utility.CheckFileExtention(file.FileName, ft))
                                {
                                    return View(vm).Warning("File extention not supported for the selected file template. Please choose a file with correct extention and try again");
                                }

                                FileParser fileParser = FileParser.New(ft, file.InputStream, fileName, EnumDataFileScreen.Upload);
                                FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

                                switch ((EnumParserResult)frm.ProcessFile())
                                {
                                    case EnumParserResult.UploadSucceed:
                                        if (ft.GenerateAuditFile)
                                        {
                                            ReturnZipFile(ft);
                                            return null;
                                        }
                                        return View(vm).Success("Data has been uploaded successfully.");
                                    case EnumParserResult.UploadFailed:
                                        return View(vm).Warning("Bulk copy process failed due to some technical issue, please consult system administrator for further assistance.");
                                    case EnumParserResult.ConfigNotSet:
                                        return View(vm).Warning("Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
                                    case EnumParserResult.NoData:
                                        return View(vm).Warning("Input data file is empty. No data to process.");
                                    case EnumParserResult.ValidationFailed:
                                        WriteCSVFile(fileParser.ValidationSummary.ToString());
                                        return null;
                                    default:
                                        throw new Exception("Unexpected ParserResult from ProcessFile()");
                                }
                            }
                        }
                    }
                    else
                    {
                        return View(vm).Warning("Data file is empty. Please select another file and try again.");
                    }
                }
                else
                {
                    return View(vm).Warning("No data file loaded. Please select a file and try again.");
                }
            }

        }

        public ActionResult UploadToRepository()
        {
            VMFileUpload vm = new VMFileUpload();
            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }

        [HttpPost]
        public ActionResult UploadToRepository(FormCollection fc)
        {
            VMFileUpload vm = new VMFileUpload();
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            TryUpdateModel(vm);
            FileTemplate ft = _ftr.GetById(vm.SelectedFileTemplateId.Value);
            vm.SelectedFT = ft;
            ft.PresetParameterizedDataFields(fc);

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file.ContentLength > 0)
                {
                    string fileName = file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1);
                    using (HttpClient client = new HttpClient())
                    {
                        using (var content = new MultipartFormDataContent())
                        {


                            if (!Utility.CheckFileExtention(file.FileName, ft))
                            {
                                return View(vm).Warning("File extention not supported for the selected file template. Please choose a file with correct extention and try again");
                            }

                            FileParser fileParser = FileParser.New(ft, file.InputStream, fileName, EnumDataFileScreen.UploadToRepository);
                            FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

                            EnumParserResult status = frm.UploadFilesToRepository();
                            switch (status)
                            {
                                case EnumParserResult.UploadToRepositorySucceed:
                                    ReturnZipFile(ft);
                                    return null;
                                case EnumParserResult.UploadToRepositoryFailed:
                                    return View(vm).Warning("Upload To Repository failed due to some technical issue, please consult system administrator for further assistance.");
                                case EnumParserResult.RepositoryNotFound:
                                    return View(vm).Warning("Repository folder not found or inaccessible, please check the settings in FT and try again. Consult system administrator for further assistance if the issue persists");
                                case EnumParserResult.SubfolderNotFound:
                                    return View(vm).Warning("The following subfolder/s not found or inaccessible, please check the settings in FT and existence of subfolder, and try again. Consult system administrator for further assistance if the issue persists <br/>" + fileParser.ValidationSummary.ToString() );
                                case EnumParserResult.FileGenerationFailed:
                                    return View(vm).Warning("Output files generation process failed due to some technical issue, please consult system administrator for further assistance.");
                                case EnumParserResult.DTACtionsFailed:
                                    return View(vm).Warning("DT actions failed due to some technical issue, please consult system administrator for further assistance.");
                                case EnumParserResult.PreGenActionsFailed:
                                    return View(vm).Warning("Pre-Gen actions failed due to some technical issue, please consult system administrator for further assistance.");
                                case EnumParserResult.ConfigNotSet:
                                    return View(vm).Warning("Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
                                case EnumParserResult.NoData:
                                    return View(vm).Warning("Input data file is empty. No data to process.");
                                case EnumParserResult.ValidationFailed:
                                    WriteCSVFile(fileParser.ValidationSummary.ToString());
                                    return null;
                                default:
                                    throw new Exception("Unexpected ParserResult from ProcessFile()");
                            }
                        }
                    }
                }
                else
                {
                    return View(vm).Warning("Data file is empty. Please select another file and try again.");
                }
            }
            else
            {
                return View(vm).Warning("No data file loaded. Please select a file and try again.");
            }

        }


        public ActionResult OutputFiles()
        {
            VMFileUpload vm = new VMFileUpload();
            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }

        [HttpPost]
        public ActionResult OutputFiles(FormCollection fc)
        {
            VMFileUpload vm = new VMFileUpload();
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            TryUpdateModel(vm);
            FileTemplate ft = _ftr.GetById(vm.SelectedFileTemplateId.Value);
            vm.SelectedFT = ft;
            ft.PresetParameterizedDataFields(fc);

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];
                if (file.ContentLength > 0)
                {
                    string fileName = file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1);
                    using (HttpClient client = new HttpClient())
                    {
                        using (var content = new MultipartFormDataContent())
                        {

                            if (!Utility.CheckFileExtention(file.FileName, ft))
                            {
                                return View(vm).Warning("File extention not supported for the selected file template. Please choose a file with correct extention and try again");
                            }

                            FileParser fileParser = FileParser.New(ft, file.InputStream, fileName, EnumDataFileScreen.GenerateFiles);
                            FileReaderManager frm = new FileReaderManager(_ftr, fileParser);


                            switch ((EnumParserResult)frm.GenerateOutputFiles())
                            {
                                case EnumParserResult.FileGenerationSucceed:
                                    ReturnZipFile(ft);
                                    return null;
                                case EnumParserResult.FileGenerationFailed:
                                    return View(vm).Warning("Output files generation process failed due to some technical issue, please consult system administrator for further assistance.");
                                case EnumParserResult.DTACtionsFailed:
                                    return View(vm).Warning("DT actions failed due to some technical issue, please consult system administrator for further assistance.");
                                case EnumParserResult.PreGenActionsFailed:
                                    return View(vm).Warning("Pre-Gen actions failed due to some technical issue, please consult system administrator for further assistance.");
                                case EnumParserResult.ConfigNotSet:
                                    return View(vm).Warning("Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
                                case EnumParserResult.NoData:
                                    return View(vm).Warning("Input data file is empty. No data to process.");
                                case EnumParserResult.ValidationFailed:
                                    WriteCSVFile(fileParser.ValidationSummary.ToString());
                                    return null;
                                default:
                                    throw new Exception("Unexpected ParserResult from ProcessFile()");
                            }
                        }
                    }
                }
                else
                {
                    return View(vm).Warning("Data file is empty. Please select another file and try again.");
                }
            }
            else
            {
                return View(vm).Warning("No data file loaded. Please select a file and try again.");
            }

        }

        //private void ReturnZipFile(FileTemplate ft)
        //{

        //    Response.ContentType = "application/zip";
        //    // If the browser is receiving a mangled zipfile, IIS Compression may cause this problem. Some members have found that
        //    //Response.ContentType = "application/octet-stream" has solved this. May be specific to Internet Explorer.

        //    Response.AppendHeader("content-disposition", "attachment; filename=\"" + ft.Name.Replace(" ", string.Empty) + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".zip\"");
        //    Response.CacheControl = "Private";
        //    Response.Cache.SetExpires(DateTime.Now.AddMinutes(3)); // or put a timestamp in the filename in the content-disposition

        //    byte[] buffer = new byte[4096];

        //    ZipOutputStream zipOutputStream = new ZipOutputStream(Response.OutputStream);
        //    zipOutputStream.SetLevel(3); //0-9, 9 being the highest level of compression

        //    foreach (var ms in ft.FileWriter.OutputFiles)
        //    {

        //        ZipEntry entry = new ZipEntry(ms.FileName);
        //        var stream = ms.Generate();
        //        entry.Size =stream.Length;
        //        // Setting the Size provides WinXP built-in extractor compatibility,
        //        //  but if not available, you can set zipOutputStream.UseZip64 = UseZip64.Off instead.

        //        zipOutputStream.PutNextEntry(entry);
        //        stream.Position = 0;
        //        int count = stream.Read(buffer, 0, buffer.Length);
        //        while (count > 0)
        //        {
        //            zipOutputStream.Write(buffer, 0, count);
        //            count = stream.Read(buffer, 0, buffer.Length);
        //            if (!Response.IsClientConnected)
        //            {
        //                break;
        //            }
        //            Response.Flush();
        //        }
        //        stream.Close();
        //    }

        //    zipOutputStream.Close();

        //    Response.Flush();
        //    Response.End();
        //}

        [HttpPost]
        public ActionResult Validate(FormCollection fc)
        {
            VMFileUpload vm = new VMFileUpload();
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            TryUpdateModel(vm);
            FileTemplate ft = _ftr.GetById(vm.SelectedFileTemplateId.Value);
            vm.SelectedFT = ft;
            ft.PresetParameterizedDataFields(fc);

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file.ContentLength >0)
                {
                    string fileName = file.FileName.Substring(file.FileName.LastIndexOf("\\") + 1);
                    using (HttpClient client = new HttpClient())
                    {
                        using (var content = new MultipartFormDataContent())
                        {

                            if (!Utility.CheckFileExtention(file.FileName, ft))
                            {
                                return View(vm).Warning("File extention not supported for the selected file template. Please choose a file with correct extention and try again");
                            }

                            FileParser fileParser = FileParser.New(ft, file.InputStream, fileName, EnumDataFileScreen.Validate);
                            FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

                            switch ((EnumParserResult)frm.ProcessFile())
                            {
                                case EnumParserResult.ValiationSucceed:
                                    return View(vm).Success("Data is valid.");
                                case EnumParserResult.ValidationFailed:
                                    WriteCSVFile(fileParser.ValidationSummary.ToString());
                                    //return RedirectToAction("Validate","File").Warning("Validation error occurred. Please see downloaded file for futher details");
                                    return null;
                                default:
                                    throw new Exception("Unexpected ParserResult from ProcessFile()");
                            }
                        }
                    }
                }
                else
                {
                    return View(vm).Warning("Data file is empty. Please select another file and try again.");
                }
            }
            else
            {
                return View(vm).Warning("No data file loaded. Please select a file and try again.");
            }

        }

        public ActionResult Validate()
        {
            VMFileUpload vm = new VMFileUpload();
            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }

        [AdminOnlyAuthorizeUser]
        public ActionResult Transform()
        {
            VMFileUpload vm = new VMFileUpload();
            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }

        [AdminOnlyAuthorizeUser]
        [HttpPost]
        public ActionResult Transform(FormCollection fc)
        {
            VMFileUpload vm = new VMFileUpload();
            vm.Clients = _cr.GetAll().ToList();
            TryUpdateModel(vm);
            if (!vm.SelectedFileTemplateId.HasValue)
            {
                return View(vm).Warning("File templated is required.");
            }

            FileTemplate fileTemplate = _ftr.GetById(vm.SelectedFileTemplateId.Value);
            if (fileTemplate.TransformData == false || fileTemplate.StoredProcedure == null)
            {
                return View(vm).Warning("Configuration for data transformation has not been set. Please update the file template and try again.");
            }

            FileReaderManager frm = new FileReaderManager(_ftr);


            if (frm.TransformData(fileTemplate))
            {
                return View(vm).Success("Data transformation process completed successfully.");
            }
            else
            {
                return View(vm).Warning("Data transformation process failed due to technical issue, please consult system administrator for further assistance.");
            }
        }

        //private void WriteCSVFile(string message)
        //{
        //    // Return the file content with response body. 
        //    Response.ContentType = "text/csv";
        //    Response.AddHeader("Content-Disposition", "attachment; filename=ValidationResult_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv");
        //    Response.Write(message);
        //    Response.End();
        //}
        
        [HttpGet]
        public ActionResult GetFileTemplates(int clientId, EnumDataFileScreen screen)
        {
            var client = _cr.GetById(clientId);
            
            switch (screen)
            {
                case EnumDataFileScreen.Validate:
                    return this.Json(client.FileTemplates.Where(x=> x.Disabled == false).Select(x => new { Id = x.Id, Name = x.Name + " ( " + x.MimeType.Extension + " )" }).OrderBy(y=> y.Name).ToList(), JsonRequestBehavior.AllowGet); 
                case EnumDataFileScreen.Upload:
                    return this.Json(client.FileTemplates.Where(x=> x.BulkUpload==true && x.Disabled == false).Select(x => new { Id = x.Id, Name = x.Name + " ( " + x.MimeType.Extension + " )" }).OrderBy(y => y.Name).ToList(), JsonRequestBehavior.AllowGet);
                case EnumDataFileScreen.Transform:
                    return this.Json(client.FileTemplates.Where(x => x.TransformData == true && x.Disabled == false).Select(x => new { Id = x.Id, Name = x.Name + " ( " + x.MimeType.Extension + " )" }).OrderBy(y => y.Name).ToList(), JsonRequestBehavior.AllowGet);
                case EnumDataFileScreen.GenerateFiles:
                    return this.Json(client.FileTemplates.Where(x => x.GenerateOutputFile == true && x.Disabled == false).Select(x => new { Id = x.Id, Name = x.Name + " ( " + x.MimeType.Extension + " )" }).OrderBy(y => y.Name).ToList(), JsonRequestBehavior.AllowGet);
                case EnumDataFileScreen.UploadToRepository:
                    return this.Json(client.FileTemplates.Where(x => x.UploadToFileRepository == true && x.Disabled == false).Select(x => new { Id = x.Id, Name = x.Name + " ( " + x.MimeType.Extension + " )" }).OrderBy(y => y.Name).ToList(), JsonRequestBehavior.AllowGet);
                case EnumDataFileScreen.DEUpload:
                    return this.Json(client.FileTemplates.Where(x => x.DataEntry == true && x.Disabled == false).Select(x => new { Id = x.Id, Name = x.Name }).OrderBy(y => y.Name).ToList(), JsonRequestBehavior.AllowGet);
                default:
                    throw new NotImplementedException();
            }
        }

        public ActionResult GetFTParams(int selectedFTId)
        {
            FileTemplate fileTemplate = _ftr.GetById(selectedFTId);

            if (fileTemplate != null && fileTemplate.ParameterizedDataFields.Count>0)
            {
                return PartialView("_FTParams", fileTemplate);
            }

            return null;
        }

    }
}
