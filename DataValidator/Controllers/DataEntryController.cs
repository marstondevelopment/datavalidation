﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [ReadOnlyAuthorizeUser]
    public class DataEntryController : ControllerBase
    {
        DataEntryRepository _der;
        private ClientRepository _cr;
        private UserRepository _ur;
        private FileTemplateRepository _ftr;

        private const string LAST_FT = "LAST_FILE_TEMPLATE";

        public DataEntryController(DataEntryRepository der, ClientRepository cr, UserRepository ur, FileTemplateRepository ftr)
        {
            _der = der;
            _ur = ur;
            _cr = cr;
            _ftr = ftr;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Upload()
        {
            VMFileUpload vm = new VMFileUpload();

            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>) _ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }

        [HttpGet]
        public ActionResult GetHMCTSClients(int FileTemplateId, int RegionId)
        {

            var clients = GetHMCTSArrestBreachClientList(FileTemplateId, RegionId);
            return this.Json(clients.Select(x => new { Id = x.Value, Name = x.Text }).OrderBy(y => y.Name).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CheckHMCTSCaseAI(int fileTemplateId, int clientId, string caseNumber, int? deId)
        {

            bool exists = _der.HMCTSBreachCaseAI(fileTemplateId, clientId, caseNumber, deId);
            return this.Json(exists, JsonRequestBehavior.AllowGet);
        }

        private List<SelectListItem> GetOffenceCodeList(int ftId)
        {

            string cacheName = DataEntryRepository.OFFENCE_CODE_CMDTXT + "_" + ftId.ToString();
            List<SelectListItem> cachedList = (List<SelectListItem>)HttpContext.Cache[cacheName];
            if (cachedList == null)
            {
                Dictionary<string, string> codeList = _der.GetOffenceCodes(ftId);
                cachedList = codeList.Select(x => new SelectListItem() { Text = x.Key, Value = x.Value }).ToList();
                HttpContext.Cache.Add(cacheName, cachedList, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(2, 0, 0), System.Web.Caching.CacheItemPriority.Normal, null);

            }
            return cachedList;
        }

        private List<SelectListItem> GetHMCTSArrestBreachClientList(int ftId, int? regionId)
        {
            DataTable dt = _der.GetHMCTSBreachClients(ftId);
            if (!regionId.HasValue)
                return new List<SelectListItem>();

            string filter = "[RegionId] = " + regionId.Value.ToString();
            var rows = dt.Select(filter);
            var clients = rows.Select(x => new SelectListItem() { Text = x["ClientName"].ToString(), Value = x["ClientId"].ToString() }).ToList();
            return clients;
        }

        private List<SelectListItem> GetHMCTSBreachRegionList(int ftId)
        {
            DataTable dt = _der.GetHMCTSBreachClients(ftId);
            var rows = dt.Select().GroupBy(x => x["RegionId"]).Select(x => x.First()).OrderBy(y => y["Region"]).ToList();
            var regions = rows.Select(x => new SelectListItem() { Text = x["Region"].ToString(), Value = x["RegionId"].ToString() }).ToList();
            regions = regions.Distinct().ToList();
            return regions;
        }


        [HttpPost]
        public ActionResult Upload(FormCollection fc)
        {
            VMFileUpload vm = new VMFileUpload();
            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();

            TryUpdateModel(vm);
            FileTemplate ft = _ftr.GetById(vm.SelectedFileTemplateId.Value);
            ft.PresetParameterizedDataFields(fc);

            if (ft.DataEntry == false || ft.DataEntryFormId == null || string.IsNullOrEmpty(ft.DEStagingTable))
            {
                return View(vm).Warning("Configuration for data entry has not been set correctly. Please update the file template and try again.");
            }

            if (Request.Form["UploadSubmit"]!=null)
            {
                FileParser fileParser = FileParser.New(ft, null, null, EnumDataFileScreen.DEUpload);
                FileReaderManager frm = new FileReaderManager(_ftr, fileParser);
                EnumParserResult processResult = frm.DEUpload(ft);
                switch (processResult)
                {
                    case EnumParserResult.UploadSucceed:
                        string systemMsg = _der.MoveFilesToProduction(ft) ?
                            "Upload process completed successfully" :
                            "Upload process completed with file transferring issues. Please consult system administrator for further details";
                        _der.UploadCompleted(ft);
                        return View(vm).Success(systemMsg);
                    case EnumParserResult.DTACtionsFailed:
                        return View(vm).Warning("DT actions failed due to some technical issue, please consult system administrator for further assistance.");
                    case EnumParserResult.UploadFailed:
                        return View(vm).Warning("Bulk copy process failed due to some technical issue, please consult system administrator for further assistance.");
                    case EnumParserResult.ConfigNotSet:
                        return View(vm).Warning("Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
                    case EnumParserResult.NoData:
                        return View(vm).Warning("Input data file is empty. No data to process.");
                    case EnumParserResult.ValidationFailed:
                        WriteCSVFile(fileParser.ValidationSummary.ToString());
                        return null;
                    default:
                        throw new Exception("Unexpected ParserResult from ProcessFile()");
                }
            }
            else if (Request.Form["PPSubmit"] != null)
            {
                FileParser fileParser = FileParser.New(ft, null, null, EnumDataFileScreen.GenerateFiles);
                FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

                switch ((EnumParserResult)frm.GenerateOutputFiles())
                {
                    case EnumParserResult.FileGenerationSucceed:
                        ReturnZipFile(ft);
                        return null;
                    case EnumParserResult.FileGenerationFailed:
                        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Output files generation process failed due to some technical issue, please consult system administrator for further assistance.");
                    case EnumParserResult.DTACtionsFailed:
                        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("DT actions failed due to some technical issue, please consult system administrator for further assistance.");
                    case EnumParserResult.PreGenActionsFailed:
                        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Pre-Gen actions failed due to some technical issue, please consult system administrator for further assistance.");
                    case EnumParserResult.ConfigNotSet:
                        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
                    case EnumParserResult.NoData:
                        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Input data file is empty. No data to process.");
                    case EnumParserResult.ValidationFailed:
                        WriteCSVFile(fileParser.ValidationSummary.ToString());
                        return null;
                    default:
                        throw new Exception("Unexpected ParserResult from ProcessFile()");
                }
            }

            throw new NotImplementedException("Request submit button name not found / not supported.");
        }
        //[HttpPost]
        //public ActionResult PreprocessData(FormCollection fc)
        //{

        //    VMFileUpload vm = new VMFileUpload();
        //    if (_ur.CurrentUser.DefaultClient != null)
        //        vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;
        //    vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();

        //    TryUpdateModel(vm);
        //    FileTemplate ft = _ftr.GetById(vm.SelectedFileTemplateId.Value);
        //    ft.PresetParameterizedDataFields(fc);

        //    if (ft.DataEntry == false || ft.DataEntryFormId == null)
        //    {
        //        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Configuration for data entry has not been set. Please update the file template and try again.");
        //    }

        //    FileParser fileParser = FileParser.New(ft, null, null, EnumDataFileScreen.GenerateFiles);
        //    FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

        //    switch ((EnumParserResult)frm.GenerateOutputFiles())
        //    {
        //        case EnumParserResult.FileGenerationSucceed:
        //            ReturnZipFile(ft);
        //            return null;
        //        case EnumParserResult.FileGenerationFailed:
        //            return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Output files generation process failed due to some technical issue, please consult system administrator for further assistance.");
        //        case EnumParserResult.DTACtionsFailed:
        //            return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("DT actions failed due to some technical issue, please consult system administrator for further assistance.");
        //        case EnumParserResult.PreGenActionsFailed:
        //            return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Pre-Gen actions failed due to some technical issue, please consult system administrator for further assistance.");
        //        case EnumParserResult.ConfigNotSet:
        //            return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
        //        case EnumParserResult.NoData:
        //            return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = vm.SelectedFileTemplateId }).Warning("Input data file is empty. No data to process.");
        //        case EnumParserResult.ValidationFailed:
        //            WriteCSVFile(fileParser.ValidationSummary.ToString());
        //            return null;
        //        default:
        //            throw new Exception("Unexpected ParserResult from ProcessFile()");
        //    }

        //}

        public ActionResult SelectFT()
        {
            VMFileUpload vm = new VMFileUpload();

            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;

            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }


        public ActionResult SelectAR()
        {
            VMFileUpload vm = new VMFileUpload();

            if (_ur.CurrentUser.DefaultClient != null)
                vm.SelectedClientId = _ur.CurrentUser.DefaultClient.Id;

            vm.Clients = _ur.CurrentUser.IsAdmin ? _cr.GetAll().ToList() : (List<Client>)_ur.GetCurrentUserAssociatedClients();
            return View(vm);
        }

        private FileTemplate GetSelectedFileTemplate(int ftId, bool refresh = false)
        {
            if (Session[LAST_FT] != null && ((FileTemplate)Session[LAST_FT]).Id == ftId && refresh == false)
                return (FileTemplate)Session[LAST_FT];

            FileTemplate ft = _ftr.GetById(ftId);
            Session.Add(LAST_FT, ft);
            return ft;
        }


        public ActionResult UploadPreview(int SelectedFileTemplateId)
        {
            FileTemplate ft = GetSelectedFileTemplate(SelectedFileTemplateId, true);

            return View(ft).Information("Number of appending cases: " + ft.DEForUpload.Count.ToString()); ;
        }

        public ActionResult Create(int SelectedFileTemplateId, int? RegionId, int? ClientId)
        {
            FileTemplate ft = GetSelectedFileTemplate(SelectedFileTemplateId);
            DEOHmcts deObj = new DEOHmcts(ft, GetOffenceCodeList(ft.Id), GetHMCTSArrestBreachClientList(ft.Id, RegionId), GetHMCTSBreachRegionList(ft.Id));
            if (RegionId.HasValue)
                deObj.RegionId = RegionId.Value;
            if (ClientId.HasValue)
                deObj.ClientId = ClientId.Value;

            return View(deObj);
        }

        [HttpPost]
        public ActionResult Create(int FileTemplateId, int RegionId, FormCollection collection, IEnumerable<HttpPostedFileBase> files)
        {
            FileTemplate ft = GetSelectedFileTemplate(FileTemplateId);
            DEOBase deObj = null;
            switch ((EnumDataEntryForm)ft.DataEntryFormId)
            {
                case EnumDataEntryForm.HMCTS:
                    deObj = new DEOHmcts(ft, GetOffenceCodeList(ft.Id), GetHMCTSArrestBreachClientList(ft.Id, RegionId), GetHMCTSBreachRegionList(ft.Id));
                    DEOHmcts hmcts = (DEOHmcts)deObj;

                    this.TryUpdateModel(hmcts);
                    if (!hmcts.IsFinancialDatedBreachCase)
                    {
                        if (!string.IsNullOrEmpty(hmcts.OffenceCodes) && hmcts.OffenceCodes.Contains("NWM;"))
                        {
                            hmcts.OffenceCodes = null;
                        }

                        if (files != null && files.Count() > 0)
                        {
                            //foreach (var item in files)
                            //{
                            //    if (item != null)
                            //    {
                            //        (hmcts).Attachments.Add(new DEOAttachment(hmcts, item));
                            //    }
                            //}
                            var arrFiles = files.ToArray();
                            for (int i = 0; i < arrFiles.Count(); i++)
                            {
                                if (arrFiles[i] != null)
                                {
                                    DEOAttachment att = new DEOAttachment(hmcts, arrFiles[i]);
                                    att.FileName = i.ToString() + "_" + att.FileName;
                                    (hmcts).Attachments.Add(att);
                                }

                            }

                        }
                    }

                    break;
                default:
                    break;
            }

            if (deObj.Validate(_der))
            {

                _der.Create(new DataEntry(ft, deObj));

                switch ((EnumDataEntryForm)ft.DataEntryFormId)
                {
                    case EnumDataEntryForm.HMCTS:
                        return RedirectToAction("Create", new { SelectedFileTemplateId = ft.Id, RegionId = ((DEOHmcts)deObj).RegionId, ClientId = ((DEOHmcts)deObj).ClientId }).Success("The case has been created successfully.");
                    default:
                        return RedirectToAction("Create", new { SelectedFileTemplateId = ft.Id }).Success("The case has been created successfully.");
                }
            }
            else
            {
                if (deObj.Errors.Length > 0)
                {
                    //modelState.Clear();
                    foreach (var modelValue in ModelState.Values)
                    {
                        modelValue.Errors.Clear();
                    }

                    return View(deObj).Warning("Failed to create the case, please correct the error and try again. " + deObj.Errors.ToString());
                }

                return View(deObj).Warning("Failed to created the case, please correct the error and try again.");
            }
        }

        public ActionResult Edit(int Id)
        {
            DataEntry de = _der.GetDataEntryById(Id);
            if (de.FileTemplate.Disabled || !de.Editable)
                return RedirectToAction("AccessDenied", "Error");

            switch ((EnumDataEntryForm)de.FileTemplate.DataEntryFormId)
            {
                case EnumDataEntryForm.HMCTS:
                    de.DEOHmcts.RegionsList = GetHMCTSBreachRegionList(de.FileTemplate.Id);
                    de.DEOHmcts.ClientsList = GetHMCTSArrestBreachClientList(de.FileTemplate.Id, de.DEOHmcts.RegionId);
                    de.DEOHmcts.OffenceCodesList = GetOffenceCodeList(de.FileTemplate.Id);
                    de.DEOHmcts.DEId = de.Id;
                    break;
                default:
                    break;
            }

            return View(de);
        }

        [HttpPost]
        public ActionResult Edit(int DEId, FormCollection collection, IEnumerable<HttpPostedFileBase> files)
        {
            DataEntry de = _der.GetDataEntryById(DEId);
            if (de.FileTemplate.Disabled || !de.Editable)
                return RedirectToAction("AccessDenied", "Error");

            DEOBase deObj = null;
            switch ((EnumDataEntryForm)de.FileTemplate.DataEntryFormId)
            {
                case EnumDataEntryForm.HMCTS:
                    var hmcts = de.DEOHmcts;
                    this.TryUpdateModel(hmcts);
                    if (!hmcts.IsFinancialDatedBreachCase)
                    {
                        if (!string.IsNullOrEmpty(hmcts.OffenceCodes) && hmcts.OffenceCodes.Contains("NWM;"))
                        {
                            hmcts.OffenceCodes = null;
                        }

                        if (files != null && files.Count() > 0)
                        {
                            DEOAttachment[] deoFiles = new DEOAttachment[4];
                            var arrFiles = files.ToArray();
                            for (int i = 0; i < arrFiles.Length; i++)
                            {
                                if (arrFiles[i] != null)
                                {
                                    DEOAttachment att = new DEOAttachment(hmcts, arrFiles[i]);
                                    att.FileName = i.ToString() + "_" + att.FileName;
                                    (hmcts).Attachments.Add(att);
                                    deoFiles[i] = att;
                                }

                            }
                            hmcts.Attachments = deoFiles.ToList();
                        }
                    }

                    hmcts.DEId = de.Id;
                    deObj = hmcts;
                    break;
                default:
                    break;
            }

            deObj.FileTemplate = de.FileTemplate;

            if (deObj.Validate(_der))
            {

                _der.Update(de);

                switch ((EnumDataEntryForm)de.FileTemplate.DataEntryFormId)
                {
                    case EnumDataEntryForm.HMCTS:
                        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = de.FileTemplateId}).Success("The case has been updated successfully.");
                    default:
                        return RedirectToAction("UploadPreview", new { SelectedFileTemplateId = de.FileTemplateId }).Success("The case has been created successfully.");
                }
            }
            else
            {
                de.DEOHmcts.RegionsList = GetHMCTSBreachRegionList(de.FileTemplate.Id);
                de.DEOHmcts.ClientsList = GetHMCTSArrestBreachClientList(de.FileTemplate.Id, de.DEOHmcts.RegionId);
                de.DEOHmcts.OffenceCodesList = GetOffenceCodeList(de.FileTemplate.Id);

                if (deObj.Errors.Length > 0)
                {
                    foreach (var modelValue in ModelState.Values)
                    {
                        modelValue.Errors.Clear();
                    }

                    return View(de).Warning("Failed to create the case, please correct the error and try again. " + deObj.Errors.ToString());
                }

                return View(de).Warning("Failed to created the case, please correct the error and try again.");
            }
        }


        [HttpPost]
        [ReadOnlyAuthorizeUser]
        public ActionResult Delete(int id)
        {
            DataEntry de = _der.GetDataEntryById(id);
            int ftId = de.FileTemplateId;
            _der.Delete(de);
            List<DataEntry> appendingCases = new List<DataEntry>();
            if (_ur.CurrentUser.RoleId > (int)EnumRole.ReadOnly)
            {
                appendingCases = _der.GetAll(x => x.FileTemplateId == ftId && x.Uploaded == false && x.Deleted == false).ToList();
            }
            else
            {
                appendingCases = _der.GetAll(x => x.FileTemplateId == ftId && x.UserId == _ur.CurrentUser.Id && x.Uploaded == false && x.Deleted == false).ToList();
            }

            return PartialView("_" + ((EnumDataEntryForm)((FileTemplate)Session["LAST_FILE_TEMPLATE"]).DataEntryFormId).ToString() + "_UploadPreview", appendingCases).Information("Number of appending cases: " + appendingCases.Count.ToString());
        }

        private const string SEARCH_DEAudit_PRMS = "DEAuditSearchPrms";
        protected DEAuditSearchPrms<DEARBase> CurrentDEAuditSearchPrms
        {
            get
            {
                return Session[SEARCH_DEAudit_PRMS] == null ? null : (DEAuditSearchPrms<DEARBase>)this.Session[SEARCH_DEAudit_PRMS];
            }
            set
            {
                Session.Add(SEARCH_DEAudit_PRMS, value);
            }
        }


        public ActionResult AuditReport(int SelectedFileTemplateId,string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, DateTime? fromDate, DateTime? toDate, int? clientId)
        {
            //FileTemplate ft = GetSelectedFileTemplate(SelectedFileTemplateId, false);
            FileTemplate ft = _ftr.GetById(SelectedFileTemplateId);

            DEAuditSearchPrms<DEARBase> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentDEAuditSearchPrms != null)
            {
                searchPrms = CurrentDEAuditSearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                switch ((EnumDataEntryForm)ft.DataEntryFormId)
                {
                    case EnumDataEntryForm.HMCTS:
                        DataTable dt = _der.GetHMCTSBreachClients(SelectedFileTemplateId);
                        var rows = dt.Select();
                        var clients = rows.Select(x => new SelectListItem() { Text = x["ClientName"].ToString(), Value = x["ClientId"].ToString() }).ToList();
                        searchPrms = new DEAuditSearchPrms<DEARBase>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, fromDate.HasValue ? fromDate.Value:DateTime.Today , toDate.HasValue ? toDate.Value : DateTime.Today, clientId, clients, SelectedFileTemplateId, _ur.GetAll().ToList(), (EnumDataEntryForm)ft.DataEntryFormId, 0,0,0 );
                        break;
                    default:
                        break;
                }

            }

            IEnumerable<DEARBase> results = null;

            bool succeed = _der.GetHMCTSAuditReport(searchPrms, ft, ref results);

            CurrentDEAuditSearchPrms = new DEAuditSearchPrms<DEARBase>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, results.ToList(), searchPrms.FromDate, searchPrms.ToDate, searchPrms.ClientId, searchPrms.Clients, ft.Id, searchPrms.Users, (EnumDataEntryForm)ft.DataEntryFormId, searchPrms.CasesImported, searchPrms.CasesNotImported, searchPrms.CasesWithError);
            if (succeed)
                return View(CurrentDEAuditSearchPrms).Information("No. Cases found: " + CurrentDEAuditSearchPrms.RowsCount.ToString());

            return View(CurrentDEAuditSearchPrms).Warning("Failed to retrieve audit information from the selected file template. Please see System Error Log for further details.");
        }

        public ActionResult GetAttachment(int id)
        {

            string[] data;

            DEAttachment attachment = _der.GetDEAttachmentById(id);
            string stagingFolder = Utility.GetAttachmentsFolder(Utility.StagingAttachmentsFolder, attachment.DataEntry.DateAdded);
            string fullPath = Path.Combine(Utility.StagingAttachmentsFolder, attachment.FileName);

            if (System.IO.File.Exists(fullPath))
            {
                using (Stream stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read,
                                        FileShare.ReadWrite))
                {
                    data = stream.ReadAllLines();
                    stream.Close();
                }

                if (data.Length > 0)
                {

                    Response.ClearHeaders();
                    Response.ContentType = attachment.ContentType;
                    switch ((EnumDataEntryForm)attachment.DataEntry.FileTemplate.DataEntryFormId)
                    {
                        case EnumDataEntryForm.HMCTS:
                            Response.AppendHeader("content-disposition", "attachment; filename=\"" + attachment.OriginalFileName.Remove(0,2) + "\"");
                            break;
                        default:
                            Response.AppendHeader("content-disposition", "attachment; filename=\"" + attachment.OriginalFileName + "\"");
                            break;
                    }
                    //Response.CacheControl = "Private";
                    //Response.Cache.SetExpires(DateTime.Now.AddMinutes(3)); // or put a timestamp in the filename in the content-disposition
                    Response.Write(string.Join(Environment.NewLine, data));
                    Response.End();
                }
                else
                {
                    Response.Write("The selected file is empty.");
                    Response.Flush();
                }
            }
            else
            {
                Response.Write("The selected file does not exist. Please consult system administrator for further assistance.");
                Response.Flush();
            }

            return null;
        }
    }
}
