﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class ReferenceListRepository : RepositoryBase<DataIntegrationEntities, ReferenceList>
    {        
        public ReferenceListRepository(DataIntegrationEntities entities) : base(entities) { }

        public override IQueryable<ReferenceList> GetAll()
        {
            return GetAll(x => true);
        }

        public ReferenceList GetById(int id)
        {
            return GetAll(x => x.Id == id).FirstOrDefault();
        }

        public void Create(ReferenceList referenceList)
        {
            Add(referenceList);
            SubmitChanges();            
        }

        public void Update(ReferenceList referenceList)
        {
            SubmitChanges();
        }

        public void Delete(ReferenceList referenceList)
        {
            referenceList.Disabled = true;
            SubmitChanges();
        }

        public void AddListItem(ReferenceListItem item)
        {
            item.Value = item.Value.ToUpper();
            _entities.ReferenceListItems.Add(item);
            SubmitChanges();
        }

        public int DeleteReferenceListItem(int id)
        {
            ReferenceListItem item =  _entities.ReferenceListItems.Where(x => x.Id == id).FirstOrDefault();
            int rlId = item.ReferenceListId;
            _entities.ReferenceListItems.Remove(item);
            SubmitChanges();
            return rlId;
        }
    }
}