﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitNameColumnIntoThree : PrmsBase
    {
        public PrmsSplitNameColumnIntoThree() { }
        public PrmsSplitNameColumnIntoThree(DTAction dt) : base(dt) { }

        [Display(Name = "Input Column Name")]
        public string InputColumnName { get; set; }
        [Display(Name = "Output Column Names")]
        public string OutputColumnNames { get; set; }


    }
}