﻿CREATE TABLE [dbo].[StoredProcedure] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [SPName]         VARCHAR (50)   NOT NULL,
    [Description]    NVARCHAR (250) NULL,
    [Params]         VARCHAR (1000) NULL,
    [DBConnectionId] INT            NOT NULL,
    [SPType]         INT            NOT NULL,
    CONSTRAINT [PK_StoredProcedures] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_StoredProcedure_DbConnection] FOREIGN KEY ([DBConnectionId]) REFERENCES [dbo].[DbConnection] ([Id])
);

