﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class ARPrmsDisplayMnCrBalance : ARPrmsBase
    {
        public ARPrmsDisplayMnCrBalance() { }
        public ARPrmsDisplayMnCrBalance(ARAction ar) : base(ar) { }

        [Display(Name = "Input Column Names")]
        public string ColumnNames { get; set; }
        [Display(Name = "Output Column Names")]
        public string OutputColumnNames { get; set; }

        private List<string> inputColumnNamesList;
        [JsonIgnore]
        public List<string> InputColumnNamesList
        {
            get
            {
                if (inputColumnNamesList == null && !string.IsNullOrEmpty(ColumnNames))
                {
                    inputColumnNamesList = ColumnNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();

                }

                return inputColumnNamesList;
            }

        }

        private List<string> outputColumnNamesList;
        [JsonIgnore]
        public List<string> OutputColumnNamesList
        {
            get
            {
                if (outputColumnNamesList == null && !string.IsNullOrEmpty(OutputColumnNames))
                {
                    outputColumnNamesList = OutputColumnNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();

                }

                return outputColumnNamesList;
            }

        }
    }
}