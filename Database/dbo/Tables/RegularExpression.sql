﻿CREATE TABLE [dbo].[RegularExpression] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [RegExText]   NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (250) NULL,
    CONSTRAINT [PK_RegularExpression] PRIMARY KEY CLUSTERED ([Id] ASC)
);

