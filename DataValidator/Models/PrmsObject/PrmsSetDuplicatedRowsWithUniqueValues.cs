﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSetDuplicatedRowsWithUniqueValues : PrmsBase
    {
        public PrmsSetDuplicatedRowsWithUniqueValues() { }
        public PrmsSetDuplicatedRowsWithUniqueValues(DTAction dt) : base(dt) { }

        [Display(Name = "Group By Column Names")]
        public string GroupByColumnNames { get; set; }
        [Display(Name = "Field Requires Unique Value")]
        public string FieldRequiresUniqueValue { get; set; }
        [Display(Name = "Separator")]
        public string Separator { get; set; }
        [Display(Name = "Replace Original Val")]
        public bool ReplaceOriginalVal { get; set; }
        //[Display(Name = "User Random Texts")]
        //public bool UseRandomTexts { get; set; }
        [Display(Name = "Order By Column Name")]
        public string OrderByColumnName { get; set; }
        [Display(Name = "Unique Value Type")]
        public int UniqueValueType { get; set; }
        [Display(Name = "Filter")]
        public string Filter { get; set; }

    }
}