﻿CREATE TABLE [dbo].[DbConnection] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (50)  NOT NULL,
    [ConnectionString] VARCHAR (500) NOT NULL,
    [Disabled]         BIT           CONSTRAINT [DF_DbConnection_Disabled] DEFAULT ((0)) NOT NULL,
    [DBServerType]     INT           NOT NULL,
    CONSTRAINT [PK_DbConnection] PRIMARY KEY CLUSTERED ([Id] ASC)
);

