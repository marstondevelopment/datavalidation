﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class PrmsGetDataFromDB : PrmsBase,IDBCmd
    {
        public PrmsGetDataFromDB() { }
        public PrmsGetDataFromDB(DTAction dt) : base(dt) { }

        [Display(Name = "Get Method")]
        public int GetMethodId { get; set; }

        [JsonIgnore]
        public EnumValidationMethod GetMethod => (EnumValidationMethod)GetMethodId;

        [Display(Name = "Command Text")]
        public int? CmdTextId { get; set; }
        [JsonIgnore]
        public CmdText CmdText { get; set; }

        [Display(Name = "Stored Procedure")]
        public int? StoredProcedureId { get; set; }
        [JsonIgnore]
        public StoredProcedure StoredProcedure { get; set; }

        [Display(Name = "Exclude Not Found Item")]
        public bool ExcludeNotFound { get; set; }

        [Display(Name = "Fields To Populate")]
        public string FieldsToPopulate { get; set; }
    }
}