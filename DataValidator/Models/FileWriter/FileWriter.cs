﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Data;

namespace Mgl.DataValidator.Models
{
    public abstract class FileWriter
    {
        public FileTemplate FileTemplate { get; set; }
        public List<OutputFile> OutputFiles { get => outputFiles; set => outputFiles = value; }

        private List<OutputFile> outputFiles = new List<OutputFile>();

        public OutputFile ProcessingFile { get;  set; }

        protected readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public FileWriter(FileTemplate ft)
        {
            FileTemplate = ft;
        }
        public bool GenerateFiles()
        {
            if (GenerateOutputDataFiles())
            {
                if (FileTemplate.GenerateAuditFile)
                {
                    return GenerateAuditFile();
                }
                return true;
            }

            return false;
        }
        public static FileWriter New(FileTemplate ft)
        {
            if (ft.DTFileWriter == null)
            {
                throw new Exception("System failed to initialise a file writer as DTFileWriter had not been configured. Update the file template and try again.");
            }

            switch ((EnumFileWriter)ft.DTFileWriter)
            {
                case EnumFileWriter.Default:
                    return new DefaultFileWriter(ft);
                case EnumFileWriter.OneStep:
                    return new OSFileWriter(ft);
                default:
                    throw new Exception("File writer " + ((EnumFileWriter)ft.DTFileWriter).ToString() + " not supported.");
            }

        }

        public void ExcludeColumns(DataTable dt)
        {
            var excludedCols = FileTemplate.BodyCols.Where(x => x.ExcludeFromOutput == true).ToList();
            bool changeStatus = false;
            foreach (var item in excludedCols)
            {
                if (dt.Columns.Contains(item.Name))
                {
                    dt.Columns.Remove(item.Name);
                    changeStatus = true;
                }
            }
            if (changeStatus)
            {
                dt.AcceptChanges();
            }
        }

        public void AddDefaultOutputFiles()
        {
            if (FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output).FirstOrDefault() == null)
            {
                if (FileTemplate.ProcessedDataTable.Rows.Count > 0)
                {
                    var processedDT = FileTemplate.ProcessedDataTable.Select().CopyToDataTable();

                    OutputFiles.Add(new OutputFile(this)
                    {
                        Data = processedDT,
                        Header = FileTemplate.HeaderDataTable,
                        Trailer = FileTemplate.TrailerDataTable,
                        FileNamePrefix = FileTemplate.ParentDataFile.FileNameNoExt,
                        FileCategory = EnumOutputFileCategory.Output,
                        FileDelimiter = FileTemplate.OutputFileDelimiter
                    });

                }

            }

        }

        public void AddExcludedFiles()
        {
            if (FileTemplate.ExcludedDataTable != null && FileTemplate.ExcludedDataTable.Rows.Count > 0)
            {
                var excludedDT = FileTemplate.ExcludedDataTable.Select().CopyToDataTable();
                //ExcludeColumns(excludedDT);
                OutputFiles.Add(new OutputFile(this)
                {
                    Data = excludedDT,
                    FileNamePrefix = "Ignored",
                    FileCategory = EnumOutputFileCategory.Ignored,
                    FileDelimiter = FileTemplate.OutputFileDelimiter
                });
            }
        }

        protected abstract bool GenerateOutputDataFiles();

        private string RenderScriptFromVariables(string script)
        {
            foreach (var item in FileTemplate.Variables)
            {                
                script = script.RenderTag(item.Name, item.FormatedOutputValue);
            }

            return script;
        }


        public bool GenerateAuditFile()
        {
            try
            {
                string arScript = FileTemplate.ARTemplateScript;

                arScript = RenderScriptFromVariables(arScript);

                if (this.FileTemplate.GenerateAuditFile)
                {
                    var arInputDataFiles = FileTemplate.FileTemplateARActions.Where(x => x.ARAction.DataSourceId == (int)EnumARSourceFile.InputDataFile).ToList();
                    var arPerOutputDataFile = FileTemplate.FileTemplateARActions.Where(x => x.ARAction.DataSourceId == (int)EnumARSourceFile.PerOutputDataFile).ToList();
                    var arAllOutputDataFiles = FileTemplate.FileTemplateARActions.Where(x => x.ARAction.DataSourceId == (int)EnumARSourceFile.AllOutputDataFiles).ToList();
                    var arPerIgnoredFile = FileTemplate.FileTemplateARActions.Where(x => x.ARAction.DataSourceId == (int)EnumARSourceFile.PerIgnoredFile).ToList();
                    var arAllIgnoredFiles = FileTemplate.FileTemplateARActions.Where(x => x.ARAction.DataSourceId == (int)EnumARSourceFile.AllIgnoredFiles).ToList();


                    // Render section for each output file
                    if (arPerOutputDataFile.Count > 0)
                    {
                        string subScriptTemplate = arScript.GetValueFromSectionTag(EnumARReservedTags.PEROUTPUTFILE.ToString());
                        StringBuilder renderedString = new StringBuilder();

                        if (!string.IsNullOrEmpty(subScriptTemplate))
                        {
                            foreach (var outputFile in this.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output))
                            {
                                ProcessingFile = outputFile;
                                string scriptTxt = subScriptTemplate;
                                foreach (var arAction in arPerOutputDataFile)
                                {
                                    arAction.ARStep.Execute(ref scriptTxt);
                                }
                                renderedString.AppendLine(scriptTxt);
                            }
                            arScript = arScript.RenderSectionTag(EnumARReservedTags.PEROUTPUTFILE.ToString(), renderedString.ToString() ?? string.Empty);
                        }
                    }

                    // Render tags for data extracted from input file
                    if (arInputDataFiles.Count > 0)
                    {
                        foreach (var arAction in arInputDataFiles)
                        {
                            arAction.ARStep.Execute(ref arScript);
                        }
                    }

                    // Render tags for data extracted from all output files
                    if (arAllOutputDataFiles.Count > 0)
                    {
                        foreach (var arAction in arAllOutputDataFiles)
                        {
                            arAction.ARStep.Execute(ref arScript);
                        }
                    }

                    // Render section for each ignored file
                    if (arPerIgnoredFile.Count > 0)
                    {
                        string subScriptTemplate = arScript.GetValueFromSectionTag(EnumARReservedTags.PERIGNOREDFILE.ToString());
                        StringBuilder renderedString = new StringBuilder();

                        if (!string.IsNullOrEmpty(subScriptTemplate))
                        {
                            foreach (var ignoredFile in this.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Ignored))
                            {
                                ProcessingFile = ignoredFile;
                                string scriptTxt = subScriptTemplate;
                                foreach (var arAction in arPerIgnoredFile)
                                {
                                    arAction.ARStep.Execute(ref scriptTxt);
                                }
                                renderedString.AppendLine(scriptTxt);
                            }
                            arScript = arScript.RenderSectionTag(EnumARReservedTags.PERIGNOREDFILE.ToString(), renderedString.ToString() ?? string.Empty);
                        }
                    }

                    OutputFiles.Add(new OutputFile(this)
                    {
                        Context = arScript,
                        FileCategory = EnumOutputFileCategory.Report,
                        FileNamePrefix = FileTemplate.ParentDataFile.FileNameNoExt
                    });


                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;

            }
        }

        public OutputFile GetDataOutputFile(string schemeName, EnumOutputFileCategory outputFileCategory)
        {
            OutputFile oFile = OutputFiles.Where(x => x.FileNamePrefix == schemeName  && x.FileCategory == outputFileCategory).FirstOrDefault();
            if (oFile == null)
            {

                switch (outputFileCategory)
                {
                    case EnumOutputFileCategory.Output:
                        oFile = new OutputFile(this)
                        {
                            Data = FileTemplate.ProcessedDataTable.Clone(),
                            Header = FileTemplate.HeaderDataTable,
                            Trailer = FileTemplate.TrailerDataTable,
                            FileNamePrefix = schemeName,
                            FileCategory = EnumOutputFileCategory.Output,
                            FileDelimiter = FileTemplate.OutputFileDelimiter
                        };
                        break;
                    case EnumOutputFileCategory.Ignored:
                        oFile = new OutputFile(this)
                        {
                            Data = FileTemplate.BUDataTable.Clone(),
                            FileNamePrefix = schemeName,
                            FileCategory = EnumOutputFileCategory.Ignored,
                            FileDelimiter = FileTemplate.OutputFileDelimiter
                        };
                        break;
                    case EnumOutputFileCategory.Exception:
                        oFile = new OutputFile(this)
                        {
                            Data = FileTemplate.BUDataTable.Clone(),
                            FileNamePrefix = schemeName,
                            FileCategory = EnumOutputFileCategory.Exception,
                            FileDelimiter = FileTemplate.OutputFileDelimiter
                        };
                        break;
                    case EnumOutputFileCategory.Report:
                        break;
                    default:
                        break;
                }

                OutputFiles.Add(oFile);
            }
            return oFile;
        }

    }
}