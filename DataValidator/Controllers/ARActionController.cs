﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class ARActionController : ControllerBase
    {
        private ARActionRepository _arr;
        private const string SEARCH_PRMS = "ARSearchPrms";
        public ARActionController(ARActionRepository arr)
        {
            _arr = arr;
        }

        protected ARSearchPrms<ARAction> CurrentSearchPrms
        {
            get
            {
                return Session[SEARCH_PRMS] == null ? null : (ARSearchPrms<ARAction>)this.Session[SEARCH_PRMS];
            }
            set
            {
                Session.Add(SEARCH_PRMS, value);
            }
        }

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, int? arActionTypeId)
        {
            ARSearchPrms<ARAction> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentSearchPrms != null)
            {
                searchPrms = CurrentSearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                searchPrms = new ARSearchPrms<ARAction>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, arActionTypeId);
            }

            ARActionViewModel vm = new ARActionViewModel(_arr, null);

            vm.ARActions = _arr.GetAll(x => x.Disabled == false && (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || (x.Description.Contains(searchPrms.SearchString)))) && (!searchPrms.ARActionTypeId.HasValue || x.ARActionTypeId == searchPrms.ARActionTypeId.Value)).OrderByDescending(ft => ft.DateUpdated).ToList();

            CurrentSearchPrms = new ARSearchPrms<ARAction>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, vm.ARActions.ToList(), searchPrms.ARActionTypeId);
            return View(CurrentSearchPrms).Information("Search results found: " + CurrentSearchPrms.RowsCount.ToString());
        }

        public ActionResult Details(int id)
        {
            ARActionViewModel vm = new ARActionViewModel(_arr, _arr.GetARActionById(id));
            vm.IsDetails = true;
            return View(vm);
        }
        
        private string ValidateARActionTypeFields(int arActionTypeId)
        {
            string message = string.Empty;
            switch ((EnumARActionType)arActionTypeId)
            {
                case EnumARActionType.DisplayMnCrBalance:
                    if (string.IsNullOrEmpty(Request.Form["ARAction.ARPrmsDisplayMnCrBalance.ColumnNames"]) ||
                        string.IsNullOrEmpty(Request.Form["ARAction.ARPrmsDisplayMnCrBalance.OutputColumnNames"]))
                        return message = "Column Names and/or Output Column Names are Required.";
                    break;
                case EnumARActionType.GetDateTime:
                    if (string.IsNullOrEmpty(Request.Form["ARAction.ARPrmsGetDateTime.DateFormat"]))
                        return message = "Date Format is Required.";
                    break;
                case EnumARActionType.GetOutputFileTotalBalance:
                    if (string.IsNullOrEmpty(Request.Form["ARAction.ARPrmsGetOutputFileTotalBalance.InputColumnName"]))
                        return message = "Input Column Name is Required.";
                    break;
                case EnumARActionType.GetTotalBalanceOfNewCases:
                    if (string.IsNullOrEmpty(Request.Form["ARAction.ARPrmsGetTotalBalanceOfNewCases.ColumnName"]) ||
                        string.IsNullOrEmpty(Request.Form["ARAction.ARPrmsGetTotalBalanceOfNewCases.OutputFormat"]))
                        return message = "Column Name and/or Output Format are Required.";
                    break;
            }
            return message;
        }

        public ActionResult GetARActionParam(int? arActionTypeId, int? arActionId)
        {

            var vm = arActionId.HasValue 
                ? new ARActionViewModel(_arr, _arr.GetARActionById(arActionId.Value))
                : new ARActionViewModel(_arr, null);

            if (arActionTypeId.HasValue)
            {
                switch ((EnumARActionType)arActionTypeId)
                {
                    case EnumARActionType.DisplayMnCrBalance:
                    case EnumARActionType.GetDateTime:
                    case EnumARActionType.GetOutputFileTotalBalance:
                    case EnumARActionType.GetTotalBalanceOfNewCases:
                    case EnumARActionType.GetDataFromGeneratedFile:
                    case EnumARActionType.GetClientScheme:
                    case EnumARActionType.GetOutputFileRowCount:
                        return PartialView("_ARPrms" + ((EnumARActionType)arActionTypeId).ToString(), vm);
                    default:
                        return null;
                }
            }

            return null;
        }

        public ActionResult Create()
        {
            ARActionViewModel vm = new ARActionViewModel(_arr, null);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(ARAction aRAction, FormCollection collection)
        {
            ARActionViewModel vm = new ARActionViewModel(_arr, aRAction);
            string message = string.Empty;

            if (vm.ARAction != null)
            {
                message = ValidateARActionTypeFields(vm.ARAction.ARActionTypeId);
                if (string.IsNullOrEmpty(message))
                {
                    _arr.Create(vm.ARAction);
                    return RedirectToAction("Index").Success("AR Action has been created successfully.");
                }
            }
            return View(vm).Danger(message);
        }

        public ActionResult Clone(int? id)
        {
            if(id.HasValue)
            {
                ARAction currentARAction = _arr.GetARActionById(id.Value);
                ARAction clonedARAction = currentARAction.Clone;
                _arr.Create(clonedARAction);
                return RedirectToAction("Edit", new { id = clonedARAction.Id }).Success("Clone of " + currentARAction.Name + " has been created successfully.");
            }
            return RedirectToAction("Index").Danger("AR Action failed to clone.");
        }

        public ActionResult Edit(int id)
        {
            ARActionViewModel vm = new ARActionViewModel(_arr, _arr.GetARActionById(id));

            if (vm.ARAction.FileTemplateARActions.Count > 1 && !string.IsNullOrEmpty(vm.ARAction.AssociatedFileTemplates))
                return View(vm).Warning("Warning: This AR Action has been referenced by the following file templates. You are advised to implement system risk assessment before making any changes.<br/>" + vm.ARAction.AssociatedFileTemplatesForCookie);

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            ARActionViewModel vm = new ARActionViewModel(_arr, _arr.GetARActionById(id));
            string message = string.Empty;
            TryUpdateModel(vm);

            if (vm.ARAction != null)
            {
                message = ValidateARActionTypeFields(vm.ARAction.ARActionTypeId);

                if (string.IsNullOrEmpty(message))
                {
                    _arr.Update(vm.ARAction);

                    if (!string.IsNullOrEmpty(Request.Form["UrlRef"]))
                    {
                        return Redirect(Request.Form["UrlRef"]).Success(vm.ARAction.Name + " AR Action has been updated successfully");
                    }
                    return RedirectToAction("Index").Success(vm.ARAction.Name + " AR Action has been updated successfully");
                }
                else
                {
                    return RedirectToAction("Edit", new { id = vm.ARAction.Id }).Danger(message);
                }
            }
            return RedirectToAction("Index").Danger("AR Action failed to update.");
        }

        public ActionResult Delete(int id)
        {
            ARActionViewModel vm = new ARActionViewModel(_arr, _arr.GetARActionById(id));

            if (vm.ARAction != null)
            {
                int associatedTemplates = vm.ARAction.FileTemplateARActions.Where(x => !x.FileTemplate.Disabled).Count();

                if (associatedTemplates > 0)
                {
                    return RedirectToAction("Index").Danger("Failed to delete " + vm.ARAction.Name + " as some active file templates are referencing it.");
                }

                _arr.Delete(vm.ARAction);

                ARSearchPrms<ARAction> searchPrms = CurrentSearchPrms;

                vm.ARActions = _arr.GetAll(x => x.Disabled == false && (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || (x.Description.Contains(searchPrms.SearchString)))) && (!searchPrms.ARActionTypeId.HasValue || x.ARActionTypeId == searchPrms.ARActionTypeId.Value)).OrderByDescending(ft => ft.DateUpdated).ToList();

                CurrentSearchPrms = new ARSearchPrms<ARAction>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, vm.ARActions.ToList(), searchPrms.ARActionTypeId);
                return PartialView("_ARActionList",CurrentSearchPrms).Information("Search results found: " + CurrentSearchPrms.RowsCount.ToString());
            }

            return View(vm).Danger(vm.ARAction.Name + " has failed to delete.");
        }


    }
}
