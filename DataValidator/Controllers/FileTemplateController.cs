﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [ReadWriteAuthorizeUser]
    public class FileTemplateController : ControllerBase
    {
        private FileTemplateRepository _ftr;
        private ClientRepository _cr;
        private DataFieldRepository _dfr;
        private UserRepository _ur;
        private const string SEARCH_PRMS = "FTSearchPrms";

        public FileTemplateController(FileTemplateRepository ftr, ClientRepository cr, DataFieldRepository dfr, UserRepository ur)
        {
            _ftr = ftr;
            _cr = cr;
            _dfr = dfr;
            _ur = ur;
        }

        protected FTSearchPrms<FileTemplate> CurrentSearchPrms
        {
            get
            {
                return Session[SEARCH_PRMS] == null ? null : (FTSearchPrms<FileTemplate>)this.Session[SEARCH_PRMS];
            }
            set
            {
                Session.Add(SEARCH_PRMS, value);
            }
        }

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, int? clientId)
        {
            FTSearchPrms<FileTemplate> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentSearchPrms != null)
            {
                searchPrms = CurrentSearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                searchPrms = new FTSearchPrms<FileTemplate>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, clientId, null);
            }

            var vm = new FileTemplateViewModel(_ftr, null, null);
            List<SelectListItem> clients = CurrentSearchPrms == null ? null : CurrentSearchPrms.Clients;
            if (_ur.CurrentUser.IsAdmin)
            {
                vm.FileTemplates = _ftr.GetAll(x => x.Disabled == false && (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || (x.Description.Contains(searchPrms.SearchString)))) && (!searchPrms.ClientId.HasValue || x.ClientId == searchPrms.ClientId.Value)).OrderByDescending(ft => ft.DateUpdated).ToList();
                if (clients == null)
                    clients = _cr.GetAll().Where(x => x.Disabled == false).Select(y => new SelectListItem() { Text = y.Name, Value = y.Id.ToString() }).ToList();
            }
            else
            {
                User user = _ur.GetUserById(_ur.CurrentUser.Id);
                vm.FileTemplates = user.AssociatedFileTemplates.Where(x => x.Disabled == false && (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || (x.Description.Contains(searchPrms.SearchString)))) && (!searchPrms.ClientId.HasValue || x.ClientId == searchPrms.ClientId.Value)).OrderByDescending(ft => ft.DateUpdated).ToList();
                if (clients == null)
                    clients = user.AssociatedClients.Where(x => x.Disabled == false).Select(y => new SelectListItem() { Text = y.Name, Value = y.Id.ToString() }).ToList();
            }
            CurrentSearchPrms = new FTSearchPrms<FileTemplate>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, vm.FileTemplates.ToList(), searchPrms.ClientId, clients);

            return View(CurrentSearchPrms).Information("Search results found: " + CurrentSearchPrms.RowsCount.ToString());

        }
        [AdminOnlyAuthorizeUser]
        public ActionResult GetDeletedTemplates()
        {
            var vm = new FileTemplateViewModel(_ftr, null, null);
            vm.FileTemplates = _ftr.GetAll(x => true && x.Disabled == true).OrderByDescending(ft => ft.DateUpdated).ThenByDescending(ft => ft.DateCreated);
            return View(vm);
        }

        [HttpPost]
        public ActionResult SortDataFields(int? id, string DFs)
        {
            if (!id.HasValue || string.IsNullOrEmpty(DFs))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var vm = new FileTemplateViewModel(_ftr, _cr, _ftr.GetById(id.Value));

            if (!vm.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int[] orderedDFArr = DFs.Split(',').Select(x => int.Parse(x)).ToArray();
            
            vm.FTDataFields = _ftr.UpdateTemplateDataFieldList(vm.FileTemplate, orderedDFArr).ToList();
            return PartialView("_DataFieldList", vm);            
        }

        [HttpPost]
        public ActionResult SortDTActions(int? id, string DTAs)
        {
            if (!id.HasValue || string.IsNullOrEmpty(DTAs))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var vm = new FileTemplateViewModel(_ftr, _cr, _ftr.GetById(id.Value));

            if (!vm.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int[] orderedDTAArr = DTAs.Split(',').Select(x => int.Parse(x)).ToArray();

            vm.FTDTActions = _ftr.UpdateTemplateDTActionList(vm.FileTemplate, orderedDTAArr).ToList();
            return PartialView("_DTActionsList", vm);
        }

        public ActionResult Details(int id)
        {
            var filetemplate = _ftr.GetById(id);
            FileTemplateViewModel vm = new FileTemplateViewModel(_ftr, _cr, filetemplate);
            if (vm.FileTemplate.Disabled || !vm.FileTemplate.Editable)
                return RedirectToAction("AccessDenied", "Error");

            vm.IsDetails = true;
            vm.FTDataFields = (List<FileTemplateDataField>) _ftr.GetFTDataFields(vm.FileTemplate.Id).ToList();
            vm.FTDTActions = _ftr.GetFTDTActions(vm.FileTemplate.Id);
            vm.FTARActions = _ftr.GetFTARActions(vm.FileTemplate.Id);
            return View(vm);
        }

        public ActionResult Create()
        {
            var vm = new FileTemplateViewModel(_ftr, _cr, null);
            return View(vm);
        }

        private void SetDEFileWriter(FileTemplate ft)
        {
            if (ft.DataEntry)
            {
                ft.OutputFileDelimiterId = 1; // CSV
                ft.OutputFileEncoding = (int)EnumOutputFileEncoding.UTF8;
                ft.DTFileWriter = (int)EnumFileWriter.OneStep;
                ft.OFExcludeColumnHeader = false;
            }
        }

        [HttpPost]
        public ActionResult Create(FormCollection form)
        {
            var vm = new FileTemplateViewModel(_ftr, _cr, null);
            TryUpdateModel(vm);
            if (ModelState.IsValid)
            {
                SetDEFileWriter(vm.FileTemplate);
                _ftr.Create(vm.FileTemplate);
                return RedirectToAction("Edit", new { id = vm.FileTemplate.Id }).Success(vm.FileTemplate.Name + " has been created successfully.");
            }
            return View(vm).Danger("The template failed to create.");
        }       

        public ActionResult Edit(int id)
        {
            var vm = new FileTemplateViewModel(_ftr, _cr, _ftr.GetById(id));

            if (vm.FileTemplate.Disabled || !vm.FileTemplate.Editable)
                return RedirectToAction("AccessDenied", "Error");

            vm.FTDataFields = (List< FileTemplateDataField >) _ftr.GetFTDataFields(vm.FileTemplate.Id);
            vm.FTDTActions =  _ftr.GetFTDTActions(vm.FileTemplate.Id);
            vm.FTARActions = _ftr.GetFTARActions(vm.FileTemplate.Id);
            return View(vm);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, FormCollection fc)
        {
            var vm = new FileTemplateViewModel(_ftr, _cr, _ftr.GetById(id));
            this.TryUpdateModel(vm);

            if (ModelState.IsValid)
            {
                if (vm.FileTemplate.Disabled || !vm.FileTemplate.Editable)
                    return RedirectToAction("AccessDenied", "Error");

                if (!vm.FileTemplate.GenerateOutputFile || !vm.FileTemplate.GenerateAuditFile)
                    _ftr.ClearFileTemplateARActionList(vm.FileTemplate.Id);

                SetDEFileWriter(vm.FileTemplate);
                _ftr.Update(vm.FileTemplate);
                return RedirectToAction("Index", vm).Success(vm.FileTemplate.Name + " has been updated successfully.");
            }
            return View(vm).Danger("The template failed to update.");
        }        
        

        public ActionResult Clone(int? id)
        {
            if (id.HasValue)
            {
                var currentTemplate = _ftr.GetById(id.Value);
                var newTemplate = currentTemplate.Clone();
                _ftr.Create(newTemplate);                
                return RedirectToAction("Edit", new { id = newTemplate.Id }).Success("Clone of " + currentTemplate.Name + " has been created successfully.");
            }         
            return RedirectToAction("Index").Danger("The template failed to clone.");
        }

        public ActionResult DeepClone(int? id)
        {
            if (id.HasValue)
            {
                var currentTemplate = _ftr.GetById(id.Value);
                var newTemplate = _ftr.DeepClone(currentTemplate);
                return RedirectToAction("Edit", new { id = newTemplate.Id }).Success("Clone of " + currentTemplate.Name + " has been created successfully.");
            }
            return RedirectToAction("Index").Danger("The template failed to clone.");
        }

        [HttpPost]
        [AdminOnlyAuthorizeUser]
        public ActionResult Delete(int id)
        {
            var vm = new FileTemplateViewModel(_ftr, null, _ftr.GetById(id));
            _ftr.Delete(vm.FileTemplate);

            FTSearchPrms<FileTemplate> searchPrms = CurrentSearchPrms;
            List<SelectListItem> clients = null;
            if (_ur.CurrentUser.IsAdmin)
            {
                vm.FileTemplates = _ftr.GetAll(x => x.Disabled == false && (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || (x.Description.Contains(searchPrms.SearchString)))) && (!searchPrms.ClientId.HasValue || x.ClientId == searchPrms.ClientId.Value)).OrderByDescending(ft => ft.DateUpdated).ToList();
                clients = _cr.GetAll().Where(x => x.Disabled == false).Select(y => new SelectListItem() { Text = y.Name, Value = y.Id.ToString() }).ToList();
            }
            else
            {
                User user = _ur.GetUserById(_ur.CurrentUser.Id);
                vm.FileTemplates = user.AssociatedFileTemplates.Where(x => x.Disabled == false && (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || (x.Description.Contains(searchPrms.SearchString)))) && (!searchPrms.ClientId.HasValue || x.ClientId == searchPrms.ClientId.Value)).OrderByDescending(ft => ft.DateUpdated).ToList();
                clients = user.AssociatedClients.Where(x => x.Disabled == false).Select(y => new SelectListItem() { Text = y.Name, Value = y.Id.ToString() }).ToList();
            }
            searchPrms = new FTSearchPrms<FileTemplate>(searchPrms.SortOrder,searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, vm.FileTemplates.ToList(), searchPrms.ClientId, clients);
            CurrentSearchPrms = searchPrms;

            return PartialView("_Index", searchPrms).Information("Search results found: " + searchPrms.RowsCount.ToString()); 
        }

        [AdminOnlyAuthorizeUser]
        public ActionResult Reinstate(int id, FormCollection collection)
        {
            var vm = new FileTemplateViewModel(_ftr, _cr, _ftr.GetById(id));
            _ftr.Reinstate(vm.FileTemplate);
            return RedirectToAction("Index").Success(vm.FileTemplate.Name + " has been reinstated successfully.");
        }

        private bool IsDataFieldExists(int? dfid, int? ftid)
        {
            
            FileTemplate ft = null;
            if (dfid.HasValue && ftid.HasValue)
            {
                ft = _ftr.GetById(ftid.Value);
                return ft.FileTemplateDataFields.Any(x => x.DataFieldId == dfid.Value);
            }            
            return false;
        }

        [HttpPost]
        public ActionResult AddDataField(int? FileTemplateId, int? DataFieldId)
        {
            var vm = new FileTemplateViewModel(_ftr, null, _ftr.GetById(FileTemplateId.Value));
            if (!DataFieldId.HasValue || !FileTemplateId.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (vm.FileTemplate.Disabled || !vm.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            FileTemplateDataField ftdf = new FileTemplateDataField();
            if (IsDataFieldExists(DataFieldId.Value, FileTemplateId.Value))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }
            else
            {
                ftdf.FileTemplateId = FileTemplateId.Value;
                ftdf.DataFieldId = DataFieldId.Value;
                vm.FTDataFields = _ftr.AddDFToTemplate(ftdf).ToList();
                return PartialView("_DataFieldList", vm);
            }            
        }

        private bool IsDTActionExists(int? dtaid, int? ftid)
        {

            FileTemplate ft = null;
            if (dtaid.HasValue && ftid.HasValue)
            {
                ft = _ftr.GetById(ftid.Value);
                return ft.FileTemplateDTActions.Any(x => x.DTActionId == dtaid.Value);
            }
            return false;
        }

        private bool IsARActionExists(int? araid, int? ftid)
        {

            FileTemplate ft = null;
            if (araid.HasValue && ftid.HasValue)
            {
                ft = _ftr.GetById(ftid.Value);
                return ft.FileTemplateARActions.Any(x => x.ARActionId == araid.Value);
            }
            return false;
        }

        [HttpPost]
        public ActionResult AddDTAction(int? FileTemplateId, int? DTActionId)
        {

            if (!DTActionId.HasValue || !FileTemplateId.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var vm = new FileTemplateViewModel(_ftr, null, _ftr.GetById(FileTemplateId.Value));
            if (vm.FileTemplate.Disabled || !vm.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            FileTemplateDTAction ftdta = new FileTemplateDTAction();
            if (IsDTActionExists(DTActionId.Value, FileTemplateId.Value))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }
            else
            {
                ftdta.FileTemplateId = FileTemplateId.Value;
                ftdta.DTActionId = DTActionId.Value;
                vm.FTDTActions = _ftr.AddDTAToTemplate(ftdta);
                return PartialView("_DTActionsList", vm);
            }
        }

        [HttpPost]
        public ActionResult AddARAction(int? FileTemplateId, int? ARActionId)
        {
            if (!ARActionId.HasValue || !FileTemplateId.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var vm = new FileTemplateViewModel(_ftr, null, _ftr.GetById(FileTemplateId.Value));

            if (vm.FileTemplate.Disabled || !vm.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            FileTemplateARAction ftara = new FileTemplateARAction();

            if (IsARActionExists(ARActionId.Value, FileTemplateId.Value))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }
            else
            {
                ftara.FileTemplateId = FileTemplateId.Value;
                ftara.ARActionId = ARActionId.Value;

                vm.FTARActions = _ftr.AddARAToTemplate(ftara);
                return PartialView("_ARActionsList", vm);

            }
        }

        [HttpPost]
        public ActionResult DeleteDataField(int? Id)
        {
            var vm = new FileTemplateViewModel(_ftr, null, null);
            FileTemplateDataField ftdf = _ftr.GetFileTemplateDataFieldById(Id.Value);

            if (ftdf == null) throw new Exception("Failed to find the File Template Data Field with id - " + Id.ToString());
            int ftid = ftdf.FileTemplateId;
            _ftr.DeleteFileTemplateDataField(ftdf);
            vm.FileTemplate = _ftr.GetById(ftid);
            if (vm.FileTemplate.Disabled || !vm.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            vm.FTDataFields = vm.FileTemplate.FileTemplateDataFields.OrderBy(x => x.Id).ToList();
            return PartialView("_DataFieldList", vm);
        }

        [HttpPost]
        public ActionResult DeleteDTAction(int? Id)
        {
            var vm = new FileTemplateViewModel(_ftr, null, null);
            FileTemplateDTAction ftdta = _ftr.GetFileTemplateDTActionById(Id.Value);

            if (ftdta == null) throw new Exception("Failed to find the File Template DT Action with id - " + Id.ToString());
            if (ftdta.FileTemplate.Disabled || !ftdta.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            int ftid = ftdta.FileTemplateId;
            _ftr.DeleteFileTemplateDTAction(ftdta);
            vm.FileTemplate = _ftr.GetById(ftid);
            vm.FTDTActions = vm.FileTemplate.FileTemplateDTActions.ToList();
            return PartialView("_DTActionsList", vm);
        }

        [HttpPost]
        public ActionResult DeleteARAction(int? Id)
        {
            var vm = new FileTemplateViewModel(_ftr, null, null);
            FileTemplateARAction ftara = _ftr.GetFileTemplateARActionById(Id.Value);

            if (ftara == null) throw new Exception("Failed to find the File Template AR Action with id - " + Id.ToString());
            if (ftara.FileTemplate.Disabled || !ftara.FileTemplate.Editable)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            int ftid = ftara.FileTemplateId;
            _ftr.DeleteFileTemplateARAction(ftara);
            vm.FileTemplate = _ftr.GetById(ftid);
            vm.FTARActions = vm.FileTemplate.FileTemplateARActions.ToList();
            return PartialView("_ARActionsList", vm);
        }

        [HttpGet]
        public ActionResult GetReferenceData(EnumReferenceData referenceData)
        {
            switch (referenceData)
            {
                case EnumReferenceData.DTActions:
                    return this.Json(_ftr.GetDTActions().Select(x => new { Id = x.Id, Name = x.ActionName}).ToList(), JsonRequestBehavior.AllowGet);
                case EnumReferenceData.ARActions:
                    return this.Json(_ftr.GetARActions().Select(x => new { Id = x.Id, Name = x.Name}).ToList(), JsonRequestBehavior.AllowGet);
                case EnumReferenceData.DataFields:
                    var objData = _ftr.GetDataFields((User)Session[Session.SessionID])
                        .Select(x => new { Id = x.Id, Name = x.Name }).ToList();
                    return this.Json(objData, JsonRequestBehavior.AllowGet);
                default:
                    throw new NotImplementedException();
            }

        }
    }
}
