﻿CREATE TABLE [dbo].[EventLog] (
    [Id]               BIGINT         IDENTITY (1, 1) NOT NULL,
    [UserId]           INT            NOT NULL,
    [IPAddress]        VARCHAR (50)   NULL,
    [FileTemplateId]   INT            NOT NULL,
    [Action]           INT            NULL,
    [FileName]         NVARCHAR (500) NULL,
    [ProcessStartedOn] DATETIME       NOT NULL,
    [ProcessEndedOn]   DATETIME       NOT NULL,
    [Succeeded]        BIT            NOT NULL,
    [Note]             NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EventLog_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);



