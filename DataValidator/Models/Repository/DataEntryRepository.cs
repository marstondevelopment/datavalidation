﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class DataEntryRepository : RepositoryBase<DataIntegrationEntities, DataEntry>
    {
        private UserRepository _ur;

        public const string OFFENCE_CODE_CMDTXT = "Columbus_Offence_Codes";
        public const string HMCTS_ARREST_BREACH_CLIENTS_CMDTXT = "HMCTS_Breach_Clients";
        public const string HMCTS_GET_CASE_ID_CMDTXT = "HMCTS_Breach_Get_Case_AI";
        public DataEntryRepository(DataIntegrationEntities entities, UserRepository ur) : base(entities)
        {
            _ur = ur;
        }

        public override IQueryable<DataEntry> GetAll()
        {
            return base.GetAll();
        }

        public DataEntry GetDataEntryById(int id)
        {
            DataEntry dataEntry = GetAll(x => x.Id == id).FirstOrDefault();
            switch ((EnumDataEntryForm) dataEntry.DataEntryFormId)
            {
                case EnumDataEntryForm.HMCTS:
                    dataEntry.DEOHmcts.Parent = dataEntry;
                    break;
                default:
                    break;
            }
            return dataEntry;
        }

        public DEAttachment GetDEAttachmentById(int id)
        {
            return _entities.DEAttachments.Where(x => x.Id == id).FirstOrDefault();
        }

        private void AppendHMCTSAttachments(DataEntry de)
        {
            if (de.DEAttachments == null)
                de.DEAttachments = new List<DEAttachment>();
            string attachmentInfo = null;
            string stagingFolder = Utility.GetAttachmentsFolder(Utility.StagingAttachmentsFolder, de.DateAdded);
            foreach (var file in de.DEOHmcts.Attachments)
            {
                string guid = Guid.NewGuid().ToString();
                DEAttachment dEAttachment = new DEAttachment
                {
                    ContentType = file.ContentType,
                    DataEntryId = de.DataEntryFormId,
                    FileName = Utility.GenerateColumbusFileName(de.DateAdded, guid),
                    OriginalFileName = file.FileName,
                    DataEntry = de,
                    FileType = (int)file.FileType
                };

                de.DEAttachments.Add(dEAttachment);
                // now save the file to staging folder
                Utility.SaveFileToFolder(stagingFolder, guid, file.FileStream);
                attachmentInfo = attachmentInfo + dEAttachment.OriginalFileName.Remove(0, 2) + ";" + dEAttachment.FileName + "|";
            }
            if (!string.IsNullOrEmpty(attachmentInfo))
                de.DEOHmcts.AttachmentInfo = attachmentInfo.Trim("|".ToCharArray());
        }

        private void UpdateHMCTSAttachments(DataEntry de)
        {
            string attachmentInfo = null;
            string stagingFolder = Utility.GetAttachmentsFolder(Utility.StagingAttachmentsFolder, de.DateAdded);
            for (int i = 0; i < de.DEOHmcts.Attachments.Count; i++)
            {
                if (de.DEOHmcts.Attachments[i]!= null)
                {
                    DEOAttachment deoAtt = de.DEOHmcts.Attachments[i];
                    DEAttachment deatt = null;
                    if (i >= de.DEAttachments.Count)
                    {
                        deatt = new DEAttachment()
                        {
                            DataEntryId = de.DataEntryFormId,
                            DataEntry = de,
                        };

                        de.DEAttachments.Add(deatt);
                    }
                    else
                    {
                        deatt = de.DEAttachments.ElementAt(i);
                        Utility.DeleteFileFromFolder(Utility.StagingAttachmentsFolder, deoAtt.FileName);
                    }

                    string guid = Guid.NewGuid().ToString();
                    deatt.ContentType = deoAtt.ContentType;
                    deatt.FileName = Utility.GenerateColumbusFileName(de.DateAdded, guid);
                    deatt.OriginalFileName = deoAtt.FileName;
                    deatt.FileType = (int)deoAtt.FileType;
                    Utility.SaveFileToFolder(stagingFolder, guid, deoAtt.FileStream);
                }
            }

            foreach (var item in de.DEAttachments)
            {
                attachmentInfo = attachmentInfo + item.OriginalFileName.Remove(0,2) + ";" + item.FileName + "|";
            }

            if (!string.IsNullOrEmpty(attachmentInfo))
                de.DEOHmcts.AttachmentInfo = attachmentInfo.Trim("|".ToCharArray());
        }

        public void Create(DataEntry de)
        {

            de.UserId = _ur.CurrentUser.Id;
            de.DateAdded = DateTime.Now;
            switch ((EnumDataEntryForm) de.DataEntryFormId)
            {
                case EnumDataEntryForm.HMCTS:
                    de.DEOHmcts.UserId = de.UserId;
                    AppendHMCTSAttachments(de);
                    break;
                default:
                    break;
            }

            de.JSONString = de.SerializePrmsObject();
            de.UpdatedBy = _ur.CurrentUser.Id;
            de.DateUpdated = DateTime.Now;

            Add(de);
            SubmitChanges();
        }

        public void Update(DataEntry de)
        {
            switch ((EnumDataEntryForm)de.DataEntryFormId)
            {
                case EnumDataEntryForm.HMCTS:
                    UpdateHMCTSAttachments(de);
                    break;
                default:
                    break;
            }

            de.JSONString = de.SerializePrmsObject();
            de.UpdatedBy = _ur.CurrentUser.Id;
            de.DateUpdated = DateTime.Now;

            SubmitChanges();
        }

        public Dictionary<string,string> GetOffenceCodes(int ftId)
        {
            Dictionary<string, string> codeList = new Dictionary<string, string>();
            FileTemplate ft = _entities.FileTemplates.Where(x => x.Id == ftId).FirstOrDefault();
            CmdText cmdText = _entities.CmdTexts.Where(x => x.Name == OFFENCE_CODE_CMDTXT).FirstOrDefault();
            DataTable dt = ft.DataProvider.GetDataCmdText(cmdText);
            codeList = dt.AsEnumerable().ToDictionary(row => row.Field<string>(0), row => row.Field<string>(1));
            return codeList;
        }

        public bool HMCTSBreachCaseAI(int ftId, int clientId, string cCR,int? deId)
        {
            bool exists = false;
            FileTemplate ft = _entities.FileTemplates.Where(x => x.Id == ftId).FirstOrDefault();
            CmdText cmdText = _entities.CmdTexts.Where(x => x.Name == HMCTS_GET_CASE_ID_CMDTXT).FirstOrDefault();
            cmdText.SQLScript = cmdText.SQLScript.RenderTag("ClientId",clientId.ToString());
            cmdText.SQLScript = cmdText.SQLScript.RenderTag("ClientCaseReference", cCR);
            DataTable dt = ft.DataProvider.GetDataCmdText(cmdText);
            _entities.Entry(cmdText).Reload();
            if (dt != null && dt.Rows.Count > 0)
                exists = true;

            if (deId.HasValue)
            {
                if (ft.DataEntries.Where(x => x.DEOHmcts != null && x.DEOHmcts.ClientId == clientId && x.DEOHmcts.CaseNumber == cCR && x.Uploaded == false && x.Deleted == false && x.Id != deId.Value).ToList().Count > 0)
                    exists = true;
            }
            else
            {
                if (ft.DataEntries.Where(x => x.DEOHmcts != null && x.DEOHmcts.ClientId == clientId && x.DEOHmcts.CaseNumber == cCR && x.Uploaded == false && x.Deleted == false).ToList().Count > 0)
                    exists = true;
            }

            return exists;
        }

        public bool GetHMCTSAuditReport(DEAuditSearchPrms<DEARBase> searchPrms, FileTemplate fileTemplate, ref IEnumerable<DEARBase> searchResults)
        {
            string sqlSelect = "SELECT TOP " + Utility.MaxListItems.ToString() + "  c.ClientCaseReference AS CaseNumber, c.FirstnameLiable1 as FirstName, c.LastnameLiable1 as LastName,c.ClientId, c.ClientName, c.BatchDate, c.Imported, c.ImportedOn, c.FUTUserId, c.CaseId, c.ErrorId, e.ErrorMessage  From oso.stg_HMCTS_Cases c LEFT JOIN OSO.SQLErrors e on c.ErrorId = e.Id ";
            string sqlWhere = string.Empty;

            if (searchPrms.FromDate.HasValue || searchPrms.ToDate.HasValue || !string.IsNullOrEmpty(searchPrms.SearchString ) || searchPrms.ClientId.HasValue || _ur.CurrentUser.RoleId == (int)EnumRole.ReadOnly)
            {
                sqlWhere = " WHERE ";

                sqlWhere = sqlWhere + (searchPrms.FromDate.HasValue ? " c.BatchDate >= '" + searchPrms.FromDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' AND" : string.Empty);
                sqlWhere = sqlWhere + (searchPrms.ToDate.HasValue ? " c.BatchDate < '" + searchPrms.ToDate.Value.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") + "' AND" : string.Empty);
                sqlWhere = sqlWhere + (!string.IsNullOrEmpty(searchPrms.SearchString) ? "(( c.ClientCaseReference like '%" + searchPrms.SearchString.Replace("'", "''") + "%') OR (c.ClientName like '%" + searchPrms.SearchString.Replace("'", "''") + "%')) AND " : string.Empty);
                sqlWhere = sqlWhere + (searchPrms.ClientId.HasValue ? " c.ClientId = " + searchPrms.ClientId.Value.ToString() + " AND" : string.Empty);
                sqlWhere = sqlWhere + (_ur.CurrentUser.RoleId == (int)EnumRole.ReadOnly ? " c.FUTUserId = " + _ur.CurrentUser.Id.ToString() + " AND" : string.Empty);
                sqlWhere = sqlWhere.Trim().TrimEnd("AND".ToCharArray());
            }

            sqlSelect = sqlSelect + (!string.IsNullOrEmpty(sqlWhere) ? sqlWhere : string.Empty) + " ORDER BY c.BatchDate DESC";
            CmdText cmdText = new CmdText()
            {
                DBConnectionId = fileTemplate.DBConnectionId.Value,
                DbConnection = fileTemplate.DbConnection,
                Name = "GetHMCTSAuditReport",
                FileTemplate = fileTemplate,
                SQLScript = sqlSelect,
                ParamCollection = new Dictionary<string, object>()
            };

            DataProvider dp = DataProvider.New(fileTemplate);
            bool succeed = false;
            try
            {
                DataTable dt = dp.GetDataCmdText(cmdText);
                List<DEHMCTSAR> results = dt.AsEnumerable().Select(x => new DEHMCTSAR()
                {
                    CaseNumber = x.Field<string>("CaseNumber"),
                    FirstName = x.Field<string>("FirstName"),
                    LastName = x.Field<string>("LastName"),
                    ClientId = x.Field<int>("ClientId"),
                    CLientName = x.Field<string>("CLientName"),
                    BatchDate = x.Field<DateTime>("BatchDate"),
                    FUTUserId = x.Field<int?>("FUTUserId"),
                    Imported = x.Field<bool>("Imported"),
                    ImportedOn = x.Field<DateTime?>("ImportedOn"),
                    ErrorId = x.Field<int?>("ErrorId"),
                    ErrorMessage = x.Field<string>("ErrorMessage"),
                    CaseId = x.Field<int?>("CaseId")
                }).ToList();

                if (results.Count>0)
                {
                    var users = _ur.GetAll();
                    foreach (var item in results)
                    {
                        if (item.FUTUserId.HasValue)
                        {
                            item.CreatedBy = users.Where(x => x.Id == item.FUTUserId.Value).FirstOrDefault().UserId;
                        }
                    }

                    searchPrms.CasesImported = results.Where(x => x.Imported == true).Count();
                    searchPrms.CasesWithError = results.Where(x => x.Imported == false && x.ErrorId != null).Count();
                    searchPrms.CasesNotImported = results.Count - searchPrms.CasesImported - searchPrms.CasesWithError; 
                }

                searchResults = results;
                succeed = true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return succeed;
        }

        public DataTable GetHMCTSBreachClients(int ftId)
        {
            string cacheName = DataEntryRepository.HMCTS_ARREST_BREACH_CLIENTS_CMDTXT + "_" + ftId.ToString();
            DataTable dt = (DataTable)HttpContext.Current.Cache[cacheName];
            if (dt == null)
            {
                FileTemplate ft = _entities.FileTemplates.Where(x => x.Id == ftId).FirstOrDefault();
                CmdText cmdText = _entities.CmdTexts.Where(x => x.Name == HMCTS_ARREST_BREACH_CLIENTS_CMDTXT).FirstOrDefault();
                dt = ft.DataProvider.GetDataCmdText(cmdText);
                HttpContext.Current.Cache.Add(cacheName, dt, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(2, 0, 0), System.Web.Caching.CacheItemPriority.Normal, null);
            }

            return dt;
        }

        public void UploadCompleted(FileTemplate ft)
        {
            List<DataEntry> deList = new List<DataEntry>();
            foreach (var de in ft.DEForUpload)
            {
                DataEntry ade = GetDataEntryById(de.Id);
                deList.Add(ade);
                ade.Uploaded = true;
                ade.UploadedOn = DateTime.Now;
                ade.UpdatedBy = _ur.CurrentUser.Id;
                ade.DateUpdated = DateTime.Now;
            }

            SubmitChanges();
        }

        internal bool MoveFilesToProduction(FileTemplate ft)
        {
            bool succeed = true;

            var attachments = ft.DEForUpload.Select(x => x.DEAttachments).ToList();

            foreach (var de in ft.DEForUpload)
            {
                foreach (var file in de.DEAttachments)
                {
                    string pf = Utility.GetAttachmentsFolder(Utility.ProductionAttachmentsFolder, de.DateAdded);
                    string source = Path.Combine(Utility.StagingAttachmentsFolder, file.FileName);
                    string production = Path.Combine(Utility.ProductionAttachmentsFolder, file.FileName);
                    if (File.Exists(production))
                    {
                        logger.Error("The file " + production + " already exists in production. Action skipped");
                        succeed = false;
                    }
                    else
                    {
                        File.Move(source, production);
                    }
                }
            }

            return succeed;
        }

        public void Delete(DataEntry de)
        {
            //Remove(de);
            de.Deleted = true;
            SubmitChanges();
        }

    }
}