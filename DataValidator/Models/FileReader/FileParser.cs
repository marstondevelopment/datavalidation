﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Data;

namespace Mgl.DataValidator.Models
{
    public abstract class FileParser
    {
        protected readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected Stream UploadedFile { get; set; }
        public StringBuilder ValidationSummary = new StringBuilder();
        public FileTemplate FileTemplate { get; set; }
        public string FileName { get; set; }
        public string FileNameNoExt
        {
            get
            {
                string val = string.Empty;
                if (!string.IsNullOrEmpty(FileName))
                    val = FileName.Substring(0, FileName.LastIndexOf("."));
                return val;
            }
        }
        public EnumDataFileScreen UserAction {get;set;}

        public bool PersistingInputFile
        {
            get
            {
                return (UserAction == EnumDataFileScreen.Upload || UserAction == EnumDataFileScreen.GenerateFiles || UserAction == EnumDataFileScreen.UploadToRepository || UserAction == EnumDataFileScreen.DEUpload);
            }

        }

        public FileParser(FileTemplate ft, Stream fs, string fileName, EnumDataFileScreen userAction)
        {
            UploadedFile = fs;
            FileTemplate = ft;
            FileName = fileName;
            UserAction = userAction;
            FileTemplate.ParentDataFile = this;
            SetValidationReportHeader();
            if (PersistingInputFile)
            {
                FileTemplate.CreateDataTables();
            }
        }
        public abstract bool Validate();
        public static FileParser New(FileTemplate ft, Stream fs, string fileName, EnumDataFileScreen userAction)
        {
            switch ((EnumMimeType) ft.MimeTypeId)
            {
                case EnumMimeType.xls:
                case EnumMimeType.xlsx:
                    return new ExcelFileParser(ft, fs, fileName, userAction);
                case EnumMimeType.csv:
                case EnumMimeType.dat:
                case EnumMimeType.txt:
                    return new TextFileParser(ft, fs, fileName, userAction);
                case EnumMimeType.xml:
                    return new XMLFileParser(ft, fs, fileName, userAction);
                case EnumMimeType.asc:
                    return new NLSFileParser(ft, fs, fileName, userAction);
                case EnumMimeType.xls5:
                case EnumMimeType.xlsx2007:
                    return new ExcelBIFF5FileParser(ft, fs, fileName, userAction);
                case EnumMimeType.de:
                    return new DEParser(ft, userAction);
                default:
                    break;
            }
            return null;
        }

        private void SetValidationReportHeader()
        {

            switch ((EnumMimeType)FileTemplate.MimeTypeId)
            {
                case EnumMimeType.xls:
                case EnumMimeType.xlsx:
                case EnumMimeType.xlsx2007:
                    ValidationSummary.AppendLine("Tab, Row No., Column Name, Value, Error Description");
                    break;
                case EnumMimeType.csv:
                case EnumMimeType.dat:
                case EnumMimeType.txt:
                case EnumMimeType.de:
                    ValidationSummary.AppendLine("Row No., Column Name, Value, Error Description");
                    break;
                case EnumMimeType.xml:
                    ValidationSummary.AppendLine("Item No., Element Name, Value, Error Description");
                    break;
                case EnumMimeType.asc:
                case EnumMimeType.xls5:
                    ValidationSummary.AppendLine("Record No., Field Name, Value, Error Description");
                    break;
                default:
                    ValidationSummary.AppendLine("Row No., Column Name, Value, Error Description");
                    break;
            }
        }




    }
}