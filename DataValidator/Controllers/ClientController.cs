﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class ClientController : ControllerBase
    {

        private ClientRepository _cr;
        private UserRepository _ur;
        public ClientController(ClientRepository cr, UserRepository ur)
        {
            _cr = cr;
            _ur = ur;
        }

        public ActionResult Index()
        {
            var vm = new ClientViewModel();
            vm.Clients = _cr.GetAll().OrderByDescending(c => c.DateUpdated).ThenByDescending(c => c.DateCreated);
            return View(vm);
        }

        public ActionResult GetDeletedClients()
        {
            var vm = new ClientViewModel();
            vm.Clients = _cr.GetAll(x => x.Disabled == true).OrderByDescending(c => c.DateUpdated).ThenByDescending(c => c.DateCreated);

            return View(vm);
        }

        public ActionResult Create()
        {
            var vm = new ClientViewModel();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(Client client)
        {
            var vm = new ClientViewModel();
            if (ModelState.IsValid)
            {                
                vm.Client = client;
                _cr.Create(vm.Client);                
                return RedirectToAction("Index").Success(vm.Client.Name + " has been created successfully.");
            }
            return RedirectToAction("Create").Danger(vm.Client.Name + " has failed to update.");
        }

        public ActionResult Details(int id)
        {
            var vm = new ClientViewModel();
            vm.Client = _cr.GetById(id);
            return View(vm);
        }

        public ActionResult Edit(int id)
        {
            ClientViewModel vm = new ClientViewModel();
            vm.Client = _cr.GetById(id);
            if (vm.Client == null) throw new HttpException(404, "Client not found...");

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection fc)
        {
            var vm = new ClientViewModel();
            vm.Client = _cr.GetById(id);
            TryUpdateModel(vm);
            if (ModelState.IsValid)
            {
                _cr.Update(vm.Client);                
                return RedirectToAction("Index").Success(vm.Client.Name + " has been updated successfully.");
            }
            return RedirectToAction("Edit", new { id = vm.Client.Id }).Danger("Failed to update the client");
        }
                
        [Description("Delete Client")]
        public ActionResult Delete(int id)
        {
            var vm = new ClientViewModel();
            vm.Client = _cr.GetById(id);            
            int associatedTemplates = vm.Client.FileTemplates.Where(x => x.ClientId == vm.Client.Id && x.Disabled == false).Count();
            int associatedUsers = vm.Client.UserClients.Where(x => x.User.Disabled == false).Count();
            if (associatedTemplates > 0 || associatedUsers > 0) return RedirectToAction("Index").Danger("Failed to delete " + vm.Client.Name + " as some active file templates and/or clients are referencing it");
            else
            {
                _cr.Delete(vm.Client);                
            }          
            return RedirectToAction("Index").Success(vm.Client.Name + " has been deleted successfully.");
        }
        
        [Description("Reinstate Client")]
        public ActionResult Reinstate(int id)
        {
            var vm = new ClientViewModel();
            vm.Client = _cr.GetById(id);
            _cr.Reinstate(vm.Client);            
            return RedirectToAction("Index").Success(vm.Client.Name + " has been reinstated successfully.");
        }
    }
}
