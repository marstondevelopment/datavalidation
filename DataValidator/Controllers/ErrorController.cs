﻿using Mgl.DataValidator.Models;
using System;
using System.Web.Mvc;


namespace Mgl.DataValidator.Controllers
{
    public class ErrorController : ControllerBase
    {
        private UserRepository _ur;
        private const string ErrorMessage = "ErrMessage";
        public ErrorController(UserRepository ur)
        {
            _ur = ur;
        }
        // GET: Error
        public ActionResult Index()
        {

            if (_ur.CurrentUser != null && (((EnumRole) _ur.CurrentUser.RoleId) > EnumRole.ReadOnly))
            {
                
                if (Session[ErrorMessage] != null)
                {
                    Exception ex = (Exception)Session[ErrorMessage];
                    string message = ex.Message + " - " + ex.StackTrace;
                    Session.Remove(ErrorMessage);
                    ViewBag.ErrMessage = message;
                }
            }
            return View();
        }

        // GET: Error/Details/5
        public ActionResult UserDisabled()
        {
            return View();
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

    }
}
