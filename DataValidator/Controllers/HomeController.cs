﻿using Mgl.DataValidator.Models;
using Security.Authorize;
using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [DisabledAuthorizeUser]
    public class HomeController : ControllerBase
    {

        private DataFileController _dataFileController;
        private UserRepository _ur;

        public HomeController(DataFileController dataFileController, UserRepository ur)
        {
            _dataFileController = dataFileController;
            _ur = ur;
        }

        public ActionResult Index()
        {
            return View();

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Logout()
        {
            return View().Success("You have been logged out successfully. </ br > Thank you for using FUT and have a nice day! ");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LogOff401(string id)
        {
            // if we've been passed HTTP authorisation
            string httpAuth = this.Request.Headers["Authorization"];
            if (!string.IsNullOrEmpty(httpAuth) &&
                httpAuth.StartsWith("basic", StringComparison.OrdinalIgnoreCase))
            {
                // build the string we expect - don't allow regular users to pass
                byte[] enc = Encoding.UTF8.GetBytes(id + ':' + id);
                string expected = "basic " + Convert.ToBase64String(enc);

                if (string.Equals(httpAuth, expected, StringComparison.OrdinalIgnoreCase))
                {
                    return Content("You are logged out.");
                }
            }

            // return a request for an HTTP basic auth token, this will cause XmlHttp to pass the new header
            this.Response.StatusCode = 401;
            this.Response.StatusDescription = "Unauthorized";
            this.Response.AppendHeader("WWW-Authenticate", "basic realm=\"My Realm\"");

            if (Request.Cookies[Utility.LAST_LOGGED_ON] != null)
            {
                HttpCookie myCookie = new HttpCookie(Utility.LAST_LOGGED_ON);
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            Session.Clear();
            Session.Abandon();

            return Content("Force AJAX component to sent header");
        }
    }
}