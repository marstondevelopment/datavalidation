﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class SplitOneColIntoMultipleCols : DTStep
    {
        private PrmsSplitOneColIntoMultipleCols prms;
        private List<string> outputFields;
        public SplitOneColIntoMultipleCols(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitOneColIntoMultipleCols;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsSplitOneColIntoMultipleCols parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            outputFields = prms.OutputColumnNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x=>x.Trim()).ToList();

            if (string.IsNullOrEmpty(prms.InputColumnName) || string.IsNullOrEmpty(prms.OutputColumnNames) || string.IsNullOrEmpty(prms.StringSeparator))
            {
                throw new MemberAccessException("Some parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

        }


        public override bool Execute()
        {
            if (prms.ImplementInDTStage)
            {
                ExecuteDTRule(ParentFTDT.FileTemplate.ProcessedDataTable);
            }
            else
            {
                ParentFTDT.FileTemplate.FileWriter.AddDefaultOutputFiles();
                var dts = ParentFTDT.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output).ToArray();
                for (int i = 0; i < dts.Length; i++)
                {
                    DataTable dt = dts[i].Data;
                    ExecuteDTRule(dt);
                }
            }

            return true;
        }

        private void ExecuteDTRule(DataTable dt)
        {
            int colIndex = dt.Columns.IndexOf(prms.InputColumnName);

            if (colIndex >= 0)
            {
                foreach (var field in outputFields)
                {
                    //Add output columns if not exist
                    if (dt.Columns[field] == null)
                    {
                        DataColumn col = dt.AddColumn(field);
                        col.SetOrdinal(colIndex);
                        colIndex++;
                    }
                }

                dt.AcceptChanges();

                var rows = dt.Select(prms.Filter);


                foreach (DataRow rw in rows)
                {
                    List<string> elements = rw[prms.InputColumnName].ToString().Split(prms.StringSeparator.ToCharArray()).Select(x => x.Trim()).ToList();

                    if (elements.Count > outputFields.Count)
                    {
                        if (!string.IsNullOrEmpty( prms.NotesColumnName))
                        {
                            rw[prms.NotesColumnName] = (rw[prms.NotesColumnName] + "|" + rw[prms.InputColumnName].ToString() + "|").TrimStart("|".ToCharArray());
                            elements.RemoveRange(outputFields.Count, elements.Count - outputFields.Count);
                        }
                    }

                    if (elements.Count > 0)
                    {
                            
                            for (int idx = 0; idx <(outputFields.Count >= elements.Count ? elements.Count : outputFields.Count); idx++)
                            {
                                rw[outputFields[idx]] = elements[idx];
                            }

                    }
                }

            }





        }
    }
}