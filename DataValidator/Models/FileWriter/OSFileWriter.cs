﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class OSFileWriter : FileWriter
    {
        public OSFileWriter(FileTemplate ft) : base(ft)
        {
        }

        protected override bool GenerateOutputDataFiles()
        {

            try
            {
                AddDefaultOutputFiles();
                AddExcludedFiles();

                var outputFiles = OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output).ToArray();
                foreach (OutputFile oFile in outputFiles)
                {
                    ExcludeColumns(oFile.Data);
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;                
            }

        }

    }
}