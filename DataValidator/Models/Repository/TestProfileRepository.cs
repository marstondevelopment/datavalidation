﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    public class TestProfileRepository : RepositoryBase<DataIntegrationEntities, TestProfile>
    {
        private UserRepository _ur;
        private StoredProcedureRepository _spr;
        public TestProfileRepository(DataIntegrationEntities entity, UserRepository ur, StoredProcedureRepository spr) : base(entity)
        {
            _ur = ur;
            _spr = spr;
        }

        public override IQueryable<TestProfile> GetAll()
        {

            var testProfiles =  base.GetAll(c => c.Disabled == false);
            foreach (var tp in testProfiles)
            {
                GetAssociatedObject(tp);
            }

            return testProfiles;
        }

        public async Task< IEnumerable<TestProfile>> GetAllAsync()
        {
            List<TestProfile> testProfiles = await Task.Run(()=>  base._entities.TestProfiles.Where(c => c.Disabled == false).ToList());
            foreach (var tp in testProfiles)
            {
                GetAssociatedObject(tp);
            }

            return testProfiles;

        }

        public async Task<TestProfile> GetByIdAsync(int id)
        {
            var tp = await this._entities.TestProfiles.FindAsync(id);
            GetAssociatedObject(tp);
            return tp;
        }


        public IEnumerable<TestResult> GetTestResults(int userId)
        {
            return _entities.TestResults.Where(x => x.UserId == userId).ToList();
        }

        private void PresetDBCmd(IDBCmd cmd, FileTemplate ft)
        {
            if (cmd.StoredProcedureId.HasValue)
            {
                cmd.StoredProcedure = _spr.GetById(cmd.StoredProcedureId.Value);
                cmd.StoredProcedure.FileTemplate = ft;
            }
            if (cmd.CmdTextId.HasValue)
            {
                cmd.CmdText = _entities.CmdTexts.Where(x => x.Id == cmd.CmdTextId.Value).FirstOrDefault();
                cmd.CmdText.FileTemplate = ft;
            }
        }


        private void GetAssociatedObject(TestProfile tp)
        {
            var actions = tp.FileTemplate.FileTemplateDTActions.Where(x => x.DTAction.DTActionTypeId == (int)EnumDTActionType.OSExcludeCasesAlreadyImported).ToList();
            foreach (var action in actions)
            {
                PresetDBCmd((IDBCmd)action.DTAction.PrmsExcludeCasesAI, tp.FileTemplate);
            }

            actions = tp.FileTemplate.FileTemplateDTActions.Where(x => x.DTAction.DTActionTypeId == (int)EnumDTActionType.GetDataFromDB).ToList();
            foreach (var action in actions)
            {
                PresetDBCmd((IDBCmd)action.DTAction.PrmsGetDataFromDB, tp.FileTemplate);
            }

            actions = tp.FileTemplate.FileTemplateDTActions.Where(x => x.DTAction.DTActionTypeId == (int)EnumDTActionType.XMappingDataFromDB).ToList();
            foreach (var action in actions)
            {
                PresetDBCmd((IDBCmd)action.DTAction.PrmsXMappingDataFromDB, tp.FileTemplate);
            }
        }

        public TestProfile GetById(int id)
        {
            var tp = GetAll(c => c.Id == id).FirstOrDefault();
            GetAssociatedObject(tp);
            return tp;
        }


        public TestResult GetTestResultById(int id)
        {
            return _entities.TestResults.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Create(TestProfile tp)
        {
            tp.CreatedBy = _ur.CurrentUser.Id;
            tp.DateCreated = DateTime.Now;
            tp.UpdatedBy = _ur.CurrentUser.Id;
            tp.DateUpdated = DateTime.Now;
            this.Add(tp);
            this.SubmitChanges();
        }

        public void CreateTestResult(TestResult tr)
        {
            //tr.UserId = _ur.GetCurrentUser().Id;
            _entities.TestResults.Add(tr);
            this.SubmitChanges();
        }

        public void Update(TestProfile tp)
        {          
            tp.DateUpdated = DateTime.Now;
            tp.UpdatedBy = _ur.CurrentUser.Id;
            base.SubmitChanges();
            
        }

        public void Delete(TestProfile tp)
        {                       
            tp.Disabled = true;
            Update(tp);           
        }

        public void Reinstate(TestProfile tp)
        {            
            tp.Disabled = false;
            Update(tp);            
        } 
        
        public IEnumerable<FileTemplate> FTList()
        {
            return _entities.FileTemplates.Where(x => !x.Disabled).ToList();
        }
    }
}