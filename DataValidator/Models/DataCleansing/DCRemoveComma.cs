﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public class DCRemoveComma : DCAction
    {
        public DCRemoveComma(DataFieldDataCleansing dc) : base(dc) { }

        public override void ExecuteAction()
        {
            if (DataFieldDataCleansing.DataField.OriginalValue != null)
                DataFieldDataCleansing.DataField.OriginalValue = DataFieldDataCleansing.DataField.OriginalValue.Replace(",", string.Empty);

            if (DataFieldDataCleansing.DataField.OriginalValues.Count > 0)
            {
                for (int i = 0; i < DataFieldDataCleansing.DataField.OriginalValues.Count; i++)
                {
                    if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValues[i]))
                    {
                        DataFieldDataCleansing.DataField.OriginalValues[i] = DataFieldDataCleansing.DataField.OriginalValues[i].Replace(",", string.Empty);
                    }

                }

            }
        }
    }
}