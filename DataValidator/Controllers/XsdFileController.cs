﻿using Mgl.DataValidator.Models;
using Security.Authorize;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class XsdFileController : ControllerBase
    {
        XsdFileRepository _efr;
        public XsdFileController(XsdFileRepository rer)
        {
            _efr = rer;
        }

        public ActionResult Index()
        {
            IEnumerable<XsdFile> regexs = _efr.GetAll();
            return View(regexs);
        }

        public ActionResult Details(int id)
        {            
            return View(_efr.GetById(id));
        }

        public ActionResult Create()
        {            
            return View(new XsdFile() { LocalFileName = "temp.xsd"});
        }

        [HttpPost]
        public ActionResult Create(XsdFile re, HttpPostedFileBase fileInput)
        {
            if (!re.OriginalFileName.ToLower().EndsWith(".xsd"))
            {
                return View(re).Warning("Invalid file type, please upload xsd file only.");
            }

            if (ModelState.IsValid)
            {
                _efr.Create(re);
                if (fileInput != null)
                {
                    string directoryPath = (ConfigurationManager.AppSettings["XSDFilePath"]).TrimEnd("\\".ToCharArray()) + "\\";
                    fileInput.SaveAs(directoryPath + re.LocalFileName);
                }

                return RedirectToAction("Index").Success("Xsd file uploaded successfully.");
            }

            return View(re);
        }

        public ActionResult Edit(int id)
        {
            return View(_efr.GetById(id));
        }


        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection, HttpPostedFileBase fileInput)
        {
            XsdFile re = _efr.GetById(id);
            TryUpdateModel(re);
            if (ModelState.IsValid)
            {
                _efr.Update(re);
                if (fileInput != null)
                {
                    string directoryPath = (ConfigurationManager.AppSettings["XSDFilePath"]).TrimEnd("\\".ToCharArray()) + "\\";
                    fileInput.SaveAs(directoryPath + re.LocalFileName);
                }

                return RedirectToAction("Index").Success(re.OriginalFileName + " has been updated successfully.");
            }
            return View(re).Danger(" Failed to update the xsd file - " + re.OriginalFileName);
        }

        public ActionResult Delete(int id)
        {
                XsdFile re = _efr.GetById(id);
                if (re.FileTemplates.Count > 0 )
                {
                    return RedirectToAction("Index").Danger(re.OriginalFileName + " cannot be deleted due to the reference of other entities.");
                }
                
                _efr.Delete(re);
                return RedirectToAction("Index").Success(re.OriginalFileName + " has been deleted successfully.");
        }
    }
}
