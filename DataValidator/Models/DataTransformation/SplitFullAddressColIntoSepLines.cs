﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class SplitFullAddressColIntoSepLines : DTStep
    {
        private PrmsSplitFullAddressColIntoSepLines prms;
        private List<string> addresslines;
        public SplitFullAddressColIntoSepLines(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitFullAddressColIntoSepLines;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsSplitFullAddressColIntoSepLines parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

            if (string.IsNullOrEmpty(prms.InputColumnName) || string.IsNullOrEmpty(prms.AddLineColNames) || string.IsNullOrEmpty(prms.StringSeparator) || string.IsNullOrEmpty(prms.PostcodeColName) || prms.RegularExpressionId == 0)
            {
                throw new MemberAccessException("Some parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

            if (prms.StringSeparator == Utility.SPACE_DELIMITER && !prms.ReferenceListId.HasValue )
                throw new MemberAccessException("Location Matrix is missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");

            addresslines = prms.AddLineColNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x => x.Trim()).ToList();
            if (prms.StringSeparator == Utility.SPACE_DELIMITER && addresslines.Count <5)
                throw new MemberAccessException("At least 5 address line columns are required for parsing address with space as separator in  " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");

            prms.PostcodeColName = prms.PostcodeColName.Trim();

        }


        public override bool Execute()
        {
            ExecuteDTRule(ParentFTDT.FileTemplate.ProcessedDataTable);
            return true;
        }

        private void ExecuteDTRule(DataTable dt)
        {
            int colIndex = dt.Columns.IndexOf(prms.InputColumnName);

            if (colIndex >= 0)
            {
                foreach (var field in addresslines)
                {
                    //Add AddressLine columns if not exist
                    if (dt.Columns[field] == null)
                    {
                        DataColumn col = dt.AddColumn(field);
                        col.SetOrdinal(colIndex);
                        colIndex++;
                    }
                }

                //Add postcode column if not exist
                if (dt.Columns[prms.PostcodeColName] == null)
                {
                    DataColumn col = dt.AddColumn(prms.PostcodeColName);
                    col.SetOrdinal(colIndex);
                    colIndex++;
                }

                dt.AcceptChanges();

                var rows = dt.Select(prms.Filter);

                Regex regex = new Regex(prms.RegularExpression.RegExText);
                Match match = null;
                foreach (DataRow rw in rows)
                {
                    string value = rw[prms.InputColumnName].ToString();
                    if (!string.IsNullOrEmpty(value))
                    {
                        value = value.Trim(Utility.CharsForTrim.ToCharArray());
                        match = null;
                        List<string> elements = null;

                        if (prms.StringSeparator == Utility.SPACE_DELIMITER)
                        {
                            match = regex.Match(value);
                            if (match.Length > 0)
                            {
                                rw[prms.PostcodeColName] = match.Value;
                                value = regex.Replace(value, string.Empty).Trim(Utility.CharsForTrim.ToCharArray());
                            }
                            value = value.ToUpper().Trim(Utility.CharsForTrim.ToCharArray());

                            int LMProcessingLevel = prms.LMProcessingLevel.HasValue? prms.LMProcessingLevel.Value : 2;
                            for (int i = 0; i < (LMProcessingLevel); i++)
                            {
                                //Try to find town and district name from the end of the string.
                                foreach (var loc in prms.ReferenceList.ReferenceListItems)
                                {
                                    var locMatrix = loc.Value.ToUpper().Split(Utility.PIPE_DELIMITER.ToCharArray());
                                    if (locMatrix.Length != 2)
                                    {
                                        throw new MemberAccessException("Location Matrix setting for " + loc + " is incorrect in " + prms.Parent.ActionName + ", please update the Reference List and try again.");
                                    }
                                    locMatrix[0] = locMatrix[0].Trim(Utility.CharsForTrim.ToCharArray());
                                    locMatrix[1] = locMatrix[1].Trim(Utility.CharsForTrim.ToCharArray());

                                    if (value.EndsWith(locMatrix[0]))
                                    {
                                        string extractedText = (locMatrix[0] + ", " + rw[addresslines[int.Parse(locMatrix[1]) - 1]]).Trim(Utility.CharsForTrim.ToCharArray());
                                        rw[addresslines[int.Parse(locMatrix[1]) - 1]] = extractedText;
                                        value = value.Remove(value.Length - locMatrix[0].Length).Trim(Utility.CharsForTrim.ToCharArray());
                                        break;
                                    }
                                }

                            }

                            // The rest go to line 1
                            rw[addresslines[0]] = value;

                        }
                        else
                        {
                            elements = value.Split(prms.StringSeparator.ToCharArray()).Select(x => x.Trim(Utility.CharsForTrim.ToCharArray())).ToList();

                            if (elements.Count > 0)
                            {
                                for (int idx = elements.Count - 1; idx >= 0; idx--)
                                {
                                    match = regex.Match(elements[idx]);
                                    if (match.Length > 0)
                                    {
                                        rw[prms.PostcodeColName] = match.Value;
                                        elements[idx] = null;
                                        break;
                                    }
                                }

                                if (match.Length > 0)
                                {
                                    elements = elements.Where(x => x != null).ToList();
                                }

                                if (elements.Count > 0)
                                {
                                    int colCount = elements.Count > addresslines.Count ? addresslines.Count : elements.Count;
                                    for (int i = 0; i < colCount; i++)
                                    {
                                        rw[addresslines[i]] = elements[i];
                                    }

                                }

                            }
                        }
                    }

                }

            }
        }
    }
}