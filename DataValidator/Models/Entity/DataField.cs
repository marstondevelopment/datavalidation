﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(MetaData))]
    public partial class DataField
    {
        public FileTemplate FileTemplate { get; set; }
        public string OriginalValue { get; set; }

        private List<string> originalValues;
        public List<string> OriginalValues
        {
            get
            {
                if (originalValues == null)
                {
                    originalValues = new List<string>();
                }

                return originalValues;
            }
            set
            {
                originalValues = value;
            }
        }

        public object ObjectType()
        {
            switch ((EnumFieldType)FieldType)
            {
                case EnumFieldType.Boolean:
                    return true;
                case EnumFieldType.Date:
                    return DateTime.MinValue;
                case EnumFieldType.Decimal:
                    return decimal.MinValue;
                case EnumFieldType.Float:
                    return float.MinValue;
                case EnumFieldType.Integer:
                    return int.MinValue;
                case EnumFieldType.String:
                    return "Testing";
                default:
                    throw new NotSupportedException();
            }
        }
        public object TypedField()
        {
            if (string.IsNullOrEmpty(OriginalValue))
                return DBNull.Value;
            try
            {
                switch ((EnumFieldType)FieldType)
                {
                    case EnumFieldType.Boolean:
                        return Convert.ChangeType(OriginalValue, typeof(bool));
                    case EnumFieldType.Date:
                        return Convert.ChangeType(OriginalValue, typeof(DateTime));
                    case EnumFieldType.Decimal:
                        return Convert.ChangeType(OriginalValue, typeof(Decimal));
                    case EnumFieldType.Float:
                        return Convert.ChangeType(OriginalValue, typeof(float));
                    case EnumFieldType.Integer:
                        return Convert.ChangeType(OriginalValue, typeof(Int32));
                    case EnumFieldType.String:
                        return OriginalValue;
                    default:
                        throw new NotSupportedException();

                }
            }
            catch (Exception ex)
            {
                if (ex is FormatException)
                    throw new Exception(Name + ": System failed to covert  data ( " + OriginalValue + ")  to type - " + ((EnumFieldType)FieldType).ToString());

                throw;
            }

        }

        public string FormatedOutputValue
        {
            get
            {
                string val = OriginalValue;
                if (!string.IsNullOrEmpty(OriginalValue) && !string.IsNullOrEmpty(OutputFormat))
                {
                    switch ((EnumFieldType)FieldType)
                    {
                        case EnumFieldType.Integer:
                            val = ((int)TypedField()).ToString(OutputFormat);
                            break;
                        case EnumFieldType.Decimal:
                            val = ((decimal)TypedField()).ToString(OutputFormat);
                            break;
                        case EnumFieldType.Float:
                            val = ((float)TypedField()).ToString(OutputFormat);
                            break;
                        case EnumFieldType.Boolean:
                            val = ((bool)TypedField()).ToString();
                            break;
                        case EnumFieldType.Date:
                            val = ((int)TypedField()).ToString(OutputFormat);
                            break;
                        case EnumFieldType.String:
                        default:
                            break;
                    }

                }

                return val;


            }

        }

        public string PresetValue { get; set; }

        [Display(Name = "Referenced By")]
        public string AssocciatedFileTemplates
        {
            get
            {
                string fileNames = string.Empty;
                var fts = FileTemplateDataFields.Where(x => x.FileTemplate.Disabled == false).ToList();
                string appPath = System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/FileTemplate/Details/";
                if (fts.Count > 0)
                {
                    foreach (var item in fts)
                    {
                        fileNames = fileNames + " | <a href='" + appPath + item.FileTemplate.Id.ToString() + "'>" + item.FileTemplate.Name + "</a>";
                    }
                    fileNames = fileNames.Trim().Trim("|".ToCharArray());
                }
                return fileNames;
            }
        }

        private Dictionary<string, string> derivedSubNodes = null;
        internal Dictionary< string,string> DerivedSubNodes
        {
            get
            {
                return derivedSubNodes;
            }
            set
            {
                derivedSubNodes = value;
            }
        }

        private List<FileTemplate> assocFileTemplates;
        public List<FileTemplate> AssocFileTemplates
        {
            get
            {
                if (assocFileTemplates == null)
                {
                    assocFileTemplates = FileTemplateDataFields.Where(x => x.FileTemplate.Disabled == false).Select(y=> y.FileTemplate).ToList();
                }
                return assocFileTemplates;
            }

        }


        public bool Editable
        {
            get
            {
                User user = ((User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]);

                if (user.IsAdmin)
                    return true;

                if (Id > 0)
                {
                    List<int> fts = AssocFileTemplates.Select(x => x.Id).ToList();

                    if (user.AssociatedFileTemplates.Any(x => fts.Contains(x.Id)))
                        return true;

                    if (CreatedBy == user.Id || UpdatedBy == user.Id)
                        return true;

                    return false;
                }

                return true;

            }

        }

        public DataField Clone
        {
            get
            {
                User user = (User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                DataField datafield = new DataField()
                {
                    Name = Name,
                    Description = Description,
                    FieldType = FieldType,
                    Disabled = Disabled,
                    ExcludeFromOutput = ExcludeFromOutput,
                    OutputColumnName = OutputColumnName,
                    XMLChildNodeXPath = XMLChildNodeXPath,
                    HasMultipleNodes = HasMultipleNodes,
                    OutputFieldOnly = OutputFieldOnly,
                    FileSection = FileSection,
                    OutputFormat = OutputFormat,
                    DefaultValue = DefaultValue,
                    CoordinateForVariable = CoordinateForVariable,
                    CreatedBy = user.Id,
                    DateCreated = DateTime.Now,
                    UpdatedBy = user.Id,
                    DateUpdated = DateTime.Now,
                    FieldWidth = FieldWidth,
                    DCExpression = DCExpression,
                    Parameterize = Parameterize,
                    ParameterLabel = ParameterLabel,
                    XMLListElementId = XMLListElementId,
                    ChildNodes  = ChildNodes
            };

                foreach(DataFieldDataCleansing dataFieldDC in DataFieldDataCleansings)
                {
                    datafield.DataFieldDataCleansings.Add(dataFieldDC.Clone());                
                }

                foreach(DataFieldRule dataFieldRule in DataFieldRules)
                {
                    datafield.DataFieldRules.Add(dataFieldRule.Clone);
                }
                return datafield;
            }
        }

        public string DisplayDescription
        {
            get
            {
                return !string.IsNullOrEmpty(Description) ? Description : "N/A";
            }
        }

        public string FieldStatus
        {
            get
            {
                return !Disabled ? "Yes" : "No";
            }
        }

        public string DisplayFieldType
        {
            get
            {
                EnumFieldType fileType = (EnumFieldType)FieldType;
                return fileType.ToString();
            }
        }

        public string DisplayFileSection
        {
            get
            {
                return (FileSection == 0 ? string.Empty:((EnumFileSection)FileSection).ToString());
            }
        }

        public string DisplayExcludeFromOutput
        {
            get
            {
                return ExcludeFromOutput == true ? "Yes" : "No";
            }
        }

        public string DisplayOutputColumnName
        {
            get
            {
                return !string.IsNullOrEmpty(OutputColumnName) ? OutputColumnName : "N/A";
            }
        }

        public string DisplayXMLChildNodeXPath
        {
            get
            {
                return !string.IsNullOrEmpty(XMLChildNodeXPath) ? XMLChildNodeXPath : "N/A";
            }
        }

        public string ParamInputType
        {
            get
            {
                string inputType = string.Empty;
                switch ((EnumFieldType) FieldType)
                {
                    case EnumFieldType.Integer:
                        inputType = " type= number ";
                        break;
                    case EnumFieldType.Decimal:
                        inputType = " type= number step= 0.01 ";
                        break;
                    case EnumFieldType.Float:
                        inputType = " type= number ";
                        break;
                    case EnumFieldType.Boolean:
                        inputType = " type= checkbox ";
                        break;
                    case EnumFieldType.String:
                        inputType = "type= text ";
                        break;
                    case EnumFieldType.Date:
                        inputType = "type= date ";
                        break;
                    default:
                        break;
                }

                if (IsMandatory)
                    inputType = inputType + " required ";
                return inputType;
            }
        }

        public bool IsMandatory
        {
            get
            {
                return DataFieldRules.Where(x=> x.VR == EnumValidationRule.MandatoryField).FirstOrDefault() != null;
            }
        }



        public void PopulateOVWithDVOrPV()
        {
            string defaultVal = DefaultValue;
            if (!string.IsNullOrEmpty(defaultVal))
            {
                string regexPattern = Utility.VariableTagPattern;
                var regex = new Regex(regexPattern);
                var results = regex.Matches(defaultVal);
                foreach (var item in results)
                {
                    string varName = item.ToString().Replace("{",string.Empty).Replace("}",string.Empty).Trim();
                    var varObj = FileTemplate.Columns.Where(x => x.Name == varName).FirstOrDefault();
                    if (varObj != null)
                        defaultVal = defaultVal.Replace(item.ToString(), varObj.OriginalValue);
                }
            }

            //Set default value before adding row
            if (string.IsNullOrEmpty(OriginalValue) && !string.IsNullOrEmpty(PresetValue))
                OriginalValue = PresetValue;
            else if (string.IsNullOrEmpty(OriginalValue) && !string.IsNullOrEmpty(defaultVal))
                OriginalValue = defaultVal;

            if (HasMultipleNodes)
            {
                string oldVal = OriginalValue;
                for (int i = 0; i < OriginalValues.Count; i++)
                {
                    //Set default value for the multinode items
                    if (string.IsNullOrEmpty(OriginalValues[i]) && !string.IsNullOrEmpty(PresetValue))
                        OriginalValues[i] = PresetValue;
                    else if (string.IsNullOrEmpty(OriginalValues[i]) && !string.IsNullOrEmpty(defaultVal))
                        OriginalValues[i] = defaultVal;
                }
                OriginalValue = oldVal;
            }
        }

        private List<DataField> childNodesColumns;
        public List<DataField> ChildNodesColumns
        {
            get
            {
                if (childNodesColumns == null)
                {
                    childNodesColumns = new List<DataField>();
                    if (!string.IsNullOrEmpty( ChildNodes) && FileTemplate != null)
                    {
                        List<string> colNames = ChildNodes.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();
                        childNodesColumns = FileTemplate.BodyCols.Where(x => colNames.Contains(x.Name)).ToList();
                    }
                }
                return childNodesColumns;
            }
        }



        public class MetaData
        {
            [Required(ErrorMessage = "Field Name is required.")]
            public string Name;
            [Display(Name = "Field Type")]
            [Required(ErrorMessage = "Field Type is required.")]
            public int FieldType;
            [Display(Name = "Updated By")]
            public int UpdatedBy;
            [Display(Name = "Date Updated")]
            public DateTime DateUpdated;
            [Display(Name = "Enabled")]
            public bool Disabled;
            [Display(Name = "Exclude From Output")]
            public bool ExcludeFromOutput;
            [Display(Name = "Output Column Name")]
            public string OutputColumnName;
            [Display(Name = "XPath")]
            public string XMLChildNodeXPath;
            [Display(Name = "Multiple Nodes")]
            public bool HasMultipleNodes;
            [Display(Name = "Output Field Only")]
            public bool OutputFieldOnly;
            [Display(Name = "File Section")]
            public int FileSection;
            [Display(Name = "Output Format")]
            public string OutputFormat;
            [Display(Name = "Default Value")]
            public string DefaultValue;
            [Display(Name = "Variable Coord. (+/-R,C)")]
            public string CoordinateForVariable;
            [Display(Name = "DC Expression")]
            public string DCExpression;
            [Display(Name = "Parameterize")]
            public bool Parameterize;
            [Display(Name = "Parameter Label")]
            public bool ParameterLabel;
            [Display(Name = "XML List Element")]
            public int? XMLListElementId;
            [Display(Name = "Child Nodes")]
            public string ChildNodes;
        }

    }
}