﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic;
using System.Web;
using System.Diagnostics;
using Microsoft.VisualBasic.FileIO;
using WebGrease.Css.Extensions;
using System.Text;

namespace Mgl.DataValidator.Models
{
    public class NLSFileParser : FileParser
    {
        public NLSFileParser(FileTemplate ft, Stream fs, string fileName, EnumDataFileScreen userAction) : base(ft, fs, fileName, userAction)
        {
        }

        protected bool ReadDataFile(string fileContext, string delimiter)
        {
            bool valid = true;
            Dictionary<int, DataField> colsMap = null;
            int colCount = FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList().Count;
            List<string> inputFields = fileContext.Split(new string[] { delimiter }, StringSplitOptions.None).ToList();

            //Remove file header
            if (FileTemplate.RemoveHeader)
            {
                if (!FileTemplate.HeaderRowsToSkip.HasValue)
                    throw new Exception("HeaderRowsToSkip not set for the file template. Update the FT settings and try again");
                if (inputFields.Count < FileTemplate.HeaderRowsToSkip.Value)
                    throw new Exception("Failed to remove file header. i.e No. line in input file is less than header rows to be removed.");

                inputFields.RemoveRange(0, FileTemplate.HeaderRowsToSkip.Value);
            }
            //Remove file footer
            if (FileTemplate.RemoveFooter)
            {
                if (!FileTemplate.FooterRowsToSkip.HasValue)
                    throw new Exception("FooterRowsToSkip not set for the file template. Update the FT settings and try again");
                if (inputFields.Count < FileTemplate.FooterRowsToSkip.Value)
                    throw new Exception("Failed to remove file footer. i.e No. line in input file is less than footer rows to be removed.");

                inputFields.RemoveRange((inputFields.Count - FileTemplate.FooterRowsToSkip.Value - 1), FileTemplate.FooterRowsToSkip.Value);
            }
            // Check if the remaining fields can be mapped to the FT data fields correctly
            if (inputFields.Count % colCount != 0)
                //Failed to map the column from the file
                throw new Exception("Failed to parse data file, " + FileTemplate.ParentDataFile.FileName + ": Number of fields not match against the file template - " + FileTemplate.Name + ". Make sure the data file is in correct format.");

            colsMap = MapColumns(inputFields);

            if (inputFields.Count == 0)
                throw new MissingFieldException(FileTemplate.Name + " has no data");

            int recCount = inputFields.Count / colCount;

            for (int i = 0; i < recCount; i++)
            {
                List<string> record = inputFields.GetRange(i * colCount, colCount);

                FileTemplate.ResetFieldValues();
                FileTemplate.RowId = i + 1;
                try
                {
                    foreach (var df in colsMap)
                    {
                        df.Value.OriginalValue = record[df.Key];
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error occurred during parsing data file at row " + FileTemplate.RowId.ToString() + ": " + ex.Message + " \n Stack Trace: " + ex.StackTrace);
                }

                //Validate each row
                if (!FileTemplate.ValidateRows())
                {
                    if (FileTemplate.TerminateProcess)
                        return false;
                    valid = false;
                }
            }

            return valid;
        }

        public override bool Validate()
        {
            bool valid = true;
            try
            {
                using (UploadedFile)
                {
                    string fileContext;

                    using (var reader = new StreamReader(UploadedFile, true))
                    {
                        fileContext = reader.ReadToEnd();
                    }

                    valid = ReadDataFile(fileContext, Environment.NewLine);
                }

            }
            catch (Exception ex)
            {
                if (ex is MissingFieldException)
                {
                    ValidationSummary.Clear();
                    ValidationSummary.AppendLine(ex.Message);
                    return false;
                }
                else
                    throw;                
            }

            return valid;
        }

        private Dictionary<int, DataField> MapColumns(List<string> inputFields)
        {
            Dictionary<int, DataField> colMap = new Dictionary<int, DataField>();
            List<DataField> dfs = FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList();

            if (FileTemplate.NoColumnName == false)
            {
                string[] colNames = inputFields.GetRange(0, dfs.Count).Select(x=> x.ToUpper()).ToArray();

                foreach (var df in dfs)
                {
                    int colIndex = Array.IndexOf(colNames, df.Name.ToUpper());
                    if (colIndex == -1)
                        //Failed to map the column from the file
                        throw new MissingFieldException("Failed to parse data file. Column - " + df.Name.ToUpper() + " not found.");
                    colMap.Add(colIndex, df);
                }
                //Remove field names from the input fields
                inputFields.RemoveRange(0, dfs.Count);
            }
            else
            {
                for (int i = 0; i < dfs.Count; i++)
                {
                    colMap.Add(i, dfs[i]); //add the cell contents (name of column) and cell index to the map
                }
            }

            return colMap;
        }
    }
}