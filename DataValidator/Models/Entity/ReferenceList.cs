﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class ReferenceList
    {
        public string ReferencedBy
        {
            get
            {
                string refBy = string.Empty;
                string appPath = System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/DataField/Details/";
                if (DataFieldRules.Count > 0)
                {
                    foreach (var item in DataFieldRules)
                    {
                        refBy  = refBy + " | <a href='" + appPath + item.DataFieldId.ToString() + "'>" + item.DataField.Name + "</a>";
                    }
                    refBy = refBy.Trim().Trim("|".ToCharArray());
                }

                return refBy;
            }
        }
        public class Metadata
        {
            [Required(ErrorMessage = "Reference List Name is required.")]
            public string Name;
            [Display(Name = "Case Sensitive")]
            public bool CaseSensitive;
        }
    }
}