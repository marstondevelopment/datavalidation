﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  Mgl.DataValidator.Models.ValidationRules;
using  Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class DbConnection 
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string DisplayConnectionAvailability
        {
            get { return !Disabled ? "Yes" : "No"; }
        }

        public class Metadata
        {
            [Display(Name = "Connection Name")]
            [Required(ErrorMessage = "Connection Name required.")]
            public string Name;
            [Display(Name = "Connection String")]
            [Required(ErrorMessage = "Connection String required.")]
            public string ConnectionString;           
            [Display(Name = "Active Connection")]
            public string DisplayConnectionAvailability;
            [Display(Name = "DB Server Type")]
            [Required(ErrorMessage = "Server Type required.")]            
            public int DBServerType;
        }
    }

}