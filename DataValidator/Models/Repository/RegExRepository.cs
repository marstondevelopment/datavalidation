﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    public class RegExRepository : RepositoryBase<DataIntegrationEntities, RegularExpression>
    {
        public RegExRepository(DataIntegrationEntities entity) : base(entity)
        {
        }

        public override IQueryable<RegularExpression> GetAll()
        {
            return base.GetAll(x=> x.Disabled == false);
        }

        public RegularExpression GetById(int id)
        {
            return _entities.RegularExpressions.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Create(RegularExpression tp)
        {
            this.Add(tp);
            this.SubmitChanges();
        }

        public void Update(RegularExpression tp)
        {          
            base.SubmitChanges();
            
        }

        public void Delete(RegularExpression tp)
        {                       
            tp.Disabled = true;
            Update(tp);           
        }

        public void Reinstate(RegularExpression tp)
        {            
            tp.Disabled = false;
            Update(tp);            
        } 

    }
}