﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Mgl.DataValidator.Models;

namespace Mgl.DataValidator.Models
{
    public class FileTemplateRepository : RepositoryBase<DataIntegrationEntities, FileTemplate>
    {
        private UserRepository _ur;
        private StoredProcedureRepository _spr;

        public const string BU_REP_MAXROWS = "{max_no_row}";
        public const string UPLOADED_ON = "UploadedOn";
        public const string IMPORTED = "Imported";
        public const string UPLOADED_BY = "UploadedBy";
        public const string FUT_USER_ID = "FUTUserId";
        public const string CLIENT_ID = "ClientId";
        public const string CLIENT_NAME = "ClientName";
        public const string BUREPORT_CLIENTS_CMDTXT = "OS_BU_Clients";

        public FileTemplateRepository(DataIntegrationEntities entity, UserRepository ur, StoredProcedureRepository spr) : base(entity)
        {
            _ur = ur;
            _spr = spr;
        }

        public override IQueryable<FileTemplate> GetAll()
        {
            return base.GetAll(x => true && x.Disabled == false);
        }

        private void PresetDBCmd(IDBCmd cmd, FileTemplate ft)
        {
            if (cmd.StoredProcedureId.HasValue)
            {
                cmd.StoredProcedure = _spr.GetById(cmd.StoredProcedureId.Value);
                cmd.StoredProcedure.FileTemplate = ft;
            }
            if (cmd.CmdTextId.HasValue)
            {
                cmd.CmdText = _entities.CmdTexts.Where(x => x.Id == cmd.CmdTextId.Value).FirstOrDefault();
                cmd.CmdText.FileTemplate = ft;
            }
        }
        public FileTemplate GetById(int id)
        {
            FileTemplate ft = GetAll(x => x.Id == id).FirstOrDefault();
            var actions = ft.FileTemplateDTActions.Where(x => x.DTAction.DTActionTypeId == (int)EnumDTActionType.OSExcludeCasesAlreadyImported 
                                                            || x.DTAction.DTActionTypeId == (int)EnumDTActionType.GetDataFromDB 
                                                            || x.DTAction.DTActionTypeId == (int)EnumDTActionType.XMappingDataFromDB
                                                            || x.DTAction.DTActionTypeId == (int)EnumDTActionType.OSConcatenateFields
                                                            || x.DTAction.DTActionTypeId == (int)EnumDTActionType.ExcludeRowsNotMatchReferenceList
                                                            || x.DTAction.DTActionTypeId == (int)EnumDTActionType.SplitAndTransposeOutputFile
                                                            || x.DTAction.DTActionTypeId == (int)EnumDTActionType.SplitFullAddressColIntoSepLines).ToList();

            foreach (var item in actions)
            {
                switch (item.DTAction.DTActionType)
                {
                    case EnumDTActionType.OSExcludeCasesAlreadyImported:
                        PresetDBCmd((IDBCmd)item.DTAction.PrmsExcludeCasesAI, ft);
                        break;

                    case EnumDTActionType.GetDataFromDB:
                        PresetDBCmd((IDBCmd)item.DTAction.PrmsGetDataFromDB, ft);
                        break;

                    case EnumDTActionType.XMappingDataFromDB:
                        PresetDBCmd((IDBCmd)item.DTAction.PrmsXMappingDataFromDB, ft);
                        break;
                    case EnumDTActionType.SplitFullAddressColIntoSepLines:
                        item.DTAction.PrmsSplitFullAddressColIntoSepLines.RegularExpression = 
                            _entities.RegularExpressions.Where(x => x.Id == item.DTAction.PrmsSplitFullAddressColIntoSepLines.RegularExpressionId).FirstOrDefault();
                        if (item.DTAction.PrmsSplitFullAddressColIntoSepLines.ReferenceListId.HasValue)
                            item.DTAction.PrmsSplitFullAddressColIntoSepLines.ReferenceList =
                                _entities.ReferenceLists.Where(x => x.Id == item.DTAction.PrmsSplitFullAddressColIntoSepLines.ReferenceListId.Value).FirstOrDefault();

                        break;
                    case EnumDTActionType.ExcludeRowsNotMatchReferenceList:
                        if (item.DTAction.PrmsExcludeRowsNotMatchReferenceList.ReferenceListId.HasValue)
                            item.DTAction.PrmsExcludeRowsNotMatchReferenceList.ReferenceList =
                                _entities.ReferenceLists.Where(x => x.Id == item.DTAction.PrmsExcludeRowsNotMatchReferenceList.ReferenceListId.Value).FirstOrDefault();

                        break;
                    case EnumDTActionType.SplitAndTransposeOutputFile:
                        if (item.DTAction.PrmsSplitAndTransposeOutputFile.RegexRefListId.HasValue)
                            item.DTAction.PrmsSplitAndTransposeOutputFile.RegexRefList=
                                _entities.ReferenceLists.Where(x => x.Id == item.DTAction.PrmsSplitAndTransposeOutputFile.RegexRefListId.Value).FirstOrDefault();

                        break;
                    case EnumDTActionType.OSConcatenateFields:
                        if (item.DTAction.PrmsConcatenateFields.RegularExpressionId.HasValue)
                            item.DTAction.PrmsConcatenateFields.RegularExpression =
                                _entities.RegularExpressions.Where(x => x.Id == item.DTAction.PrmsConcatenateFields.RegularExpressionId.Value).FirstOrDefault();
                        if (item.DTAction.PrmsConcatenateFields.RegExRefListId.HasValue)
                            item.DTAction.PrmsConcatenateFields.RegExRefList =
                                _entities.ReferenceLists.Where(x => x.Id == item.DTAction.PrmsConcatenateFields.RegExRefListId.Value).FirstOrDefault();
                        break;
                    default:
                        break;
                }
            }

            var vrs = ft.FileTemplateDataFields.Where(x => x.DataField.DataFieldRules.Count(y => y.VR == EnumValidationRule.SP_Validation) > 0).ToList();
            foreach (var vr in vrs)
            {
                var sps = vr.DataField.DataFieldRules.Where(x => x.VR == EnumValidationRule.SP_Validation).ToList();
                foreach (var sp in sps)
                {
                    sp.StoredProcedure.FileTemplate = ft;
                }

            }

            return ft;
        }


        public FileTemplateDataField GetFileTemplateDataFieldById(int id)
        {
            return _entities.FileTemplateDataFields.Where(x => x.Id == id).FirstOrDefault();
        }

        public FileTemplateDTAction GetFileTemplateDTActionById(int id)
        {
            return _entities.FileTemplateDTActions.Where(x => x.Id == id).FirstOrDefault();
        }

        public FileTemplateARAction GetFileTemplateARActionById(int id)
        {
            return _entities.FileTemplateARActions.Where(x => x.Id == id).FirstOrDefault();
        }

        public void DeleteFileTemplateDataField(FileTemplateDataField ftdf)
        {
            _entities.FileTemplateDataFields.Remove(ftdf);
            this.SubmitChanges();
        }

        public void DeleteFileTemplateDTAction(FileTemplateDTAction ftdta)
        {
            _entities.FileTemplateDTActions.Remove(ftdta);
            SubmitChanges();
        }

        public void DeleteFileTemplateARAction(FileTemplateARAction ftara)
        {
            _entities.FileTemplateARActions.Remove(ftara);
            SubmitChanges();
        }

        public void ClearFileTemplateARActionList(int fileTemplateId)
        {
             var ftaraList = _entities.FileTemplateARActions.Where(x => x.FileTemplateId == fileTemplateId);
            _entities.FileTemplateARActions.RemoveRange(ftaraList);
            SubmitChanges();
        }

        public IEnumerable<FileTemplateDataField> UpdateTemplateDataFieldList(FileTemplate fileTemplate, int[] dfIds)
        {
            _entities.FileTemplateDataFields.RemoveRange(fileTemplate.FileTemplateDataFields);
            this.SubmitChanges();
            List<FileTemplateDataField> dfs = new List<FileTemplateDataField>();
            foreach (var id in dfIds)
            {
                dfs.Add(new FileTemplateDataField()
                {
                    DataFieldId = id,
                    FileTemplateId = fileTemplate.Id
                });
            }
            _entities.FileTemplateDataFields.AddRange(dfs);
            this.SubmitChanges();
            return _entities.FileTemplateDataFields.Include("DataField").Where(x => x.FileTemplateId == fileTemplate.Id).OrderBy(x => x.Id).ToList();
        }

        public IEnumerable<FileTemplateDTAction> UpdateTemplateDTActionList(FileTemplate fileTemplate, int[] dtaIds)
        {
            _entities.FileTemplateDTActions.RemoveRange(fileTemplate.FileTemplateDTActions);
            this.SubmitChanges();
            List<FileTemplateDTAction> ftdta = new List<FileTemplateDTAction>();
            foreach (var id in dtaIds)
            {
                ftdta.Add(new FileTemplateDTAction()
                {
                    DTActionId = id,
                    FileTemplateId = fileTemplate.Id
                });
            }
            _entities.FileTemplateDTActions.AddRange(ftdta);
            this.SubmitChanges();            
            return _entities.FileTemplateDTActions.Include("DTAction").Where(x => x.FileTemplateId == fileTemplate.Id).OrderBy(x => x.Id).ToList();
        }

        public IEnumerable<DbConnection> GetDbConnections()
        {
            List<DbConnection> dbConnections = this._entities.DbConnections.Where(x => !x.Disabled).ToList();
            return dbConnections;
        }

        public IEnumerable<MimeType> GetMimeTypes()
        {
            List<MimeType> mimeTypes = this._entities.MimeTypes.ToList();
            return mimeTypes;
        }

        public void CreateConnection(DbConnection dbConnection)
        {
            this._entities.DbConnections.Add(dbConnection);
            SubmitChanges();
            return;
        }

        public void DeleteConnection(DbConnection dbConnection)
        {
            dbConnection.Disabled = true;
            SubmitChanges();
        }

        public void Create(FileTemplate template)
        {
            template.CreatedBy = _ur.CurrentUser.Id;
            template.UpdatedBy = _ur.CurrentUser.Id;
            template.DateCreated = DateTime.Now;
            template.DateUpdated = DateTime.Now;
            Add(template);
            SubmitChanges();
            _ur.RefreshCurrentUser();
            return;
        }

        public void Update(FileTemplate template)
        {
            template.UpdatedBy = _ur.CurrentUser.Id;
            template.DateUpdated = DateTime.Now;
            this.SubmitChanges();
        }

        public void UpdateConnection(DbConnection connection)
        {
            SubmitChanges();
        }

        public void Delete(FileTemplate template)
        {
            template.Disabled = true;
            Update(template);
            _ur.RefreshCurrentUser();
        }

        public void Reinstate(FileTemplate template)
        {
            template.Disabled = false;
            Update(template);
            _ur.RefreshCurrentUser();
        }

        public IEnumerable<DataField> GetDataFields(User user)
        {

            if (user.IsAdmin)
                return _entities.DataFields.Where(x => !x.Disabled).OrderByDescending(x => x.DateUpdated).Take(Utility.MaxListItems);
            else
            {
                if (user.AssociatedFileTemplates != null && user.AssociatedFileTemplates.Count > 0)
                {
                    var ftIds = user.AssociatedFileTemplates.Select(x => x.Id).ToList();
                    var results = _entities.DataFields.Where(x => !x.Disabled
                        && (x.FileTemplateDataFields.Any(y => (ftIds.Contains(y.FileTemplateId))) || x.CreatedBy == user.Id || x.UpdatedBy == user.Id)).OrderByDescending(x => x.DateUpdated).Take(Utility.MaxListItems);
                    return results;
                }
                else
                {
                    return _entities.DataFields.Where(x => !x.Disabled
                        && (x.CreatedBy == user.Id || x.UpdatedBy == user.Id)).OrderByDescending(x => x.DateUpdated).Take(Utility.MaxListItems);
                }

            }
        }

        public FileTemplate DeepClone(FileTemplate ft)
        {
            FileTemplate ftClone = new FileTemplate()
            {
                Name = string.Format("Copy of " + ft.Name),
                Description = ft.Description,
                DBConnectionId = ft.DBConnectionId,
                Disabled = ft.Disabled,
                MimeTypeId = ft.MimeTypeId,
                ClientId = ft.ClientId,
                BulkUpload = ft.BulkUpload,
                BUStagingTable = ft.BUStagingTable,
                TransformData = ft.TransformData,
                DTStoredProcedureId = ft.DTStoredProcedureId,
                DTFileWriter = ft.DTFileWriter,
                RemoveHeader = ft.RemoveHeader,
                RemoveFooter = ft.RemoveFooter,
                NoColumnName = ft.NoColumnName,
                DelimiterId = ft.DelimiterId,
                GenerateOutputFile = ft.GenerateOutputFile,
                OutputFileDelimiterId = ft.OutputFileDelimiterId,
                XMLRootNodeXPath = ft.XMLRootNodeXPath,
                HeaderRowsToSkip = ft.HeaderRowsToSkip,
                FooterRowsToSkip = ft.FooterRowsToSkip,
                GenerateAuditFile = ft.GenerateAuditFile,
                ARTemplateScript = ft.ARTemplateScript,
                AuditFileExtenstion = ft.AuditFileExtenstion,
                OutputFileEncoding = ft.OutputFileEncoding,
                OFExcludeColumnHeader = ft.OFExcludeColumnHeader,
                UploadToFileRepository = ft.UploadToFileRepository,
                RepositoryFilePath = ft.RepositoryFilePath,
                FNPAsSubfolder = ft.FNPAsSubfolder,
                FNTimestamp = ft.FNTimestamp,
                UseXsd = ft.UseXsd,
                XsdFileId = ft.XsdFileId,
                OverrideDefaultDTActionSequence = ft.OverrideDefaultDTActionSequence,
                MergeDTAStages = ft.MergeDTAStages,
                DataEntry = ft.DataEntry,
                DataEntryFormId = ft.DataEntryFormId,
                DEStagingTable = ft.DEStagingTable,
                BUReport = ft.BUReport,
                BUReportClientIds = ft.BUReportClientIds,
                BUReportCmdTextId = ft.BUReportCmdTextId,
                ARFileNamePrefix = ft.ARFileNamePrefix,
                ARFileNameNoSuffix = ft.ARFileNameNoSuffix
            };
            foreach (var item in ft.FileTemplateDataFields)
            {
                ftClone.FileTemplateDataFields.Add(new FileTemplateDataField() { DataField = item.DataField.Clone, FileTemplate = ftClone });
            }

            foreach (var item in ft.FileTemplateDTActions)
            {
                ftClone.FileTemplateDTActions.Add(new FileTemplateDTAction { DTAction = item.DTAction.Clone, FileTemplate = ftClone });
            }

            foreach (var item in ft.FileTemplateARActions)
            {
                ftClone.FileTemplateARActions.Add(new FileTemplateARAction { ARAction = item.ARAction.Clone, FileTemplate = ftClone });
            }

            Create(ftClone);

            //Refresh the current user profile to reflect this changes
            //_ur.CurrentUser.AssociatedFileTemplates.Add(ftClone);
           // _ur.RefreshCurrentUser();

            return ftClone;
        }

        public IEnumerable<DTAction> GetDTActions()
        {
            return _entities.DTActions.OrderByDescending(y => y.DateUpdated).Take(Utility.MaxListItems).ToList();
        }

        public IEnumerable<FileTemplateDataField> GetFTDataFields(int ftId)
        {
            return _entities.FileTemplateDataFields.Where(x => !x.DataField.Disabled && x.FileTemplateId == ftId).OrderBy(x => x.Id).ToList();
        }

        public IEnumerable<FileDelimiter> GetFileDelimiters()
        {
            return _entities.FileDelimiters.ToList();
        }
        public IEnumerable<XsdFile> GetXsdFiles()
        {
            return _entities.XsdFiles.ToList();
        }

        public IEnumerable<ARAction> GetARActions()
        {
            return _entities.ARActions.OrderByDescending(y => y.DateUpdated).Take(Utility.MaxListItems).ToList();
        }

        public IEnumerable<FileTemplateDataField> AddDFToTemplate(FileTemplateDataField templateDataField)
        {
            base._entities.FileTemplateDataFields.Add(templateDataField);
            this.SubmitChanges();
            return _entities.FileTemplateDataFields.Include("DataField").Where(x => x.FileTemplateId == templateDataField.FileTemplateId).OrderBy(x => x.Id).ToList();
        }

        public IEnumerable<FileTemplateDTAction> AddDTAToTemplate(FileTemplateDTAction templateDTAction)
        {
            _entities.FileTemplateDTActions.Add(templateDTAction);
            SubmitChanges();
            return _entities.FileTemplateDTActions.Include("DTAction").Where(x => x.FileTemplateId == templateDTAction.FileTemplateId).ToList();
        }

        public IEnumerable<FileTemplateARAction> AddARAToTemplate(FileTemplateARAction templateARAction)
        {
            _entities.FileTemplateARActions.Add(templateARAction);
            SubmitChanges();
            return _entities.FileTemplateARActions.Include("ARAction").Where(x => x.FileTemplateId == templateARAction.FileTemplateId).ToList();
        }

        public IEnumerable<FileTemplateDTAction> GetFTDTActions(int ftId)
        {
            return _entities.FileTemplateDTActions.Where(x=> x.FileTemplateId == ftId).OrderBy(x=> x.Id).ToList();
        }

        public IEnumerable<FileTemplateARAction> GetFTARActions(int ftId)
        {
            return _entities.FileTemplateARActions.Where(x=> x.FileTemplateId == ftId).OrderBy(x => x.Id).ToList();
        }

        public void UpdateEventLog(EventLog eventLog)
        {
            eventLog.UserId = _ur.CurrentUser.Id;
            _entities.EventLogs.Add(eventLog);
            SubmitChanges();
        }

        public bool GetBUAuditReport(BUAuditSearchPrms<DataRow> searchPrms, FileTemplate fileTemplate, ref IEnumerable<DataRow> searchResults)
        {
            string sqlSelect = fileTemplate.BUSReportCmdText.SQLScript.Replace(BU_REP_MAXROWS, Utility.MaxListItems.ToString());
            string sqlWhere = string.Empty;

            if (searchPrms.FromDate.HasValue || searchPrms.ToDate.HasValue || searchPrms.ClientId.HasValue || _ur.CurrentUser.RoleId == (int)EnumRole.ReadOnly)
            {
                sqlWhere = " WHERE ";

                sqlWhere = sqlWhere + (searchPrms.FromDate.HasValue ? " UploadedOn >= '" + searchPrms.FromDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' AND" : string.Empty);
                sqlWhere = sqlWhere + (searchPrms.ToDate.HasValue ? " UploadedOn < '" + searchPrms.ToDate.Value.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") + "' AND" : string.Empty);
                if (searchPrms.ClientId.HasValue)
                {
                    sqlWhere = sqlWhere + " ClientId = " + searchPrms.ClientId.Value.ToString() + " AND";
                }
                else
                {
                    sqlWhere = sqlWhere + (!string.IsNullOrEmpty(fileTemplate.BUReportClientIds) ? " ClientId in (" + fileTemplate.BUReportClientIds.Replace(" ", string.Empty) + ") AND" : string.Empty);
                }
                sqlWhere = sqlWhere + (_ur.CurrentUser.RoleId == (int)EnumRole.ReadOnly ? " FUTUserId = " + _ur.CurrentUser.Id.ToString() + " AND" : string.Empty);
                sqlWhere = sqlWhere.Trim().TrimEnd("AND".ToCharArray());
            }

            sqlSelect = sqlSelect + (!string.IsNullOrEmpty(sqlWhere) ? " " + sqlWhere : string.Empty) + " ORDER BY UploadedOn DESC";
            fileTemplate.BUSReportCmdText.SQLScript = sqlSelect;
            DataProvider dp = DataProvider.New(fileTemplate);
            bool succeed = false;
            try
            {
                DataTable dt = dp.GetDataCmdText(fileTemplate.BUSReportCmdText);          
                dt.Columns.Add(UPLOADED_BY).SetOrdinal(dt.Columns[FUT_USER_ID].Ordinal + 1);
                dt.Columns.Add(CLIENT_NAME).SetOrdinal(dt.Columns[CLIENT_ID].Ordinal + 1);

                if (dt.Rows.Count > 0)
                {
                    var users = _ur.GetAll().ToList();
                    foreach (DataRow row  in dt.Rows)
                    {
                        if (row[FUT_USER_ID] != null)
                        {
                            User user = users.Where(x => x.Id == int.Parse(row[FUT_USER_ID].ToString())).ToList().FirstOrDefault();
                            row[UPLOADED_BY] = user.UserId;
                        }
                        if (row[CLIENT_ID] != null)
                        {
                            var cl= searchPrms.Clients.Where(x => x.Value == row[CLIENT_ID].ToString()).FirstOrDefault();
                            if (cl != null)
                            {
                                row[CLIENT_NAME] = cl.Text;
                            }
                        }

                    }

                    searchPrms.CasesImported = dt.Select("[Imported] = 1").Count();
                    searchPrms.CasesWithError = dt.Select("[Imported] = 0 AND [ErrorMessage] IS NOT NULL").Count();
                    searchPrms.AppendingCases = dt.Select("[Imported] = 0 AND [ErrorMessage] IS NULL").Count();

                    dt.Columns.Remove(FUT_USER_ID);
                    dt.Columns.Remove(CLIENT_ID);
                }

                searchResults = dt.AsEnumerable();
                succeed = true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            return succeed;
        }

        public List<SelectListItem> GetBUReportClients(FileTemplate ft)
        {
            string cacheName = BUREPORT_CLIENTS_CMDTXT + "_" + ft.Id.ToString();
            List<SelectListItem> clients = (List<SelectListItem>)HttpContext.Current.Cache[cacheName];

            if (clients == null)
            {
                CmdText cmdText = _entities.CmdTexts.Where(x => x.Name == BUREPORT_CLIENTS_CMDTXT).FirstOrDefault();
                cmdText.SQLScript = cmdText.SQLScript.Replace("{ClientIds}", ft.BUReportClientIds.Replace(" ", string.Empty));
                var dt = ft.DataProvider.GetDataCmdText(cmdText);
                var rows = dt.Select();
                clients = rows.Select(x => new SelectListItem() { Text = x["Name"].ToString(), Value = x["Id"].ToString() }).ToList();
                HttpContext.Current.Cache.Add(cacheName, clients, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(2, 0, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                _entities.Entry(cmdText).Reload();
            }

            return clients;
        }
    }
}