﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public class VRFieldType : VRBase
    {
        public VRFieldType(DataFieldRule df) : base(df)
        {
        }

        public override bool ValidateField()
        {
            switch ((EnumFieldType)ParentFieldRule.DataField.FieldType)
            {
                case EnumFieldType.Integer:
                    if (!int.TryParse(ParentFieldRule.DataField.OriginalValue, out int intOut))
                    {
                        AppendError();
                        return false;
                    }

                    break;
                case EnumFieldType.Decimal:
                    if (!decimal.TryParse(ParentFieldRule.DataField.OriginalValue, out decimal decOut ))
                    {
                        AppendError();
                        return false;
                    }
                    break;
                case EnumFieldType.Float:
                    if (!float.TryParse(ParentFieldRule.DataField.OriginalValue, out float fltOut))
                    {
                        AppendError();
                        return false;
                    }
                    break;
                case EnumFieldType.Boolean:
                    if (!bool.TryParse(ParentFieldRule.DataField.OriginalValue, out bool bOut))
                    {
                        AppendError();
                        return false;
                    }
                    break;
                case EnumFieldType.Date:
                    if (!DateTime.TryParse(ParentFieldRule.DataField.OriginalValue, out DateTime dtOut))
                    {
                        AppendError();
                        return false;
                    }
                    break;
                default:
                    break;
            }

            return true;
        }
    }
}