﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetIgnoredFileRowCount : ARStep
    {
        public ARGetIgnoredFileRowCount(FileTemplateARAction ftar) : base(ftar)
        {
        }
        public override bool Execute(ref string arScript)
        {
            int rowCount = 0;

            if (ParentFTAR.ARAction.DataSource == EnumARSourceFile.PerIgnoredFile)
            {
                OutputFile ignoredFile = ParentFTAR.FileTemplate.FileWriter.ProcessingFile;
                rowCount = ignoredFile.Data.Rows.Count;
            }
            else
            {
                foreach (OutputFile item in ParentFTAR.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Ignored))
                {
                    rowCount = rowCount + item.Data.Rows.Count;
                }
            }

            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, rowCount.ToString());
            return true;
        }
    }
}