﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public abstract class DEARBase
    {
        [Display(Name = "Imported")]
        public bool Imported { get; set; }
        [Display(Name = "Imported On")]
        public DateTime? ImportedOn { get; set; }
        [Display(Name = "User Id")]
        public int? FUTUserId { get; set; }
        public int? ErrorId { get; set; }
        [Display(Name = "Error")]
        public string ErrorMessage { get; set; }
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }
    }
}