﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsComputeAggregateFunction : PrmsBase
    {
        public PrmsComputeAggregateFunction() { }
        public PrmsComputeAggregateFunction(DTAction dt) : base(dt) { }

        [Display(Name = "Variable Name")]
        public string VariableName { get; set; }

        [Display(Name = "Total Field Name")]
        public string TotalFieldName { get; set; }

        [Display(Name = "Aggre. Expression")]
        public string AggregateExpression { get; set; }

        [Display(Name = "Filter")]
        public string Filter { get; set; }

    }
}