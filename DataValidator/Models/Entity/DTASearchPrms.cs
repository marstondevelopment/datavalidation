﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class DTASearchPrms<T>:SearchPrms<T>
    {
        public DTASearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults,int? dtActionTypeId):base( sortOrder,  searchString,  page,  pageSize,  defaultPageSize, allResults)
        {
            DTActionTypeId = dtActionTypeId;
        }

        [Display(Name = "DT Action Type")]
        public int? DTActionTypeId { get; set; }

    }
}