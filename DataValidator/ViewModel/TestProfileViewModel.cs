﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.ViewModel
{
    public class TestProfileViewModel
    {
        private TestProfileRepository _tpr;
        public TestProfileViewModel(TestProfileRepository tpr, TestProfile testProfile)
        {
            _tpr = tpr;
            TestProfiles = _tpr.GetAll().ToList();
            TestResults = _tpr.GetTestResults(((User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]).Id);
            TestProfile = testProfile;
        }

        public TestProfile TestProfile { get; set; }

        public IEnumerable<TestProfile> TestProfiles;
        public IEnumerable<TestResult> TestResults { get; set; }

        private List<SelectListItem> selectFTItems = null;

        public List<SelectListItem> SelectFTItems
        {
            get
            {
                if (selectFTItems == null)
                {
                    selectFTItems = new List<SelectListItem>();
                    selectFTItems.AddRange(_tpr.FTList().Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));
                }
                return selectFTItems;
            }
        }
    }
}