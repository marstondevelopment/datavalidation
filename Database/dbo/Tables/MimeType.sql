﻿CREATE TABLE [dbo].[MimeType] (
    [Id]        INT          IDENTITY (1, 1) NOT NULL,
    [Name]      VARCHAR (50) NOT NULL,
    [Extension] VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_MIMEType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

