﻿using log4net;
using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Mgl.DataValidator
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private User _user;
        private UserRepository _ur;
        private const string LAST_LOGGED_ON = "LAST_LOGGED_ON";
        protected void Application_Start()
        {
            // Register Inversion of Control dependencies
            IoCConfig.RegisterDependencies();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(MvcApplication));

        public UserRepository UserRepository
        {
            get
            {
                if (_ur == null)
                {
                    _ur = new UserRepository(new DataIntegrationEntities());
                }
                return _ur;
            }
        }

        void Application_Error(Object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError().GetBaseException();
            logger.Error(ex);
        }

        protected void Application_AuthorizeRequest(object sender, EventArgs e)
        {

            if (User.Identity != null && HttpContext.Current.Request.Cookies[LAST_LOGGED_ON] == null)
            {
                string[] ntLogon = User.Identity.Name.Split("\\".ToCharArray());
                _user = UserRepository.GetUser(ntLogon[0], ntLogon[1]);

                if (_user == null)
                {
                    UserRepository.AddUser(new Models.User()
                    {
                        UserId = ntLogon[1],
                        DomainName = ntLogon[0],
                        RoleId = (int)EnumRole.ReadOnly
                    });
                }
                else
                {
                    if (HttpContext.Current.Request.Cookies[LAST_LOGGED_ON] == null)
                    {
                        //Record user last logged on date.
                        UserRepository.UpdateUser(_user);
                    }
                }

                HttpCookie httpCookie = new HttpCookie(LAST_LOGGED_ON);
                DateTime now = DateTime.Now;
                httpCookie.Value = now.ToString();
                httpCookie.Expires = now.AddHours(1);
                Response.Cookies.Add(httpCookie);
            }

        }

        protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
        {
            //if (HttpContext.Current.Session != null && HttpContext.Current.Session[HttpContext.Current.Session.SessionID] == null)
            //{
            //    if (_user == null)
            //    {
            //        string[] ntLogon = User.Identity.Name.Split("\\".ToCharArray());
            //        _user = UserRepository.GetUser(ntLogon[0], ntLogon[1]);
            //    }
            //    HttpContext.Current.Session.Add(HttpContext.Current.Session.SessionID, _user);
            //}

            if (HttpContext.Current.Session != null && HttpContext.Current.Session[HttpContext.Current.Session.SessionID] == null)
            {
                _user = UserRepository.CurrentUser;

            }

        }
    }
}
