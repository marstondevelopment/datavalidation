﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public abstract class DataProvider
    {
        
        protected FileTemplate _ft;
        public DataProvider(FileTemplate fileTemplate)
        {
            _ft = fileTemplate;
        }

        public abstract bool UploadData();
        public abstract bool ValidateData(StoredProcedure sp);
        public abstract bool ValidateDataCmdText(CmdText ct);
        public abstract bool TransformData();

        public abstract DataTable GetData(StoredProcedure sp);
        public abstract DataTable GetDataCmdText(CmdText ct);


        public static DataProvider New(FileTemplate fileTemplate)
        {
            switch ((EnumDBServer) fileTemplate.DbConnection.DBServerType)
            {
                case EnumDBServer.SqlServer:
                    return new SqlSvrDataProvider(fileTemplate);
                case EnumDBServer.MySQL:
                    return new MySQLDataProvider(fileTemplate);
                case EnumDBServer.Oracle:
                default:
                    throw new NotImplementedException("Data Provider not implemented for this DB server - " + ((EnumDBServer)fileTemplate.DbConnection.DBServerType).ToString());
            }

        }

        protected DataTable GetBUSourceTbl()
        {
            DataTable sourceTbl = null;
            if (_ft.UseProcessedTbl)
            {
                sourceTbl = _ft.ProcessedDataTable;
                sourceTbl.CopyAttributes(_ft.BUDataTable);
            }
            else
            {
                sourceTbl = _ft.BUDataTable;
            }

            foreach (var df in _ft.BodyCols.Where(x => x.ExcludeFromOutput == true).ToList())
            {
                sourceTbl.Columns.Remove(df.Name);
            }

            foreach (var df in _ft.BodyCols.Where(x => x.ExcludeFromOutput == false && !string.IsNullOrEmpty(x.OutputColumnName)).ToList())
            {
                DataColumn col = sourceTbl.Columns[df.Name];
                col.ColumnName = df.OutputColumnName;
            }

            return sourceTbl;
        }
    }
}