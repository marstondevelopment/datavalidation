﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.ViewModel
{
    public class DBConnectionViewModel
    {
        public IEnumerable<DbConnection> Connections { get; set; }
        public DbConnection DBConnection { get; set; }
    }
}