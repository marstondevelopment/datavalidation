﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitColumn : PrmsBase
    {
        public PrmsSplitColumn() { }
        public PrmsSplitColumn(DTAction dt) : base(dt) { }

        [Display(Name = "Input Column Name")]
        public string InputColumnName { get; set; }
        [Display(Name = "Output Column Names")]
        public string OutputColumnNames { get; set; }
        public string Prefix { get; set; }
        [Display(Name = "Output Column Prefix")]
        public string OutputColumnHasPrefix { get; set; }
    }
}