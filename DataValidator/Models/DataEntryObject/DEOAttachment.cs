﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class DEOAttachment: DEOBase 
    {
        public DEOAttachment() { }

        public DEOAttachment(DEOBase parent, HttpPostedFileBase file ) 
        {
            _parent = parent;
            string fileExt = Path.GetExtension(file.FileName.ToLower());
            ContentType = Utility.ContentTypes.Where(x => x.Key == fileExt).FirstOrDefault().Value;
            EnumContentType ct;
            if (Enum.TryParse(fileExt.Trim(".".ToCharArray()), out ct))
            {
                FileType = ct;
            }

            FileName = file.FileName;
            FileStream = file;
        }
        private DEOBase _parent;

        [Display(Name = "File Name")]
        public string FileName { get; set; }

        [Display(Name = "Content Type")]
        public string ContentType { get; set; }

        [Display(Name = "File Type")]
        public EnumContentType FileType{ get; set; }

        [Display(Name = "File Content")]
        public HttpPostedFileBase FileStream { get; set; }

        public override bool Validate(DataEntryRepository der)
        {
            bool valid = true;
            if (string.IsNullOrEmpty(FileName) || string.IsNullOrEmpty(ContentType) || (FileStream == null || FileStream.InputStream.Length<=0))
            {
                string error = "File Name, Content Type, and File Content are required for DEOAttachment ";
                _parent.Errors.AppendLine(error);
                if (_parent.FileTemplate.ParentDataFile != null)
                    _parent.FileTemplate.ParentDataFile.ValidationSummary.AppendLine(error);
                valid = false;
            }
            return valid;
        }
    }
}