﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public class VRMinFieldLength : VRBase
    {
        public VRMinFieldLength(DataFieldRule df) : base(df)
        {
        }

        public override bool ValidateField()
        {
            if (!string.IsNullOrEmpty(ParentFieldRule.DataField.OriginalValue))
            {
                if (ParentFieldRule.DataField.OriginalValue.Length < ParentFieldRule.MinFieldLength)
                {
                    AppendError();
                    return false;
                }
            }

            return true;
        }
    }
}