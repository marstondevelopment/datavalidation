﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using PagedList;
using System.Configuration;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class DTActionController : ControllerBase
    {

        private DTActionRepository _dtr;
        private const string SEARCH_PRMS = "DTASearchPrms";
        public DTActionController(DTActionRepository dtr)
        {
            _dtr = dtr;
        }

        protected DTASearchPrms<DTAction> CurrentSearchPrms
        {
            get
            {
                return Session[SEARCH_PRMS] == null ? null : (DTASearchPrms<DTAction>)this.Session[SEARCH_PRMS];
            }
            set
            {
                Session.Add(SEARCH_PRMS, value);
            }
        }

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, int? dtActionTypeId)
        {


            DTASearchPrms<DTAction> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentSearchPrms != null)
            {
                searchPrms = CurrentSearchPrms;                
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                searchPrms = new DTASearchPrms<DTAction>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, dtActionTypeId);
            }

            var dTActions = _dtr.GetAll().Where(x => (string.IsNullOrEmpty(searchPrms.SearchString) || (x.ActionName.Contains(searchPrms.SearchString))) && (!searchPrms.DTActionTypeId.HasValue || x.DTActionTypeId == searchPrms.DTActionTypeId.Value)).OrderByDescending(x => x.DateUpdated).ToList();
            CurrentSearchPrms = new DTASearchPrms<DTAction>(searchPrms.SortOrder,searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, dTActions.ToList(), searchPrms.DTActionTypeId);

            return View(CurrentSearchPrms).Information("Search results found: " + CurrentSearchPrms.RowsCount.ToString());
        }


        public ActionResult Details(int id)
        {
            DTActionViewModel vm = new DTActionViewModel(_dtr, _dtr.GetDTActionById(id));
            vm.IsDetails = true;
            return View(vm);
        }

        public ActionResult GetDTActionParam(int? dtActionTypeId, int? dtActionId)
        {
            DTActionViewModel vm = dtActionId.HasValue
                ? new DTActionViewModel(_dtr, _dtr.GetDTActionById(dtActionId.Value))
                : new DTActionViewModel(_dtr, null);

            if (dtActionTypeId.HasValue)
            {
                return PartialView("_" + ((EnumDTActionType)dtActionTypeId.Value).ToString(), vm);
            }

            return null;
        }

        private string ValidateDTActionFields(int dtActionTypeId)
        {
            string message = string.Empty;
            decimal result;
            switch ((EnumDTActionType)dtActionTypeId)
            {
                case EnumDTActionType.OSExcludeCasesAlreadyImported:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsExcludeCasesAI.ValidationMethodId"]))
                        message = "Please select a validation method and a SP/Command Text accordingly, and try again.";

                    if (Request.Form["DTAction.PrmsExcludeCasesAI.ValidationMethodId"] == "1" && string.IsNullOrEmpty(Request.Form["DTAction.PrmsExcludeCasesAI.StoredProcedureId"]))
                        message = "Stored Procedure required.";

                    if (Request.Form["DTAction.PrmsExcludeCasesAI.ValidationMethodId"] == "2" && string.IsNullOrEmpty(Request.Form["DTAction.PrmsExcludeCasesAI.CmdTextId"]))
                        message = "Command Text required.";

                    break;
                case EnumDTActionType.OSGroupBy:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsGroupBy.FieldNames"]))
                        return message = "Field Names Required";
                    break;
                case EnumDTActionType.OSMergeDebts:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsMergeDebts.DebtFieldName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsMergeDebts.TotalFieldName"]))
                        message = "Debt Field Name and Grouping Field Names are Required.";
                    break;
                case EnumDTActionType.OSExcludeCasesLessThanThreadholdVal:
                    if (!decimal.TryParse(Request.Form["DTAction.PrmsExcludeCasesLLTV.MinThreadhold"], out result))
                    {
                        return message = "Number required for Min Threadhold";
                    }

                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsExcludeCasesLLTV.MinThreadhold"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsExcludeCasesLLTV.FieldName"]))
                        return message = "Min Threadhold and Field Name are Required.";
                    break;
                case EnumDTActionType.OSExtractComments:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsExtractComments.CommentFieldName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsExtractComments.OutputFieldName"]))
                        return message = "Comment Field Name and Output Field Name are Required.";
                    break;
                case EnumDTActionType.OSSplitOutputFileBasedOnFields:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitOutputFile.FieldValues"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitOutputFile.Scheme"]))
                        return message = "Field Values and Scheme are Required.";
                    break;
                case EnumDTActionType.ExcludeSelectedRows:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsExcludeSelectedRows.FieldValues"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsExcludeSelectedRows.Scheme"]))
                        return message = "Field Values and Scheme are Required.";
                    break;
                case EnumDTActionType.OSSplitColumn:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitColumn.InputColumnName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitColumn.OutputColumnNames"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitColumn.Prefix"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitColumn.OutputColumnHasPrefix"]))
                        return message = "Input Column Name, Output Column Names, Prefix and Output Column HasPrefix are Required.";
                    break;
                case EnumDTActionType.OSConcatenateFields:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsConcatenateFields.InputFieldNames"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsConcatenateFields.OutputFieldName"]))
                        return message = "Input Field Names and Output Field Name are Required.";
                    break;
                case EnumDTActionType.ConcatenateMultiNodesDFs:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsConcatenateMultiNodesDFs.InputFieldNames"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsConcatenateMultiNodesDFs.OutputFieldName"]))
                        return message = "Input Field Names and Output Field Name are Required.";
                    break;
                case EnumDTActionType.SplitAndTransposeOutputFile:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitAndTransposeOutputFile.IDColName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitAndTransposeOutputFile.OtherColNames"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitAndTransposeOutputFile.Scheme"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitAndTransposeOutputFile.OutputColNames"])
                        )
                        return message = "ID Col. Name, Other Col. Names, Scheme, and 2 Output Col. Names are Required.";
                    break;
                case EnumDTActionType.OSAddProcessInfo:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsAddProcessInfo.FieldName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsAddProcessInfo.ProcessInfoId"]))
                        return message = "Field Name and Process Info are Required.";
                    break;
                case EnumDTActionType.OSGetSumOfColumns:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetSumOfColumns.ColumnNames"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetSumOfColumns.TotalFieldName"]))
                        return message = "Column Names and Total Field Name are Required.";
                    break;
                case EnumDTActionType.OSExtractLinkedCasesAfterGroupBy:
                    if (!decimal.TryParse(Request.Form["DTAction.PrmsExtractLinkedCasesAfterGroupBy.ThresholdValue"], out result))
                    {
                        return message = "Number required for Threadhold Value";
                    }
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsExtractLinkedCasesAfterGroupBy.GBFieldNames"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsExtractLinkedCasesAfterGroupBy.FilterFieldName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsExtractLinkedCasesAfterGroupBy.ThresholdValue"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsExtractLinkedCasesAfterGroupBy.CompOpId"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsExtractLinkedCasesAfterGroupBy.SchemeName"])
                        )
                        return message = "GB Field Names, Filter Field Name, Threshold Value, Comparison Operator and Scheme Name are Required.";
                    break;
                case EnumDTActionType.OSSplitNameColumnIntoThree:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitNameColumnIntoThree.InputColumnName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitNameColumnIntoThree.OutputColumnNames"]))
                        return message = "Input Column Name and Output Column Names are required.";
                    break;
                case EnumDTActionType.GetDataFromDB:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetDataFromDB.GetMethodId"]))
                        message = "Please select a get method and a SP/Command Text accordingly, and try again.";

                    if (Request.Form["DTAction.PrmsGetDataFromDB.GetMethodId"] == "1" && string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetDataFromDB.StoredProcedureId"]))
                        message = "Stored Procedure required.";

                    if (Request.Form["DTAction.PrmsGetDataFromDB.GetMethodId"] == "2" && string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetDataFromDB.CmdTextId"]))
                        message = "Command Text required.";

                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetDataFromDB.FieldsToPopulate"]))
                        message = "Fields To Populate required";

                    break;
                case EnumDTActionType.GetTotalOfAColumn:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetTotalOfAColumn.ColumnName"])
                        || string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetTotalOfAColumn.DataSourceFileId"])
                        || string.IsNullOrEmpty(Request.Form["DTAction.PrmsGetTotalOfAColumn.TotalFieldName"]))
                        return message = "Column Name and Total Field Name are Required.";
                    break;
                case EnumDTActionType.SplitOutputAfterGrouping:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitOutputAfterGrouping.GroupingFields"])
                        || string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitOutputAfterGrouping.FNPrefixField"]))
                        return message = "Grouping Fields and FN Prefix Field are Required.";
                    break;
                case EnumDTActionType.SetDuplicatedRowsWithUniqueValues:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsSetDuplicatedRowsWithUniqueValues.GroupByColumnNames"])
                        || string.IsNullOrEmpty(Request.Form["DTAction.PrmsSetDuplicatedRowsWithUniqueValues.FieldRequiresUniqueValue"]))
                        return message = "Grouping By Column Names and Field Requires Unique Value are Required.";
                    break;
                case EnumDTActionType.SplitFullAddressColIntoSepLines:
                    if (string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitFullAddressColIntoSepLines.InputColumnName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitFullAddressColIntoSepLines.AddLineColNames"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitFullAddressColIntoSepLines.PostcodeColName"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitFullAddressColIntoSepLines.RegularExpressionId"]) ||
                        string.IsNullOrEmpty(Request.Form["DTAction.PrmsSplitFullAddressColIntoSepLines.StringSeparator"])
                        )
                        return message = "Input Column Name, AddLine Col Names, Postcode Col Names, Regular Expression and String Separator are Required.";

                    if (Request.Form["DTAction.PrmsSplitFullAddressColIntoSepLines.StringSeparator"] == Utility.SPACE_DELIMITER
                        && (string.IsNullOrEmpty( Request.Form["DTAction.PrmsSplitFullAddressColIntoSepLines.ReferenceListId"]))
                        )
                        message = "Please select a reference list for location matrix, and try again.";
                    break;
                default:
                    break;
            }

            return message;
        }

        public ActionResult Create()
        {
            DTActionViewModel vm = new DTActionViewModel(_dtr, null);
            return View(vm);
        }


        [HttpPost]
        public ActionResult Create(DTAction dtAction, FormCollection collection)
        {
            DTActionViewModel vm = new DTActionViewModel(_dtr, dtAction);
            string message = string.Empty;
            if (vm.DTAction != null)
            {
                message = ValidateDTActionFields(vm.DTAction.DTActionTypeId);
                if (string.IsNullOrEmpty(message))
                {
                    _dtr.Create(vm.DTAction);
                    return RedirectToAction("Index").Success("DT Action has been created successfully.");
                }
            }
            return View(vm).Danger(message);
        }

        public ActionResult Clone(int? id)
        {
            if (id.HasValue)
            {
                DTAction currentDTAction = _dtr.GetDTActionById(id.Value);
                DTAction clonedDTAction = currentDTAction.Clone;
                _dtr.Create(clonedDTAction);
                return RedirectToAction("Edit", new { id = clonedDTAction.Id }).Success("Clone of " + currentDTAction.ActionName + " has been created successfully.");

            }
            return RedirectToAction("Index").Danger("DT Action failed to clone.");
        }

        public ActionResult Delete(int id)
        {
            DTActionViewModel vm = new DTActionViewModel(_dtr, _dtr.GetDTActionById(id));

            if (vm.DTAction != null)
            {
                int associatedTemplates = vm.DTAction.FileTemplateDTActions.Where(x => x.FileTemplate.Disabled == false).Count();

                if (associatedTemplates > 0)
                {
                    return RedirectToAction("Index").Danger("Failed to delete " + vm.DTAction.ActionName + " as some active file templates are referencing it.");
                }
                _dtr.Delete(vm.DTAction);

                DTASearchPrms<DTAction> searchPrms = CurrentSearchPrms;

                var dTActions = _dtr.GetAll().Where(x => (string.IsNullOrEmpty(searchPrms.SearchString) || (x.ActionName.Contains(searchPrms.SearchString))) && (!searchPrms.DTActionTypeId.HasValue || x.DTActionTypeId == searchPrms.DTActionTypeId.Value)).OrderByDescending(x => x.DateUpdated).ToList();
                CurrentSearchPrms = new DTASearchPrms<DTAction>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, dTActions.ToList(), searchPrms.DTActionTypeId);
                return PartialView("_Index", CurrentSearchPrms);
            }
            return RedirectToAction("Index").Danger(vm.DTAction.ActionName + " has failed to delete.");
        }


        public ActionResult Edit(int id)
        {

            DTActionViewModel vm = new DTActionViewModel(_dtr, _dtr.GetDTActionById(id));

            if (vm.DTAction.FileTemplateDTActions.Count >1 && !string.IsNullOrEmpty(vm.DTAction.AssociatedFileTemplate))
                return View(vm).Warning("Warning: This DT Action has been referenced by the following file templates. You are advised to implement system risk assessment before making any changes.<br/>" + vm.DTAction.AssociatedFileTemplatesForCookie);

            return View(vm);
        }


        [HttpPost]
        public ActionResult Edit(int? id, FormCollection collection)
        {
            DTActionViewModel vm = new DTActionViewModel(_dtr, _dtr.GetDTActionById(id.Value));
            string message = string.Empty;

            TryUpdateModel(vm);

            if (vm.DTAction != null)
            {
                message = ValidateDTActionFields(vm.DTAction.DTActionTypeId);
                if (string.IsNullOrEmpty(message))
                {

                    _dtr.Update(vm.DTAction);
                    if (!string.IsNullOrEmpty(Request.Form["UrlRef"]))
                    {
                        return Redirect(Request.Form["UrlRef"]).Success(vm.DTAction.ActionName + " DT Action has been updated successfully");
                    }
                    return RedirectToAction("Index").Success(vm.DTAction.ActionName + " DT Action has been updated successfully");
                }
                else
                {
                    return View(vm).Danger(message);
                }
            }
            return RedirectToAction("Index").Danger("DT Action failed to update.");
        }

    }
}
