﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class DTActionManager
    {
        private static Dictionary<EnumDTActionType, int> dtActionsExecutionOrder;
        //This dictionary dictates the execution order of the DTAction. Re-arranging the order will impact the results of the output data
        public static Dictionary<EnumDTActionType, int> DTActionsExecutionOrder
        {
            get
            {
                if (dtActionsExecutionOrder == null)
                {
                    int index = 1;
                    dtActionsExecutionOrder = new Dictionary<EnumDTActionType, int>
                    {
                        { EnumDTActionType.GetDataFromDB, index++ },
                        { EnumDTActionType.XMappingDataFromDB, index++ },
                        { EnumDTActionType.OSGetSumOfColumns, index++ },
                        { EnumDTActionType.SetDuplicatedRowsWithUniqueValues, index++ },
                        { EnumDTActionType.OSGroupBy, index++ },
                        { EnumDTActionType.OSMergeDebts, index++ },
                        { EnumDTActionType.OSExcludeCasesAlreadyImported, index++ },
                        { EnumDTActionType.OSExcludeCasesLessThanThreadholdVal, index++ },
                        { EnumDTActionType.OSExtractComments, index++ },
                        { EnumDTActionType.OSConcatenateFields, index++ },
                        { EnumDTActionType.ConcatenateMultiNodesDFs, index++ },
                        { EnumDTActionType.ConcatenateXMLListItemsDFs, index++ },
                        { EnumDTActionType.OSTransformDefaulterAddresses, index++ },
                        { EnumDTActionType.OSAssignDefaultPostcode, index++ },
                        { EnumDTActionType.OSNCTransformDefaulterAddresses, index++ },
                        { EnumDTActionType.OSExtractLinkedCasesAfterGroupBy, index++ },
                        { EnumDTActionType.TruncateFieldsExceedingMaxLength, index++ },
                        { EnumDTActionType.ComputeAggregateFunction, index++ },
                        { EnumDTActionType.OSSplitOutputFileBasedOnFields, index++ },
                        { EnumDTActionType.SplitAndReshapeOutputFiles, index++ },
                        { EnumDTActionType.SplitAndTransposeOutputFile, index++ },
                        { EnumDTActionType.SplitOutputAfterGrouping, index++ },
                        { EnumDTActionType.OSSplitColumn, index++ },
                        { EnumDTActionType.SplitFullAddressColIntoSepLines, index++ },
                        { EnumDTActionType.OSSplitNameColumnIntoThree, index++ },
                        { EnumDTActionType.SplitOneColIntoMultipleCols, index++ },
                        { EnumDTActionType.OSAddProcessInfo, index++ },
                        { EnumDTActionType.GetTotalOfAColumn, index++ },
                        { EnumDTActionType.ExcludeSelectedRows, index++ },
                        { EnumDTActionType.ExcludeRowsNotMatchReferenceList, index++ }
                    };
                }
                return dtActionsExecutionOrder;
            }
        }
        private static Dictionary<EnumDTActionType, EnumDTActionGroup> dtActionsGroup;
        public static Dictionary<EnumDTActionType, EnumDTActionGroup> DTActionsGroup
        {
            get
            {
                if (dtActionsGroup == null)
                {
                    dtActionsGroup = new Dictionary<EnumDTActionType, EnumDTActionGroup>
                    {
                        { EnumDTActionType.OSExcludeCasesAlreadyImported, EnumDTActionGroup.DT },
                        { EnumDTActionType.GetDataFromDB, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSGroupBy, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSMergeDebts, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSExcludeCasesLessThanThreadholdVal, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSExtractComments, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSConcatenateFields, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSSplitOutputFileBasedOnFields, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSSplitColumn, EnumDTActionGroup.FW },
                        { EnumDTActionType.OSAddProcessInfo, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSGetSumOfColumns, EnumDTActionGroup.DT },
                        { EnumDTActionType.XMappingDataFromDB, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSExtractLinkedCasesAfterGroupBy, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSSplitNameColumnIntoThree, EnumDTActionGroup.FW },
                        { EnumDTActionType.GetTotalOfAColumn, EnumDTActionGroup.DT },
                        { EnumDTActionType.SplitOutputAfterGrouping, EnumDTActionGroup.DT },
                        { EnumDTActionType.SplitOneColIntoMultipleCols, EnumDTActionGroup.FW },
                        { EnumDTActionType.ExcludeSelectedRows, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSTransformDefaulterAddresses, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSNCTransformDefaulterAddresses, EnumDTActionGroup.DT },
                        { EnumDTActionType.SetDuplicatedRowsWithUniqueValues, EnumDTActionGroup.DT },
                        { EnumDTActionType.TruncateFieldsExceedingMaxLength, EnumDTActionGroup.DT },
                        { EnumDTActionType.SplitFullAddressColIntoSepLines, EnumDTActionGroup.DT },
                        { EnumDTActionType.ComputeAggregateFunction, EnumDTActionGroup.DT },
                        { EnumDTActionType.OSAssignDefaultPostcode, EnumDTActionGroup.DT },
                        { EnumDTActionType.SplitAndReshapeOutputFiles, EnumDTActionGroup.DT },
                        { EnumDTActionType.ExcludeRowsNotMatchReferenceList, EnumDTActionGroup.DT },
                        { EnumDTActionType.ConcatenateMultiNodesDFs, EnumDTActionGroup.DT },
                        { EnumDTActionType.SplitAndTransposeOutputFile, EnumDTActionGroup.DT },
                        { EnumDTActionType.ConcatenateXMLListItemsDFs, EnumDTActionGroup.DT }
                    };
                }
                return dtActionsGroup;
            }
        }

    }
}