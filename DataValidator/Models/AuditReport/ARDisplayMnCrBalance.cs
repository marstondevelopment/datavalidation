﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARDisplayMnCrBalance : ARStep
    {
        private ARPrmsDisplayMnCrBalance prms;
        public ARDisplayMnCrBalance(FileTemplateARAction ftar) : base(ftar)
        {
            prms = ftar.ARAction.ARPrmsDisplayMnCrBalance;
            if (prms == null)
            {
                throw new MemberAccessException("ARDisplayMnCrBalance parameters are missing for " + ftar.ARAction.Name + ", please update the configuration settings and try again.");
            }
            if (prms.InputColumnNamesList == null || prms.OutputColumnNamesList == null || prms.InputColumnNamesList.Count != prms.OutputColumnNamesList.Count)
            {
                throw new MemberAccessException("ARDisplayMnCrBalance number of columns for input and output not match" + ftar.ARAction.Name + ", please update the settings correctly and try again.");
            }
        }
        public override bool Execute(ref string arScript)
        {

            StrBuilder.AppendLine(string.Join(Utility.DEFAULT_DELIMITER, prms.OutputColumnNamesList));

            //ParentFTAR.FileTemplate.RefreshGroupedByTable();

            if (ParentFTAR.FileTemplate.GroupedDataTable != null)
            {
                var mergedRows = ParentFTAR.FileTemplate.GroupedDataTable.Where(x => x.Count() > 1).ToList();
                if (mergedRows != null && mergedRows.Count > 0)
                {
                    foreach (var group in mergedRows)
                    {
                        foreach (DataRow row in group)
                        {
                            List<string> fieldVals = new List<string>();
                            foreach (var col in prms.InputColumnNamesList)
                            {
                                DataColumn dc = ParentFTAR.FileTemplate.BUDataTable.Columns[col];
                                fieldVals.Add(row[col].ObjToString(dc).WrapInQuotesIfContains(Utility.DEFAULT_DELIMITER));
                            }

                            StrBuilder.AppendLine(string.Join(Utility.DEFAULT_DELIMITER, fieldVals));
                        }

                    }
                }

            }

            arScript = arScript.RenderTag(prms.Parent.TagName, StrBuilder.ToString());
            return true;
        }


    }
}