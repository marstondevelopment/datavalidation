﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  Mgl.DataValidator.Models.ValidationRules;
using  Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    //[Serializable]
    [MetadataType(typeof(Metadata))]
    public partial class Client
    {
        public string DisplayClientStatus
        {
            get
            {
                return !Disabled ? "Yes" : "No";
            }
        }

        public string DisplayDescription
        {
            get
            {
                return !string.IsNullOrEmpty(Description) ? Description : "N/A";
            }
        }

        public class Metadata
        {
            [Required(ErrorMessage = "Client name is required.")]
            public string Name;
            [Display(Name = "Enabled")]
            public bool Disabled;
        }
    }
}