﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Diagnostics;
using NPOI.HSSF.Extractor;
using ExcelDataReader;

namespace Mgl.DataValidator.Models
{
    public class ExcelBIFF5FileParser : FileParser
    {
        public ExcelBIFF5FileParser(FileTemplate ft, Stream fs, string fileName, EnumDataFileScreen userAction) : base(ft, fs, fileName, userAction)
        {
        }


        public override bool Validate()
        {
            bool valid = true;
            Dictionary<int, DataField> colsMap = null;
            try
            {
                using (UploadedFile)
                {

                    IExcelDataReader reader = ExcelDataReader.ExcelReaderFactory.CreateReader(UploadedFile);
                    var ds = reader.AsDataSet();

                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        var tbl = ds.Tables[i];

                        colsMap = null;
                        if (tbl.Rows.Count > 0)
                        {
                            
                            FileTemplate.CurrentFileName = tbl.TableName;
                            // Copy data from excel row to columns in DataRow
                            for (int rowIndex = 0; rowIndex < tbl.Rows.Count; rowIndex++)
                            {
                                DataRow row = tbl.Rows[rowIndex];

                                //Remove Header
                                if (FileTemplate.RemoveHeader && rowIndex < FileTemplate.HeaderRowsToSkip)
                                    continue;

                                if (row.IsEmpty())
                                    break;

                                //Map columns
                                if (colsMap == null)
                                {
                                    colsMap = MapColumns(row);
                                    if (FileTemplate.NoColumnName == false) //skip the line if the file has column name
                                        continue;
                                }

                                //Remove Footer
                                if (FileTemplate.RemoveFooter && rowIndex >= tbl.Rows.Count - FileTemplate.FooterRowsToSkip)
                                    continue;

                                FileTemplate.ResetFieldValues();
                                FileTemplate.RowId = rowIndex + 1;
                                try
                                {
                                    foreach (var col in colsMap)
                                    {
                                        var cell = row[col.Key];
                                        col.Value.OriginalValue = cell.ObjToString(tbl.Columns[col.Key]);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error occurred during parsing data file at row " + FileTemplate.RowId.ToString() + ": " + ex.Message + " \n Stack Trace: " + ex.StackTrace);
                                }

                                //Validate each row
                                if (!FileTemplate.ValidateRows())
                                {
                                    if (FileTemplate.TerminateProcess)
                                        return false;
                                    valid = false;
                                }
                            }

                        }

                    }

                }

            }
            catch (Exception ex)
            {
                if (ex is MissingFieldException || ex is ArgumentException)
                {
                    ValidationSummary.Clear();
                    ValidationSummary.AppendLine(ex.Message);
                    //ValidationSummary.AppendLine("*********************************************************************************************");
                    //ValidationSummary.AppendLine("*** Warning:  The selected file template can only parse files with extension: xls or xlsx.***");
                    //ValidationSummary.AppendLine("*********************************************************************************************");

                    return false;
                }
                else
                    throw;
            }


            return valid;
        }

        private Dictionary<int, DataField> MapColumns(DataRow row)
        {
            Dictionary<int, DataField> colMap = new Dictionary<int, DataField>();


            if (FileTemplate.NoColumnName == false)  // With column header
            {
                for (int i = 0; i < row.Table.Columns.Count; i++)
                {
                    row.Table.Columns[i].ColumnName = row[i].ToString().Trim().ToUpper();
                }


                foreach (var df in FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList())
                {
                    DataColumn col = row.Table.Columns[df.Name.Trim().ToUpper()];
                    if (col == null)
                    {
                        throw new MissingFieldException("Failed to parse data file. " + FileTemplate.CurrentFileName + ": Column - " + df.Name.ToUpper() + " not found. Make sure there is a corresponding column in data file.");
                    }

                    colMap.Add(col.Ordinal, df);

                }
            }
            else // Without column header
            {
                var inputCols = FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList();
                if (inputCols.Count > row.Table.Columns.Count)
                {
                    throw new MissingFieldException("Failed to parse data file. Number of columns from input file( " + row.Table.Columns.Count.ToString() + ") is less than " + FileTemplate.CurrentFileName + " (" + inputCols.Count.ToString() +  "). Please ensure the file format and data fields setting are in sync, and try again.");
                }
                for (int i = 0; i < inputCols.Count; i++)
                {
                    colMap.Add(i, inputCols[i]); //add the cell contents (name of column) and cell index to the map
                }
            }

            return colMap;
        }



    }
}