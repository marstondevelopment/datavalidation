﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsGetTotalOfAColumn : PrmsBase
    {
        public PrmsGetTotalOfAColumn() { }
        public PrmsGetTotalOfAColumn(DTAction dt) : base(dt) { }

        [Display(Name = "Column Name")]
        public string ColumnName { get; set; }
        [Display(Name = "Total Field Name")]
        public string TotalFieldName { get; set; }

        [Display(Name = "Data Source File")]
        public int DataSourceFileId { get; set; }
        [JsonIgnore]
        public EnumDTSourceFile DTSourceFile
        {
            get
            {
                return (EnumDTSourceFile)DataSourceFileId;
            }
        }
    }
}