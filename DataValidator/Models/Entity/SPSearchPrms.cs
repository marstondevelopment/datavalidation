﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class SPSearchPrms<T>:SearchPrms<T>
    {
        public SPSearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults,int? dbConnectionId, int? spTypeId, List<SelectListItem> dbConnections):base( sortOrder,  searchString,  page,  pageSize,  defaultPageSize, allResults)
        {
            DBConnectionId = dbConnectionId;
            SPTypeId = spTypeId;
            DBConnections = dbConnections;
        }
        [Display(Name = "DB Connection")]
        public int? DBConnectionId { get; set; }
        [Display(Name = "SP Type")]
        public int? SPTypeId { get; set; }

        public List<SelectListItem> DBConnections;

    }
}