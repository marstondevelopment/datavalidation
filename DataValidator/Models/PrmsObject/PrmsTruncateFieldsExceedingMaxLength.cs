﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsTruncateFieldsExceedingMaxLength : PrmsBase
    {
        public PrmsTruncateFieldsExceedingMaxLength() { }
        public PrmsTruncateFieldsExceedingMaxLength(DTAction dt) : base(dt) { }

        [Display(Name = "Truncated Field Names")]
        public string TruncatedFieldNames { get; set; }


        [Display(Name = "Max Field Lengths")]
        public string MaxFieldLengths { get; set; }

        [Display(Name = "Field Separator")]
        public string FieldSeparator { get; set; }

        [Display(Name = "Notes Field Name")]
        public string NotesFieldName { get; set; }

        [Display(Name = "Overwrite Notes Field")]
        public bool OverwriteNotesField { get; set; }

        [Display(Name = "Include Field Name")]
        public bool IncludeFieldName { get; set; }

        [Display(Name = "Filters")]
        public string Filters { get; set; }

        public string DisplayFieldSeparator
        {
            get
            {
                return !string.IsNullOrEmpty(FieldSeparator) ? FieldSeparator : "N/A";
            }
        }       
    }
}