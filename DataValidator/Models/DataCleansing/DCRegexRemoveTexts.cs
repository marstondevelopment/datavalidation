﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public class DCRegexRemoveTexts : DCAction
    {
        public DCRegexRemoveTexts(DataFieldDataCleansing dc) : base(dc) { }

        public override void ExecuteAction()
        {
            RegexOptions options = RegexOptions.Singleline | RegexOptions.IgnoreCase;
            Regex regex = new Regex(this.DataFieldDataCleansing.RegularExpression.RegExText, options);

            if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValue))
            {
                DataFieldDataCleansing.DataField.OriginalValue = regex.Replace(DataFieldDataCleansing.DataField.OriginalValue, string.Empty).Trim(Utility.CharsForTrim.ToCharArray());
            }

            if (DataFieldDataCleansing.DataField.OriginalValues.Count > 0)
            {
                for (int i = 0; i < DataFieldDataCleansing.DataField.OriginalValues.Count; i++)
                {
                    if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValues[i]))
                    {
                        DataFieldDataCleansing.DataField.OriginalValues[i] = regex.Replace(DataFieldDataCleansing.DataField.OriginalValues[i],string.Empty).Trim(Utility.CharsForTrim.ToCharArray());
                    }
                }

            }

        }


    }
}