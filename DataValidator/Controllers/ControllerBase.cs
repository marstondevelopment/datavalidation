﻿using ICSharpCode.SharpZipLib.Zip;
using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    public class ControllerBase : Controller
    {
        protected readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected override void OnException(ExceptionContext filterContext)
        {            
            filterContext.ExceptionHandled = true;

            //Log the error!!
            logger.Error(filterContext.Exception);

            Session.Add("ErrMessage", filterContext.Exception);
            //Redirect or return a view, but not both.
            filterContext.Result = RedirectToAction("Index", "Error");
        }

        protected void WriteCSVFile(string message)
        {
            // Return the file content with response body. 
            Response.ContentType = "text/csv";
            Response.AddHeader("Content-Disposition", "attachment; filename=ValidationResult_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".csv");
            Response.Write(message);
            Response.End();
        }

        protected void ReturnZipFile(FileTemplate ft)
        {

            Response.ContentType = "application/zip";
            // If the browser is receiving a mangled zipfile, IIS Compression may cause this problem. Some members have found that
            //Response.ContentType = "application/octet-stream" has solved this. May be specific to Internet Explorer.

            Response.AppendHeader("content-disposition", "attachment; filename=\"" + ft.Name.Replace(" ", string.Empty) + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".zip\"");
            Response.CacheControl = "No-cache";
            //Response.Cache.SetExpires(DateTime.Now.AddMinutes(3)); // or put a timestamp in the filename in the content-disposition

            byte[] buffer = new byte[4096];

            ZipOutputStream zipOutputStream = new ZipOutputStream(Response.OutputStream);
            zipOutputStream.SetLevel(3); //0-9, 9 being the highest level of compression

            foreach (var ms in ft.FileWriter.OutputFiles)
            {

                ZipEntry entry = new ZipEntry(ms.FileName);
                var stream = ms.Generate();
                entry.Size = stream.Length;
                // Setting the Size provides WinXP built-in extractor compatibility,
                //  but if not available, you can set zipOutputStream.UseZip64 = UseZip64.Off instead.

                zipOutputStream.PutNextEntry(entry);
                stream.Position = 0;
                int count = stream.Read(buffer, 0, buffer.Length);
                while (count > 0)
                {
                    zipOutputStream.Write(buffer, 0, count);
                    count = stream.Read(buffer, 0, buffer.Length);
                    if (!Response.IsClientConnected)
                    {
                        break;
                    }
                    Response.Flush();
                }
                stream.Close();
            }

            zipOutputStream.Close();

            Response.Flush();
            Response.End();
        }
    }
}