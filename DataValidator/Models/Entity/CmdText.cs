﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mgl.DataValidator.Models.ValidationRules;
using Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class CmdText
    {
        private Dictionary<string, object> _paramCollection;
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Dictionary<string, object> ParamCollection
        {
            get
            {
                if (!string.IsNullOrEmpty(FieldNames))
                {
                    _paramCollection = new Dictionary<string, object>();
                    string[] cols = FieldNames.Split(",".ToCharArray()).Select(x => x.Trim()).ToArray();
                    foreach (var item in cols)
                    {
                        try
                        {
                            DataField dataField = FileTemplate.BodyCols.Where(x => x.Name.ToUpper() == item.ToUpper()).FirstOrDefault();
                            if (dataField == null)
                            {
                                throw new MissingFieldException("Data field not found for CmdText parameter - " + item);
                            }
                            _paramCollection.Add(item, dataField.TypedField());
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                            throw;
                        }

                    }
                }

                return _paramCollection;
            }

            set
            {
                _paramCollection = value;
            }
        }

        public VRSPValidation ParentVR { get; set; }


        public FileTemplate FileTemplate;

        public bool Validate()
        {
            return FileTemplate.DataProvider.ValidateDataCmdText(this);
        }

        public DataTable GetData()
        {
            return FileTemplate.DataProvider.GetDataCmdText(this);
        }
        public string DescriptionDisplay
        {
            get { return !string.IsNullOrEmpty(Description) ? Description : "N/A"; }
        }

        public class Metadata
        {
            //[Display(Name = "Command Text")]
            //[Required(ErrorMessage = "Stored Procedure name required.")]
            //public string SPName;
            [Display(Name = "Connection")]
            [Required(ErrorMessage = "Database connection name required.")]
            public int DBConnectionId;
            //[Display(Name = "SP Type")]
            //[Required(ErrorMessage = "Stored Procedure type required.")]
            //public int SPType;
            [Display(Name = "SQL Script")]
            public string SQLScript;
        }
    }
}