﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsGroupBy : PrmsBase
    {
        public PrmsGroupBy() { }
        public PrmsGroupBy(DTAction dt) : base(dt) { }

        [Display(Name = "Field Names")]
        public string FieldNames { get; set; }
    }
}