﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public abstract class DEOBase
    {
        public DEOBase() { }
        public DEOBase(FileTemplate ft)
        {
            fileTemplate = ft;
            FileTemplateId = ft.Id;
        }
        public DEOBase(DataEntry de)
        {
            fileTemplate = de.FileTemplate;
            FileTemplateId = de.FileTemplateId;
            parent = de;
        }
        [Required()]
        [Display(Name = "File Template")]
        public int FileTemplateId { get; set; }



        [JsonIgnore]
        public FileTemplate FileTemplate { get => fileTemplate; set => fileTemplate = value; }
        [JsonIgnore]
        public StringBuilder Errors { get => errors; set => errors = value; }
        [JsonIgnore]
        public DataEntry Parent { get => parent; set => parent = value; }

        private FileTemplate fileTemplate;
        private StringBuilder errors = new StringBuilder();
        private DataEntry parent;




        public abstract bool Validate(DataEntryRepository der);


    }
}