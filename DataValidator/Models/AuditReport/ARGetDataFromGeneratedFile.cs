﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetDataFromGeneratedFile : ARStep
    {
        private ARPrmsGetDataFromGeneratedFile prms;
        private List<string> tagNames;
        public ARGetDataFromGeneratedFile(FileTemplateARAction ftar) : base(ftar)
        {
            prms = ftar.ARAction.ARPrmsGetDataFromIgnoredFile;
            if (prms == null)
            {
                throw new MemberAccessException("ARGetDataFromGeneratedFile parameters are missing for " + ftar.ARAction.Name + ", please update the configuration settings and try again.");
            }

            tagNames = prms.Parent.TagName.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).ToList();
                
        }
        public override bool Execute(ref string arScript)
        {
            OutputFile ignoredFile = ParentFTAR.FileTemplate.FileWriter.ProcessingFile;
            string perRowSection = arScript.GetValueFromSectionTag(EnumARReservedTags.PERROW.ToString());
            StrBuilder.Clear();
            if (!string.IsNullOrEmpty(prms.FileNamePrefixs))
            {
                string[] fileNamePrefixes = prms.FileNamePrefixs.Split(Utility.DEFAULT_DELIMITER.ToCharArray());
                if (fileNamePrefixes.Where(x=> x.StartsWith(ignoredFile.FileNamePrefix)).Count() <= 0)
                {
                    //arScript = arScript.RenderSectionTag(EnumARReservedTags.PERROW.ToString(), StrBuilder.ToString() ?? string.Empty);
                    arScript = string.Empty;
                    return true;
                }

            }
            foreach (DataRow rw in ignoredFile.Data.Rows)
            {
                string renderedScript = perRowSection; 
                foreach (var tagName in tagNames)
                {
                    renderedScript = renderedScript.RenderTag(tagName, rw[tagName].ObjToString(ignoredFile.Data.Columns[tagName]));
                    if (!string.IsNullOrEmpty(prms.ExclusionReasonTagName))
                    {
                        renderedScript = renderedScript.RenderTag(prms.ExclusionReasonTagName, ignoredFile.DTActionMessage);
                    }
                }
                StrBuilder.AppendLine(renderedScript);
            }

            arScript = arScript.RenderSectionTag(EnumARReservedTags.PERROW.ToString(), StrBuilder.ToString() ?? string.Empty);
            return true;
        }


    }
}