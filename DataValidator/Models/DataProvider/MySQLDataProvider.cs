﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using System.IO;
using System.Text;

namespace Mgl.DataValidator.Models
{
    public class MySQLDataProvider : DataProvider
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MySQLDataProvider(FileTemplate fileTemplate) : base(fileTemplate)
        {
            FieldTerminator = "\t";
            LineTerminator = "\n";
        }

        public string FieldTerminator { get; set; }
        public string LineTerminator { get; set; }

        public override DataTable GetData(StoredProcedure sp)
        {
            try
            {
                using (var sqlConn = new MySqlConnection(sp.DbConnection.ConnectionString))
                {
                    sqlConn.Open();
                    MySqlCommand command = new MySqlCommand(sp.SPName, sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddRange(sp.ParamCollection.Select(x => new SqlParameter(x.Key, x.Value)).ToArray());
                    using (MySqlDataReader dr = command.ExecuteReader())
                    {
                        var tb = new DataTable();
                        tb.Load(dr);
                        return tb;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(sp.SPName, ex);
                throw;
            }
        }

        public override DataTable GetDataCmdText(CmdText ct)
        {
            try
            {
                using (var sqlConn = new MySqlConnection(ct.DbConnection.ConnectionString))
                {
                    sqlConn.Open();

                    string cmdText = ct.SQLScript;
                    foreach (var item in ct.ParamCollection)
                    {
                        cmdText = cmdText.RenderTag(item.Key, item.Value.ToString());
                    }

                    MySqlCommand command = new MySqlCommand(cmdText, sqlConn);
                    command.CommandType = CommandType.Text;
                    using (MySqlDataReader dr = command.ExecuteReader())
                    {
                        var tb = new DataTable();
                        tb.Load(dr);
                        return tb;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ct.Name, ex);
                throw;
            }
        }

        public override bool TransformData()
        {
            try
            {
                if (_ft.StoredProcedure.SPType != (int)EnumSPType.DTSP)
                {
                    throw new Exception("The SP is not set up for data transformation. Update the SP settings and try again.");
                }
                using (var sqlConn = new MySqlConnection(_ft.DbConnection.ConnectionString))
                {
                    sqlConn.Open();
                    using (var command = new MySqlCommand(_ft.StoredProcedure.SPName, sqlConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        var result = command.ExecuteScalar();
                        return result.ToString() == "1" ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(_ft.StoredProcedure.SPName, ex);
                return false;
            }
        }

        public override bool UploadData()
        {
            string fileName = Path.GetTempFileName();

            try
            {
                byte[] fieldTerm = Encoding.UTF8.GetBytes(FieldTerminator);

                byte[] lineTerm = Encoding.UTF8.GetBytes(LineTerminator);

                PrepareFile(fileName, fieldTerm, lineTerm);

                LoadData(fileName);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }
            finally
            {
                File.Delete(fileName);
            }

            return true;
        }
        
        public override bool ValidateData(StoredProcedure sp)
        {
            try
            {
                using (var sqlConn = new MySqlConnection(sp.DbConnection.ConnectionString))
                {
                    sqlConn.Open();

                    using (var command = new MySqlCommand(sp.SPName, sqlConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(sp.ParamCollection.Select(x => new SqlParameter(x.Key, x.Value)).ToArray());
                        var result = command.ExecuteScalar();
                        if (result == null)
                            return false;

                        return result.ToString() == "1" ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(sp.SPName, ex);
                return false;
            }
        }

        public override bool ValidateDataCmdText(CmdText ct)
        {
            try
            {
                //sp.SetPrmsCollection(sp.FileTemplate.BodyCols);

                using (var sqlConn = new MySqlConnection(ct.DbConnection.ConnectionString))
                {
                    sqlConn.Open();

                        string cmdText = ct.SQLScript;
                        foreach (var item in ct.ParamCollection)
                        {
                            cmdText = cmdText.RenderTag(item.Key, item.Value.ToString());
                        }

                        using (var command = new MySqlCommand(cmdText, sqlConn))
                        {
                            command.CommandType = CommandType.Text;
                            var result = command.ExecuteScalar();
                        // The select statement returns the count of rows that matches the criterea. 0 means not found, and return true, otherwise some occurrence found in db and return false.
                            if (result == null)
                                return false;
                            return result.ToString() == "0" ? true : false;
                        }


                }
            }
            catch (Exception ex)
            {
                logger.Error(ct.Name, ex);
                return false;
            }
        }


        private void LoadData(string fileName)
        {
            using (MySqlConnection Conn = new MySqlConnection(_ft.DbConnection.ConnectionString))
            {
                Conn.Open();
                MySqlBulkLoader bl = new MySqlBulkLoader(Conn)
                {

                    FieldTerminator = FieldTerminator,
                    LineTerminator = LineTerminator,
                    TableName = _ft.BUStagingTable,
                    FileName = fileName
                };
                bl.Load();
            }
        }

        private void PrepareFile(string fileName, byte[] fieldTerm, byte[] lineTerm)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Append))
            {
                foreach (DataRow row in GetBUSourceTbl().Rows)
                {
                    int i = 0;

                    foreach (object val in row.ItemArray)
                    {
                        byte[] bytes;

                        if (val is DateTime)
                        {
                            DateTime theDate = (DateTime)val;
                            string dateStr = theDate.ToString("yyyy-MM-dd HH:mm:ss");
                            bytes = Encoding.UTF8.GetBytes(dateStr);
                        }
                        else
                            bytes = Encoding.UTF8.GetBytes(val.ToString());

                        fs.Write(bytes, 0, bytes.Length);

                        i++;

                        if (i < row.ItemArray.Length)
                            fs.Write(fieldTerm, 0, fieldTerm.Length);
                    }

                    fs.Write(lineTerm, 0, lineTerm.Length);
                }
            }
        }
    }
}