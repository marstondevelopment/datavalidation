﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  Mgl.DataValidator.Models.ValidationRules;
using  Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    //[Serializable]
    [MetadataType(typeof(Metadata))]
    public partial class User
    {
        public bool IsAdmin
        {
            get
            {
                return RoleId == (int)EnumRole.Admin;
            }
        }
        public string DisplayAdminStatus
        {
            get
            {
                return IsAdmin ? "Yes" : "No";
            }
        }
        public string DisplayDisabledStatus
        {
            get
            {
                return !Disabled ? "Yes" : "No";
            }
        }

        public Client DefaultClient
        {
            get
            {
                Client c = null;
                UserClient  uc = UserClients.Where(x => x.IsDefault == true).FirstOrDefault();
                if (uc != null)
                    c = uc.Client;
                return c;
            }
        }

        public List<Client>AssociatedClients
        {
            get
            {
                return UserClients.Select(x => x.Client).ToList();
            }
        }

        public string ClientNames
        {
            get
            {
                string names = string.Empty;

                if (AssociatedClients.Count>0)
                    names = string.Join(", ", AssociatedClients.Select(x => x.Name).ToArray());
                return names;

            }

        }
       
        private List<FileTemplate> associactedFileTempaltes;
        public List<FileTemplate> AssociatedFileTemplates
        {
            get
            {
                if (associactedFileTempaltes == null)
                {
                    associactedFileTempaltes = UserClients.SelectMany(x => x.Client.FileTemplates).Where(y=> y.Disabled == false).ToList();
                }

                return associactedFileTempaltes;
            }
        }

        public class Metadata
        {
            [Required(ErrorMessage = "User id is required.")]
            [Display(Name = "User Id")]
            public string UserId;
            [Display(Name = "User Name")]
            public string UserName;
            [Display(Name = "Created On")]
            public DateTime CreatedDate;
            [Display(Name = "Last Logged On")]
            public DateTime LastLoggedOnDate;
            [Display(Name = "No. of Visits")]
            public int VisitsCount;
            [Display(Name = "Domain Name")]
            [Required(ErrorMessage = "Domain is required.")]
            public string DomainName;
            [Display(Name = "Admin")]
            public bool IsAdmin;
            [Display(Name = "Enabled")]
            public bool Disabled;
            [Required(ErrorMessage = "User Role is required.")]
            [Display(Name = "User Role")]
            public int RoleId;

        }
    }
}