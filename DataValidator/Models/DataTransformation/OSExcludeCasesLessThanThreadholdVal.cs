﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSExcludeCasesLessThanThreadholdVal : DTStep
    {
        private PrmsExcludeCasesLLTV prms;
        public OSExcludeCasesLessThanThreadholdVal(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsExcludeCasesLLTV;
            if (prms == null)
            {
                throw new MemberAccessException("Threadhold parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            if ((!string.IsNullOrEmpty(prms.EFPrefix)) && (string.IsNullOrEmpty(prms.CaseRefColName) || string.IsNullOrEmpty(prms.ExMessage)))
            {
                throw new MemberAccessException("PrmsExcludeCasesLLTV parameters are missing/configured incorrectly for " + ftdt.DTAction.ActionName + ", please update the configuration for exception file and try again.");
            }

        }
        public override bool Execute()
        {
            string filter = "[" + prms.FieldName + "] < " + prms.MinThreadhold.ToString();
            var rows = ParentFTDT.FileTemplate.ProcessedDataTable.Select(filter);

            if (rows != null && rows.Length > 0)
            {
                if (!string.IsNullOrEmpty(prms.EFPrefix))
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var rw in rows)
                    {
                        if (!sb.ToString().Contains(rw[prms.CaseRefColName].ToString())) // This is to prevent repeating the logging for cases that has same case ref.
                        {

                            string ex = prms.ExMessage;
                            ex = ex.RenderTag(prms.CaseRefColName, rw[prms.CaseRefColName].ToString());
                            string selectFilter = "[" + prms.CaseRefColName + "] = '" + rw[prms.CaseRefColName].ToString() + "'";
                            DataRow[] duplicatedCases = ParentFTDT.FileTemplate.BUDataTable.Select(selectFilter);
                            foreach (var item in duplicatedCases)
                            {
                                sb.AppendLine(ex.RenderTag("LineNumber", (ParentFTDT.FileTemplate.BUDataTable.Rows.IndexOf(item) + 1).ToString()));
                            }
                        }
                    }
                    if (sb.Length > 0)
                    {
                        var ef = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(prms.EFPrefix, EnumOutputFileCategory.Exception);
                        ef.Context = ef.Context + sb.ToString();
                    }
                }

                DataTable dt = string.IsNullOrEmpty(prms.OFPrefix) ? 
                    ParentFTDT.FileTemplate.ExcludedDataTable : 
                    ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(prms.OFPrefix, EnumOutputFileCategory.Ignored).Data;

                for (int i = 0; i < rows.Length; i++)
                {
                    AppendExcludedRows(rows[i], dt);
                    ParentFTDT.FileTemplate.ProcessedDataTable.Rows.Remove(rows[i]);
                }

                ParentFTDT.FileTemplate.ProcessedDataTable = ParentFTDT.FileTemplate.ProcessedDataTable.Copy();
            }


            return true;

        }


    }
}