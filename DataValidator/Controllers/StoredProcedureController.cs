﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System.Linq;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class StoredProcedureController : ControllerBase
    {
        private StoredProcedureRepository _spr;
        private const string SEARCH_SP_PRMS = "SPSearchPrms";

        public StoredProcedureController(StoredProcedureRepository spr)
        {
            _spr = spr;
        }

        protected SPSearchPrms<StoredProcedure> CurrentSPSearchPrms
        {
            get
            {
                return Session[SEARCH_SP_PRMS] == null ? null : (SPSearchPrms<StoredProcedure>)this.Session[SEARCH_SP_PRMS];
            }
            set
            {
                Session.Add(SEARCH_SP_PRMS, value);
            }
        }

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, int? dbConnectionId, int? spTypeId)
        {
            SPSearchPrms<StoredProcedure> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentSPSearchPrms != null)
            {
                searchPrms = CurrentSPSearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                searchPrms = new SPSearchPrms<StoredProcedure>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, dbConnectionId,spTypeId,
                     _spr.GetConnections().Select(x => new SelectListItem() { Text = x.Name, Value = x.Id.ToString() }).OrderBy(y => y.Text).ToList()
                    );
            }

            var sps = _spr.GetAll(x => (string.IsNullOrEmpty(searchPrms.SearchString) || (x.SPName.Contains(searchPrms.SearchString) || x.Description.Contains(searchPrms.SearchString))) && (!searchPrms.DBConnectionId.HasValue || x.DBConnectionId == searchPrms.DBConnectionId.Value) && (!searchPrms.SPTypeId.HasValue || x.SPType == searchPrms.SPTypeId.Value)).OrderByDescending(x => x.SPName).Take(Utility.MaxListItems);
            _spr.GetAssociatedDTACtions(sps);
            CurrentSPSearchPrms = new SPSearchPrms<StoredProcedure>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, sps.ToList(), searchPrms.DBConnectionId, searchPrms.SPTypeId, searchPrms.DBConnections);

            return View(CurrentSPSearchPrms).Information("Search results found: " + CurrentSPSearchPrms.RowsCount.ToString());

        }

        public ActionResult Create()
        {
            var vm = new StoredProcedureViewModel(_spr, new StoredProcedure());            
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(StoredProcedure storedProcedure)
        {
            var vm = new StoredProcedureViewModel(_spr, storedProcedure);            

            if (ModelState.IsValid)
            {
                _spr.Create(vm.StoredProcedure);
                return RedirectToAction("Index").Success(vm.StoredProcedure.SPName + " has been created successfully.");
            }
            return RedirectToAction("Create").Danger("Failed to create the Store Procedure");
        }

        public ActionResult Details(int id)
        {
            var vm = new StoredProcedureViewModel(_spr, _spr.GetById(id));
            return View(vm);
        }

        public ActionResult Edit(int id)
        {
            var vm = new StoredProcedureViewModel(_spr, _spr.GetById(id));

            if (!string.IsNullOrEmpty(vm.StoredProcedure.AssociatedFileTemplates) && vm.StoredProcedure.AssociatedFileTemplates.IndexOf("|") > 0)
                return View(vm).Warning("Warning: This stored procedure has been referenced by the following FTs/DTActions. You are advised to implement system risk assessment before making any changes.<br/>" + vm.StoredProcedure.AssociatedFileTemplatesForCookie);

            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var vm = new StoredProcedureViewModel(_spr, _spr.GetById(id));            

            TryUpdateModel(vm);

            if (ModelState.IsValid)
            {
                _spr.Update(vm.StoredProcedure);
                return RedirectToAction("Index").Success(vm.StoredProcedure.SPName + " has been updated successfully."); ; 
            }
            return RedirectToAction("Edit").Danger("Failed to update the Store Procedure"); ;
        }       
    }
}
