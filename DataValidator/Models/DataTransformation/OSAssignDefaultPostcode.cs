﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSAssignDefaultPostcode : DTStep
    {

        private const string OTHER_DEFAULTER_ADDRESS_PATTERN = "Add{ADDLINENO}Liable{LIABLENO}";
        private const string ADD_LINE_NO_TAG = "{ADDLINENO}";
        private const string LIABLE_NO_TAG = "{LIABLENO}";
        private const string SECONDARY_DEFAULTER_POSTCODE_PREFIX = "AddPostCodeLiable";
        private const string WARRANT_ADDRESS_PREFIX = "DebtAddress";
        private const string WARRANT_ADDRESS_Postcode = "DebtAddressPostcode";

        public OSAssignDefaultPostcode(FileTemplateDTAction ftdt) : base(ftdt)
        {

        }
        public override bool Execute()
        {
            var rows = ParentFTDT.FileTemplate.ProcessedDataTable.Select();

            foreach (DataRow row in rows)
            {
                string warrantAddressLine1 = GetText(row, WARRANT_ADDRESS_PREFIX + 1);
                string warrantAddressPostcode = GetText(row, WARRANT_ADDRESS_Postcode);

                if (!string.IsNullOrEmpty(warrantAddressLine1) && string.IsNullOrEmpty(warrantAddressPostcode))
                {
                    var colName = ParentFTDT.FileTemplate.ProcessedDataTable.Columns[ParentFTDT.FileTemplate.Columns.Where(x => x.Name == WARRANT_ADDRESS_Postcode || x.OutputColumnName == WARRANT_ADDRESS_Postcode).FirstOrDefault().Name].ToString();

                    row[colName] = Utility.DefaultPostcodeOS;
                }

                for (int liableNo = 1; liableNo <= 5; liableNo++)
                {
                    var i = 1;
                    string defAddressLine1ColName = OTHER_DEFAULTER_ADDRESS_PATTERN.Replace(ADD_LINE_NO_TAG, i.ToString()).Replace(LIABLE_NO_TAG, liableNo.ToString());

                    string defaulterAddressLine1 = GetText(row, defAddressLine1ColName);
                    string defaulterPostcode = GetText(row, SECONDARY_DEFAULTER_POSTCODE_PREFIX + liableNo.ToString());

                    //addressLine1 is avialble but postcode, assign with default postcode
                    if (!string.IsNullOrEmpty(defaulterAddressLine1) && string.IsNullOrEmpty(defaulterPostcode))
                    {
                        var colName = ParentFTDT.FileTemplate.ProcessedDataTable.Columns[ParentFTDT.FileTemplate.Columns.Where(x => x.Name == SECONDARY_DEFAULTER_POSTCODE_PREFIX + liableNo.ToString() || x.OutputColumnName == SECONDARY_DEFAULTER_POSTCODE_PREFIX + liableNo.ToString()).FirstOrDefault().Name].ToString();
                        row[colName] = Utility.DefaultPostcodeOS;
                    }

                }
            }

            return true;
        }

        private string GetText(DataRow row, string fieldName)
        {
            string conStr = string.Empty;

            var outputColumnDf = ParentFTDT.FileTemplate.Columns.Where(x => x.Name == fieldName || x.OutputColumnName == fieldName).FirstOrDefault();
            if (outputColumnDf == null)
                throw new Exception(this.ParentFTDT.DTAction.ActionName + " failed to find column, " + fieldName + " from staging table, please update the configuration and try again.");
            fieldName = outputColumnDf.Name;

            conStr = row[fieldName].ToString();

            return conStr;
        }
    }
}