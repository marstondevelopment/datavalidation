﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsGetSumOfColumns : PrmsBase
    {
        public PrmsGetSumOfColumns() { }
        public PrmsGetSumOfColumns(DTAction dt) : base(dt) { }

        [Display(Name = "Column Names")]
        public string ColumnNames { get; set; }
        [Display(Name = "Total Field Name")]
        public string TotalFieldName { get; set; }
    }
}