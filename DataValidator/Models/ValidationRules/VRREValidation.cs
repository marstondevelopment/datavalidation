﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public class VRREValidation : VRBase
    {
        public VRREValidation(DataFieldRule df) : base(df)
        {
        }

        public override bool ValidateField()
        {

            if (!string.IsNullOrEmpty(ParentFieldRule.DataField.OriginalValue))
            {
                if (this.ParentFieldRule.RegularExpression == null)
                {
                    throw new MemberAccessException("Regular Expression is not set for validation rule - " + this.ParentFieldRule.DataField.Name);
                }
                ParentFieldRule.DataField.OriginalValue = ParentFieldRule.DataField.OriginalValue.Trim();
                Regex expression = new Regex(ParentFieldRule.RegularExpression.RegExText);
                if (!expression.IsMatch(ParentFieldRule.DataField.OriginalValue))
                {
                    AppendError();
                    return false;
                }
            }
            return true;
        }
    }
}