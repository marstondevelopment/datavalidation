﻿CREATE TABLE [dbo].[User] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [UserId]           NVARCHAR (100) NULL,
    [UserName]         NVARCHAR (100) NULL,
    [CreatedDate]      DATETIME       NOT NULL,
    [LastLoggedOnDate] DATETIME       NOT NULL,
    [VisitsCount]      INT            CONSTRAINT [DF_User_VisitsCount] DEFAULT ((0)) NOT NULL,
    [DomainName]       VARCHAR (50)   NULL,
    [IsAdmin]          BIT            CONSTRAINT [DF_User_IsAdmin] DEFAULT ((0)) NOT NULL,
    [Disabled]         BIT            CONSTRAINT [DF_User_Disabled] DEFAULT ((0)) NOT NULL,
    [ClientId]         INT            NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_User_Client] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Client] ([Id])
);

