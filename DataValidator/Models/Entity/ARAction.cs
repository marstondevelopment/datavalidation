﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mgl.DataValidator.Models.ValidationRules;
using Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;
using Mgl.DataValidator.Models.DataTransformation;
using Newtonsoft.Json;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class ARAction
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int COOKIE_MAX_LENGTH = 1600;

        public EnumARActionType ARActionType
        {
            get
            {
                return (EnumARActionType)ARActionTypeId;
            }
        }

        public EnumARSourceFile DataSource => (EnumARSourceFile)DataSourceId;

        public string SerializePrmsObject()
        {
            string prms = string.Empty;
            switch (ARActionType)
            {
                case EnumARActionType.DisplayMnCrBalance:
                    if (ARPrmsDisplayMnCrBalance != null)
                    {
                        prms = JsonConvert.SerializeObject(ARPrmsDisplayMnCrBalance);
                    }
                    break;
                case EnumARActionType.GetDateTime:
                    if (ARPrmsGetDateTime != null)
                    {
                        prms = JsonConvert.SerializeObject(ARPrmsGetDateTime);
                    }
                    break;
                case EnumARActionType.GetOutputFileTotalBalance:
                    if (ARPrmsGetOutputFileTotalBalance != null)
                    {
                        prms = JsonConvert.SerializeObject(ARPrmsGetOutputFileTotalBalance);
                    }
                    break;
                case EnumARActionType.GetTotalBalanceOfNewCases:
                    if (ARPrmsGetTotalBalanceOfNewCases != null)
                    {
                        prms = JsonConvert.SerializeObject(ARPrmsGetTotalBalanceOfNewCases);
                    }
                    break;
                case EnumARActionType.GetDataFromGeneratedFile:
                    if (ARPrmsGetDataFromIgnoredFile != null)
                    {
                        prms = JsonConvert.SerializeObject(ARPrmsGetDataFromIgnoredFile);
                    }
                    break;
                case EnumARActionType.GetClientScheme:
                    if (ARPrmsGetClientScheme != null)
                    {
                        prms = JsonConvert.SerializeObject(ARPrmsGetClientScheme);
                    }
                    break;
                case EnumARActionType.GetOutputFileRowCount:
                    if (ARPrmsGetOutputFileRowCount != null)
                    {
                        prms = JsonConvert.SerializeObject(ARPrmsGetOutputFileRowCount);
                    }
                    break;
                default:
                    break;
            }
            return prms;
        }

        private T GetJSONObject<T>() where T : ARPrmsBase
        {
            if (string.IsNullOrEmpty(PrmsObject))
                return null;
            T obj = null;
            try
            {
                obj = (T)JsonConvert.DeserializeObject<T>(PrmsObject);
                obj.Parent = this;
                return obj;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ARAction Clone
        {
            get
            {
                User user = (User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                ARAction ara = new ARAction()
                {
                    Name = Name,
                    Description = Description,
                    ARActionTypeId = ARActionTypeId,
                    PrmsObject = PrmsObject,
                    TagName = TagName,
                    DataSourceId = DataSourceId,
                    CreatedBy = user.Id,
                    DateCreated = DateTime.Now,
                    UpdatedBy = user.Id,
                    DateUpdated = DateTime.Now
                };
                return ara;
            }
        }

        private ARPrmsDisplayMnCrBalance arPrmsDisplayMnCrBalance;
        public ARPrmsDisplayMnCrBalance ARPrmsDisplayMnCrBalance
        {
            get
            {
                if (arPrmsDisplayMnCrBalance == null && ARActionType == EnumARActionType.DisplayMnCrBalance)
                {
                    arPrmsDisplayMnCrBalance = GetJSONObject<ARPrmsDisplayMnCrBalance>();
                }
                return arPrmsDisplayMnCrBalance;
            }
            set
            {
                arPrmsDisplayMnCrBalance = value;
            }
        }

        private ARPrmsGetDataFromGeneratedFile arPrmsGetDataFromIgnoredFile;
        public ARPrmsGetDataFromGeneratedFile ARPrmsGetDataFromIgnoredFile
        {
            get
            {
                if (arPrmsGetDataFromIgnoredFile == null && ARActionType == EnumARActionType.GetDataFromGeneratedFile)
                {
                    arPrmsGetDataFromIgnoredFile = GetJSONObject<ARPrmsGetDataFromGeneratedFile>();
                }
                return arPrmsGetDataFromIgnoredFile;
            }
            set
            {
                arPrmsGetDataFromIgnoredFile = value;
            }
        }

        private ARPrmsGetDateTime aRPrmsGetDateTime;
        public ARPrmsGetDateTime ARPrmsGetDateTime
        {
            get
            {
                if (aRPrmsGetDateTime == null && ARActionType == EnumARActionType.GetDateTime)
                {
                    aRPrmsGetDateTime = GetJSONObject<ARPrmsGetDateTime>();
                }
                return aRPrmsGetDateTime;
            }
            set
            {
                aRPrmsGetDateTime = value;
            }
        }

        private ARPrmsGetOutputFileTotalBalance aRPrmsGetOutputFileTotalBalance;
        public ARPrmsGetOutputFileTotalBalance ARPrmsGetOutputFileTotalBalance
        {
            get
            {
                if (aRPrmsGetOutputFileTotalBalance == null && ARActionType == EnumARActionType.GetOutputFileTotalBalance)
                {
                    aRPrmsGetOutputFileTotalBalance = GetJSONObject<ARPrmsGetOutputFileTotalBalance>();
                }
                return aRPrmsGetOutputFileTotalBalance;
            }
            set
            {
                aRPrmsGetOutputFileTotalBalance = value;
            }
        }

        private ARPrmsGetClientScheme aRPrmsGetClientScheme;
        public ARPrmsGetClientScheme ARPrmsGetClientScheme
        {
            get
            {
                if (aRPrmsGetClientScheme == null && ARActionType == EnumARActionType.GetClientScheme)
                {
                    aRPrmsGetClientScheme = GetJSONObject<ARPrmsGetClientScheme>();
                }
                return aRPrmsGetClientScheme;
            }
            set
            {
                aRPrmsGetClientScheme = value;
            }
        }

        private ARPrmsGetTotalBalanceOfNewCases aRPrmsGetTotalBalanceOfNewCases;
        public ARPrmsGetTotalBalanceOfNewCases ARPrmsGetTotalBalanceOfNewCases
        {
            get
            {
                if (aRPrmsGetTotalBalanceOfNewCases == null && ARActionType == EnumARActionType.GetTotalBalanceOfNewCases)
                {
                    aRPrmsGetTotalBalanceOfNewCases = GetJSONObject<ARPrmsGetTotalBalanceOfNewCases>();
                }
                return aRPrmsGetTotalBalanceOfNewCases;
            }
            set
            {
                aRPrmsGetTotalBalanceOfNewCases = value;
            }
        }

        private ARPrmsGetOutputFileRowCount aRPrmsGetOutputFileRowCount;
        public ARPrmsGetOutputFileRowCount ARPrmsGetOutputFileRowCount
        {
            get
            {
                if (aRPrmsGetOutputFileRowCount == null && ARActionType == EnumARActionType.GetOutputFileRowCount)
                {
                    aRPrmsGetOutputFileRowCount = GetJSONObject<ARPrmsGetOutputFileRowCount>();
                }
                return aRPrmsGetOutputFileRowCount;
            }
            set
            {
                aRPrmsGetOutputFileRowCount = value;
            }
        }
        [Display(Name = "Referenced By")]
        public string AssociatedFileTemplates
        {
            get
            {
                string FTLink = string.Empty;
                string appPath = System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/FileTemplate/Details/";
                List<FileTemplateARAction> ftaraList = FileTemplateARActions.Where(x => !x.FileTemplate.Disabled).ToList();

                if (ftaraList.Count > 0)
                {
                    foreach (var ftara in ftaraList)
                    {
                        FTLink = FTLink + " | <a href='" + appPath + ftara.FileTemplateId.ToString() + "'>" + ftara.FileTemplate.Name + "</a>";
                    }
                    FTLink = FTLink.Trim().Trim("|".ToCharArray());
                }

                return FTLink;
            }
        }

        public string AssociatedFileTemplatesForCookie
        {
            get
            {
                string assocItemNames = AssociatedFileTemplates;
                if (!string.IsNullOrEmpty(assocItemNames) && assocItemNames.Length > COOKIE_MAX_LENGTH)
                {
                    assocItemNames = assocItemNames.Substring(0, COOKIE_MAX_LENGTH);
                    assocItemNames = assocItemNames.Substring(0, assocItemNames.LastIndexOf("|")) + "...";
                }

                return assocItemNames;
            }
        }

        public class Metadata
        {
            [Required(ErrorMessage = "Action Name required.")]
            [Display(Name = "AR Action Name")]
            public string Name;
            [Display(Name = "AR Action Types")]
            public int ARActionTypeId;
            [Required(ErrorMessage = "Tag Name required.")]
            [Display(Name = "Tag Name/s")]
            public string TagName;
            [Required(ErrorMessage = "Data Source required.")]
            [Display(Name = "Data Source")]
            public int DataSourceId;
        }

    }

}