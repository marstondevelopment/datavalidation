﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class ARActionRepository : RepositoryBase<DataIntegrationEntities, ARAction>
    {
        private UserRepository _ur;

        public ARActionRepository(DataIntegrationEntities entities, UserRepository ur) : base(entities)
        {
            _ur = ur;
        }

        public override IQueryable<ARAction> GetAll()
        {
            return base.GetAll();
        }

        public ARAction GetARActionById(int id)
        {
            return GetAll().Where(x => x.Id == id).FirstOrDefault();
        }

        public void Create(ARAction arAction)
        {
            arAction.PrmsObject = arAction.SerializePrmsObject();
            arAction.CreatedBy = _ur.CurrentUser.Id;
            arAction.UpdatedBy = _ur.CurrentUser.Id;
            arAction.DateCreated = DateTime.Now;
            arAction.DateUpdated = DateTime.Now;
            Add(arAction);
            SubmitChanges();
        }

        public void Update(ARAction arAction)
        {
            arAction.PrmsObject = arAction.SerializePrmsObject();
            arAction.UpdatedBy = _ur.CurrentUser.Id;
            arAction.DateUpdated = DateTime.Now;
            SubmitChanges();
        }

        public void Delete(ARAction ARAction)
        {
            Remove(ARAction);
            SubmitChanges();
        }
    }
}