﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSExtractLinkedCaseAfterGroupBy : DTStep
    {
        private PrmsExtractLinkedCasesAfterGroupBy prms;
        private List<string> gbFieldNames;
        public OSExtractLinkedCaseAfterGroupBy(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsExtractLinkedCasesAfterGroupBy;
            if (ftdt.DTAction.PrmsExtractLinkedCasesAfterGroupBy == null)
            {
                throw new MemberAccessException("PrmsExtractLinkedCasesAfterGroupBy  has not been set for DTAction - " + ParentFTDT.DTAction.ActionName);
            }
            gbFieldNames = prms.GBFieldNames.Split(",".ToCharArray()).ToList();
            if (gbFieldNames.Count <=0 || string.IsNullOrEmpty(prms.FilterFieldName) || string.IsNullOrEmpty(prms.SchemeName) || prms.ThresholdValue <=0)
            {
                throw new Exception("The parameters not set up correctly  for DTAction - " + ParentFTDT.DTAction.ActionName);
            }
            gbFieldNames.ForEach(x => x = x.Trim());
            prms.SchemeName = prms.SchemeName.Trim();
            prms.FilterFieldName = prms.FilterFieldName.Trim();
        }


        public override bool Execute()
        {
            var stagingDT = ParentFTDT.FileTemplate.ProcessedDataTable.Copy();
            var columnsToGroupBy = stagingDT.Columns.Cast<DataColumn>()
                .Where(c => gbFieldNames.Contains(c.ColumnName, StringComparer.InvariantCultureIgnoreCase))
                .ToArray();
            var comparer = new MultiFieldComparer();
            var groupedData = stagingDT.AsEnumerable()
                .GroupBy(row => columnsToGroupBy.Select(c => row[c]), comparer);

            IEnumerable<IGrouping<Object, DataRow>> linkedCasesGroups = null;

            int includeSingleCases = prms.IncludeSingleCases ? 0 : 1;
            if (string.IsNullOrEmpty(prms.DBValueColumnName))
            {
                switch (prms.ComparisonOperator)
                {
                    case EnumComparisonOperator.Greater:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) > prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.GreaterOrEqual:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) >= prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.Equal:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) == prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.LessThan:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) < prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.LessThanOrEqual:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) <= prms.ThresholdValue).Select(z => z);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (prms.ComparisonOperator)
                {
                    case EnumComparisonOperator.Greater:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && (x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) + x.Max(y => y.Field<decimal>(prms.DBValueColumnName))) > prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.GreaterOrEqual:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && (x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) + x.Max(y => y.Field<decimal>(prms.DBValueColumnName))) >= prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.Equal:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && (x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) + x.Max(y => y.Field<decimal>(prms.DBValueColumnName))) == prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.LessThan:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && (x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) + x.Max(y => y.Field<decimal>(prms.DBValueColumnName))) < prms.ThresholdValue).Select(z => z);
                        break;
                    case EnumComparisonOperator.LessThanOrEqual:
                        linkedCasesGroups = groupedData.Where(x => x.Count() > includeSingleCases && (x.Sum(y => y.Field<decimal>(prms.FilterFieldName)) + x.Max(y => y.Field<decimal>(prms.DBValueColumnName))) <= prms.ThresholdValue).Select(z => z);
                        break;
                    default:
                        break;
                }
            }


            if (linkedCasesGroups!= null && linkedCasesGroups.Count() > 0)
            {
                OutputFile outputFile = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(prms.SchemeName,EnumOutputFileCategory.Output);
                foreach (var group in linkedCasesGroups)
                {
                    foreach (var rw in group)
                    {
                        outputFile.Data.ImportRow(rw);
                        stagingDT.Rows.Remove(rw);
                    }
                }

                ParentFTDT.FileTemplate.ProcessedDataTable = stagingDT;
            }

            return true;
        }
    }
}