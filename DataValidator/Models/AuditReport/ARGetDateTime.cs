﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetDateTime : ARStep
    {
        private ARPrmsGetDateTime prms;
        public ARGetDateTime(FileTemplateARAction ftar) : base(ftar)
        {
            prms = ftar.ARAction.ARPrmsGetDateTime;
            if (prms == null)
            {
                throw new MemberAccessException("ARGetDateTime parameters are missing for " + ftar.ARAction.Name + ", please update the configuration settings and try again.");
            }
        }
        public override bool Execute(ref string arScript)
        {
            string dateTime = string.IsNullOrEmpty(prms.DateFormat) ? DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") : DateTime.Now.ToString(prms.DateFormat);
            arScript = arScript.RenderTag(prms.Parent.TagName, dateTime);

            return true;
        }


    }
}