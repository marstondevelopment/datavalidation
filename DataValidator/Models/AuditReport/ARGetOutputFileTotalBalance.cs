﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetOutputFileTotalBalance : ARStep
    {

        private ARPrmsGetOutputFileTotalBalance prms;
        public ARGetOutputFileTotalBalance(FileTemplateARAction ftar) : base(ftar)
        {
            prms = ftar.ARAction.ARPrmsGetOutputFileTotalBalance;
            if (prms == null)
            {
                throw new MemberAccessException("ARPrmsGetOutputFileTotalBalance parameters are missing for " + ftar.ARAction.Name + ", please update the configuration settings and try again.");
            }
            prms.InputColumnName = prms.InputColumnName.Trim();
        }

        private List<string> GetColumns(string wildcard, DataTable dt)
        {
            List<string> wildCardNames = new List<string>();

            string[] names = dt.Columns.Cast<DataColumn>()
            .Where(y => y.ColumnName.StartsWith(prms.InputColumnName.Substring(0, prms.InputColumnName.Length - 1)))
             .Select(x => x.ColumnName)
             .ToArray();
            if (names != null && names.Length > 0)
            {
                wildCardNames.AddRange(names);
            }

            return wildCardNames;
        }

        private decimal GetTotal(List<string> columns, DataTable table)
        {
            decimal total = 0;
            foreach (string colName in columns)
            {
                var val = table.Compute("Sum([" + colName + "])", "");
                if (val != DBNull.Value)
                    total = total + (decimal)val;
            }
            return total;
        }

        public override bool Execute(ref string arScript)
        {
            decimal total = 0;
            List<string> wildCardNames = new List<string>();

            switch (prms.Parent.DataSource)
            {
                case EnumARSourceFile.InputDataFile:
                    List<string> columns = prms.InputColumnName.EndsWith("*") ? GetColumns(prms.InputColumnName, this.ParentFTAR.FileTemplate.BUDataTable) : new List<string> { prms.InputColumnName };
                    total = GetTotal(columns, this.ParentFTAR.FileTemplate.BUDataTable);
                    break;
                case EnumARSourceFile.AllOutputDataFiles:
                    foreach (var outputFile in this.ParentFTAR.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output))
                    {
                        columns = prms.InputColumnName.EndsWith("*") ? GetColumns(prms.InputColumnName, outputFile.Data) : new List<string> { prms.InputColumnName };
                        total = total + GetTotal(columns, outputFile.Data);
                    }
                    break;
                case EnumARSourceFile.PerOutputDataFile:
                default:
                    throw new NotImplementedException();
            }

            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, total.ObjToString(this.ParentFTAR.FileTemplate.BUDataTable.Columns[prms.InputColumnName.Trim("*".ToCharArray())]));
            return true;
        }
    }
}