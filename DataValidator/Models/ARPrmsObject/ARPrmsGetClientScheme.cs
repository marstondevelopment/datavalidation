﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class ARPrmsGetClientScheme : ARPrmsBase
    {
        public ARPrmsGetClientScheme() { }
        public ARPrmsGetClientScheme(ARAction ar) : base(ar) { }

        [Display(Name = "Full File Name")]
        public bool FullFileName { get; set; }
    }
}