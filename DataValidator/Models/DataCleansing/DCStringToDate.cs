﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public class DCStringToDate : DCAction
    {

        public DCStringToDate(DataFieldDataCleansing dc) : base(dc) { }
        public override void ExecuteAction()
        {
            if (string.IsNullOrEmpty(DataFieldDataCleansing.InputDateFormat))
            {
                throw new MissingFieldException("InputDateFormat is missing for the operation. Check the settings for data cleansing action.");
            }

            if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValue))
            {
                DataFieldDataCleansing.DataField.OriginalValue = DataFieldDataCleansing.DataField.OriginalValue.ToDate(DataFieldDataCleansing.InputDateFormat, DataFieldDataCleansing.DataField.OutputFormat);
            }

            if (DataFieldDataCleansing.DataField.OriginalValues.Count>0)
            {
                for (int i = 0; i < DataFieldDataCleansing.DataField.OriginalValues.Count; i++)
                {
                    if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValues[i]))
                    {
                        DataFieldDataCleansing.DataField.OriginalValues[i] = DataFieldDataCleansing.DataField.OriginalValues[i].ToDate(DataFieldDataCleansing.InputDateFormat, DataFieldDataCleansing.DataField.OutputFormat);
                    }
                }
            }
        }
    }
}