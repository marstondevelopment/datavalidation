﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetClientScheme : ARStep
    {
        private OutputFile outputFile;
        private ARPrmsGetClientScheme prms;
        public ARGetClientScheme(FileTemplateARAction ftar) : base(ftar)
        {
            prms = ftar.ARAction.ARPrmsGetClientScheme;
        }
        public override bool Execute(ref string arScript)
        {
            outputFile = ParentFTAR.FileTemplate.FileWriter.ProcessingFile;

            if ( outputFile == null)
                throw new MissingMemberException("ProcessingFile is missing");
            if (prms != null && prms.FullFileName == true)
            {
                arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, outputFile.FileName);
                return true;
            }

            string fileNamePrefix = outputFile.FileName.Substring(0,outputFile.FileName.LastIndexOf("_"));
            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, fileNamePrefix );
            return true;
        }


    }
}