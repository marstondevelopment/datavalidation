﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using Mgl.DataValidator.Models.DataCleansing;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(MetaData))]
    public partial class DataFieldDataCleansing
    {
        private DCAction _dCAction;

        public DCAction DCAction
        {
            get
            {
                if (_dCAction == null)
                {
                    _dCAction = DCAction.New(this);
                }
                return _dCAction;
            }
        }

        public EnumDCAction Action
        {
            get
            {
                return (EnumDCAction)DCId;
            }
        }

        //public EnumDCInfoType? ProcessInfo
        //{
        //    get
        //    {
        //        if (ProcessInfoId.HasValue)
        //        return (EnumDCInfoType)ProcessInfoId;
        //        return null;
        //    }
        //}

        public DataFieldDataCleansing Clone()
        {
            return new DataFieldDataCleansing()
            {
                NewText = NewText,
                OldText = OldText,
                DataFieldId = DataFieldId,
                DCId = DCId,
                ProcessInfoId = ProcessInfoId,
                PaddingChar = PaddingChar,
                PaddingLength = PaddingLength,
                InputDateFormat = InputDateFormat,
                RegularExpressionId = RegularExpressionId
            };
        }


        public List<SelectListItem> RegularExpressions = null;

        public class MetaData
        {

            [Required(ErrorMessage = "Action is required.")]
            [Display(Name = "Action")]
            public int DCId;
            [Display(Name = "Old Text")]
            public string OldText;
            [Display(Name = "New Text")]
            public string NewText;
            [Display(Name = "Process Info")]
            public int ProcessInfoId;
            [Display(Name = "Regex")]
            public int RegularExpressionId;
        }
    }
}