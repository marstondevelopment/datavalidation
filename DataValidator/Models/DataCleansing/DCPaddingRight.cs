﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public class DCPaddingRight : DCAction
    {
        private const string ORIGINAL_VAL_TAG = "<*>";
        public DCPaddingRight(DataFieldDataCleansing dc) : base(dc) { }

        public override void ExecuteAction()
        {
            if (string.IsNullOrEmpty(DataFieldDataCleansing.PaddingChar) || !DataFieldDataCleansing.PaddingLength.HasValue)
            {
                throw new MissingFieldException("PaddingLength or PaddingChar is missing for the operation. Check the settings for data cleansing action.");
            }

            if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValue))
                DataFieldDataCleansing.DataField.OriginalValue = DataFieldDataCleansing.DataField.OriginalValue.PadRight(DataFieldDataCleansing.PaddingLength.Value, DataFieldDataCleansing.PaddingChar.ToCharArray()[0]);

            if (DataFieldDataCleansing.DataField.OriginalValues.Count > 0)
            {
                for (int i = 0; i < DataFieldDataCleansing.DataField.OriginalValues.Count; i++)
                {
                    if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValues[i]))
                        DataFieldDataCleansing.DataField.OriginalValues[i] = DataFieldDataCleansing.DataField.OriginalValues[i].PadRight(DataFieldDataCleansing.PaddingLength.Value, DataFieldDataCleansing.PaddingChar.ToCharArray()[0]);
                }
            }
        }

    }
}