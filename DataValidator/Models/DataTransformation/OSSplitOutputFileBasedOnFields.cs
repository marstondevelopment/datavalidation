﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WebGrease.Css.Extensions;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSSplitOutputFileBasedOnFields : DTStep
    {
        private PrmsSplitOutputFile prms;
        List<string> filters;
        List<string> schemes;
        public OSSplitOutputFileBasedOnFields(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitOutputFile;
            if (ftdt.DTAction.PrmsSplitOutputFile == null)
            {
                throw new MemberAccessException("PrmsSplitOutputFile  has not been set for DTAction - " + ParentFTDT.DTAction.ActionName);
            }
            filters = prms.FieldValues.Split(",".ToCharArray()).ToList();
            schemes = prms.Scheme.Split(",".ToCharArray()).ToList();

            if (filters.Count != schemes.Count)
            {
                throw new Exception("Filters and schemes are not set up correctly (i.e Not in sync) for DTAction - " + ParentFTDT.DTAction.ActionName);
            }

            schemes.ForEach( x => x = x.Trim());

        }
        public override bool Execute()
        {

            for (int i = 0; i < filters.Count; i++)
            {
                DataRow[] rows = ParentFTDT.FileTemplate.ProcessedDataTable.Select(filters[i]);
                if (rows.Length > 0)
                {
                    OutputFile outputFile = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(schemes[i].Trim(),EnumOutputFileCategory.Output);
                    foreach (DataRow rw in rows)
                    {
                        outputFile.Data.ImportRow(rw);
                    }
                    outputFile.Data.AcceptChanges();
                    rows.ForEach(x => x.Delete());
                    ParentFTDT.FileTemplate.ProcessedDataTable.AcceptChanges();
                }
            }

            return true;
        }
    }
}