﻿using Mgl.DataValidator.Models;
using Security.Authorize;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [ReadWriteAuthorizeUser]
    public class SupportController : ControllerBase
    {

        public SupportController(StoredProcedureRepository spr, UserRepository ur, EventLogRepository elr)
        {
            _spr = spr;
            _ur = ur;
            _elr = elr;
        }

        private UserRepository _ur;
        private EventLogRepository _elr;
        private StoredProcedureRepository _spr;
        private const string SEARCH_EL_PRMS = "ELSearchPrms";
        private const string SEARCH_SE_PRMS = "SESearchPrms";
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SystemErrorLog()
        {

            string[] data;
            if (System.IO.File.Exists(Utility.ErrorLogFilePath))
            {
                using (Stream stream = System.IO.File.Open(Utility.ErrorLogFilePath, FileMode.Open, FileAccess.Read,
                                        FileShare.ReadWrite))
                {
                    data = stream.ReadAllLines();
                    stream.Close();
                }

                if (data.Length > 0)
                {

                    Response.ClearHeaders();
                    Response.ContentType = "text/xml;charset=UTF-8";
                    Response.AppendHeader("content-disposition", "attachment; filename=\"SystemErrors_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xml\"");
                    //Response.CacheControl = "Private";
                    //Response.Cache.SetExpires(DateTime.Now.AddMinutes(3)); // or put a timestamp in the filename in the content-disposition
                    Response.Write(string.Join(Environment.NewLine, data));
                    Response.End();
                }
                else
                {
                    Response.Write("The error log file is empty.");
                    Response.Flush();
                }
            }
            else
            {
                Response.Write("The error log file does not exist.");
                Response.Flush();
            }

            return null;
        }

        protected ELSearchPrms<EventLog> CurrentELSearchPrms
        {
            get
            {
                return Session[SEARCH_EL_PRMS] == null ? null : (ELSearchPrms<EventLog>)this.Session[SEARCH_EL_PRMS];
            }
            set
            {
                Session.Add(SEARCH_EL_PRMS, value);
            }
        }

        protected SESearchPrms<SQLError> CurrentSESearchPrms
        {
            get
            {
                return Session[SEARCH_SE_PRMS] == null ? null : (SESearchPrms<SQLError>)this.Session[SEARCH_SE_PRMS];
            }
            set
            {
                Session.Add(SEARCH_SE_PRMS, value);
            }
        }

        public ActionResult SystemEventLog(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, DateTime? fromDate, DateTime? toDate, int? eventTypeId, int? userId)
        {
            ELSearchPrms<EventLog> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentELSearchPrms != null)
            {
                searchPrms = CurrentELSearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                searchPrms = new ELSearchPrms<EventLog>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, fromDate, toDate, eventTypeId, userId,
                     _ur.GetAll().Select(x => new SelectListItem() { Text = x.UserId, Value = x.Id.ToString() }).OrderBy(y=> y.Text).ToList()
                    );
            }
            DateTime? toDateDummy = null;
            if (searchPrms.ToDate.HasValue)
                toDateDummy = searchPrms.ToDate.Value.AddDays(1);

            var eventLogs = _elr.GetAll(x => (string.IsNullOrEmpty(searchPrms.SearchString) || (x.FileTemplate.Name.Contains(searchPrms.SearchString))) && (!searchPrms.FromDate.HasValue || x.ProcessStartedOn >= searchPrms.FromDate.Value) && (!toDateDummy.HasValue || x.ProcessStartedOn < toDateDummy.Value) && (!searchPrms.EventTypeId.HasValue || x.Action == searchPrms.EventTypeId.Value) && (!searchPrms.UserId.HasValue || x.UserId == searchPrms.UserId.Value)).OrderByDescending(x => x.ProcessStartedOn).Take(Utility.MaxListItems);
            CurrentELSearchPrms = new ELSearchPrms<EventLog>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, eventLogs.ToList(), searchPrms.FromDate, searchPrms.ToDate, searchPrms.EventTypeId, searchPrms.UserId, searchPrms.Users);

            return View(CurrentELSearchPrms).Information("Search results found: " + CurrentELSearchPrms.RowsCount.ToString());

        }
        [AdminOnlyAuthorizeUser]
        public ActionResult SQLErrorLog(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize, DateTime? fromDate, DateTime? toDate, int? dbConnectionId)
        {
            SESearchPrms<SQLError> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentSESearchPrms != null)
            {
                searchPrms = CurrentSESearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                var dbconns = _spr.GetConnections().OrderBy(x => x.Name).ToList();

                if (!dbConnectionId.HasValue)
                    dbConnectionId = dbconns.FirstOrDefault().Id;

                searchPrms = new SESearchPrms<SQLError>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null, fromDate, toDate, dbConnectionId, dbconns);
            }

            FileTemplate fileTemplate = new FileTemplate()
            {
                DBConnectionId = searchPrms.DBConnectionId,
                DbConnection = searchPrms.DBConnections.Where(x => x.Id == searchPrms.DBConnectionId).FirstOrDefault()
            };

            string sqlSelect = "SELECT TOP " + Utility.MaxListItems.ToString() + " Id,ErrorNumber,Severity,State,ErrorProcedure,ErrorLine,ErrorMessage,ExecutedOn FROM[oso].[SQLErrors] " ;
            string sqlWhere = string.Empty;

            if (searchPrms.FromDate.HasValue || searchPrms.ToDate.HasValue || !string.IsNullOrEmpty(searchPrms.SearchString))
            {
                sqlWhere = " WHERE ";
                sqlWhere = sqlWhere + (searchPrms.FromDate.HasValue ? " ExecutedOn >= '" + searchPrms.FromDate.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' AND" : string.Empty);
                sqlWhere = sqlWhere + (searchPrms.ToDate.HasValue ? " ExecutedOn < '" + searchPrms.ToDate.Value.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") + "' AND" : string.Empty);
                sqlWhere = sqlWhere + (!string.IsNullOrEmpty(searchPrms.SearchString) ? "(( ErrorMessage like '%" + searchPrms.SearchString.Replace("'","''") + "%') OR (ErrorProcedure like '%" + searchPrms.SearchString.Replace("'","''") + "%')) AND " : string.Empty);
                sqlWhere = sqlWhere.Trim().TrimEnd("AND".ToCharArray());
            }

            sqlSelect = sqlSelect + (!string.IsNullOrEmpty(sqlWhere) ? sqlWhere : string.Empty) + " ORDER BY ExecutedOn DESC";
            CmdText cmdText = new CmdText()
            {
                DBConnectionId = searchPrms.DBConnectionId.Value,
                DbConnection = searchPrms.DBConnections.Where(x => x.Id == searchPrms.DBConnectionId).FirstOrDefault(),
                Name = "GetSQLError",
                FileTemplate = fileTemplate,
                SQLScript = sqlSelect,
                ParamCollection = new Dictionary<string, object>()
            };

            List<SQLError> seLog = new List<SQLError>();
            DataProvider dp = DataProvider.New(fileTemplate);
            bool succeed = false;
            try
            {
                DataTable dt = dp.GetDataCmdText(cmdText);
                seLog =  dt.AsEnumerable().Select(x => new SQLError()
                  {
                      ErrorMessage = x.Field<string>("ErrorMessage"),
                      ErrorLine = x.Field<int>("ErrorLine"),
                      ErrorNumber = x.Field<int>("ErrorNumber"),
                      ErrorProcedure = x.Field<string>("ErrorProcedure"),
                      ExecutedOn = x.Field<DateTime>("ExecutedOn"),
                      Severity = x.Field<int>("Severity"),
                     State = x.Field<int>("State")

                }).ToList();
                succeed = true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }

            CurrentSESearchPrms = new SESearchPrms<SQLError>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, seLog.ToList(), searchPrms.FromDate, searchPrms.ToDate, searchPrms.DBConnectionId, searchPrms.DBConnections);
            if (succeed)
                return View(CurrentSESearchPrms).Information("Search results found: " + CurrentSESearchPrms.RowsCount.ToString());

            return View(CurrentSESearchPrms).Warning("Failed to retrieve SQL error information from the selected DB. Please see System Error Log for further details.");
        }

    }
}