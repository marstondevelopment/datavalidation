﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.ViewModel
{
    public class DataFieldViewModel
    {
        private DataFieldRepository _dfr;

        public DataFieldViewModel(DataFieldRepository dfr, DataField df )
        {
            _dfr = dfr;
            DataField = df;
            SPs = _dfr.GetSPs().Where(x => x.SPType == (int)EnumSPType.VRSP).ToList();
            RegExs = _dfr.GetRegExs();
            RefLists = _dfr.GetReferenceLists();
        }

        public DataField DataField { get; set; }
        public List<StoredProcedure> SPs { get; set; }
        public List<RegularExpression> RegExs { get; set; }
        public FileTemplateDataField FileTemplateData { get; set; }        
        public DataFieldRule DataFieldRule { get; set; }
        public List<ReferenceList> RefLists { get; set; }


    }
}