﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace Mgl.DataValidator.Models
{
    public static class Utility
    {
        public const string DEFAULT_DELIMITER = ",";
        public const string SPACE_DELIMITER = " ";
        public const string PIPE_DELIMITER = "|";
        public const int ONESTEP_MAX_LENGTH = 250;
        public const string ONESTEP_STRING_SEPARATOR = ";";
        public const string FIELD_PROPERTIES = "PROPERTIES";
        public const string LAST_LOGGED_ON = "LAST_LOGGED_ON";
        public const string DB_NEW_LINE = "\\r\\n";
        public const string DB_TAB = "\\t";

        public static List<string> GetColumns(string wildcard, DataTable dt)
        {
            List<string> wildCardNames = new List<string>();

            string[] names = dt.Columns.Cast<DataColumn>()
            .Where(y => y.ColumnName.StartsWith(wildcard.Substring(0, wildcard.Length - 1)))
             .Select(x => x.ColumnName)
             .ToArray();
            if (names != null && names.Length > 0)
            {
                wildCardNames.AddRange(names);
            }

            return wildCardNames;
        }

        public static void AddColumns(this DataTable dataTable, DataField dataField)
        {
            int index = dataTable.Columns.IndexOf(dataField.Name);

            for (int i = 1; i <= dataField.OriginalValues.Count; i++)
            {
                if (dataTable.Columns.Contains(dataField.Name + i.ToString()))
                {
                    index = dataTable.Columns.IndexOf(dataField.Name + i.ToString());
                }
                else
                {
                    DataColumn col = dataTable.Columns.Add(dataField.Name + i.ToString(), dataField.ObjectType().GetType());
                    col.ExtendedProperties.Add(FIELD_PROPERTIES, dataField);
                    col.SetOrdinal(++index);
                    if (!string.IsNullOrEmpty(dataField.DCExpression))
                        col.Expression = dataField.DCExpression;

                }
            }
        }

        public static void AddColumn(this DataTable dataTable, DataField dataField)
        {

            DataColumn dc = dataTable.Columns.Add(dataField.Name, dataField.ObjectType().GetType());
            dc.ExtendedProperties.Add(FIELD_PROPERTIES, dataField);
            if (!string.IsNullOrEmpty(dataField.DCExpression))
                dc.Expression = dataField.DCExpression;
        }

        public static List<DataColumn> AddColumnForChildNode(this DataTable dataTable, DataField dataField)
        {
            List<DataColumn> dcs = null;
            if ( dataField.DerivedSubNodes != null && dataField.DerivedSubNodes.Count>0)
            {
                dcs = new List<DataColumn>();
                foreach (var item in dataField.DerivedSubNodes)
                {
                    DataColumn dc = dataTable.Columns[item.Key];
                    if (dc == null)
                    {
                        dc = dataTable.Columns.Add(item.Key, dataField.ObjectType().GetType());
                        dc.ExtendedProperties.Add(FIELD_PROPERTIES, dataField);
                        if (!string.IsNullOrEmpty(dataField.DCExpression))
                            dc.Expression = dataField.DCExpression;
                    }
                    dcs.Add(dc);
                }
            }

            return dcs;
        }

        public static void CopyAttributes(this DataTable dataTable, DataTable sourceTable)
        {

            foreach (DataColumn col in sourceTable.Columns)
            {

                if (col.ExtendedProperties.ContainsKey(FIELD_PROPERTIES) && dataTable.Columns.Contains(col.ColumnName) && !dataTable.Columns[col.ColumnName].ExtendedProperties.ContainsKey(FIELD_PROPERTIES))
                    dataTable.Columns[col.ColumnName].ExtendedProperties.Add(FIELD_PROPERTIES, col.ExtendedProperties[FIELD_PROPERTIES]);
            }
        }

        public static DataColumn AddColumn(this DataTable dataTable, string fieldName)
        {
            DataColumn dc = dataTable.Columns[fieldName];
            if (dc == null)
            {
                dc = dataTable.Columns.Add(fieldName, typeof(string));
            }

            return dc;
        }

        private static string charsForTrim;

        public static string CharsForTrim
        {
            get
            {
                if (string.IsNullOrEmpty(charsForTrim))
                {
                    charsForTrim = ConfigurationManager.AppSettings["CharsForTrim"];
                }

                return charsForTrim;
            }

        }

        private static string stagingAttachmentsFolder;

        public static string StagingAttachmentsFolder
        {
            get
            {
                if (string.IsNullOrEmpty(stagingAttachmentsFolder))
                {
                    stagingAttachmentsFolder = ConfigurationManager.AppSettings["StagingAttachmentsFolder"];
                }

                return stagingAttachmentsFolder;
            }

        }


        private static string productionAttachmentsFolder;

        public static string ProductionAttachmentsFolder
        {
            get
            {
                if (string.IsNullOrEmpty(productionAttachmentsFolder))
                {
                    productionAttachmentsFolder = ConfigurationManager.AppSettings["ProductionAttachmentsFolder"];
                }

                return productionAttachmentsFolder;
            }

        }

        public static string GetAttachmentsFolder(string filePath, DateTime addedOn)
        {

            string storageFolder = Path.Combine(filePath, addedOn.ToString("yyyy.MM.dd").Replace('.', Path.DirectorySeparatorChar));
            if (!Directory.Exists(storageFolder))
            {
                Directory.CreateDirectory(storageFolder);
            }            
            return storageFolder;
        }

        public static string GenerateColumbusFileName(DateTime addedOn, string fileName)
        {
            var relativePath = addedOn.ToString("yyyy.MM.dd").Replace('.', Path.DirectorySeparatorChar);
            return Path.Combine(relativePath, fileName);
        }

        public static void SaveFileToFolder(string folder, string fileName, HttpPostedFileBase file)
        {
            string fullPath = Path.Combine(folder, fileName);
            file.SaveAs(fullPath);

        }

        public static void DeleteFileFromFolder(string folder, string fileName)
        {
            string fullPath = Path.Combine(folder, fileName);
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }

        }


        private static int maxListItems = -1;

        public static int MaxListItems
        {
            get
            {
                if (maxListItems == -1)
                {
                    maxListItems = int.Parse(ConfigurationManager.AppSettings["MaxListItems"]);
                }

                return maxListItems;
            }

        }

        private static string defaultPostcodeOS = string.Empty;

        public static string DefaultPostcodeOS
        {
            get
            {
                if (string.IsNullOrEmpty(defaultPostcodeOS))
                {
                    defaultPostcodeOS = ConfigurationManager.AppSettings["DefaultPostcodeOS"];
                }

                return defaultPostcodeOS;
            }

        }

        private static string variableTagPattern = string.Empty;

        public static string VariableTagPattern
        {
            get
            {
                if (string.IsNullOrEmpty(variableTagPattern))
                {
                    variableTagPattern = ConfigurationManager.AppSettings["VariableTagPattern"];
                }

                return variableTagPattern;
            }

        }

        private static int defaultPageSize = -1;

        public static int DefaultPageSize
        {
            get
            {
                if (defaultPageSize == -1)
                {
                    defaultPageSize = int.Parse(ConfigurationManager.AppSettings["DefaultPageSize"]);
                }

                return defaultPageSize;
            }

        }

        private static string errorLogFilePath = string.Empty;

        public static string ErrorLogFilePath
        {
            get
            {
                if (string.IsNullOrEmpty(errorLogFilePath))
                {
                    errorLogFilePath = ConfigurationManager.AppSettings["ErrorLogFilePath"];
                }

                return errorLogFilePath;
            }

        }

        private static string concatColNames = string.Empty;

        public static string ConcatColNames
        {
            get
            {
                if (string.IsNullOrEmpty(concatColNames))
                {
                    concatColNames = ConfigurationManager.AppSettings["ConcatColNames"];
                }

                return concatColNames;
            }

        }

        private static void SetValuesForSubNodes(DataRow row, DataField df)
        {
            foreach (var item in df.DerivedSubNodes)
            {
                df.OriginalValue = item.Value;
                row[item.Key] = df.TypedField();
            }
        }

        public static DataRow AddRow(this DataTable inDataTable, List<DataField> cols)
        {
            var row = inDataTable.NewRow();
            foreach (var df in cols)
            {
                if (df.DerivedSubNodes != null)
                {
                    inDataTable.AddColumnForChildNode(df);
                    SetValuesForSubNodes(row, df);
                }
                else if (df.OutputFieldOnly == false || !string.IsNullOrEmpty(df.OriginalValue))
                {

                    if (df.HasMultipleNodes)
                    {
                        inDataTable.AddColumns(df);
                        string oldVal = df.OriginalValue;
                        for (int i = 0; i < df.OriginalValues.Count; i++)
                        {
                            df.OriginalValue = df.OriginalValues[i];
                            row[df.Name + (i + 1).ToString()] = df.TypedField();
                        }
                        df.OriginalValue = oldVal;
                    }
                    else
                    {
                        row[df.Name] = df.TypedField();
                    }
                }
            }

            inDataTable.Rows.Add(row);

            return row;

        }

        public static string[] ReadAllLines(this Stream stream)
        {
            List<String> allLines = new List<string>();
            using (StreamReader sr = new StreamReader(stream))
            {
                stream.Position = 0;
                while (sr.Peek() >= 0)
                {
                    allLines.Add(sr.ReadLine());
                }
            }

            return allLines.ToArray();

        }

        public static string ToCsv(this DataTable inDataTable, string delimiter, bool includeHeaders = true)
        {
            delimiter = delimiter ?? DEFAULT_DELIMITER;
            delimiter = delimiter.Replace(DB_NEW_LINE, Environment.NewLine);
            delimiter = delimiter.Replace(DB_TAB, "\t");

            var builder = new StringBuilder();
            var columnNames = inDataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            if (includeHeaders)
                builder.AppendLine(string.Join(delimiter, columnNames));
            foreach (DataRow row in inDataTable.Rows)
            {
                IEnumerable<string> fields = null;
                if (delimiter == DEFAULT_DELIMITER)
                {
                    fields = row.ItemArray.Select(field => field.ToString().WrapInQuotesIfContains(delimiter));
                }
                else
                {
                    fields = row.ItemArray.Select(field => field.ToString());
                }

                builder.AppendLine(string.Join(delimiter, fields));
            }

            string retVal = builder.ToString();
            if (retVal.Length >= 2)
                //To remove the extra new line characters (\r\n) in the output file
                retVal = retVal.Substring(0, retVal.Length - Environment.NewLine.Length);

            return retVal;
        }

        // By using this method we can convert datatable to xml
        public static string ConvertDatatableToXML(this DataTable dt)
        {
            MemoryStream str = new MemoryStream();
            dt.WriteXml(str, XmlWriteMode.IgnoreSchema, true);
            str.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(str);
            string xmlstr;
            xmlstr = sr.ReadToEnd();
            return (xmlstr);
        }

        public static string RenderTag(this string arScript, string tagName, string context)
        {
            string tag = "{" + tagName + "}";
            return arScript.Replace(tag, context);
        }

        public static string RenderSectionTag(this string arScript, string tagName, string context)
        {
            string tag = "{" + tagName + "}" + GetValueFromSectionTag(arScript, tagName) + "{/" + tagName + "}";
            return arScript.Replace(tag, context);
        }

        public static string GetValueFromSectionTag(this string arScript, string tagName)
        {
            string startTag = "{" + tagName + "}";
            string endTag = "{/" + tagName + "}";
            int sPos = arScript.IndexOf(startTag) + startTag.Length;
            int ePos = arScript.IndexOf(endTag);
            return (sPos >= 0 && ePos > 0) ? arScript.Substring(sPos, ePos - sPos) : string.Empty; 
        }

        public static string ToCsv(this DataTable inDataTable, string delimiter, DataTable sourceTable, bool includeHeaders = true)
        {
            if (sourceTable == null)
            {
                throw new NullReferenceException("source Table is required for output format.");
            }

            inDataTable.CopyAttributes(sourceTable);
            delimiter = delimiter ?? DEFAULT_DELIMITER;
            delimiter = delimiter.Replace(DB_NEW_LINE, Environment.NewLine);
            delimiter = delimiter.Replace(DB_TAB, "\t");

            var builder = new StringBuilder();

            if (includeHeaders)
            {
                var columnNames = inDataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
                List<string> outputColNames = new List<string>();
                foreach (string item in columnNames)
                {
                    if (inDataTable.Columns[item].ExtendedProperties.ContainsKey(FIELD_PROPERTIES))
                    {
                        DataField df = (DataField)inDataTable.Columns[item].ExtendedProperties[FIELD_PROPERTIES];
                        if (!string.IsNullOrEmpty(df.OutputColumnName))
                        {
                            string newName = df.OutputColumnName;
                            if (df.HasMultipleNodes)
                            {
                                newName = newName + item.Substring(df.Name.Length);
                            }


                            outputColNames.Add(newName);
                        }
                        else
                        {
                            outputColNames.Add(item);
                        }
                    }
                    else
                    {
                        outputColNames.Add(item);
                    }
                }
                builder.AppendLine(string.Join(delimiter, outputColNames));
            }

            foreach (DataRow row in inDataTable.Rows)
            {
                List<string> fields = new List<string>();

                for (int i = 0; i < inDataTable.Columns.Count; i++)
                {
                    DataColumn dc = inDataTable.Columns[i];
                    string val = row[i].ObjToString(dc).Replace("\"","\"\"").WrapInQuotesIfContains(delimiter);
                    fields.Add(val);

                }

                string rec = string.Join(delimiter, fields);

                builder.AppendLine(string.Join(delimiter, fields));
            }

            string retVal = builder.ToString();
            if (retVal.Length >= 2)
                //To remove the extra new line characters (\r\n) in the output file
                retVal = retVal.Substring(0, retVal.Length - Environment.NewLine.Length);

            return retVal;
        }


        public static string ToXml(this DataTable inDataTable, DataTable sourceTable, string root, string nodeForRow)
        {

            var columnNames = inDataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            List<string> outputColNames = new List<string>();
            foreach (string item in columnNames)
            {
                if (inDataTable.Columns[item].ExtendedProperties.ContainsKey(FIELD_PROPERTIES))
                {
                    DataField df = (DataField)inDataTable.Columns[item].ExtendedProperties[FIELD_PROPERTIES];
                    if (!string.IsNullOrEmpty(df.OutputColumnName))
                    {
                        string newName = df.OutputColumnName;
                        if (df.HasMultipleNodes)
                        {
                            newName = newName + item.Substring(df.Name.Length);
                        }


                        outputColNames.Add(newName);
                    }
                    else
                    {
                        outputColNames.Add(item);
                    }
                }
                else
                {
                    outputColNames.Add(item);
                }
            }

            List<string[]> dataRows = new List<string[]>();
            foreach (DataRow row in inDataTable.Rows)
            {
                List<string> fields = new List<string>();

                for (int i = 0; i < inDataTable.Columns.Count; i++)
                {
                    
                    DataColumn dc = inDataTable.Columns[i];
                    string val = row[i].ObjToString(dc);
                    fields.Add(val);

                }
                dataRows.Add(fields.ToArray());
            }

            XDocument xmlDoc = new XDocument();
            XElement xRoot = new XElement(root.RemoveNoneAlphabeticChars());
            foreach (var rw in dataRows.ToArray())
            {
                XElement xRow = new XElement(nodeForRow.RemoveNoneAlphabeticChars());

                for (int i = 0; i < outputColNames.Count-1; i++)
                {
                    XElement xCol = new XElement(outputColNames[i].RemoveNoneAlphabeticChars(), rw[i]);
                    xRow.Add(xCol);
                }

                xRoot.Add(xRow);

            }
            xmlDoc.Add(xRoot);

            return xmlDoc.ToString();
        }

        public static string CorrectCharEscapeSequences(this string inString)
        {
            if (!string.IsNullOrEmpty(inString))
                return inString.Replace("\\r", "\r").Replace("\\n", "\n");

            return null;
        }


        public static string RemoveNoneAlphabeticChars(this string inString)
        {
            return new string(inString.Where(c => Char.IsLetter(c) || c == '\'').ToArray());            
        }
        public static string WrapInQuotesIfContains(this string inString, string delimiter)
        {
            if (string.IsNullOrEmpty(inString))
                return null;
            if ( delimiter == DEFAULT_DELIMITER  && (inString.Contains(delimiter) || inString.Contains("\r") || inString.Contains("\n") || inString.Contains(Environment.NewLine)))
                return "\"" + inString + "\"";
            return inString;
        }


        public static string ObjToString(this object inObj, DataColumn dc)
        {
            string val = inObj.ToString();
            string outputFormat = String.Empty;

            if (dc.ExtendedProperties[FIELD_PROPERTIES] != null)
            {
                outputFormat = ((DataField)dc.ExtendedProperties[FIELD_PROPERTIES]).OutputFormat;
            }

            if (dc.DataType != typeof(System.String))
            {
                if (!string.IsNullOrEmpty(val) && !string.IsNullOrEmpty(outputFormat))
                {
                    if (inObj.GetType() == typeof(DateTime))
                    {
                        val = ((DateTime)inObj).ToString(outputFormat);
                    }
                    if (inObj.GetType() == typeof(System.Decimal))
                    {
                        val = ((decimal)inObj).ToString(outputFormat);
                    }
                    if (inObj.GetType() == typeof(System.Double))
                    {
                        val = ((double)inObj).ToString(outputFormat);
                    }
                    if (inObj.GetType() == typeof(int))
                    {
                        val = ((int)inObj).ToString(outputFormat);
                    }
                }

            }

            return val;
        }

        public static IEnumerable<string> SplitBy(this string str, int chunkLength)
        {
            if (String.IsNullOrEmpty(str)) throw new ArgumentException();
            if (chunkLength < 1) throw new ArgumentException();

            for (int i = 0; i < str.Length; i += chunkLength)
            {
                if (chunkLength + i > str.Length)
                    chunkLength = str.Length - i;

                yield return str.Substring(i, chunkLength);
            }
        }


        public static IEnumerable<string> SmartSplit(this string str, int chunkLength)
        {
            if (String.IsNullOrEmpty(str)) throw new ArgumentException();
            if (chunkLength < 1) throw new ArgumentException();

            string regexPattern = @"(.{1,cl})(?:\s|$)".Replace("cl",chunkLength.ToString());
            var regex = new Regex(regexPattern);
            var results = regex.Matches(str)
                                       .Cast<Match>()
                                       .Select(m => m.Groups[1].Value)
                                       .ToList();
            return results;
        }

        public static bool CheckFileExtention(string fileName, FileTemplate fileTemplate)
        {
            int pos = fileName.LastIndexOf(".");
            if (pos <= 0)
                return false;
            string extention = fileName.Substring(pos + 1).ToUpper();
            return fileTemplate.MimeType.Extension.ToUpper().Contains(extention);
        }


        public static string RenderRazorViewToString(this Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public static string ToDate(this string str, string expectedFormat, string outputFormat)
        {
            DateTime dateValue;
            if (DateTime.TryParseExact(str, expectedFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
            {
                //Parsed Successfully   
                return dateValue.ToString(outputFormat);
            }
            else
                throw new Exception("Failed to convert string to date for value: " + str);

        }

        public static bool IsEmpty(this DataRow row)
        {
            return (row == null || row.ItemArray.All(i => i.IsNullEquivalent()) || row[0].ToString().ToUpper() == "N/A");
        }

        public static bool IsNullEquivalent(this object value)
        {
            return value == null
                   || value is DBNull
                   || string.IsNullOrWhiteSpace(value.ToString());
        }

        public static string GetBaseUrl()
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            if (appUrl != "/")
                appUrl = "/" + appUrl;
            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }

        public static string ToYesNo(this Boolean b)
        {
            return b ? "Yes" : "No";
        }

        public static MvcHtmlString YesNo(this HtmlHelper htmlHelper, bool yesNo)
        {
            var text = yesNo ? "Yes" : "No";
            return new MvcHtmlString(text);
        }

        public static List<T> ConvertDataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public static bool HasProperty(this object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName) != null;
        }

        public static string GetPropertyValue(this object obj, string propertyName)
        {
            try
            {
                //return obj.GetType().GetProperty(propertyName).GetValue(obj) as string;
                return Convert.ToString(obj.GetType().GetProperty(propertyName).GetValue(obj));
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to get value of the property " + propertyName + " from the object(" + obj.GetType().Name +  ")." , ex);
            }
        }




        private static Dictionary<string, string> contentTypes;
        public static Dictionary<string, string> ContentTypes {
            get
            {
                if (contentTypes == null)
                {
                    contentTypes = new Dictionary<string, string>()
                    {
                        { ".xls","application/vnd.ms-excel" },
                        { ".xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
                        { ".csv","text/csv" },
                        { ".txt","text/plain" },
                        { ".doc","application/msword" },
                        { ".docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
                        { ".pdf","application/pdf" },
                        { ".gif","image/gif" },
                        { ".jpg","image/jpeg" },
                        { ".jpeg","image/jpeg" },
                        { ".zip","application/zip" },
                        { ".7z","application/x-7z-compressed" }
                    };
                }
                return contentTypes;

            }
        }
    }
}