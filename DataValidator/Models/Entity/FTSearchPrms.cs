﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;


namespace Mgl.DataValidator.Models
{
    public class FTSearchPrms<T>:SearchPrms<T>
    {
        public FTSearchPrms(string sortOrder, string searchString, int? page, int? pageSize, int defaultPageSize, List<T> allResults,int? clientId,List<SelectListItem> clients):base( sortOrder,  searchString,  page,  pageSize,  defaultPageSize, allResults)
        {
            ClientId = clientId;
            Clients = clients;
        }

        [Display(Name = "Client")]
        public int? ClientId { get; set; }

        public List<SelectListItem> Clients { get; set; }

    }
}