﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsExcludeRowsNotMatchReferenceList : PrmsBase
    {
        public PrmsExcludeRowsNotMatchReferenceList() { }
        public PrmsExcludeRowsNotMatchReferenceList(DTAction dt) : base(dt) { }

        [Display(Name = "Input Columns")]
        public string InputColumnNames { get; set; }

        [Display(Name = "Check Matched Rows")]
        public bool CheckMatchedRows { get; set; }

        [Display(Name = "Keep Invalid Row")]
        public bool KeepInvalidRow { get; set; }

        [Display(Name = "Copy From")]
        public string FromColumn { get; set; }

        [Display(Name = "Copy To")]
        public string ToColumn { get; set; }

        [Display(Name = "Filter")]
        public string Filter { get; set; }

        [Display(Name = "Reference List")]
        public int? ReferenceListId { get; set; }

        [Display(Name = "FN Prefix")]
        public string FNPrefix { get; set; }
        [Display(Name = "Exclusion Reason")]
        public string ExclusionReason { get; set; }

        [JsonIgnore]
        public ReferenceList ReferenceList { get; set; }

    }
}