﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic;
using System.Web;
using System.Diagnostics;
using Microsoft.VisualBasic.FileIO;
using WebGrease.Css.Extensions;

namespace Mgl.DataValidator.Models
{
    public class TextFileParser : FileParser
    {
        public TextFileParser(FileTemplate ft, Stream fs, string fileName, EnumDataFileScreen userAction) : base(ft, fs, fileName, userAction)
        {
        }

        private const int ROW_INDEX = 0;
        private const int COL_INDEX = 1;
        public const string FIXED_WIDTH_DELIMITER = "_";


        public override bool Validate()
        {
            bool valid = true;
            Dictionary<int, DataField> colsMap = null;
            try
            {

                using (UploadedFile)
                {

                    Dictionary<int, string[]> dataRows = new Dictionary<int, string[]>();

                    //For fixed width data file
                    if (FileTemplate.FileDelimiter != null && FileTemplate.FileDelimiter.Delimiter == FIXED_WIDTH_DELIMITER)
                    {
                        string[] allLines = UploadedFile.ReadAllLines();

                        if (allLines.Length > 0)
                        {
                            dataRows = new Dictionary<int, string[]>();
                            int[] fieldWidths = FileTemplate.BodyCols.Where(x => x.FieldWidth != null && x.FieldWidth > 0).Select(y => y.FieldWidth.Value).ToArray();

                            for (int i = 0; i < allLines.Length; i++)
                            {
                                if ((FileTemplate.RemoveHeader && i < FileTemplate.HeaderRowsToSkip) || (FileTemplate.RemoveFooter && i > allLines.Length - (FileTemplate.FooterRowsToSkip + 1)))
                                {
                                    dataRows.Add(i + 1, new string[] { allLines[i] });
                                }
                                else
                                {
                                    dataRows.Add(i + 1, ParseFixedWidthData(allLines[i], fieldWidths, i + 1));
                                }
                            }
                        }

                    }
                    else // All other type of text files using delimiter
                    {
                        //Note: TextFieldParser uses default encoding: UTF8
                        using (TextFieldParser parser = new TextFieldParser(UploadedFile))
                        {
                            parser.TextFieldType = FieldType.Delimited;
                            parser.SetDelimiters(FileTemplate.DelimiterId.HasValue ? FileTemplate.FileDelimiter.Delimiter : ",");
                            int rowCount = 0;
                            while (!parser.EndOfData)
                            {
                                string[] fields = parser.ReadFields();
                                ++rowCount;
                                dataRows.Add(rowCount, fields);
                            }
                            parser.Close();
                        }
                    }

                    // To persist variables before the validation
                    foreach (DataField item in FileTemplate.Variables.Where(x => !string.IsNullOrEmpty(x.CoordinateForVariable)).ToList())
                    {
                        int[] coord;

                        try
                        {
                            coord = item.CoordinateForVariable.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x => int.Parse(x)).ToArray();
                            if (coord[ROW_INDEX] < 0)
                            {
                                coord[ROW_INDEX] = dataRows.Count + coord[ROW_INDEX] + 1;
                            }
                        }
                        catch (Exception)
                        {
                            throw new Exception("System failed to convert PositionInHeader into int[], please check the settings and try again.");
                        }


                        //Note row index start from 1 and col index starts from 0 here

                        if (coord[ROW_INDEX] > dataRows.Count)
                            throw new Exception("Row index is out of range for " + item.Name + ".CoordinateForVariable, please check the settings and try again.");

                        if (coord[COL_INDEX] > dataRows[coord[ROW_INDEX]].Length)
                        {
                            throw new Exception("Column index is out of range for " + item.Name + ".CoordinateForVariable, please check the settings and try again.");
                        }

                        item.OriginalValue = dataRows[coord[ROW_INDEX]][coord[COL_INDEX] - 1];
                    }




                    for (int i = 1; i <= dataRows.Count; i++)
                    {
                        //Remove first
                        if (FileTemplate.RemoveHeader && i <= FileTemplate.HeaderRowsToSkip)
                            continue;
                        //Remove last row
                        if (FileTemplate.RemoveFooter && i > dataRows.Count - FileTemplate.FooterRowsToSkip)
                            continue;

                        if (colsMap == null)
                        {
                            // Map the columns 
                            colsMap = MapColumns(dataRows[i]);
                            if (FileTemplate.NoColumnName == false)
                                continue;
                        }

                        FileTemplate.ResetFieldValues();
                        FileTemplate.RowId = i;
                        try
                        {
                            foreach (var df in colsMap)
                            {
                                df.Value.OriginalValue = dataRows[i][df.Key];
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error occurred during parsing data file at row " + i.ToString() + ": " + ex.Message + " \n Stack Trace: " + ex.StackTrace );
                        }

                        //Validate each row
                        if (!FileTemplate.ValidateRows())
                        {
                            if (FileTemplate.TerminateProcess)
                                return false;
                            valid = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                if (ex is MissingFieldException)
                {
                    ValidationSummary.Clear();
                    ValidationSummary.AppendLine(ex.Message);
                    //ValidationSummary.AppendLine("*********************************************************************************************");
                    //ValidationSummary.AppendLine("*** Warning:  The selected file template can only parse files with extension: txt or csv. ***");
                    //ValidationSummary.AppendLine("*********************************************************************************************");
                    return false;
                }
                else
                    throw;
            }


            return valid;
        }

        private string[] ParseFixedWidthData(string currentLine, int[] fieldWidths, int lineNo)
        {
            List<string> fields = new List<string>();
            int curPos = 0;

            for (int i = 0; i < fieldWidths.Length; i++)
            {
                fields.Add(currentLine.Substring(curPos, fieldWidths[i]));
                curPos = curPos + fieldWidths[i];
            }
            return fields.ToArray();
        }


        private Dictionary<int, DataField> MapColumns(string[] fields)
        {


            Dictionary<int, DataField> colMap = new Dictionary<int, DataField>();
            var ftInputFields = FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList();

            if (FileTemplate.NoColumnName == false)
            {
                for (int i = 0; i < fields.Length; i++)
                {
                    fields[i] = fields[i].ToUpper();
                }

                foreach (var df in ftInputFields)
                {
                    int colIndex = Array.IndexOf(fields, df.Name.ToUpper());
                    if (colIndex == -1)
                        //Failed to map the column from the file
                        throw new MissingFieldException("Failed to parse data file. Column - " + df.Name.ToUpper() + " not found.");
                    colMap.Add(colIndex, df);
                }
            }
            else
            {
                if (fields.Length < ftInputFields.Count)
                {
                    //Failed to map the column from the file
                    throw new MissingFieldException("Failed to parse data file. Number of columns from input file( " + fields.Length.ToString() + ") is less than " + FileTemplate.CurrentFileName + " (" + ftInputFields.Count.ToString() + "). Please ensure the file format and data fields setting are in sync, and try again.");

                }

                for (int i = 0; i < ftInputFields.Count; i++)
                {
                    colMap.Add(i, ftInputFields[i]); //add the cell contents (name of column) and cell index to the map
                }
            }

            return colMap;
        }
    }
}