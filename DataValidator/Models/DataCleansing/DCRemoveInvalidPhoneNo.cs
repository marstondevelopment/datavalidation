﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public class DCRemoveInvalidPhoneNo : DCAction
    {
        public DCRemoveInvalidPhoneNo(DataFieldDataCleansing dc) : base(dc) { }
        private const string REGEX_PTN = "^(?:\\+\\d{1,3}|0\\d{1,3}|00\\d{1,2})?(?:\\s?\\(\\d+\\))?(?:[-\\/\\s.]|\\d)+$";
        public override void ExecuteAction()
        {
            if (DataFieldDataCleansing.DataField.OriginalValue != null)
                DataFieldDataCleansing.DataField.OriginalValue = AddLeadingZero(DataFieldDataCleansing.DataField.OriginalValue);

            if (DataFieldDataCleansing.DataField.OriginalValues.Count > 0)
            {
                for (int i = 0; i < DataFieldDataCleansing.DataField.OriginalValues.Count; i++)
                {
                    if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValues[i]))
                    {
                        DataFieldDataCleansing.DataField.OriginalValues[i] = AddLeadingZero(DataFieldDataCleansing.DataField.OriginalValues[i]);
                    }

                }

            }

        }

        private string AddLeadingZero(string phoneNO)
        {
            string formattedNo = phoneNO;
            if (!string.IsNullOrEmpty(formattedNo))
            {
                formattedNo = formattedNo.Trim().Replace(" ", string.Empty);

                Regex regex = new Regex(REGEX_PTN);
                if (regex.IsMatch(formattedNo))
                {
                    formattedNo = formattedNo.Replace("+44", "0");
                    if (formattedNo.Length == 10 && formattedNo.First().ToString() != "+" && formattedNo.First().ToString() != "0")
                    {
                        formattedNo = "0" + formattedNo;
                    }
                    else if (formattedNo.Length > 11 || formattedNo.Length < 10)
                    {
                        formattedNo = string.Empty;
                    }

                    phoneNO = formattedNo;
                }
                else
                {
                    phoneNO = string.Empty;
                }

            }

            return phoneNO;
        }

    }
}