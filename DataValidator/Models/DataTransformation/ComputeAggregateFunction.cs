﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class ComputeAggregateFunction : DTStep
    {
        private PrmsComputeAggregateFunction prms;
        public ComputeAggregateFunction(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsComputeAggregateFunction;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsComputeAggregateFunction parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

            if (string.IsNullOrEmpty(prms.AggregateExpression))
                throw new MemberAccessException("AggregateExpression is missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");

            if (string.IsNullOrEmpty(prms.TotalFieldName) && string.IsNullOrEmpty(prms.VariableName))
                throw new MemberAccessException("Total Field Name and Vairable Name are both missing for " + ftdt.DTAction.ActionName + ". The DT Action requires at least one field for the output. Please update the configuration settings and try again.");


        }

        private bool SetValueForDataField(DataTable dt, string fieldName, object val)
        {
            if (dt != null && dt.Columns.Contains(fieldName))
            {
                UpdateColumn(dt, fieldName, val);
                return true;
            }

            return false;
        }

        public override bool Execute()
        {
            object val = (decimal)ParentFTDT.FileTemplate.ProcessedDataTable.Compute(prms.AggregateExpression, prms.Filter);

            if (!string.IsNullOrEmpty(prms.TotalFieldName))
            {
                bool colExists=false;
                if (SetValueForDataField(ParentFTDT.FileTemplate.HeaderDataTable, prms.TotalFieldName,val))
                    colExists = true;
                if (SetValueForDataField(ParentFTDT.FileTemplate.ProcessedDataTable, prms.TotalFieldName, val))
                    colExists = true;
                if (SetValueForDataField(ParentFTDT.FileTemplate.TrailerDataTable, prms.TotalFieldName, val))
                    colExists = true;

                if (!colExists)
                    throw new MemberAccessException(ParentFTDT.DTAction.ActionName + ": Failed to find data field (" + prms.TotalFieldName + ") from data field list, please update the configuration settings and try again.");

            }

            if (!string.IsNullOrEmpty(prms.VariableName))
            {
                DataField df = ParentFTDT.FileTemplate.Variables.Where(x => x.Name.ToUpper() == prms.VariableName.ToUpper()).FirstOrDefault();
                if (df == null)
                {
                    throw new MemberAccessException(ParentFTDT.DTAction.ActionName + ": Failed to find the variable (" + prms.VariableName + ") from data field list, please update the configuration settings and try again.");
                }

                df.OriginalValue = val.ToString();
            }

            return true;
                 
        }
    }
}