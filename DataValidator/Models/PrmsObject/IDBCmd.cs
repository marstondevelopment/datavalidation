﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mgl.DataValidator.Models
{
    public interface IDBCmd
    {

        [Display(Name = "Command Text")]
        int? CmdTextId { get; set; }
        [JsonIgnore]
        CmdText CmdText { get; set; }

        [Display(Name = "Stored Procedure")]
        int? StoredProcedureId { get; set; }
        [JsonIgnore]
        StoredProcedure StoredProcedure { get; set; }
    }
}
