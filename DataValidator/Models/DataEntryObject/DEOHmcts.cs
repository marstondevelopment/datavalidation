﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{

    public class DEOHmcts : DEOBase
    {
        public enum EnumHMCTSDocCat
        {
            [Display(Name = "Warrant")]
            Warrant = 0,
            [Display(Name = "OAIS")]
            OAIS,
            [Display(Name = "NOMS")]
            NOMS,
            [Display(Name = "Additional")]
            Additional,
        }

        public DEOHmcts() { }
        public DEOHmcts(FileTemplate ft, List<SelectListItem> ocList, List<SelectListItem> clientsList, List<SelectListItem> regionsList) : base(ft)
        {
            Country = "United Kingdom";
            OffenceCodesList = ocList.Where(x => x.Text.StartsWith("MK-") || x.Text.StartsWith("BRW-")).ToList();
            OffenceCodesList.Insert(0, new SelectListItem() { Text = "NWM", Value = "No Warning Marker" });
            ClientsList = clientsList;
            RegionsList = regionsList;
        }
        public DEOHmcts(DataEntry de, List<SelectListItem> ocList, List<SelectListItem> clientsList, List<SelectListItem> regionsList) : base(de)
        {
            Country = "United Kingdom";
            OffenceCodesList = ocList.Where(x => x.Text.StartsWith("MK-") || x.Text.StartsWith("BRW-")).ToList();
            OffenceCodesList.Insert(0, new SelectListItem() { Text = "NWM", Value = "No Warning Marker" });
            ClientsList = clientsList;
            RegionsList = regionsList;
        }


        [Display(Name = "Data Entry Id")]
        public int? DEId { get; set; }

        [Required()]
        [Display(Name = "Region")]
        public int RegionId { get; set; }

        [Required()]
        [Display(Name = "Client Name")]
        public int ClientId { get; set; }

        [Required()]
        [Display(Name = "Issuing Court")]
        public string IssuingCourt { get; set; }

        [Required()]
        [Display(Name = "Date of Order")]
        public DateTime DateOfOrder { get; set; }

        [Required()]
        [Display(Name = "Date Received")]
        public DateTime DateReceived { get; set; }

        [Display(Name = "Bail Date")]
        [Required()]
        public DateTime? BailDate { get; set; }

        [Required()]
        [Display(Name = "Sitting Court")]
        public string SittingCourt { get; set; }

        [Required()]
        [Display(Name = "Case Number")]
        public string CaseNumber { get; set; }

        [Required()]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required()]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Display(Name = "Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [Required()]
        [Display(Name = "Address Line1")]
        [MaxLength(70, ErrorMessage = "Address Line 1 exceeds max length(70)")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address Line2")]
        [MaxLength(310, ErrorMessage = "Address Line 2 exceeds max length(310)")]
        public string AddressLine2 { get; set; }

        [Display(Name = "Address Line3")]
        [MaxLength(90, ErrorMessage = "Address Line 3 exceeds max length(90)")]
        public string AddressLine3 { get; set; }

        [Display(Name = "Address Line4")]
        [MaxLength(90, ErrorMessage = "Address Line 4 exceeds max length(90)")]
        public string AddressLine4 { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required()]
        [Display(Name = "Post Code")]
        [RegularExpression(@"^(\w{2}\d{1}\w{1}|\w{1}\d{1}\w{1}|\w{1,2}\d{1,2})\s*\d{1}\w{1,2}$", ErrorMessage = "Post Code is invalid")]
        [MaxLength(12, ErrorMessage = "Postcode exceeds max length(12)")]
        public string Postcode { get; set; }

        [Display(Name = "NI Number")]
        [RegularExpression(@"^[a-zA-Z]{2}[0-9]{6}[a-zA-Z]{1}$", ErrorMessage = "NI Number is not valid")]
        public string NINumber { get; set; }

        [Display(Name = "Phone No 1")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$", ErrorMessage = "Telephone 1 is not valid")]
        public string Telephone1 { get; set; }

        [Display(Name = "Phone No 2")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$", ErrorMessage = "Telephone 2 is not valid")]
        public string Telephone2 { get; set; }

        [Display(Name = "Phone No 3")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$", ErrorMessage = "Telephone 3 is not valid")]
        public string Telephone3 { get; set; }

        [Display(Name = "Phone No 4")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$", ErrorMessage = "Telephone 4 is not valid")]
        public string Telephone4 { get; set; }

        [Display(Name = "Phone No 5")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$", ErrorMessage = "Telephone 5 is not valid")]
        public string Telephone5 { get; set; }

        [Display(Name = "Known Alias")]
        [MaxLength(50, ErrorMessage = "Known Alias exceeds max length(50)")]
        public string KnownAlias { get; set; }

        [Display(Name = "Matter (800 chars max)")]
        public string Matter { get; set; }

        [Display(Name = "PNC ID")]
        [Required()]
        public string PNCId { get; set; }

        [Display(Name = "Date Of Check")]
        [Required()]
        public DateTime? DateOfCheck { get; set; }


        [Display(Name = "Offence Codes")]
        public string OffenceCodes { get; set; }

        [Display(Name = "Public")]
        public string Public { get; set; }
        [Display(Name = "EA")]
        public string EA { get; set; }
        [Display(Name = "Police")]
        public string Police { get; set; }
        [Display(Name = "Attachment Info")]
        public string AttachmentInfo { get; set; }
        [Display(Name = "User")]
        public int? UserId { get; set; }

        [JsonIgnore]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [JsonIgnore]
        [Display(Name = "Code Description")]
        public string CodeDescription { get; set; }

        [JsonIgnore]
        [Display(Name = "Offence Date")]
        public string OffenceDate { get; set; }


        private string clientName;

        [Display(Name = "Client Name")]
        public string ClientName {
            get
            {
                if (string.IsNullOrEmpty(clientName) && ClientsList != null)
                {
                    var cl = ClientsList.Where(x => x.Value == ClientId.ToString()).FirstOrDefault();
                    if (cl != null)
                    {
                        clientName = cl.Text;
                    }
                }
                return clientName;
            }

            set
            {
                clientName = value;
            }
        }

        [JsonIgnore]
        public bool IsDatedBreachCase
        {
            get
            {
                return ClientName.Contains("Dated");
            }
        }

        [JsonIgnore]
        public bool IsFinancialDatedBreachCase
        {
            get
            {
                return ClientName.Contains("Financial Dated Bail");
            }
        }

        public DEAttachment GetDEAttachment(EnumHMCTSDocCat docCat)
        {
            DEAttachment att = null;
            if (Parent.DEAttachments != null)
                att = Parent.DEAttachments.Where(x => x.OriginalFileName.StartsWith(((int)docCat).ToString() + "_")).FirstOrDefault();

            return att;
        }

        public override bool Validate(DataEntryRepository der)
        {
            bool valid = true;
            string error = string.Empty;

            if (der.HMCTSBreachCaseAI(FileTemplate.Id, ClientId, CaseNumber, DEId))
            {
                error = "<br/>The Case Number (" + CaseNumber + ") is either already exist in Columbus or currently appending on the queue for cases upload.\n ";
                valid = false;
            }

            if (!IsFinancialDatedBreachCase)
            {
                if (!DEId.HasValue)
                {
                    if (Attachments == null || Attachments.Count < 1)
                    {
                        error = "<br/>HMCTS case requires an attachment for Warrant.\n ";
                        valid = false;
                    }
                }

                foreach (var attachment in Attachments)
                {
                    if (attachment!= null && attachment.FileType != EnumContentType.pdf)
                    {
                        error = error + "<br/>HMCTS case only accept pdf file as attachment. i.e " + attachment.FileName + " is not valid\n ";
                        valid = false;
                    }
                }

                if (!valid)
                {
                    Errors.AppendLine(error);
                    if (FileTemplate.ParentDataFile != null)
                        FileTemplate.ParentDataFile.ValidationSummary.AppendLine(error);
                }
            }

            return valid;
        }

        private static List<string> issuingCourts;
        [JsonIgnore]
        public static List<string> IssuingCourts {
            get
            {
                if (issuingCourts == null)
                {
                    string configStr = ConfigurationManager.AppSettings["HMCTS_IssuingCourts"];
                    issuingCourts = configStr.Split("|".ToCharArray()).ToList();
                    issuingCourts.Sort();
                }
                return issuingCourts;
            }

        }

        [JsonIgnore]
        public static IEnumerable<SelectListItem> IssuingCourtsDDL
        {
            get
            {
                var icList = IssuingCourts.Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                icList[0].Selected = true;
                return icList;
            }
        }

        [JsonIgnore]
        public static IEnumerable<SelectListItem> SittingCourtsDDL
        {
            get
            {
                var icList = SittingCourts.Select(x => new SelectListItem() { Text = x, Value = x }).ToList();
                icList[0].Selected = true;
                return icList;
            }

        }

        private static List<string> sittingCourts;
        [JsonIgnore]
        public static List<string> SittingCourts {
            get
            {
                if (sittingCourts == null)
                {
                    string configStr = ConfigurationManager.AppSettings["HMCTS_SittingCourts"];
                    sittingCourts = configStr.Split("|".ToCharArray()).ToList();
                    sittingCourts.Sort();
                }
                return sittingCourts;
            }
        }

        private static List<string> riskLevels;
        [JsonIgnore]
        public static List<string> RiskLevels
        {
            get
            {
                if (riskLevels == null)
                {
                    riskLevels = new List<string>() { "Low", "Medium", "High" };
                }
                return riskLevels;
            }
        }

        [JsonIgnore]
        public List<SelectListItem> OffenceCodesList { get; set; }

        [JsonIgnore]
        public List<SelectListItem> ClientsList { get; set; }

        [JsonIgnore]
        public List<SelectListItem> RegionsList { get; set; }

        [JsonIgnore]
        [Display(Name = "Case Attachments")]
        public List<DEOAttachment> Attachments { get => attachments; set => attachments = value; }
        private List<DEOAttachment> attachments = new List<DEOAttachment>();

        [JsonIgnore]
        public List<WarningMarker> WarningMakerList {
            get
            {
                List<WarningMarker> wms = new List<WarningMarker>();
                if (!string.IsNullOrEmpty(OffenceCodes))
                {
                    var delStrs = OffenceCodes.Split("|".ToCharArray());
                    foreach (string item in delStrs)
                    {
                        wms.Add(new WarningMarker(item));
                    }
                }
                return wms;

            }

        }
    }

    public class WarningMarker
    {
        public WarningMarker(string delimtedStr)
        {
            var fields = delimtedStr.Split(";".ToCharArray());

            Code = fields[0];
            OffenceDate = fields[1];
            Description = fields[0];
        }
        public string Code { get; set; }
        public string OffenceDate { get; set; }
        public string Description { get; set; }
    }




}
