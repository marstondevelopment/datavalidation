﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System.Linq;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class DbConnectionController : ControllerBase
    {
        protected FileTemplateRepository _ftr;
        protected ClientRepository _cr;
        public DbConnectionController(FileTemplateRepository ftr, ClientRepository cr)
        {
            _ftr = ftr;
            _cr = cr;
        }

        public ActionResult Index()
        {
            DBConnectionViewModel vm = new DBConnectionViewModel();
            vm.Connections = _ftr.GetDbConnections().OrderBy(x => x.Name);
            return View(vm);
        }

        public ActionResult Details(int id)
        {
            DBConnectionViewModel vm = new DBConnectionViewModel();
            vm.DBConnection = _ftr.GetDbConnections().Where(x => x.Id == id).FirstOrDefault();
            return View(vm);
        }

        public ActionResult Create()
        {
            DBConnectionViewModel vm = new DBConnectionViewModel();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(FormCollection fc)
        {
            DBConnectionViewModel vm = new DBConnectionViewModel();
            TryUpdateModel(vm);
            if (ModelState.IsValid)
            {
                _ftr.CreateConnection(vm.DBConnection);
                return RedirectToAction("Index").Success("DB Connection has been created successfully.");
            }
            return RedirectToAction("Create").Success("Failed to create DB Connection.");
        }

        public ActionResult Edit(int id)
        {
            DBConnectionViewModel vm = new DBConnectionViewModel();
            vm.DBConnection = _ftr.GetDbConnections().Where(x => x.Id == id).FirstOrDefault();
            return View(vm);
        }


        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            DBConnectionViewModel vm = new DBConnectionViewModel();
            vm.DBConnection = _ftr.GetDbConnections().Where(x => x.Id == id).FirstOrDefault();
            TryUpdateModel(vm);
            if (ModelState.IsValid)
            {
                _ftr.UpdateConnection(vm.DBConnection);
                return RedirectToAction("Index").Success(vm.DBConnection.Name + " has been updated successfully.");
            }
            return View(vm).Danger(vm.DBConnection.Name + " failed to update.");
        }

        public ActionResult Delete(int id)
        {
            DBConnectionViewModel vm = new DBConnectionViewModel();

            if (ModelState.IsValid)
            {
                vm.DBConnection = _ftr.GetDbConnections().Where(x => x.Id == id).FirstOrDefault();
                if (vm.DBConnection.FileTemplates.Count > 0 || vm.DBConnection.StoredProcedures.Count > 0)
                {
                    return RedirectToAction("Index").Danger(vm.DBConnection.Name + " connection cannot be deleted due to the reference of other entities.");
                }
                _ftr.DeleteConnection(vm.DBConnection);
                return RedirectToAction("Index").Success(vm.DBConnection.Name + " has been deleted successfully.");

            }
            return RedirectToAction("Index").Danger(vm.DBConnection.Name + " has failed to delete.");
        }
    }
}
