﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{

    //  ***************************** Important! Do not re-order defined items in each of the enums below.  ********************************************************
    //  ***************************** The value of each item is mapped to a field value in database.        ********************************************************  
    //  ***************************** New item should be appended to the bottom of the items list.          ********************************************************


    public enum EnumFieldType
    {
        [Display(Name = "Integer")]
        Integer = 1,
        [Display(Name = "Decimal")]
        Decimal,
        [Display(Name = "Float")]
        Float,
        [Display(Name = "Boolean")]
        Boolean,
        [Display(Name = "String")]
        String,
        [Display(Name = "Date")]
        Date
    }

    public enum EnumValidationRule
    {
        [Display(Name = "Maximum Field Length")]
        MaxFieldLength = 1,
        [Display(Name = "Minimum Field Length")]
        MinFieldLength,
        [Display(Name = "Mandatory Field")]
        MandatoryField,
        [Display(Name = "Field Type")]
        FieldType,
        [Display(Name = "Stored Procedure Validataion")]
        SP_Validation,
        [Display(Name = "VRM Vehicle Registration")]
        VRM_VehicleReg,
        [Display(Name = "Regular Expression")]
        RE_Validation,
        [Display(Name = "Reference List")]
        RL_Validation
    }

    public enum EnumDCAction
    {
        [Display(Name = "Remove Space")]
        RemoveSpace = 1,
        [Display(Name = "Remove Comma")]
        RemoveComma,
        [Display(Name = "Replace Text")]
        ReplaceText,
        [Display(Name = "Pad Left")]
        PadLeft,
        [Display(Name = "Pad Right")]
        PadRight,
        [Display(Name = "String To Date Format")]
        StringToDate,
        [Display(Name = "Trim Both End")]
        Trim,
        [Display(Name = "To Uppercase")]
        ToUppercase,
        [Display(Name = "To Lowercase")]
        ToLowercase,
        [Display(Name = "Format Valid Phone NO")]
        FormatValidPhoneNO,
        [Display(Name = "Regex Remove Texts")]
        RegexRemoveTexts,
        [Display(Name = "Remove Invalid Phone NO")]
        RemoveInvalidPhoneNO,
        [Display(Name = "Trim Non-Alphabetic Chars")]
        DCTrimNonAlphabetChars
    }

    public enum EnumMimeType
    {
        [Display(Name = "xls extension")]
        xls = 1,
        [Display(Name = "xlsx extenstion")]
        xlsx,
        [Display(Name = "csv extension")]
        csv,
        [Display(Name = "txt extension")]
        txt,
        [Display(Name = "xml extension")]
        xml,
        [Display(Name = "asc extension")]
        asc,
        [Display(Name = "xls(Excel 5) extension")]
        xls5,
        [Display(Name = "xlsx 2007+")]
        xlsx2007,
        [Display(Name = "dat extension")]
        dat,
        [Display(Name = "data entry")]
        de
    }

    public enum EnumContentType
    {
        [Display(Name = "Excel File")]
        xls = 1,
        [Display(Name = "Excel Spreadsheet")]
        xlsx,
        [Display(Name = "csv file")]
        csv,
        [Display(Name = "text file")]
        txt,
        [Display(Name = "MS document")]
        doc,
        [Display(Name = "docx file")]
        docx,
        [Display(Name = "pdf file")]
        pdf,
        [Display(Name = "gif file")]
        gif,
        [Display(Name = "jpg file")]
        jpg,
        [Display(Name = "jpeg file")]
        jpeg,
        [Display(Name = "zip file")]
        zip
    }

    public enum EnumDomain
    {
        MGL = 1,
        ITSERVICES
    }

    public enum EnumSPType
    {
        [Display(Name = "VR Stored Procedure")]
        VRSP = 1,
        [Display(Name = "DT Stored Procedure")]
        DTSP
    }

    public enum EnumValidationMethod
    {
        [Display(Name = "Stored Procedure")]
        SP = 1,
        [Display(Name = "Command Text")]
        CmdTxt
    }
    public enum EnumDBServer
    {
        [Display(Name = "SQL Server")]
        SqlServer = 1,
        [Display(Name = "Oracle")]
        Oracle,
        [Display(Name = "MySQL")]
        MySQL
    }

    public enum EnumParserResult
    {
        UploadSucceed,
        ValiationSucceed,
        ConfigNotSet,
        UploadFailed,
        ValidationFailed,
        DTActionsSucceed,
        DTACtionsFailed,
        PreGenActionsSucceed,
        PreGenActionsFailed,
        FileGenerationSucceed,
        FileGenerationFailed,
        NoData,
        UploadToRepositorySucceed,
        UploadToRepositoryFailed,
        RepositoryNotFound,
        SubfolderNotFound
    }

    public enum EnumDataFileScreen
    {
        [Display(Name = "Data Validation")]
        Validate = 1,
        [Display(Name = "Bulk Upload")]
        Upload,
        [Display(Name = "Data Transformation")]
        Transform,
        [Display(Name = "Data Pre-processing")]
        GenerateFiles,
        [Display(Name = "File Repository")]
        UploadToRepository,
        [Display(Name = "DE Upload")]
        DEUpload,
        [Display(Name = "Data Entry")]
        DataEntry
    }

    public enum EnumReferenceData
    {
        [Display(Name = "DT Actions")]
        DTActions = 1,
        [Display(Name = "Data Fields")]
        DataFields,
        [Display(Name = "AR Actions")]
        ARActions
    }

    public enum EnumFileWriter
    {
        [Display(Name = "Default File Writer")]
        Default = 1,
        [Display(Name = "OneStep File Writer")]
        OneStep
    }

    public enum EnumFileSection
    {
        [Display(Name = "Body")]
        Body = 1,
        [Display(Name = "Header")]
        Header,
        [Display(Name = "Footer")]
        Footer,
        [Display(Name = "Variable")]
        Variable
    }


    public enum EnumDTActionType
    {
        [Display(Name = "OneStep Exclude Cases Already Imported")]
        OSExcludeCasesAlreadyImported = 1,
        [Display(Name = "OneStep Data Group By")]
        OSGroupBy,
        [Display(Name = "OneStep Merge Debts")]
        OSMergeDebts,
        [Display(Name = "OneStep Exclude Cases Less Than Threadhold Value")]
        OSExcludeCasesLessThanThreadholdVal,
        [Display(Name = "OneStep Extract Comments")]
        OSExtractComments,
        [Display(Name = "OneStep Split Output File Based On Filters")]
        OSSplitOutputFileBasedOnFields,
        [Display(Name = "OneStep Split Input Column Into 2 (FW)")]
        OSSplitColumn,
        [Display(Name = "OneStep Concatenate Fields")]
        OSConcatenateFields,
        [Display(Name = "OneStep Add Process Info")]
        OSAddProcessInfo,
        [Display(Name = "OneStep Get Sum Of Columns")]
        OSGetSumOfColumns,
        [Display(Name = "OneStep Extract Linked Cases After Group By")]
        OSExtractLinkedCasesAfterGroupBy,
        [Display(Name = "OneStep Split Name Column Into 3 (FW)")]
        OSSplitNameColumnIntoThree,
        [Display(Name = "Get Data From DB")]
        GetDataFromDB,
        [Display(Name = "Get Sum Of A Column")]
        GetTotalOfAColumn,
        [Display(Name = "Split Output File After Grouping")]
        SplitOutputAfterGrouping,
        [Display(Name = "Cross Mapping Data From DB")]
        XMappingDataFromDB,
        [Display(Name = "Split One Column Into Multiple (FW)")]
        SplitOneColIntoMultipleCols,
        [Display(Name = "Exclude Selected Rows")]
        ExcludeSelectedRows,
        [Display(Name = "OneStep Transform Defaulter Addresses")]
        OSTransformDefaulterAddresses,
        [Display(Name = "Set Duplicated Rows With Unique Values")]
        SetDuplicatedRowsWithUniqueValues,
        [Display(Name = "OneStep New Case Transform Defaulter Addresses")]
        OSNCTransformDefaulterAddresses,
        [Display(Name = "Truncate Fields Exceeding Max Length")]
        TruncateFieldsExceedingMaxLength,
        [Display(Name = "Split Full Address field Into Seperate fields")]
        SplitFullAddressColIntoSepLines,
        [Display(Name = "Compute Aggregate Function")]
        ComputeAggregateFunction,
        [Display(Name = "OneStep Assign Default Postcode")]
        OSAssignDefaultPostcode,
        [Display(Name = "Split And Reshape Output Files")]
        SplitAndReshapeOutputFiles,
        [Display(Name = "Exclude Rows Not Matching Reference List")]
        ExcludeRowsNotMatchReferenceList,
        [Display(Name = "Concatenate Multi Nodes DFs")]
        ConcatenateMultiNodesDFs,
        [Display(Name = "LSC Split And Transpose Output Data")]
        SplitAndTransposeOutputFile,
        [Display(Name = "Concatenate XML List Item")]
        ConcatenateXMLListItemsDFs
    }

    public enum EnumDTInfoType
    {
        [Display(Name = "Data File Name")]
        DataFileName = 1,
        [Display(Name = "Current Short Date")]
        CurrentShortDate,
        [Display(Name = "Current Date Time")]
        CurrentDateTime,
        [Display(Name = "Row Count")]
        RowCount,
        [Display(Name = "No. Rows In Input FIle")]
        TotalNoRowsForInput,
         [Display(Name = "Current User Id")]
        CurrentUserId
    }
    public enum EnumOutputFileCategory
    {
        [Display(Name = "Output File")]
        Output = 1,
        [Display(Name = "Ignored File")]
        Ignored,
        [Display(Name = "File Generation Report")]
        Report,
        [Display(Name = "Exception File")]
        Exception
    }

    public enum EnumDTActionGroup
    {
        /// <summary>
        /// Data Transformation
        /// </summary>
        DT = 1,
        /// <summary>
        /// File Writer
        /// </summary>
        FW
    }


    public enum EnumARActionType
    {
        [Display(Name = "Client Ref, Balance")]
        DisplayMnCrBalance = 1,
        [Display(Name = "Get User Name")]
        GetUserName,
        [Display(Name = "Get Input File Name")]
        GetInputFileName,
        [Display(Name = "Get Date Time")]
        GetDateTime,
        [Display(Name = "Get Input File Row Count")]
        GetInputFileRowCount,
        [Display(Name = "Get Total Row Count (All Output Files)")]
        GetOutputFileRowCount,
        [Display(Name = "Get Total Balance (Input/All Output Files)")]
        GetOutputFileTotalBalance,
        [Display(Name = "Get All/Per Ignored File Row Count")]
        GetIgnoredFileRowCount,
        [Display(Name = "Get Client Scheme (Each Output/Ignored File)")]
        GetClientScheme,
        [Display(Name = "Get Number Of Cases (Each Output File)")]
        GetNoOfNewCases,
        [Display(Name = "Get Total Balance (Each Output File)")]
        GetTotalBalanceOfNewCases,
        [Display(Name = "Get Data From File (Each Output/Ignored File)")]
        GetDataFromGeneratedFile
    }

    public enum EnumARMimeType
    {
        [Display(Name = "csv extension")]
        csv = 1,
        [Display(Name = "txt extension")]
        txt,
        [Display(Name = "xml extension")]
        xml
    }

    public enum EnumARSourceFile
    {
        [Display(Name = "Input Data File")]
        InputDataFile = 1,
        [Display(Name = "Per Output Data File")]
        PerOutputDataFile,
        [Display(Name = "All Output Data Files")]
        AllOutputDataFiles,
        [Display(Name = "Per Ignored File")]
        PerIgnoredFile,
        [Display(Name = "All Ignored Files")]
        AllIgnoredFiles
    }

    public enum EnumDTSourceFile
    {
        [Display(Name = "Input Data File")]
        InputData = 1,
        [Display(Name = "Output Data Files")]
        OutputData,
    }

    public enum EnumARReservedTags
    {
        PEROUTPUTFILE = 1,
        PERIGNOREDFILE,
        PERROW
    }

    public enum EnumComparisonOperator
    {
        [Display(Name = ">")]
        Greater = 1,
        [Display(Name = ">=")]
        GreaterOrEqual,
        [Display(Name = "=")]
        Equal,
        [Display(Name = "<")]
        LessThan,
        [Display(Name = "<=")]
        LessThanOrEqual

    }

    public enum EnumNameTitle
    {
        [Display(Name = "Mr.")]
        MR = 1,
        [Display(Name = "Master")]
        MASTER,
        [Display(Name = "Mrs.")]
        MRS,
        [Display(Name = "Ms.")]
        MS,
        [Display(Name = "Miss")]
        MISS,
        [Display(Name = "Nonbinary")]
        MX,
        [Display(Name = "Sir")]
        SIR,
        [Display(Name = "Company")]
        CO,
        [Display(Name = "Doctor")]
        DR
    }

    public enum EnumOutputFileEncoding
    {
        [Display(Name = "UTF8")]
        UTF8 = 1,
        [Display(Name = "Unicode")]
        Unicode,
        [Display(Name = "UTF32")]
        UTF32,
        [Display(Name = "UTF7")]
        UTF7,
        [Display(Name = "ASSCII")]
        ASSCII,
    }

    public enum EnumRole
    {
        [Display(Name = "Read Only")]
        ReadOnly = 1,
        [Display(Name = "Read & Write")]
        ReadWrite,
        [Display(Name = "Administrator")]
        Admin
    }

    public enum EnumUniqueValueType
    {
        [Display(Name = "Counter Number")]
        CounterNumber = 1,
        [Display(Name = "Concatenate Columns")]
        ConcatenateColumns,
        [Display(Name = "Random Text")]
        RandomText,
        [Display(Name = "Date Time")]
        DateTime,
    }

    public enum EnumDataEntryForm
    {
        [Display(Name = "HMCTS")]
        HMCTS = 1
    }
    public enum EnumAttachmentFileType
    {
        [Display(Name = "pdf")]
        PDF = 1
    }
    public enum EnumXmlListElement
    {
        //[Display(Name = "Select a XML list element type...")]
        //NotSelected = 0,
        [Display(Name = "XML List")]
        XMLList = 1,
        [Display(Name = "XML List Item")]
        XMLListItem,
        [Display(Name = "XML Group Node")]
        XMLGroupNode,
        [Display(Name = "Leaf Node")]
        LeafNode
    }
}