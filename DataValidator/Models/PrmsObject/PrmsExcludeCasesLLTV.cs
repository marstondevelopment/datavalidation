﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{    
    public class PrmsExcludeCasesLLTV : PrmsBase
    {
        public PrmsExcludeCasesLLTV() { }
        public PrmsExcludeCasesLLTV(DTAction dt) : base(dt) { }

        [Display(Name = "Min Threadhold")]
        public decimal MinThreadhold { get; set; }
        [Display(Name = "Field Name")]
        public string FieldName { get; set; }
        [Display(Name = "Output File Prefix")]
        public string OFPrefix { get; set; }

        [Display(Name = "Exception File Prefix")]
        public string EFPrefix { get; set; }
        [Display(Name = "Exception Message")]
        public string ExMessage { get; set; }
        [Display(Name = "Case Ref. Col Name")]
        public string CaseRefColName { get; set; }
        [Display(Name = "DB Value Column Name")]
        public string DBValueColumnName { get; set; }


    }
}