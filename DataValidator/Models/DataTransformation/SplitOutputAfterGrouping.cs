﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class SplitOutputAfterGrouping : DTStep
    {
        private PrmsSplitOutputAfterGrouping prms;
        private List<string> gbFieldNames;
        public SplitOutputAfterGrouping(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitOutputAfterGrouping;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsSplitOutputAfterGrouping  has not been set for DTAction - " + ParentFTDT.DTAction.ActionName);
            }
            gbFieldNames = prms.GroupingFields.Split(",".ToCharArray()).ToList();
            if (gbFieldNames.Count <=0 || string.IsNullOrEmpty(prms.FNPrefixField))
            {
                throw new Exception("The parameters not set up correctly  for DTAction - " + ParentFTDT.DTAction.ActionName);
            }

            gbFieldNames.ForEach(x => x = x.Trim());
            prms.FNPrefixField = prms.FNPrefixField.Trim();
        }


        public override bool Execute()
        {
            var stagingDT = ParentFTDT.FileTemplate.ProcessedDataTable.Copy();
            var columnsToGroupBy = stagingDT.Columns.Cast<DataColumn>()
                .Where(c => gbFieldNames.Contains(c.ColumnName, StringComparer.InvariantCultureIgnoreCase))
                .ToArray();
            var comparer = new MultiFieldComparer();
            var groupedData = stagingDT.AsEnumerable()
                .GroupBy(row => columnsToGroupBy.Select(c => row[c]), comparer);

            if (groupedData != null && groupedData.Count() > 0)
            {
                foreach (var group in groupedData)
                {
                    string fnPrefix = group.FirstOrDefault()[prms.FNPrefixField].ToString();
                    if (!string.IsNullOrEmpty(prms.FNPrefixFollowedBy))
                    {
                        fnPrefix = fnPrefix + "_" + prms.FNPrefixFollowedBy;
                    }

                    OutputFile outputFile = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile( fnPrefix  , EnumOutputFileCategory.Output);

                    foreach (var rw in group)
                    {
                        outputFile.Data.ImportRow(rw);
                        stagingDT.Rows.Remove(rw);
                    }

                }
                stagingDT.AcceptChanges();
                ParentFTDT.FileTemplate.ProcessedDataTable = stagingDT;
            }

            return true;
        }
    }
}