﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class SetDuplicatedRowsWithUniqueValues : DTStep
    {
        private List<string> colNames;
        private PrmsSetDuplicatedRowsWithUniqueValues prms;
        public SetDuplicatedRowsWithUniqueValues(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSetDuplicatedRowsWithUniqueValues;
            if (prms == null || string.IsNullOrEmpty(prms.GroupByColumnNames) || string.IsNullOrEmpty(prms.FieldRequiresUniqueValue))
            {
                throw new MemberAccessException("Parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            colNames = prms.GroupByColumnNames.Split(",".ToCharArray()).ToList();

        }
        public override bool Execute()
        {
            string[] preselColNames = null;
            List<DataColumn> dcs = new List<DataColumn>();
            string separator = (string.IsNullOrEmpty(prms.Separator) ? string.Empty : prms.Separator);
            int count = 0;

            if (ParentFTDT.FileTemplate.ProcessedDataTable.Rows.Count > 0)
            {

                if (!string.IsNullOrEmpty(Utility.ConcatColNames))
                {
                    preselColNames = Utility.ConcatColNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray());

                    foreach (var name in preselColNames)
                    {
                        if (ParentFTDT.FileTemplate.Columns.Where(x => x.OutputColumnName == name).FirstOrDefault() != null)
                            dcs.Add(ParentFTDT.FileTemplate.ProcessedDataTable.Columns[ParentFTDT.FileTemplate.Columns.Where(x => x.OutputColumnName == name).FirstOrDefault().Name]);
                        else if (ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(name))
                            dcs.Add(ParentFTDT.FileTemplate.ProcessedDataTable.Columns[name]);
                    }
                    if ((prms.UniqueValueType == 0 || (EnumUniqueValueType)prms.UniqueValueType == EnumUniqueValueType.ConcatenateColumns) && dcs.Count <= 0)
                    {
                        throw new MemberAccessException("Column names not set for columns concatenation on " + ParentFTDT.DTAction.ActionName + ", please update the configuration settings and try again.");
                    }
                }

                List<DataRow> singleCases = new List<DataRow>();
                // Generate unique CCR for single cases which are already imported to database
                if ( !string.IsNullOrEmpty(prms.Filter))
                {
                    singleCases.AddRange(ParentFTDT.FileTemplate.ProcessedDataTable.Select(prms.Filter).ToList());
                    if (singleCases.Count > 0)
                    {
                        foreach (DataRow row in singleCases)
                        {
                            string prefix = prms.ReplaceOriginalVal ? string.Empty : row[prms.FieldRequiresUniqueValue].ObjToString(ParentFTDT.FileTemplate.ProcessedDataTable.Columns[prms.FieldRequiresUniqueValue]) + separator;
                            string ending = GenerateUniqueValue(dcs, row, separator,++count);
                            row[prms.FieldRequiresUniqueValue] = prefix + ending;
                        }
                    }
                }

                var columnsToGroupBy = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
                    .Where(c => colNames.Contains(c.ColumnName, StringComparer.InvariantCultureIgnoreCase))
                    .ToArray();
                var comparer = new MultiFieldComparer();
                var groupedDT = ParentFTDT.FileTemplate.ProcessedDataTable.AsEnumerable()
                    .GroupBy(row => columnsToGroupBy.Select(c => row[c]), comparer);
                foreach (var grp in groupedDT)
                {
                    if (grp.Count() > 1)
                    {
                        var duplicatedRows = string.IsNullOrEmpty(prms.OrderByColumnName) ? grp.ToList() : grp.OrderBy(x => x[prms.OrderByColumnName]).ToList();
                        string newVal = string.Empty;
                        count = 0;
                        foreach (var row in duplicatedRows)
                        {
                            if (!singleCases.Contains(row))
                            {
                                string prefix = prms.ReplaceOriginalVal ? string.Empty : row[prms.FieldRequiresUniqueValue].ObjToString(ParentFTDT.FileTemplate.ProcessedDataTable.Columns[prms.FieldRequiresUniqueValue]) + separator;
                                string ending = GenerateUniqueValue(dcs, row, separator, ++count);
                                newVal = prefix + ending;
                                row[prms.FieldRequiresUniqueValue] = newVal;
                            }

                        }
                    }
                }

                ParentFTDT.FileTemplate.ProcessedDataTable.AcceptChanges();
            }

            return true;
        }

        private string GenerateUniqueValue(List<DataColumn> dcs, DataRow row, string separator,int count)
        {
            string ending = string.Empty;
            switch ((EnumUniqueValueType)prms.UniqueValueType)
            {
                case EnumUniqueValueType.DateTime:
                    ending = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
                    System.Threading.Thread.Sleep(1);
                    break;
                case EnumUniqueValueType.RandomText:
                    ending = Guid.NewGuid().ToString("n").Substring(0, 8).ToUpper();
                    break;
                case EnumUniqueValueType.CounterNumber:
                    //ending = count.ToString("00000000");
                    ending = count.ToString();
                    break;
                default:
                    // Concatenate selected fields for duplicated cases
                    ending = ConcatenateFields(dcs, row, separator);
                    break;

            }

            return ending;
        }


        private string ConcatenateFields(List<DataColumn> selectedCols, DataRow row, string separator)
        {
            string uniqueCCR = string.Empty;
            foreach (var col in selectedCols)
            {
                if (!string.IsNullOrEmpty(row[col.ColumnName].ToString()))
                {
                    switch (col.DataType.Name.ToString())
                    {
                        case ("DateTime"):
                            uniqueCCR = uniqueCCR + separator + ((DateTime)row[col.ColumnName]).ToString("ddMMyy");
                            break;
                        case ("Decimal"):
                            uniqueCCR = uniqueCCR + separator + ((Decimal)row[col.ColumnName]).ToString("0.00").Replace(".", separator);
                            break;
                        default:
                            uniqueCCR = uniqueCCR + separator + row[col.ColumnName].ToString().Replace(" ", string.Empty);
                            break;
                    }
                }
            }

            uniqueCCR = uniqueCCR.Trim(separator.ToCharArray());
            return uniqueCCR;
        }
    }

}