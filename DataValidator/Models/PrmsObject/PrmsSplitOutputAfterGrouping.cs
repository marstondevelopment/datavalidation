﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitOutputAfterGrouping : PrmsBase
    {
        public PrmsSplitOutputAfterGrouping() { }
        public PrmsSplitOutputAfterGrouping(DTAction dt) : base(dt) { }

        [Display(Name = "Grouping Fields")]
        public string GroupingFields { get; set; }
        [Display(Name = "FN Prefix Field")]
        public string FNPrefixField { get; set; }
        [Display(Name = "Prefix Followed By")]
        public string FNPrefixFollowedBy { get; set; }
    }
}