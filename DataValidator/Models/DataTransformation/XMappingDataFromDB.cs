﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class XMappingDataFromDB : DTStep
    {
        private PrmsXMappingDataFromDB prms;
        private List<string> fieldsToBePopulated;
        private string[] mappingCols;

        public XMappingDataFromDB(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsXMappingDataFromDB;
            if (prms == null || (prms.GetMethod==EnumValidationMethod.SP && prms.StoredProcedure == null) || (prms.GetMethod== EnumValidationMethod.CmdTxt && prms.CmdText == null))
            {
                throw new MemberAccessException("PrmsXMappingDataFromDB parameters are missing/configured incorrectly for " + ftdt.DTAction.ActionName + ", please update the configuration and try again.");
            }

            fieldsToBePopulated = prms.FieldsToPopulate.Split(",".ToCharArray()).Select(x=> x.Trim()).ToList();
            mappingCols = prms.MappingColumn.Split(Utility.DEFAULT_DELIMITER.ToCharArray());
        }

        private string FormatSelectFilter(DataRow rw, DataTable xrefDT)
        {
            string filter = string.Empty;

            foreach (var colName in mappingCols)
            {
                string searchVal = rw[colName].ObjToString(ParentFTDT.FileTemplate.ProcessedDataTable.Columns[colName]);

                if (!string.IsNullOrEmpty(searchVal))
                {
                    DataField dfSource = ParentFTDT.FileTemplate.BodyCols.Where(x => x.Name == colName ).FirstOrDefault();
                    switch ((EnumFieldType)dfSource.FieldType)
                    {
                        case EnumFieldType.Integer:
                        case EnumFieldType.Decimal:
                        case EnumFieldType.Float:
                        case EnumFieldType.Boolean:
                            break;
                        case EnumFieldType.String:
                        case EnumFieldType.Date:
                            searchVal = "'" + searchVal.Replace("'","''") + "'";
                            break;
                        default:
                            break;
                    }

                    DataField df = ParentFTDT.FileTemplate.Columns.Where(x => x.Name.Trim() == colName.Trim() || (x.OutputColumnName != null && x.OutputColumnName.Trim() == colName.Trim())).FirstOrDefault();
                    DataColumn dc = xrefDT.Columns[df.Name];
                    if (dc == null)
                    {
                        dc = xrefDT.Columns[df.OutputColumnName];
                    }

                    filter = filter + "[" + dc.ColumnName + "] = " + searchVal + " AND ";

                }

            }

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.Substring(0, filter.Length - 5);
            }

            return filter;
        }

        public override bool Execute()
        {
            List<DataRow> exRows = new List<DataRow>();

            DataTable dt = null;

            switch (prms.GetMethod)
            {
                case EnumValidationMethod.SP:

                    dt = prms.StoredProcedure.GetData();
                    break;
                case EnumValidationMethod.CmdTxt:
                    dt = prms.CmdText.GetData();
                    break;
                default:
                    break;
            }

            if (dt == null || dt.Rows.Count == 0)
            {
                throw new Exception("Cross mapping table is empty or not found from database");
            }

            foreach (DataRow  rw in ParentFTDT.FileTemplate.ProcessedDataTable.Rows)
            {

                string filter = FormatSelectFilter(rw, dt);
                if (!string.IsNullOrEmpty(filter))
                {
                    DataRow dr = dt.Select(filter).FirstOrDefault();
                    if (dr != null)
                    {
                        foreach (var fieldName in fieldsToBePopulated)
                        {
                            rw[fieldName] = dr[fieldName];
                        }
                    }
                    else
                    {
                        exRows.Add(rw);
                    }
                }
            }

            if (prms.ExcludeNotFound && exRows.Count > 0)
            {
                for (int i = 0; i < exRows.Count; i++)
                {
                    ParentFTDT.FileTemplate.ExcludedDataTable.ImportRow(exRows[i]);
                    ParentFTDT.FileTemplate.ProcessedDataTable.Rows.Remove(exRows[i]);
                }
            }

            return true;
        }



    }
}