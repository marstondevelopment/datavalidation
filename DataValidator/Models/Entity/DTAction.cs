﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class DTAction
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const int COOKIE_MAX_LENGTH = 1600;
        public EnumDTActionType DTActionType
        {
            get
            {
                return (EnumDTActionType)DTActionTypeId;
            }
        }

        public int ExecutionOrder
        {
            get
            {
                return DTActionManager.DTActionsExecutionOrder[(EnumDTActionType)DTActionTypeId];
            }
        }

        public EnumDTActionGroup ActionGroup
        {
            get
            {
                return DTActionManager.DTActionsGroup[(EnumDTActionType)DTActionTypeId];
            }
        }

        public EnumDTActionType DTActionTypeName
        {
            get
            {
                return (EnumDTActionType)DTActionTypeId;
            }
        }

        public DTAction Clone
        {
            get
            {
                User user = (User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                DTAction dta = new DTAction()
                {
                    ActionName = ActionName,
                    DTActionTypeId = DTActionTypeId,
                    PrmsObject = PrmsObject,
                    CreatedBy = user.Id,
                    DateCreated = DateTime.Now,
                    UpdatedBy = user.Id,
                    DateUpdated = DateTime.Now
                };
                return dta;
            }
        }

        public string SerializePrmsObject()
        {
            string prms = string.Empty;
            switch (DTActionType)
            {
                case EnumDTActionType.OSExcludeCasesAlreadyImported:
                    if (PrmsExcludeCasesAI != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsExcludeCasesAI);
                    }
                    break;
                case EnumDTActionType.OSGroupBy:
                    if (PrmsGroupBy != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsGroupBy);
                    }
                    break;
                case EnumDTActionType.OSMergeDebts:
                    if (PrmsMergeDebts != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsMergeDebts);
                    }
                    break;
                case EnumDTActionType.OSExcludeCasesLessThanThreadholdVal:
                    if (PrmsExcludeCasesLLTV != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsExcludeCasesLLTV);
                    }
                    break;
                case EnumDTActionType.OSExtractComments:
                    if (PrmsExtractComments != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsExtractComments);
                    }
                    break;
                case EnumDTActionType.OSSplitOutputFileBasedOnFields:
                    if (PrmsSplitOutputFile != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitOutputFile);
                    }
                    break;
                case EnumDTActionType.ExcludeSelectedRows:
                    if (PrmsExcludeSelectedRows != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsExcludeSelectedRows);
                    }
                    break;
                case EnumDTActionType.OSSplitColumn:
                    if (PrmsSplitColumn != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitColumn);
                    }
                    break;
                case EnumDTActionType.OSConcatenateFields:
                    if (PrmsConcatenateFields != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsConcatenateFields);
                    }
                    break;
                case EnumDTActionType.OSAddProcessInfo:
                    if (PrmsAddProcessInfo != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsAddProcessInfo);
                    }
                    break;
                case EnumDTActionType.OSGetSumOfColumns:
                    if (PrmsGetSumOfColumns != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsGetSumOfColumns);
                    }
                    break;
                case EnumDTActionType.OSExtractLinkedCasesAfterGroupBy:
                    if (PrmsExtractLinkedCasesAfterGroupBy != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsExtractLinkedCasesAfterGroupBy);
                    }
                    break;
                case EnumDTActionType.OSSplitNameColumnIntoThree:
                    if (PrmsSplitNameColumnIntoThree != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitNameColumnIntoThree);
                    }
                    break;
                case EnumDTActionType.SplitOneColIntoMultipleCols:
                    if (PrmsSplitOneColIntoMultipleCols != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitOneColIntoMultipleCols);
                    }
                    break;
                case EnumDTActionType.GetDataFromDB:
                    if (PrmsGetDataFromDB != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsGetDataFromDB);
                    }
                    break;
                case EnumDTActionType.GetTotalOfAColumn:
                    if (PrmsGetTotalOfAColumn != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsGetTotalOfAColumn);
                    }
                    break;
                case EnumDTActionType.SplitOutputAfterGrouping:
                    if (PrmsSplitOutputAfterGrouping != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitOutputAfterGrouping);
                    }
                    break;
                case EnumDTActionType.XMappingDataFromDB:
                    if (PrmsXMappingDataFromDB != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsXMappingDataFromDB);
                    }
                    break;
                case EnumDTActionType.SetDuplicatedRowsWithUniqueValues:
                    if (PrmsSetDuplicatedRowsWithUniqueValues != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSetDuplicatedRowsWithUniqueValues);
                    }
                    break;
                case EnumDTActionType.OSNCTransformDefaulterAddresses:
                    if (PrmsNCTransformDefaultersAddresses != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsNCTransformDefaultersAddresses);
                    }
                    break;
                case EnumDTActionType.TruncateFieldsExceedingMaxLength:
                    if (PrmsTruncateFieldsExceedingMaxLength != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsTruncateFieldsExceedingMaxLength);
                    }
                    break;
                case EnumDTActionType.SplitFullAddressColIntoSepLines:
                    if (PrmsSplitFullAddressColIntoSepLines != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitFullAddressColIntoSepLines);
                    }
                    break;
                case EnumDTActionType.ComputeAggregateFunction:
                    if (PrmsComputeAggregateFunction != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsComputeAggregateFunction);
                    }
                    break;
                case EnumDTActionType.SplitAndReshapeOutputFiles:
                    if (PrmsSplitAndReshapeOutputFiles != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitAndReshapeOutputFiles);
                    }
                    break;
                case EnumDTActionType.ExcludeRowsNotMatchReferenceList:
                    if (PrmsExcludeRowsNotMatchReferenceList != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsExcludeRowsNotMatchReferenceList);
                    }
                    break;
                case EnumDTActionType.ConcatenateMultiNodesDFs:
                    if (PrmsConcatenateMultiNodesDFs != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsConcatenateMultiNodesDFs);
                    }
                    break;
                case EnumDTActionType.SplitAndTransposeOutputFile:
                    if (PrmsSplitAndTransposeOutputFile != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsSplitAndTransposeOutputFile);
                    }
                    break;
                case EnumDTActionType.ConcatenateXMLListItemsDFs:
                    if (PrmsConcatenateXMLListItemsDFs != null)
                    {
                        prms = JsonConvert.SerializeObject(PrmsConcatenateXMLListItemsDFs);
                    }
                    break;
                default:
                    break;
            }
            return prms;
        }

        private T GetJSONObject<T>() where T : PrmsBase
        {
            if (string.IsNullOrEmpty(PrmsObject))
                return null;
            T obj = null;
            try
            {
                obj = (T)JsonConvert.DeserializeObject<T>(PrmsObject);
                obj.Parent = this;
                return obj;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private PrmsExcludeCasesAI prmsExcludeCasesAI;
        public PrmsExcludeCasesAI PrmsExcludeCasesAI
        {
            get
            {
                if (prmsExcludeCasesAI == null && DTActionType == EnumDTActionType.OSExcludeCasesAlreadyImported)
                {
                    prmsExcludeCasesAI = GetJSONObject<PrmsExcludeCasesAI>();
                }
                return prmsExcludeCasesAI;
            }
            set
            {
                prmsExcludeCasesAI = value;
            }
        }

        private PrmsGetDataFromDB prmsGetDataFromDB;
        public PrmsGetDataFromDB PrmsGetDataFromDB
        {
            get
            {
                if (prmsGetDataFromDB == null && DTActionType == EnumDTActionType.GetDataFromDB)
                {
                    prmsGetDataFromDB = GetJSONObject<PrmsGetDataFromDB>();
                }
                return prmsGetDataFromDB;
            }
            set
            {
                prmsGetDataFromDB = value;
            }
        }

        private PrmsXMappingDataFromDB prmsXMappingDataFromDB;
        public PrmsXMappingDataFromDB PrmsXMappingDataFromDB
        {
            get
            {
                if (prmsXMappingDataFromDB == null && DTActionType == EnumDTActionType.XMappingDataFromDB)
                {
                    prmsXMappingDataFromDB = GetJSONObject<PrmsXMappingDataFromDB>();
                }
                return prmsXMappingDataFromDB;
            }
            set
            {
                prmsXMappingDataFromDB = value;
            }
        }

        private PrmsSetDuplicatedRowsWithUniqueValues prmsSetDuplicatedRowsWithUniqueValues;
        public PrmsSetDuplicatedRowsWithUniqueValues PrmsSetDuplicatedRowsWithUniqueValues
        {
            get
            {
                if (prmsSetDuplicatedRowsWithUniqueValues == null && DTActionType == EnumDTActionType.SetDuplicatedRowsWithUniqueValues)
                {
                    prmsSetDuplicatedRowsWithUniqueValues = GetJSONObject<PrmsSetDuplicatedRowsWithUniqueValues>();
                }
                return prmsSetDuplicatedRowsWithUniqueValues;
            }
            set
            {
                prmsSetDuplicatedRowsWithUniqueValues = value;
            }
        }


        private PrmsGetTotalOfAColumn prmsGetTotalOfAColumn;
        public PrmsGetTotalOfAColumn PrmsGetTotalOfAColumn
        {
            get
            {
                if (prmsGetTotalOfAColumn == null && DTActionType == EnumDTActionType.GetTotalOfAColumn)
                {
                    prmsGetTotalOfAColumn = GetJSONObject<PrmsGetTotalOfAColumn>();
                }
                return prmsGetTotalOfAColumn;
            }
            set
            {
                prmsGetTotalOfAColumn = value;
            }
        }



        private PrmsAddProcessInfo prmsAddProcessInfo;
        public PrmsAddProcessInfo PrmsAddProcessInfo
        {
            get
            {
                if (prmsAddProcessInfo == null && DTActionType == EnumDTActionType.OSAddProcessInfo)
                {
                    prmsAddProcessInfo = GetJSONObject<PrmsAddProcessInfo>();
                }

                return prmsAddProcessInfo;
            }
            set
            {
                prmsAddProcessInfo = value;
            }

        }


        private PrmsGroupBy prmsGroupBy;
        public PrmsGroupBy PrmsGroupBy
        {
            get
            {
                if (prmsGroupBy == null && DTActionType == EnumDTActionType.OSGroupBy)
                {
                    prmsGroupBy = GetJSONObject<PrmsGroupBy>();
                }
                return prmsGroupBy;
            }
            set
            {
                prmsGroupBy = value;
            }

        }

        private PrmsExcludeCasesLLTV prmsExcludeCasesLLTV;
        public PrmsExcludeCasesLLTV PrmsExcludeCasesLLTV
        {
            get
            {
                if (prmsExcludeCasesLLTV == null && DTActionType == EnumDTActionType.OSExcludeCasesLessThanThreadholdVal)
                {
                    prmsExcludeCasesLLTV = GetJSONObject<PrmsExcludeCasesLLTV>();
                }
                return prmsExcludeCasesLLTV;
            }
            set
            {
                prmsExcludeCasesLLTV = value;
            }

        }

        private PrmsConcatenateFields prmsConcatenateFields;
        public PrmsConcatenateFields PrmsConcatenateFields
        {
            get
            {
                if (prmsConcatenateFields == null && DTActionType == EnumDTActionType.OSConcatenateFields)
                {
                    prmsConcatenateFields = GetJSONObject<PrmsConcatenateFields>();
                }
                return prmsConcatenateFields;
            }
            set
            {
                prmsConcatenateFields = value;
            }

        }

        private PrmsConcatenateMultiNodesDFs prmsConcatenateMultiNodesDFs;
        public PrmsConcatenateMultiNodesDFs PrmsConcatenateMultiNodesDFs
        {
            get
            {
                if (prmsConcatenateMultiNodesDFs == null && DTActionType == EnumDTActionType.ConcatenateMultiNodesDFs)
                {
                    prmsConcatenateMultiNodesDFs = GetJSONObject<PrmsConcatenateMultiNodesDFs>();
                }
                return prmsConcatenateMultiNodesDFs;
            }
            set
            {
                prmsConcatenateMultiNodesDFs = value;
            }

        }

        private PrmsSplitAndTransposeOutputFile prmsSplitAndTransposeOutputFile;
        public PrmsSplitAndTransposeOutputFile PrmsSplitAndTransposeOutputFile
        {
            get
            {
                if (prmsSplitAndTransposeOutputFile == null && DTActionType == EnumDTActionType.SplitAndTransposeOutputFile)
                {
                    prmsSplitAndTransposeOutputFile = GetJSONObject<PrmsSplitAndTransposeOutputFile>();
                }
                return prmsSplitAndTransposeOutputFile;
            }
            set
            {
                prmsSplitAndTransposeOutputFile = value;
            }

        }

        private PrmsTruncateFieldsExceedingMaxLength prmsTruncateFieldsExceedingMaxLength;
        public PrmsTruncateFieldsExceedingMaxLength PrmsTruncateFieldsExceedingMaxLength
        {
            get
            {
                if (prmsTruncateFieldsExceedingMaxLength == null && DTActionType == EnumDTActionType.TruncateFieldsExceedingMaxLength)
                {
                    prmsTruncateFieldsExceedingMaxLength = GetJSONObject<PrmsTruncateFieldsExceedingMaxLength>();
                }
                return prmsTruncateFieldsExceedingMaxLength;
            }
            set
            {
                prmsTruncateFieldsExceedingMaxLength = value;
            }

        }

        private PrmsExtractComments prmsExtractComments;
        public PrmsExtractComments PrmsExtractComments
        {
            get
            {
                if (prmsExtractComments == null && DTActionType == EnumDTActionType.OSExtractComments)
                {
                    prmsExtractComments = GetJSONObject<PrmsExtractComments>();
                }
                return prmsExtractComments;
            }
            set
            {
                prmsExtractComments = value;
            }

        }

        private PrmsComputeAggregateFunction prmsComputeAggregateFunction;
        public PrmsComputeAggregateFunction PrmsComputeAggregateFunction
        {
            get
            {
                if (prmsComputeAggregateFunction == null && DTActionType == EnumDTActionType.ComputeAggregateFunction)
                {
                    prmsComputeAggregateFunction = GetJSONObject<PrmsComputeAggregateFunction>();
                }
                return prmsComputeAggregateFunction;
            }
            set
            {
                prmsComputeAggregateFunction = value;
            }

        }

        private PrmsMergeDebts prmsMergeDebts;
        public PrmsMergeDebts PrmsMergeDebts
        {
            get
            {
                if (prmsMergeDebts == null && DTActionType == EnumDTActionType.OSMergeDebts)
                {
                    prmsMergeDebts = GetJSONObject<PrmsMergeDebts>();
                }
                return prmsMergeDebts;
            }
            set
            {
                prmsMergeDebts = value;
            }

        }

        private PrmsGetSumOfColumns prmsGetSumOfColumns;
        public PrmsGetSumOfColumns PrmsGetSumOfColumns
        {
            get
            {
                if (prmsGetSumOfColumns == null && DTActionType == EnumDTActionType.OSGetSumOfColumns)
                {
                    prmsGetSumOfColumns = GetJSONObject<PrmsGetSumOfColumns>();
                }

                return prmsGetSumOfColumns;
            }
            set
            {
                prmsGetSumOfColumns = value;
            }

        }

        private PrmsSplitColumn prmsSplitColumn;
        public PrmsSplitColumn PrmsSplitColumn
        {
            get
            {
                if (prmsSplitColumn == null && DTActionType == EnumDTActionType.OSSplitColumn)
                {
                    prmsSplitColumn = GetJSONObject<PrmsSplitColumn>();
                }
                return prmsSplitColumn;
            }
            set
            {
                prmsSplitColumn = value;
            }

        }

        private PrmsSplitNameColumnIntoThree prmsSplitNameColumnIntoThree;
        public PrmsSplitNameColumnIntoThree PrmsSplitNameColumnIntoThree
        {
            get
            {
                if (prmsSplitNameColumnIntoThree == null && DTActionType == EnumDTActionType.OSSplitNameColumnIntoThree)
                {
                    prmsSplitNameColumnIntoThree = GetJSONObject<PrmsSplitNameColumnIntoThree>();
                }
                return prmsSplitNameColumnIntoThree;
            }
            set
            {
                prmsSplitNameColumnIntoThree = value;
            }
        }

        private PrmsSplitFullAddressColIntoSepLines prmsSplitFullAddressColIntoSepLines;
        public PrmsSplitFullAddressColIntoSepLines PrmsSplitFullAddressColIntoSepLines
        {
            get
            {
                if (prmsSplitFullAddressColIntoSepLines == null && DTActionType == EnumDTActionType.SplitFullAddressColIntoSepLines)
                {
                    prmsSplitFullAddressColIntoSepLines = GetJSONObject<PrmsSplitFullAddressColIntoSepLines>();
                }
                return prmsSplitFullAddressColIntoSepLines;
            }
            set
            {
                prmsSplitFullAddressColIntoSepLines = value;
            }
        }

        private PrmsExcludeRowsNotMatchReferenceList prmsExcludeRowsNotMatchReferenceList;
        public PrmsExcludeRowsNotMatchReferenceList PrmsExcludeRowsNotMatchReferenceList
        {
            get
            {
                if (prmsExcludeRowsNotMatchReferenceList == null && DTActionType == EnumDTActionType.ExcludeRowsNotMatchReferenceList)
                {
                    prmsExcludeRowsNotMatchReferenceList = GetJSONObject<PrmsExcludeRowsNotMatchReferenceList>();
                }
                return prmsExcludeRowsNotMatchReferenceList;
            }
            set
            {
                prmsExcludeRowsNotMatchReferenceList = value;
            }
        }

        private PrmsSplitOneColIntoMultipleCols prmsSplitOneColIntoMultipleCols;
        public PrmsSplitOneColIntoMultipleCols PrmsSplitOneColIntoMultipleCols
        {
            get
            {
                if (prmsSplitOneColIntoMultipleCols == null && DTActionType == EnumDTActionType.SplitOneColIntoMultipleCols)
                {
                    prmsSplitOneColIntoMultipleCols = GetJSONObject<PrmsSplitOneColIntoMultipleCols>();
                }
                return prmsSplitOneColIntoMultipleCols;
            }
            set
            {
                prmsSplitOneColIntoMultipleCols = value;
            }
        }

        private PrmsNCTransformDefaultersAddresses prmsNCTransformDefaultersAddresses;
        public PrmsNCTransformDefaultersAddresses PrmsNCTransformDefaultersAddresses
        {
            get
            {
                if (prmsNCTransformDefaultersAddresses == null && DTActionType == EnumDTActionType.OSNCTransformDefaulterAddresses)
                {
                    prmsNCTransformDefaultersAddresses = GetJSONObject<PrmsNCTransformDefaultersAddresses>();
                }
                return prmsNCTransformDefaultersAddresses;
            }
            set
            {
                prmsNCTransformDefaultersAddresses = value;
            }
        }

        private PrmsExcludeSelectedRows prmsExcludeSelectedRows;
        public PrmsExcludeSelectedRows PrmsExcludeSelectedRows
        {
            get
            {
                if (prmsExcludeSelectedRows == null && DTActionType == EnumDTActionType.ExcludeSelectedRows)
                {
                    prmsExcludeSelectedRows = GetJSONObject<PrmsExcludeSelectedRows>();
                }
                return prmsExcludeSelectedRows;
            }
            set
            {
                prmsExcludeSelectedRows = value;
            }

        }

        private PrmsSplitAndReshapeOutputFiles prmsSplitAndReshapeOutputFiles;
        public PrmsSplitAndReshapeOutputFiles PrmsSplitAndReshapeOutputFiles
        {
            get
            {
                if (prmsSplitAndReshapeOutputFiles == null && DTActionType == EnumDTActionType.SplitAndReshapeOutputFiles)
                {
                    prmsSplitAndReshapeOutputFiles = GetJSONObject<PrmsSplitAndReshapeOutputFiles>();
                }
                return prmsSplitAndReshapeOutputFiles;
            }
            set
            {
                prmsSplitAndReshapeOutputFiles = value;
            }

        }

        private PrmsSplitOutputFile prmsSplitOutputFile;
        public PrmsSplitOutputFile PrmsSplitOutputFile
        {
            get
            {
                if (prmsSplitOutputFile == null && DTActionType == EnumDTActionType.OSSplitOutputFileBasedOnFields)
                {
                    prmsSplitOutputFile = GetJSONObject<PrmsSplitOutputFile>();
                }
                return prmsSplitOutputFile;
            }
            set
            {
                prmsSplitOutputFile = value;
            }

        }

        private PrmsExtractLinkedCasesAfterGroupBy prmsExtractLinkedCasesAfterGroupBy;
        public PrmsExtractLinkedCasesAfterGroupBy PrmsExtractLinkedCasesAfterGroupBy
        {
            get
            {
                if (prmsExtractLinkedCasesAfterGroupBy == null && DTActionType == EnumDTActionType.OSExtractLinkedCasesAfterGroupBy)
                {
                    prmsExtractLinkedCasesAfterGroupBy = GetJSONObject<PrmsExtractLinkedCasesAfterGroupBy>();
                }
                return prmsExtractLinkedCasesAfterGroupBy;
            }
            set
            {
                prmsExtractLinkedCasesAfterGroupBy = value;
            }

        }


        private PrmsSplitOutputAfterGrouping prmsSplitOutputAfterGrouping;
        public PrmsSplitOutputAfterGrouping PrmsSplitOutputAfterGrouping
        {
            get
            {
                if (prmsSplitOutputAfterGrouping == null && DTActionType == EnumDTActionType.SplitOutputAfterGrouping)
                {
                    prmsSplitOutputAfterGrouping = GetJSONObject<PrmsSplitOutputAfterGrouping>();
                }
                return prmsSplitOutputAfterGrouping;
            }
            set
            {
                prmsSplitOutputAfterGrouping = value;
            }

        }

        [Display(Name = "Referenced By")]
        public string AssociatedFileTemplate
        {
            get
            {
                string FTLinks = string.Empty;
                List<FileTemplateDTAction> ftdaList = FileTemplateDTActions.Where(x => !x.FileTemplate.Disabled).ToList();
                string appPath = System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/FileTemplate/Details/";
                if (ftdaList.Count > 0)
                {
                    foreach(FileTemplateDTAction ftdf in ftdaList)
                    {
                        FTLinks = FTLinks + " | <a href='" + appPath + ftdf.FileTemplateId.ToString() + "'>" + ftdf.FileTemplate.Name + "</a>";
                    }
                    FTLinks = FTLinks.Trim().Trim("|".ToCharArray());
                }
               
                return FTLinks;
            }
        }


        public string AssociatedFileTemplatesForCookie
        {
            get
            {
                string assocItemNames = AssociatedFileTemplate;
                if (!string.IsNullOrEmpty(assocItemNames) && assocItemNames.Length > COOKIE_MAX_LENGTH)
                {
                    assocItemNames = assocItemNames.Substring(0, COOKIE_MAX_LENGTH);
                    assocItemNames = assocItemNames.Substring(0, assocItemNames.LastIndexOf("|")) + "...";
                }

                return assocItemNames;
            }
        }

        private PrmsConcatenateXMLListItemsDFs prmsConcatenateXMLListItemsDFs;
        public PrmsConcatenateXMLListItemsDFs PrmsConcatenateXMLListItemsDFs
        {
            get
            {
                if (prmsConcatenateXMLListItemsDFs == null && DTActionType == EnumDTActionType.ConcatenateXMLListItemsDFs)
                {
                    prmsConcatenateXMLListItemsDFs = GetJSONObject<PrmsConcatenateXMLListItemsDFs>();
                }
                return prmsConcatenateXMLListItemsDFs;
            }
            set
            {
                prmsConcatenateXMLListItemsDFs = value;
            }

        }

        public class Metadata
        {
            [Required(ErrorMessage = "Action Name required.")]
            [Display(Name = "Action Name")]
            public string ActionName;
            [Display(Name = "DT Action Types")]
            public int DTActionTypeId;
        }

    }

}