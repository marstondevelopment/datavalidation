﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class GetTotalOfAColumn : DTStep
    {
        private PrmsGetTotalOfAColumn prms;
        public GetTotalOfAColumn(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsGetTotalOfAColumn;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsGetTotalOfAColumn parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
        }



        public override bool Execute()
        {


            object val = (decimal)ParentFTDT.FileTemplate.ProcessedDataTable.Compute("Sum([" + prms.ColumnName + "])", "");

            if (ParentFTDT.FileTemplate.HeaderDataTable != null && ParentFTDT.FileTemplate.HeaderDataTable.Columns.Contains(prms.TotalFieldName))
            {
                UpdateColumn(ParentFTDT.FileTemplate.HeaderDataTable, prms.TotalFieldName, val);
            }
            if (ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(prms.TotalFieldName))
            {
                UpdateColumn(ParentFTDT.FileTemplate.ProcessedDataTable, prms.TotalFieldName, val);
            }
            if (ParentFTDT.FileTemplate.TrailerDataTable != null && ParentFTDT.FileTemplate.TrailerDataTable.Columns.Contains(prms.TotalFieldName))
            {
                UpdateColumn(ParentFTDT.FileTemplate.TrailerDataTable, prms.TotalFieldName, val);
            }

            return true;
                 
        }
    }
}