﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Diagnostics;


namespace Mgl.DataValidator.Models
{
    public class ExcelFileParser : FileParser
    {
        public ExcelFileParser(FileTemplate ft, Stream fs, string fileName, EnumDataFileScreen userAction) : base(ft, fs, fileName, userAction)
        {
        }

        public override bool Validate()
        {
            bool valid = true;
            IWorkbook workbook = null;                
            ISheet worksheet = null;
            Dictionary<int, DataField> colsMap = null;
            try
            {
                using (UploadedFile)
                {
                    //Workbook factory determine the file type, and create workbook accordingly to support different extension for excel file.
                    workbook = WorkbookFactory.Create(UploadedFile); 
                    for (int i = 0; i < workbook.NumberOfSheets; i++)
                    {
                        worksheet = workbook.GetSheetAt(i);

                        colsMap = null;
                        if (worksheet.LastRowNum > 0)
                        {

                            FileTemplate.CurrentFileName = worksheet.SheetName;
                            // Copy data from excel row to columns in DataRow
                            for (int rowIndex = worksheet.FirstRowNum; rowIndex <= worksheet.LastRowNum ; rowIndex++)
                            {
                                IRow row = worksheet.GetRow(rowIndex);

                                //Remove Header
                                if (FileTemplate.RemoveHeader && rowIndex< worksheet.FirstRowNum + FileTemplate.HeaderRowsToSkip)
                                {
                                    continue;
                                }

                                if (IsEmptyRow(row))
                                {
                                    break;
                                }

                                if (colsMap == null)
                                {
                                    colsMap = MapColumns(row);

                                    if (FileTemplate.NoColumnName == false) //skip the line if the file has column name
                                        continue;
                                }
                                //Remove Footer
                                if (FileTemplate.RemoveFooter && rowIndex > worksheet.LastRowNum-FileTemplate.FooterRowsToSkip)
                                    continue;

                                FileTemplate.ResetFieldValues();
                                FileTemplate.RowId = rowIndex + 1;
                                try
                                {
                                    foreach (var col in colsMap)
                                    {
                                        var cell = row.GetCell(col.Key, MissingCellPolicy.RETURN_BLANK_AS_NULL);
                                        if (cell != null)
                                        {
                                            switch (cell.CellType)
                                            {
                                                case CellType.Numeric:
                                                    col.Value.OriginalValue = DateUtil.IsCellDateFormatted(cell) ? cell.DateCellValue.ToString() : cell.NumericCellValue.ToString();
                                                    break;
                                                case CellType.String:
                                                    col.Value.OriginalValue = cell.StringCellValue;
                                                    break;
                                                case CellType.Boolean:
                                                    col.Value.OriginalValue = cell.BooleanCellValue.ToString();
                                                    break;
                                                default:
                                                    break;
                                            }

                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Error occurred during parsing data file at row " + FileTemplate.RowId.ToString() + ": " + ex.Message + " \n Stack Trace: " + ex.StackTrace);
                                }

                                //Validate each row
                                if (!FileTemplate.ValidateRows())
                                {
                                    if (FileTemplate.TerminateProcess)
                                        return false;
                                    valid = false;
                                }
                            }

                        }

                    }

                }

            }
            catch (Exception ex)
            {
                if (ex is MissingFieldException || ex is ArgumentException)
                {
                    ValidationSummary.Clear();
                    ValidationSummary.AppendLine(ex.Message);
                    //ValidationSummary.AppendLine("*********************************************************************************************");
                    //ValidationSummary.AppendLine("*** Warning:  The selected file template can only parse files with extension: xls or xlsx.***");
                    //ValidationSummary.AppendLine("*********************************************************************************************");

                    return false;
                }
                else
                    throw;                
            }


            return valid;
        }

        private Dictionary<int, DataField> MapColumns(IRow row)
        {
            Dictionary<int,DataField> colMap = new Dictionary<int, DataField>();

            if (FileTemplate.NoColumnName == false)  // With column header
            {
                foreach (var df in FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList())
                {
                    var cell = row.Cells.Where(x => x.StringCellValue.Trim().ToUpper() == df.Name.ToUpper()).FirstOrDefault();
                    if (cell == null)
                    {

                        //Failed to map the column from the file
                        throw new MissingFieldException("Failed to parse data file. Worksheet " + FileTemplate.CurrentFileName + ": Column - " + df.Name.ToUpper() + " not found. Make sure there is a corresponding column in data file.");

                    }
                    colMap.Add(cell.ColumnIndex, df); //add the cell contents (name of column) and cell index to the map

                }
            }
            else // Without column header
            {
                var inputCols = FileTemplate.BodyCols.Where(x => x.OutputFieldOnly != true).ToList();
                if (inputCols.Count > row.Cells.Count)
                {
                    throw new MissingFieldException("Failed to parse data file. Number of columns from input file( " + row.Cells.Count.ToString() + ") is less than " + FileTemplate.CurrentFileName + " (" + inputCols.Count.ToString() + "). Please ensure the file format and data fields setting are in sync, and try again.");

                }
                for (int i = 0; i < inputCols.Count; i++)
                {
                    colMap.Add(i, inputCols[i]); //add the cell contents (name of column) and cell index to the map
                }
            }



            return colMap;
        }

        private bool IsEmptyRow(IRow row)
        {
            //all cells are empty, so is a 'blank row'
            if (row== null || row.Cells.All(d =>d==null || d.CellType == CellType.Blank) )
                return true;

            if (row.Cells.First().CellType == CellType.String && row.Cells.First().StringCellValue.ToUpper() == "N/A")
                return true;

            return false;
        }

    }
}