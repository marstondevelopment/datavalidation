﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public partial class FileDelimiter
    {
        public string DelimitersFullDisplay
        {
            get
            {
                return !string.IsNullOrEmpty(Description) ? string.Format(Description + " (" + Delimiter + ")") : Delimiter;
            }
        }
    }
}