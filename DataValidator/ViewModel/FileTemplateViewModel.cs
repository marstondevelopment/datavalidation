﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.ViewModel
{
    public class FileTemplateViewModel
    {
        public FileTemplateViewModel(FileTemplateRepository ftr, ClientRepository cr, FileTemplate ft)
        {
            _ftr = ftr;
            _cr = cr;
            FileTemplate = ft;
            Connections = _ftr.GetDbConnections();
        }
        private ClientRepository _cr;
        private FileTemplateRepository _ftr;
        public bool IsDetails;

        public FileTemplate FileTemplate { get; set; }
        public IEnumerable<FileTemplate> FileTemplates { get; set; }
        public IEnumerable<DbConnection> Connections { get; set; }
        public IEnumerable<MimeType> Templates { get; set; }
        public IEnumerable<Client> Clients { get; set; }
        public IEnumerable<DataField> Fields { get; set; }
        public DataField DataField { get; set; }
        public DTAction DTAction { get; set; }
        public FileTemplateDataField FTDataField { get; set; }
        public List<FileTemplateDataField> FTDataFields { get; set; }
        public IEnumerable<FileTemplateDTAction> FTDTActions { get; set; }
        public FileTemplateDTAction FTDTAction { get; set; }
        public IEnumerable<FileTemplateARAction> FTARActions { get; set; }
        public ARAction ARAction { get; set; }
        public FileTemplateARAction FTARAction { get; set; }

        private List<SelectListItem> clientListItems = null;
        private List<SelectListItem> fileTypeListItems = null;
        private List<SelectListItem> connectionListItems = null;
        private List<SelectListItem> inDelimitersListItems = null;
        private List<SelectListItem> outDelimitersListItems = null;
        private List<SelectListItem> xsdFilesListItems = null;

        public List<SelectListItem> SelectClientItems
        {
            get
            {
                if (clientListItems == null)
                {
                    clientListItems = new List<SelectListItem>();
                    User user = ((User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]);
                    var cList = user.IsAdmin ? _cr.GetAll().ToList() : user.AssociatedClients;
                    clientListItems.AddRange(cList.OrderBy(x => x.Name)
                        .Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));
                }
                return clientListItems;
            }
        }

        public List<SelectListItem> SelectMimeTypeItems
        {
            get
            {
                if (fileTypeListItems == null)
                {
                    fileTypeListItems = new List<SelectListItem>();
                    fileTypeListItems.AddRange(_ftr.GetMimeTypes().OrderBy(x => x.Name)
                        .Select(x => new SelectListItem { Text = x.Name + "|" + x.Extension, Value = x.Id.ToString() }));
                }
                return fileTypeListItems;
            }
        }

        public List<SelectListItem> SelectConnectionItems
        {
            get
            {
                if (connectionListItems == null)
                {
                    connectionListItems = new List<SelectListItem>();
                    connectionListItems.AddRange(_ftr.GetDbConnections().Where(x => !x.Disabled).OrderBy(x => x.Name)
                        .Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }));
                }
                return connectionListItems;
            }
        }

        public List<SelectListItem> SelectDataFields
        {
            get
            {
                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> SelectDTActionsList
        {
            get
            {
                return new List<SelectListItem>();
            }
        }

        public List<SelectListItem> SelectOutputDelimitersList
        {
            get
            {
                if (outDelimitersListItems == null)
                {
                    outDelimitersListItems = new List<SelectListItem>();
                    outDelimitersListItems.AddRange(_ftr.GetFileDelimiters()
                        .Select(x => new SelectListItem { Text = (x.DelimitersFullDisplay + " --> " + x.FileExtenstion) , Value = x.Id.ToString() }));
                }
                return outDelimitersListItems;
            }
        }

        public List<SelectListItem> SelectInputDelimitersList
        {
            get
            {
                if (inDelimitersListItems == null)
                {
                    inDelimitersListItems = new List<SelectListItem>();
                    inDelimitersListItems.AddRange(_ftr.GetFileDelimiters()
                        .Select(x => new SelectListItem { Text = x.DelimitersFullDisplay, Value = x.Id.ToString() }));
                }
                return inDelimitersListItems;
            }
        }

        public List<SelectListItem> SelectXsdFilesList
        {
            get
            {
                if (xsdFilesListItems == null)
                {
                    xsdFilesListItems = new List<SelectListItem>();
                    xsdFilesListItems.AddRange(_ftr.GetXsdFiles()
                        .Select(x => new SelectListItem { Text = x.Description + " | " + x.OriginalFileName, Value = x.Id.ToString() }));
                }
                return xsdFilesListItems;
            }
        }

        public List<SelectListItem> SelectARActionsList
        {
            get
            {
                return new List<SelectListItem>();
            }
        }
    }
}