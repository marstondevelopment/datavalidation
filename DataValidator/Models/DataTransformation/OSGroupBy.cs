﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSGroupBy : DTStep
    {
        private List<string> fieldNames; 
        public OSGroupBy(FileTemplateDTAction ftdt) : base(ftdt)
        {
            PrmsGroupBy prms = ftdt.DTAction.PrmsGroupBy;
            if (prms == null)
            {
                throw new MemberAccessException("Group By parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            fieldNames = prms.FieldNames.Split(",".ToCharArray()).ToList();

        }
        public override bool Execute()
        {
            if (ParentFTDT.FileTemplate.ProcessedDataTable.Rows.Count > 0)
            {
                var stagingDT = ParentFTDT.FileTemplate.ProcessedDataTable.Copy();
                var columnsToGroupBy = stagingDT.Columns.Cast<DataColumn>()
                    .Where(c => fieldNames.Contains(c.ColumnName, StringComparer.InvariantCultureIgnoreCase))
                    .ToArray();
                var comparer = new MultiFieldComparer();
                ParentFTDT.FileTemplate.GroupedDataTable = stagingDT.AsEnumerable()
                    .GroupBy(row => columnsToGroupBy.Select(c => row[c]), comparer);

                ParentFTDT.FileTemplate.ProcessedDataTable = ParentFTDT.FileTemplate.GroupedDataTable
                .Select(grouping => grouping.FirstOrDefault())
                .CopyToDataTable();

            }


            return true;

        }
    }


    public class MultiFieldComparer : IEqualityComparer<IEnumerable<object>>
    {
        public bool Equals(IEnumerable<object> x, IEnumerable<object> y)
        {
            if (x == null || y == null) return false;
            return x.SequenceEqual(y);
        }

        public int GetHashCode(IEnumerable<object> objects)
        {
            if (objects == null) return 0;
            unchecked
            {
                int hash = 17;
                foreach (object obj in objects)
                    hash = hash * 23 + (obj == null ? 0 : obj.GetHashCode());
                return hash;
            }
        }
    }
}