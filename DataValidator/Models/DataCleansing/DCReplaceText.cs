﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public class DCReplaceText : DCAction
    {
        private const string ORIGINAL_VAL_TAG = "<*>";
        public DCReplaceText(DataFieldDataCleansing dc) : base(dc) { }
        public override void ExecuteAction()
        {
            if (string.IsNullOrEmpty(DataFieldDataCleansing.OldText))
            {
                throw new MissingFieldException("OldText or NewText is missing for the operation. Check the settings for data cleansing action.");
            }
            DataFieldDataCleansing.OldText = DataFieldDataCleansing.OldText.CorrectCharEscapeSequences();
            if (!string.IsNullOrEmpty(DataFieldDataCleansing.NewText))
            {
                DataFieldDataCleansing.NewText = DataFieldDataCleansing.NewText.CorrectCharEscapeSequences();
            }

            if (DataFieldDataCleansing.DataField.OriginalValue != null)
                DataFieldDataCleansing.DataField.OriginalValue = DataFieldDataCleansing.DataField.OriginalValue.Replace(DataFieldDataCleansing.OldText, DataFieldDataCleansing.NewText ?? string.Empty);
            if (DataFieldDataCleansing.DataField.OriginalValues.Count>0)
            {
                for (int i = 0; i < DataFieldDataCleansing.DataField.OriginalValues.Count; i++)
                {
                    if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValues[i]))
                    {
                        if (DataFieldDataCleansing.OldText == ORIGINAL_VAL_TAG && DataFieldDataCleansing.NewText.Contains("<*>"))
                        {
                            DataFieldDataCleansing.DataField.OriginalValues[i] = DataFieldDataCleansing.NewText.Replace(ORIGINAL_VAL_TAG, DataFieldDataCleansing.DataField.OriginalValues[i]);
                        }
                        else
                        {
                            DataFieldDataCleansing.DataField.OriginalValues[i] = DataFieldDataCleansing.DataField.OriginalValues[i].Replace(DataFieldDataCleansing.OldText, DataFieldDataCleansing.NewText ?? string.Empty);
                        }
                    }

                }

            }
        }
    }
}