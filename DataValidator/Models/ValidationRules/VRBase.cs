﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mgl.DataValidator.Models;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public abstract class VRBase
    {
        public VRBase(DataFieldRule rule)
        {
            ParentFieldRule = rule;
            ErrorDescription = ValidationRuleError.VRErrors[(EnumValidationRule)rule.VRId]?? "The value is invalid";
        }

        public DataFieldRule ParentFieldRule { get; set; }
        protected string ErrorDescription { get; set; }

        public virtual void CleanField()
        {
            foreach (var item in ParentFieldRule.DataField.DataFieldDataCleansings.OrderBy(x=> x.Id).ToList())
            {
                item.DCAction.ExecuteAction();
            }
            return;
        }

        public abstract bool ValidateField();
        public virtual bool IsValid()
        {
            return ValidateField();
        }

        public static VRBase New(DataFieldRule dfr)
        {
            switch ((EnumValidationRule)dfr.VRId)
            {
                case EnumValidationRule.MinFieldLength:
                    return new VRMinFieldLength(dfr);
                case EnumValidationRule.MaxFieldLength:
                    return new VRMaxFieldLength(dfr);
                case EnumValidationRule.MandatoryField:
                    return new VRMandatoryField(dfr);
                case EnumValidationRule.FieldType:
                    return new VRFieldType(dfr);
                case EnumValidationRule.SP_Validation:
                    return new VRSPValidation(dfr);
                case EnumValidationRule.VRM_VehicleReg:
                    return  new VRVRMVehicleReg(dfr);
                case EnumValidationRule.RE_Validation:
                    return new VRREValidation(dfr);
                case EnumValidationRule.RL_Validation:
                    return new VRRLValidation(dfr);
                default:
                    throw new NotSupportedException();
            }
        }

        public void AppendError()
        {
            switch ((EnumMimeType)ParentFieldRule.DataField.FileTemplate.MimeTypeId)
            {
                case EnumMimeType.xls:
                case EnumMimeType.xlsx:
                case EnumMimeType.xlsx2007:
                    this.ParentFieldRule.DataField.FileTemplate.ParentDataFile.ValidationSummary.AppendLine(
                        this.ParentFieldRule.DataField.FileTemplate.CurrentFileName + "," + this.ParentFieldRule.DataField.FileTemplate.RowId.ToString()
                        + "," + this.ParentFieldRule.DataField.Name + "," + ParentFieldRule.DataField.OriginalValue.WrapInQuotesIfContains(Utility.DEFAULT_DELIMITER) + "," + (ParentFieldRule.ErrorMessage ?? ErrorDescription)
                        );
                    break;
                case EnumMimeType.csv:
                case EnumMimeType.dat:
                case EnumMimeType.txt:
                case EnumMimeType.xml:
                case EnumMimeType.asc:
                case EnumMimeType.xls5:
                case EnumMimeType.de:
                    this.ParentFieldRule.DataField.FileTemplate.ParentDataFile.ValidationSummary.AppendLine( this.ParentFieldRule.DataField.FileTemplate.RowId.ToString()
                            + "," + this.ParentFieldRule.DataField.Name + "," + ParentFieldRule.DataField.OriginalValue.WrapInQuotesIfContains(Utility.DEFAULT_DELIMITER) + "," + (ParentFieldRule.ErrorMessage ?? ErrorDescription)
                            );
                    break;
                default:
                    break;
            }
        }

    }
}