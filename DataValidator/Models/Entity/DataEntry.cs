﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class DataEntry
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DataEntry(FileTemplate ft, DEOBase deObj)
        {
            FileTemplateId = ft.Id;
            DataEntryFormId = ft.DataEntryFormId.Value;

            switch ((EnumDataEntryForm)ft.DataEntryFormId)
            {
                case EnumDataEntryForm.HMCTS:
                    this.DEOHmcts = (DEOHmcts)deObj;
                    break;
                default:
                    throw new NotImplementedException("Data entry form not supported.");
            }
        }

        public EnumDataEntryForm DataEntryForm
        {
            get
            {
                return (EnumDataEntryForm) DataEntryFormId;
            }
        }

        public string SerializePrmsObject()
        {
            string json = string.Empty;
            switch (DataEntryForm)
            {
                case EnumDataEntryForm.HMCTS:
                    json = JsonConvert.SerializeObject(DEOHmcts);
                    break;
                default:
                    break;
            }
            return json;
        }

        private T GetJSONObject<T>() where T : DEOBase
        {
            if (string.IsNullOrEmpty(this.JSONString))
                return null;
            T obj = null;
            try
            {
                obj = (T)JsonConvert.DeserializeObject<T>(JSONString);
                //obj.Parent = this;
                return obj;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private DEOHmcts deoHmcts;
        public DEOHmcts DEOHmcts
        {
            get
            {
                if (deoHmcts == null && DataEntryForm == EnumDataEntryForm.HMCTS)
                {
                    deoHmcts = GetJSONObject<DEOHmcts>();
                }
                return deoHmcts;
            }
            set
            {
                deoHmcts = value;
            }
        }

        public DEOBase GetDEObject()
        {
            DEOBase jsonObj = null;
            switch (DataEntryForm)
            {
                case EnumDataEntryForm.HMCTS:
                    jsonObj = GetJSONObject<DEOHmcts>();
                    break;
                default:
                    break;
            }
            return jsonObj;
        }

        public DEOBase GetCurrentDEObject()
        {
            switch (DataEntryForm)
            {
                case EnumDataEntryForm.HMCTS:
                    return DEOHmcts;
                default:
                    return null;
            }
        }

        public bool Editable
        {
            get
            {
                User user = (User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                if (user.RoleId > (int)EnumRole.ReadOnly)
                    return true;

                if (Id > 0)
                {
                    return (this.UserId == user.Id);
                }

                return false;
            }

        }

        public class Metadata
        {
            //[Required(ErrorMessage = "Action Name required.")]
            //[Display(Name = "Action Name")]
            //public string ActionName;
            //[Display(Name = "DT Action Types")]
            //public int DTActionTypeId;
        }

    }

}