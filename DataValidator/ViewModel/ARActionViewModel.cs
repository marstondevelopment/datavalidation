﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.ViewModel
{
    public class ARActionViewModel
    {
        private ARActionRepository _arr;
        public ARActionViewModel(ARActionRepository arr, ARAction arAction)
        {
            _arr = arr;
            ARAction = arAction;
        }

        public bool IsDetails;

        public ARAction ARAction { get; set; }
        public IEnumerable<ARAction> ARActions { get; set; }
    }
}