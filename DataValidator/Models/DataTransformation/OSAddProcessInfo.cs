﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSAddProcessInfo : DTStep
    {
        private PrmsAddProcessInfo prms;
        public OSAddProcessInfo(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ParentFTDT.DTAction.PrmsAddProcessInfo; 
            if (prms == null)
            {
                throw new MemberAccessException("PrmsAddProcessInfo parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
        }
        public override bool Execute()
        {


            object val = string.Empty;
            switch (prms.ProcessInfo)
            {
                case EnumDTInfoType.DataFileName:
                    val = ParentFTDT.FileTemplate.ParentDataFile.FileName;
                    break;
                case EnumDTInfoType.CurrentShortDate:
                    val = DateTime.Now;
                    break;
                case EnumDTInfoType.CurrentDateTime:
                    val = DateTime.Now;
                    break;
                case EnumDTInfoType.TotalNoRowsForInput:
                    val = ParentFTDT.FileTemplate.NoInputRows;
                    break;
                case EnumDTInfoType.CurrentUserId:
                    val = ((User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]).Id;
                    break;
                case EnumDTInfoType.RowCount:
                default:
                    break;
            }

            if (prms.ProcessInfo == EnumDTInfoType.RowCount)
            {
                if (ParentFTDT.FileTemplate.FileWriter.OutputFiles.Count > 0)
                {
                    foreach (OutputFile outputFile in ParentFTDT.FileTemplate.FileWriter.OutputFiles)
                    {
                        if (outputFile.FileCategory == EnumOutputFileCategory.Output)
                        {
                            int i = 1;
                            foreach (DataRow dr in outputFile.Data.Rows)
                            {
                                dr[prms.FieldName] = i.ToString();
                                i++;
                            }
                        }
                    }

                }
                else
                {
                    int i = 1;
                    foreach (DataRow dr in ParentFTDT.FileTemplate.ProcessedDataTable.Rows)
                    {
                        dr[prms.FieldName] = i.ToString();
                        i++;
                    }

                }
            }
            else
            {
                if (ParentFTDT.FileTemplate.HeaderDataTable != null && ParentFTDT.FileTemplate.HeaderDataTable.Columns.Contains(prms.FieldName))
                {
                    UpdateColumn(ParentFTDT.FileTemplate.HeaderDataTable, prms.FieldName, val);
                }
                if (ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(prms.FieldName))
                {
                    UpdateColumn(ParentFTDT.FileTemplate.ProcessedDataTable, prms.FieldName, val);
                }
                if (ParentFTDT.FileTemplate.TrailerDataTable != null && ParentFTDT.FileTemplate.TrailerDataTable.Columns.Contains(prms.FieldName))
                {
                    UpdateColumn(ParentFTDT.FileTemplate.TrailerDataTable, prms.FieldName, val);
                }
            }

            return true;
        }



    }
}