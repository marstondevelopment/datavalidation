﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic;
using System.Web;
using System.Diagnostics;
using Microsoft.VisualBasic.FileIO;
using WebGrease.Css.Extensions;

namespace Mgl.DataValidator.Models
{
    public class DEParser : FileParser
    {
        public DEParser(FileTemplate ft, EnumDataFileScreen userAction) : base(ft, null,null, userAction)
        {
        }

        private const int ROW_INDEX = 0;
        private const int COL_INDEX = 1;
        private User user = ((User)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]);

        public override bool Validate()
        {
            bool valid = true;
            List<DataField> inputCols = FileTemplate.BodyCols.Where(x => x.OutputFieldOnly == false).ToList();
            try
            {
                List<DataEntry> des = FileTemplate.DEForUpload;

                if (des.Count > 0)
                {
                    if (!CheckColMapping(inputCols, des[0]))
                        return false;
                    for (int i = 0; i < des.Count; i++)
                    {
                        var jsonObj = des[i].GetDEObject();
                        FileTemplate.ResetFieldValues();
                        FileTemplate.RowId = i;
                        try
                        {
                            foreach (var df in inputCols)
                            {
                                df.OriginalValue = jsonObj.GetPropertyValue(df.Name);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error occurred during parsing data file at row " + i.ToString() + ": " + ex.Message + " \n Stack Trace: " + ex.StackTrace);
                        }

                        //Validate each row
                        if (!FileTemplate.ValidateRows())
                        {
                            if (FileTemplate.TerminateProcess)
                                return false;
                            valid = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex is MissingFieldException)
                {
                    ValidationSummary.Clear();
                    ValidationSummary.AppendLine(ex.Message);
                    //ValidationSummary.AppendLine("*********************************************************************************************");
                    //ValidationSummary.AppendLine("*** Warning:  The selected file template can only parse files with extension: txt or csv. ***");
                    //ValidationSummary.AppendLine("*********************************************************************************************");
                    return false;
                }
                else
                    throw;
            }


            return valid;
        }


        private bool CheckColMapping(List<DataField> inputCols, DataEntry de )
        {
            bool valid = true;
            var jsonObj = de.GetDEObject();
            foreach (var col in inputCols)
            {
                if (!jsonObj.HasProperty(col.Name))
                {
                    FileTemplate.ParentDataFile.ValidationSummary.AppendLine("Failed to map column " + col.Name + " to property in DE object. Ensure the DE class includes the property for the file template.");
                    valid = false;
                }
            }
            return valid;
        }

    }
}