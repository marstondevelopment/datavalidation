﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSExtractComments : DTStep
    {
        private PrmsExtractComments prms;
        public OSExtractComments(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsExtractComments;
            if (prms == null)
            {
                throw new MemberAccessException("OSExtractComments parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
        }
        public override bool Execute()
        {
            //Add the note column if it doesn't exist
            if (!ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(prms.OutputFieldName))
            {
                ParentFTDT.FileTemplate.ProcessedDataTable.AddColumn(prms.OutputFieldName);
            }

            foreach (DataRow row in ParentFTDT.FileTemplate.ProcessedDataTable.Rows)
            {
                AppendComments(row);
            }

            return true;
        }

        private string ConcatenateFields(string[] fields, DataRow dr)
        {
            string values = string.Empty;

            foreach (var fieldName in fields)
            {
                DataField df = ParentFTDT.FileTemplate.BodyCols.Where(x => x.Name == fieldName).FirstOrDefault();
                if (prms.ExcludeFieldName)
                {
                    values = values + dr[fieldName].ObjToString(ParentFTDT.FileTemplate.BUDataTable.Columns[fieldName]) + Utility.ONESTEP_STRING_SEPARATOR;
                }
                else
                {
                    string outputName = string.IsNullOrEmpty(df.OutputColumnName) ? fieldName : df.OutputColumnName;
                    values = values + outputName + ":" + dr[fieldName].ObjToString(ParentFTDT.FileTemplate.BUDataTable.Columns[fieldName]) + Utility.ONESTEP_STRING_SEPARATOR;
                }
            }

            return values;
        }

        private void AppendComments(DataRow row)
        {
            string[] commentFields = prms.CommentFieldName.Split(Utility.DEFAULT_DELIMITER.ToCharArray());
            string comments = string.Empty;

            if (GroupByFields != null)
            {

                string filter = string.Empty;
                foreach (var item in GroupByFields)
                { 
                    if (string.IsNullOrWhiteSpace(row[item].ToString()))
                        filter = filter + "[" + item + "] is null" + " AND ";
                    else
                        filter = filter + "[" + item + "]" + "='" +  row[item].ToString().Replace("'", "''") + "' AND ";
                }
                filter = filter.Trim().Remove(filter.LastIndexOf(" AND "));
                List<DataRow> rows = ParentFTDT.FileTemplate.BUDataTable.Select(filter).ToList();
                if (rows.Count > 1)
                {
                    int rwIndex = 0;
                    foreach (var rw in rows)
                    {
                        rwIndex++;
                        if (prms.ExcludeParentRow && rwIndex == 1)
                            continue;
                        comments = comments + ConcatenateFields(commentFields, rw);
                    }
                }
            }
            else
            {
                comments = comments + ConcatenateFields(commentFields, row);
            }

            comments = comments.Trim(Utility.ONESTEP_STRING_SEPARATOR.ToCharArray());
            if (this.ParentFTDT.FileTemplate.OutputFileDelimiter.Delimiter == Utility.DEFAULT_DELIMITER)
            {
                comments = comments.Replace("\"", "\"\"");
            }

            if (prms.SplitOutputString && comments.Length > Utility.ONESTEP_MAX_LENGTH)
                comments = string.Join(Utility.ONESTEP_STRING_SEPARATOR, comments.SplitBy(Utility.ONESTEP_MAX_LENGTH).ToArray());

            row[prms.OutputFieldName] = comments;
        }
    }
}