﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class PrmsBase
    {
        public PrmsBase() { }
        public PrmsBase(DTAction dt)
        {
            parent = dt;
        }

        [Display(Name = "Implement In DT Stage")]
        public bool ImplementInDTStage { get; set; }

        [JsonIgnore]
        public DTAction Parent { get => parent; set => parent = value; }

        private DTAction parent;

    }
}