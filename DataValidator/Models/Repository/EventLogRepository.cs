﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    public class EventLogRepository : RepositoryBase<DataIntegrationEntities, EventLog>
    {
        private UserRepository _ur;

        public EventLogRepository(DataIntegrationEntities entity, UserRepository ur) : base(entity)
        {
            _ur = ur;
        }

        public override IQueryable<EventLog> GetAll()
        {
            return base.GetAll();
        }

        public EventLog GetById(int id)
        {
            return GetAll(x => x.Id == id).FirstOrDefault();
        }
    }
}