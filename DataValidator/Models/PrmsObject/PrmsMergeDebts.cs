﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsMergeDebts : PrmsBase
    {
        public PrmsMergeDebts() { }
        public PrmsMergeDebts(DTAction dt) : base(dt) { }

        [Display(Name = "Aggregation Field Name")]
        public string DebtFieldName { get; set; }
        [Display(Name = "Total Field Name")]
        public string TotalFieldName { get; set; }
        [Display(Name = "Group By Name")]
        public string GroupByName { get; set; }
    }
}