﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitAndReshapeOutputFiles : PrmsBase
    {
        public PrmsSplitAndReshapeOutputFiles() { }
        public PrmsSplitAndReshapeOutputFiles(DTAction dt) : base(dt) { }

        [Display(Name = "Filters")]
        public string FieldValues { get; set; }
        [Display(Name = "Scheme Names")]
        public string Scheme { get; set; }
        [Display(Name = "Columns Excluded")]
        public string ColumnExcluded { get; set; }
        [Display(Name = "Keep Selected Rows")]
        public bool KeepSelectedRow { get; set; }
    }
}