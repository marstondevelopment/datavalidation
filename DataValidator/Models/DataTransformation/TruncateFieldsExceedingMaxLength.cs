﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class TruncateFieldsExceedingMaxLength : DTStep
    {
        private PrmsTruncateFieldsExceedingMaxLength prms;
        private string[] truncatedFieldNames;
        private int[] maxFieldLengths;

        public TruncateFieldsExceedingMaxLength(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsTruncateFieldsExceedingMaxLength;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsTruncateFieldsExceedingMaxLength parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

            truncatedFieldNames = prms.TruncatedFieldNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray());
            maxFieldLengths = prms.MaxFieldLengths.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x => int.Parse(x)).ToArray();

            if (truncatedFieldNames.Length != maxFieldLengths.Length)
            {
                throw new MemberAccessException("Number of Truncated Field Names and Max Field Lengths not in sync in " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }

        }
        public override bool Execute()
        {

            //Add the note column if it doesn't exist
            if (!string.IsNullOrEmpty(prms.NotesFieldName) && !ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(prms.NotesFieldName))
            {
                ParentFTDT.FileTemplate.ProcessedDataTable.AddColumn(prms.NotesFieldName);
            }
            var rows = !string.IsNullOrEmpty(prms.Filters) ? ParentFTDT.FileTemplate.ProcessedDataTable.Select(prms.Filters) : ParentFTDT.FileTemplate.ProcessedDataTable.Select();

            foreach (DataRow row in rows)
            {
                string notes = string.Empty;
                if (!string.IsNullOrEmpty(prms.NotesFieldName) && prms.OverwriteNotesField == false)
                {
                    notes = row[prms.NotesFieldName].ToString();
                }

                for (int i = 0; i < truncatedFieldNames.Length; i++)
                {
                    if (row[truncatedFieldNames[i]].ToString().Length > maxFieldLengths[i])
                    {
                        //Archive the original value to Notes
                        if (!string.IsNullOrEmpty(prms.NotesFieldName))
                        {
                            notes = notes + prms.FieldSeparator + GetText(row, truncatedFieldNames[i]); 
                        }

                        row[truncatedFieldNames[i]] = row[truncatedFieldNames[i]].ToString().Substring(0, maxFieldLengths[i]);
                    }
                }

                if (!string.IsNullOrEmpty(prms.NotesFieldName) && !string.IsNullOrEmpty(notes))
                {
                    row[prms.NotesFieldName] = notes.Trim(prms.FieldSeparator.ToCharArray());
                }

            }

            return true;
        }

        private string GetText(DataRow row, string fieldName)
        {
            fieldName = fieldName.Trim();
            string conStr = string.Empty;
            DataColumn dc = (DataColumn)this.ParentFTDT.FileTemplate.BUDataTable.Columns[fieldName];
            if (!string.IsNullOrEmpty(row[fieldName].ToString()))
            {
                if (dc != null)
                {
                    conStr = row[fieldName].ObjToString(dc) + prms.FieldSeparator;
                }
                else
                {
                    conStr = row[fieldName].ToString() + prms.FieldSeparator;
                }

                if (prms.IncludeFieldName)
                    conStr = fieldName + ":" + conStr;
            }

            return conStr;
        }


    }
}