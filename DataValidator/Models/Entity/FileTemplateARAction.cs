﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  Mgl.DataValidator.Models.ValidationRules;
using  Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;
using Mgl.DataValidator.Models.DataTransformation;
using Mgl.DataValidator.Models.AuditReport;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class FileTemplateARAction
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ARStep arStep;
        public ARStep ARStep
        {
            get
            {
                if (arStep == null)
                {
                    arStep = ARStep.New(this);
                }
                return arStep;
            }

        }


        public class Metadata
        {
            //[Display(Name = "Connection Name")]
            //[Required(ErrorMessage = "Connection Name required.")]
            //public string Name;
        }
    }

}