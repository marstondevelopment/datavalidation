﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetUserName : ARStep
    {
        public ARGetUserName(FileTemplateARAction ftar) : base(ftar)
        {
        }
        public override bool Execute(ref string arScript)
        {
            string userName = HttpContext.Current.User.Identity.Name;
            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, userName);
            return true;
        }
    }
}