﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class PrmsSplitFullAddressColIntoSepLines : PrmsBase
    {
        public PrmsSplitFullAddressColIntoSepLines() { }
        public PrmsSplitFullAddressColIntoSepLines(DTAction dt) : base(dt) { }

        [Display(Name = "Input Column Name")]
        public string InputColumnName { get; set; }

        [Display(Name = "AddLine Col Names")]
        public string AddLineColNames { get; set; }

        [Display(Name = "Postcode Col Names")]
        public string PostcodeColName { get; set; }

        [Display(Name = "String Separator")]
        public string StringSeparator { get; set; }

        [Display(Name = "Filter")]
        public string Filter { get; set; }

        [Display(Name = "Regular Expression")]
        public int RegularExpressionId { get; set; }

        [JsonIgnore]
        public RegularExpression RegularExpression { get; set; }

        [Display(Name = "Location Matrix(LM)")]
        public int? ReferenceListId { get; set; }
        [Display(Name = "LM Processing Level")]
        public int? LMProcessingLevel { get; set; }

        [JsonIgnore]
        public ReferenceList ReferenceList { get; set; }

    }
}