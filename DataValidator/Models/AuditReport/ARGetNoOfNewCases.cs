﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetNoOfNewCases : ARStep
    {
        private OutputFile prms;
        public ARGetNoOfNewCases(FileTemplateARAction ftar) : base(ftar)
        {
        }
        public override bool Execute(ref string arScript)
        {
            prms = ParentFTAR.FileTemplate.FileWriter.ProcessingFile;

            if ( prms == null)
                throw new MissingMemberException("ProcessingFile is missing");

            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, prms.Data.Rows.Count.ToString());
            return true;
        }


    }
}