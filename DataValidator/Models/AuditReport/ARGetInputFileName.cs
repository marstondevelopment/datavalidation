﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetInputFileName : ARStep
    {
        public ARGetInputFileName(FileTemplateARAction ftar) : base(ftar)
        {
        }
        public override bool Execute(ref string arScript)
        {
            string fileName = ParentFTAR.FileTemplate.ParentDataFile.FileName;
            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, fileName);

            return true;
        }
    }
}