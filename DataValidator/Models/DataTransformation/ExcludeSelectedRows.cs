﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models.DataTransformation
{
    public class ExcludeSelectedRows : DTStep
    {
        private PrmsExcludeSelectedRows prms;
        List<string> filters;
        List<string> schemes;
        List<string> exclusionReasons;

        public const string IgnoredFile = "{Ignored}";

        public ExcludeSelectedRows(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsExcludeSelectedRows;
            if (prms == null)
            {
                throw new MemberAccessException("PrmsSplitOutputFile  has not been set for DTAction - " + ParentFTDT.DTAction.ActionName);
            }
            filters = prms.FieldValues.Split(",".ToCharArray()).ToList();
            schemes = prms.Scheme.Split(",".ToCharArray()).ToList();

            if (filters.Count != schemes.Count)
            {
                throw new Exception("Filters and schemes are not set up correctly (i.e Not in sync) for DTAction - " + ParentFTDT.DTAction.ActionName);
            }
            if (!string.IsNullOrEmpty(prms.ExclusionReasons))
            {
                if (prms.ExclusionReasons.Split(",".ToCharArray()).ToList().Count != filters.Count)
                {
                    throw new Exception("Exclusion Reasons are not set up correctly (i.e Not in sync) for DTAction - " + ParentFTDT.DTAction.ActionName);
                }
                else
                {
                    exclusionReasons = prms.ExclusionReasons.Split(",".ToCharArray()).ToList();
                    exclusionReasons.ForEach(x => x = x.Trim());
                }

            }
            

            schemes.ForEach( x => x = x.Trim());
        }
        public override bool Execute()
        {

            for (int i = 0; i < filters.Count; i++)
            {
                var rows = ParentFTDT.FileTemplate.ProcessedDataTable.Select(filters[i]);
                if (rows.Length > 0)
                {
                    OutputFile outputFile = null;

                    schemes[i] = schemes[i].Replace(IgnoredFile, string.Empty);
                    outputFile = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(schemes[i].Trim(), EnumOutputFileCategory.Ignored);
                    for (int j = 0; j < rows.Length; j++)
                    {
                        outputFile.Data.ImportRow(rows[j]);
                        rows[j].Delete();
                    }

                    if (exclusionReasons != null)
                    {
                        outputFile.DTActionMessage = exclusionReasons[i];

                    }
                    ParentFTDT.FileTemplate.ProcessedDataTable.AcceptChanges();
                }
            }

            return true;
        }
    }
}