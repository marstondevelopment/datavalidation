﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.AuditReport
{
    public class ARGetInputFileRowCount : ARStep
    {
        public ARGetInputFileRowCount(FileTemplateARAction ftar) : base(ftar)
        {
        }
        public override bool Execute(ref string arScript)
        {            
            arScript = arScript.RenderTag(ParentFTAR.ARAction.TagName, ParentFTAR.FileTemplate.NoInputRows.ToString());
            return true;
        }
    }
}