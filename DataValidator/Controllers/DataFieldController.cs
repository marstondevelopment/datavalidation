﻿
using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [ReadWriteAuthorizeUser]
    public class DataFieldController : ControllerBase
    {


        protected FileTemplateRepository _ftr;
        protected DataFieldRepository _dfr;
        protected RegExRepository _regexr;
        public DataFieldController(FileTemplateRepository ftr, DataFieldRepository dfr, RegExRepository regExr)
        {
            _ftr = ftr;
            _dfr = dfr;
            _regexr = regExr;
        }

        private const string SEARCH_PRMS = "DFSearchPrms";

        protected SearchPrms<DataField> CurrentSearchPrms
        {
            get
            {
                return this.Session[SEARCH_PRMS] == null ? null : (SearchPrms<DataField>)this.Session[SEARCH_PRMS];
            }
            set
            {
                Session.Add(SEARCH_PRMS, value);
            }
        }


        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page, int? pageSize)
        {
            SearchPrms<DataField> searchPrms = null;

            if (string.IsNullOrEmpty(sortOrder) && string.IsNullOrEmpty(currentFilter) && string.IsNullOrEmpty(searchString)
                && !page.HasValue && !pageSize.HasValue && CurrentSearchPrms != null)
            {
                searchPrms = CurrentSearchPrms;
            }
            else
            {
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                searchPrms = new SearchPrms<DataField>(sortOrder, searchString, page, pageSize, Utility.DefaultPageSize, null);
            }

            List<DataField> dfs = null;
            User user = ((User)Session[Session.SessionID]);
            if (user.IsAdmin)
                dfs = _dfr.GetAll().Where(x => string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || x.Description.Contains(searchPrms.SearchString))).OrderByDescending(x => x.DateUpdated).ToList();
            else
            {
                if (user.AssociatedFileTemplates != null && user.AssociatedFileTemplates.Count > 0)
                {
                    List<int> ftIds = user.AssociatedFileTemplates.Select(x => x.Id).ToList();
                    dfs = _dfr.GetAll().Where(x => (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || x.Description.Contains(searchPrms.SearchString))) && (x.FileTemplateDataFields.Any(y => ftIds.Contains(y.FileTemplateId)) || x.CreatedBy == user.Id || x.UpdatedBy == user.Id)).OrderByDescending(x => x.DateUpdated).ToList();
                }
                else
                    dfs = _dfr.GetAll().Where(x => (string.IsNullOrEmpty(searchPrms.SearchString) || (x.Name.Contains(searchPrms.SearchString) || x.Description.Contains(searchPrms.SearchString))) && (x.CreatedBy == user.Id || x.UpdatedBy == user.Id)).OrderByDescending(x => x.DateUpdated).ToList();
            }

            CurrentSearchPrms = new SearchPrms<DataField>(searchPrms.SortOrder, searchPrms.SearchString, searchPrms.PageNo, searchPrms.PageSize, Utility.DefaultPageSize, dfs);

            return View(CurrentSearchPrms).Information("Search results found: " + CurrentSearchPrms.RowsCount.ToString());
        }        

        public ActionResult Details(int id)
        {
            DataFieldViewModel vm = new DataFieldViewModel(_dfr, _dfr.GetById(id));
            return View(vm);
        }

        public ActionResult Create()
        {
            DataFieldViewModel vm = new DataFieldViewModel(_dfr, null);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            DataFieldViewModel vm = new DataFieldViewModel(_dfr, null);
            TryUpdateModel(vm);
            if (ModelState.IsValid)
            {
                _dfr.Create(vm.DataField);
                return RedirectToAction("Edit", new { id = vm.DataField.Id });
            }
            else
            {
                return View(vm);
            }
        }

        public ActionResult Clone(int? id)
        {
            if (id.HasValue)
            {
                DataField currentDF = _dfr.GetById(id.Value);
                DataField newDF = currentDF.Clone;
                _dfr.Create(newDF);
                return RedirectToAction("Edit", new { id = newDF.Id }).Success("Clone of " + currentDF.Name + " has been created successfully.");
            }
            return RedirectToAction("Index").Danger("Data Field failed to clone.");
        }

        public ActionResult Edit(int id)
        {
            var dataField = _dfr.GetById(id);
            if (!dataField.Editable)
                return RedirectToAction("AccessDenied", "Error");

            DataFieldViewModel vm = new DataFieldViewModel(_dfr, dataField);
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            var vm = new DataFieldViewModel(_dfr, _dfr.GetById(id));

            if (vm.DataField == null)
                throw new Exception("Data field not found with id " + id.ToString());

            if (!vm.DataField.Editable)
                return RedirectToAction("AccessDenied", "Error");

            TryUpdateModel(vm);
            if (ModelState.IsValid)
            {
                //Update the item
                _dfr.Update(vm.DataField);
                if (!string.IsNullOrEmpty(Request.Form["UrlRef"]))
                {
                    return Redirect(Request.Form["UrlRef"]).Success(vm.DataField.Name + " data field has been updated successfully");
                }
                return RedirectToAction("Index").Success(vm.DataField.Name + " data field has been updated successfully");
            }

            //Return with error
            return View(vm);
        }

        [AdminOnlyAuthorizeUser]
        public ActionResult Delete(int id)
        {
            DataField df = _dfr.GetById(id);
            if (df == null)
            {
                throw new NullReferenceException("Data field (" + id.ToString() + ") not found");
            }

            int associatedTemplates = df.FileTemplateDataFields.Where(x => x.FileTemplate.Disabled == false).Count();
            if (associatedTemplates > 0)
            {
                return RedirectToAction("Index").Danger("Failed to delete " + df.Name + " as some active file templates are referencing it.");
            }
            else
            {
                _dfr.Delete(df);
                return RedirectToAction("Index").Success(df.Name + " has been deleted successfully.");
            }
        }

        [HttpPost]
        public ActionResult SortDCActions(int id, string DCAIdList)
        {
            DataField df = _dfr.GetById(id);
            int[] orderedDCAArr = DCAIdList.Split(',').Select(x => int.Parse(x)).ToArray();
            List<DataFieldDataCleansing> dfdcs = new List<DataFieldDataCleansing>();
            foreach (var dfdcId in orderedDCAArr)
            {
                var dfdc = df.DataFieldDataCleansings.Where(x => x.Id == dfdcId).FirstOrDefault();
                if (dfdc == null)
                {
                    throw new Exception("DataFieldDataCleansing item not found with id:" + dfdcId.ToString());
                }
                dfdcs.Add(dfdc.Clone());
            }

            df = _dfr.ReorderDataFieldDataCleasning(df, dfdcs);
            return PartialView("_ActionList", df.DataFieldDataCleansings.ToList());
        }

        [HttpPost]
        public ActionResult DCActionUpdate(int? DataFieldId, int? DCId, string OldText, string NewText, string ProcessInfo, int? PaddingLength,string PaddingChar,string InputDateFormat, int? RegularExpressionId)
        {
            if ((!DataFieldId.HasValue || !DCId.HasValue)
                || ((DCId.Value == (int)EnumDCAction.ReplaceText) && (string.IsNullOrEmpty(OldText)))
                || ((DCId.Value == (int)EnumDCAction.PadLeft || DCId.Value == (int)EnumDCAction.PadRight) && (string.IsNullOrEmpty(PaddingChar) || !PaddingLength.HasValue))
                || ((DCId.Value == (int)EnumDCAction.StringToDate) && (string.IsNullOrEmpty(InputDateFormat)))
                || ((DCId.Value == (int)EnumDCAction.RegexRemoveTexts || DCId.Value == (int)EnumDCAction.RegexRemoveTexts) && (!RegularExpressionId.HasValue))
               )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            DataFieldDataCleansing dc = new DataFieldDataCleansing()
            {
                DataFieldId = DataFieldId.Value,
                DCId = DCId.Value,
                OldText = OldText,
                NewText = NewText,
                PaddingLength = PaddingLength,
                PaddingChar = PaddingChar,
                InputDateFormat = InputDateFormat,
                RegularExpressionId = RegularExpressionId
            };

            if (!string.IsNullOrEmpty(ProcessInfo))
                dc.ProcessInfoId = int.Parse(ProcessInfo);

            _dfr.AddDataFieldAction(dc);

            List<DataFieldDataCleansing> actions = _dfr.GetById(DataFieldId.Value).DataFieldDataCleansings.OrderBy(x => x.Id).ToList();
            return PartialView("_ActionList", actions);

        }

        [HttpPost]
        public ActionResult DCActionDelete(int? Id)
        {
            if (!Id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFieldDataCleansing dc = _dfr.GetDataFieldAction(Id.Value);
            if (dc == null)
            {
                throw new Exception("Failed to find DataFieldDataCleansings with id - " + Id.ToString());
            }
            int dfId = dc.DataFieldId;

            _dfr.DeleteDataFieldAction(dc);

            List<DataFieldDataCleansing> actions = _dfr.GetById(dfId).DataFieldDataCleansings.OrderBy(x => x.Id).ToList();
            return PartialView("_ActionList", actions);

        }

        [HttpPost]
        public ActionResult VRUpdate(int? DataFieldId, int? VRId, int? StoredProcedureId, int? RegularExpressionId, int? MaxFieldLength, int? MinFieldLength, int? ReferenceListId, string ErrorMessage, bool TerminateIfInvalid)
        {
            if ((!DataFieldId.HasValue || !VRId.HasValue)
                || ((VRId.Value == (int)EnumValidationRule.SP_Validation) && (!StoredProcedureId.HasValue))
                || ((VRId.Value == (int)EnumValidationRule.RE_Validation) && (!RegularExpressionId.HasValue))
                || ((VRId.Value == (int)EnumValidationRule.MaxFieldLength) && (!MaxFieldLength.HasValue))
                || ((VRId.Value == (int)EnumValidationRule.MinFieldLength) && (!MinFieldLength.HasValue))
                || ((VRId.Value == (int)EnumValidationRule.RL_Validation) && (!ReferenceListId.HasValue))
                )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            DataFieldRule dr = new DataFieldRule()
            {
                DataFieldId = DataFieldId.Value,
                VRId = VRId.Value,
                MaxFieldLength = MaxFieldLength,
                MinFieldLength = MinFieldLength,
                RegularExpressionId = RegularExpressionId,
                StoredProcedureId = StoredProcedureId,
                ReferenceListId = ReferenceListId,
                ErrorMessage = ErrorMessage,
                TerminateIfInvalid = TerminateIfInvalid
            };

            _dfr.AddVR(dr);

            List<DataFieldRule> vrs = _dfr.GetVRByDataFieldId(dr.DataFieldId);
            return PartialView("_VRList", vrs);

        }

        [HttpPost]
        public ActionResult VRDelete(int? Id)
        {
            if (!Id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFieldRule dr = _dfr.GetVR(Id.Value);
            if (dr == null)
            {
                throw new Exception("Failed to find DataFieldRule with id - " + Id.ToString());
            }
            int dfId = dr.DataFieldId;

            _dfr.DeleteVR(dr);

            List<DataFieldRule> vrs = _dfr.GetVRByDataFieldId(dfId);
            return PartialView("_VRList", vrs);

        }


    }
}
