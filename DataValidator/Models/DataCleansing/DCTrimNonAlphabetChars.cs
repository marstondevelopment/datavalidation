﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public class DCTrimNonAlphabetChars : DCAction
    {
        public DCTrimNonAlphabetChars(DataFieldDataCleansing dc) : base(dc) { }

        public override void ExecuteAction()
        {
            if (DataFieldDataCleansing.DataField.OriginalValue != null)
                DataFieldDataCleansing.DataField.OriginalValue = DataFieldDataCleansing.DataField.OriginalValue.Trim(Utility.CharsForTrim.ToCharArray());

            if (DataFieldDataCleansing.DataField.OriginalValues.Count > 0)
            {
                for (int i = 0; i < DataFieldDataCleansing.DataField.OriginalValues.Count; i++)
                {
                    if (!string.IsNullOrEmpty(DataFieldDataCleansing.DataField.OriginalValues[i]))
                    {
                        DataFieldDataCleansing.DataField.OriginalValues[i] = DataFieldDataCleansing.DataField.OriginalValues[i].Trim(Utility.CharsForTrim.ToCharArray());
                    }

                }

            }

        }
    }
}