﻿using Mgl.DataValidator.Models;
using Mgl.DataValidator.ViewModel;
using Security.Authorize;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class TestProfilesController : ControllerBase
    {
        private TestProfileRepository _tpr;
        private UserRepository _ur;
        private FileTemplateRepository _ftr;

        public TestProfilesController(TestProfileRepository tpr, UserRepository ur, FileTemplateRepository ftr)
        {
            _tpr = tpr;
            _ur = ur;
            _ftr = ftr;
        }

        // GET: TestProfiles
        public ActionResult Index()
        {
            var testProfiles = _tpr.GetAll();
            return View(testProfiles);
        }

        // GET: TestProfiles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TestProfile testProfile = _tpr.GetById(id.Value);
            if (testProfile == null)
            {
                return HttpNotFound();
            }
            return View(testProfile);
        }

        // GET: TestProfiles/Create
        public ActionResult Create()
        {
            TestProfileViewModel vm = new TestProfileViewModel(_tpr, null);
            return View(vm);
        }
       
        [HttpPost]        
        public ActionResult Create(TestProfile testProfile, HttpPostedFileBase fileInput)
        {
            TestProfileViewModel vm = new TestProfileViewModel(_tpr, testProfile);

            if (ModelState.IsValid)
            {
                if (fileInput != null)
                {
                    string fileName = Path.GetFileName(fileInput.FileName);
                    string directoryPath = (ConfigurationManager.AppSettings["TestFilePath"]).TrimEnd("\\".ToCharArray()) + "\\";
                    fileInput.SaveAs(directoryPath + fileName);
                }
                _tpr.Create(vm.TestProfile);
                return RedirectToAction("Index").Success("Test profile successfully created");
            }
            
            return View(vm);
        }

        public ActionResult Edit(int id)
        {
            TestProfileViewModel vm = new TestProfileViewModel(_tpr, _tpr.GetById(id));           
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            TestProfileViewModel vm = new TestProfileViewModel(_tpr, _tpr.GetById(id));
            TryUpdateModel(vm);
            if(ModelState.IsValid)
            {
                _tpr.Update(vm.TestProfile);
                return RedirectToAction("Index").Success(vm.TestProfile.FileTemplate.Name + " has been successfully updated.");
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var testprofile = _tpr.GetById(id);
            _tpr.Delete(testprofile);
            var testProfiles = _tpr.GetAll(x => !x.Disabled).OrderByDescending(x => x.DateUpdated).ToList();
            return PartialView("_Index", testProfiles);
        }

        // GET: TestProfiles/RunTests
        public ActionResult ProgressionTest()
        {
            TestProfileViewModel vm = new TestProfileViewModel(_tpr, null);
            return View(vm);
        }

        public ActionResult RunTest(int Id)
        {
            var testProfile = _tpr.GetById(Id);
            Execute(testProfile);
            return PartialView("_TestResult", testProfile);
        }
        public string GetTestResult(int Id)
        {
            var testResult = _tpr.GetTestResultById(Id);
            return testResult.HTMLTable;
        }

        public async Task<string> RunTestsAsync()
        {
            string results = await ExecuteAsync();
            return "Progression test has been intitiated as requested. The tests report will be available once the process is completed successfully.";
        }

        private async Task<string> ExecuteAsync()
        {
            DateTime processStart = DateTime.Now;
            string appPath = Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/TestProfiles/Details/";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<table class='table table-hover table-bordered table-sm ' id='tblResults'><thead class='thead-dark'><tr><th></th><th colspan='2'>Total No. Rows</th><th colspan='2'>Total No. Cases</th><th colspan='2'>Sum Of New Cases</th><th colspan='2'>Total No. Ignored Rows</th><th></th></tr><tr><th>File Template</th><th>Expected</th><th>Actual</th><th>Expected</th><th>Actual</th><th>Expected</th><th>Actual</th><th>Expected</th><th>Actual</th><th>Test Result</th></tr></thead><tbody>");
            var testProfiles = await _tpr.GetAllAsync();

            foreach (var tp in testProfiles)
            {
                TestProfile test = await _tpr.GetByIdAsync(tp.Id);
                Execute(test);
                sb.Append("<tr>");
                sb.Append("<td><a href = \"" + appPath + test.Id.ToString() + "\">" + test.FileTemplate.Name + "</a></td>");
                sb.Append("<td>" + test.TotalNoOfRowsExpected.ToString() + "</td>");
                sb.Append("<td>" + test.TotalNoRows.ToString() + "</td>");
                sb.Append("<td>" + test.TotalNoNewCasesExpected.ToString() + "</td>");
                sb.Append("<td>" + test.TotalNoNewCases.ToString() + "</td>");
                sb.Append("<td>" + test.TotalBalanceForNewCasesExpected.ToString("0.00") + "</td>");
                sb.Append("<td>" + test.TotalBalanceForNewCases.ToString("0.00") + "</td>");
                sb.Append("<td>" + test.TotalNoIgnoredRowsExpected.ToString() + "</td>");
                sb.Append("<td>" + test.TotalNoIgnoredRows.ToString() + "</td>");
                sb.Append("<td>" + test.TestResult + "</td>");
                sb.AppendLine("</tr>");
                if (test.ResultStatus == false && !string.IsNullOrEmpty(test.Error))
                {
                    sb.Append("<tr>");
                    sb.Append("<td></td><td colspan='9'>Error Description: " + test.Error + "</td>");
                    sb.AppendLine("</tr>");
                }
            }

            sb.AppendLine("</tbody></table>");

            TestResult tr = new TestResult()
            {
                HTMLTable = sb.ToString(),
                ProcessStart = processStart,
                ProcessEnd = DateTime.Now,
                UserId = _ur.CurrentUser.Id
            };
            _tpr.CreateTestResult(tr);

            return sb.ToString();
        }



        public void Execute(TestProfile tp)
        {
            tp.ResultStatus = true;
            try
            {
                //string fullPath = Server.MapPath(ConfigurationManager.AppSettings["TestFilePath"] + tp.InputFileName);
                string fullPath = (ConfigurationManager.AppSettings["TestFilePath"]).TrimEnd("\\".ToCharArray()) + "\\" + tp.InputFileName;
                using (FileStream fsSource = new FileStream(fullPath, FileMode.Open, FileAccess.Read))
                {
                    if (!Utility.CheckFileExtention(fsSource.Name, tp.FileTemplate))
                    {
                        tp.UpdateStatus(false, "File extention not supported for the selected file template. Please choose a file with correct extention and try again");
                        return;
                    }

                    FileParser fileParser = FileParser.New(tp.FileTemplate, fsSource, tp.InputFileName, EnumDataFileScreen.GenerateFiles);
                    FileReaderManager frm = new FileReaderManager(_ftr, fileParser);

                    switch ((EnumParserResult)frm.GenerateOutputFiles())
                    {
                        case EnumParserResult.FileGenerationSucceed:
                            ValidateResult(tp);
                            break;
                        case EnumParserResult.FileGenerationFailed:
                            tp.UpdateStatus(false, "Output files generation process failed due to some technical issue, please consult system administrator for further assistance.");
                            break;
                        case EnumParserResult.DTACtionsFailed:
                            tp.UpdateStatus(false, "DT actions failed due to some technical issue, please consult system administrator for further assistance.");
                            break;
                        case EnumParserResult.PreGenActionsFailed:
                            tp.UpdateStatus(false, "Pre-Gen actions failed due to some technical issue, please consult system administrator for further assistance.");
                            break;
                        case EnumParserResult.ConfigNotSet:
                            tp.UpdateStatus(false, "Configuration has not been setup correctly to initiate the bulk insert process, please update the file template and try again. ");
                            break;
                        case EnumParserResult.NoData:
                            tp.UpdateStatus(false, "Input data file is empty. No data to process.");
                            break;
                        case EnumParserResult.ValidationFailed:
                            tp.UpdateStatus(false, fileParser.ValidationSummary.ToString());
                            break;
                        default:
                            throw new Exception("Unexpected ParserResult from ProcessFile()");
                    }
                }
            }
            catch (Exception ex)
            {
                tp.UpdateStatus(false, ex.Message);

            }
            return;
        }

        private void ValidateResult(TestProfile tp)
        {

            //Total No. rows
            tp.TotalNoRows = tp.FileTemplate.NoInputRows;

            //Total number of new cases and total sum for new cases
            int rowCount = 0;
            decimal total = 0;
            foreach (var outputFile in tp.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output).ToList())
            {
                rowCount = rowCount + outputFile.Data.Rows.Count;
                if (tp.TotalBalanceForNewCasesExpected > 0 && !string.IsNullOrEmpty(tp.DebtColumnName))
                    total = total + (decimal)outputFile.Data.Compute("Sum([" + tp.DebtColumnName + "])", "");
            }

            tp.TotalNoNewCases = rowCount;
            tp.TotalBalanceForNewCases = total;

            //Total number of ignored cases
            rowCount = 0;
            total = 0;
            foreach (var ignoredFile in tp.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Ignored).ToList())
            {
                rowCount = rowCount + ignoredFile.Data.Rows.Count;
            }

            tp.TotalNoIgnoredRows = rowCount;

            tp.ResultStatus = (tp.TotalBalanceForNewCasesExpected == tp.TotalBalanceForNewCases && tp.TotalNoIgnoredRows == tp.TotalNoIgnoredRowsExpected
                && tp.TotalNoNewCases == tp.TotalNoNewCasesExpected && tp.TotalNoOfRowsExpected == tp.TotalNoRows);

        }
    }
}
