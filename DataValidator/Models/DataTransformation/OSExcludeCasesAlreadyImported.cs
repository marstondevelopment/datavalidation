﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSExcludeCasesAlreadyImported : DTStep
    {
        private PrmsExcludeCasesAI prms;
        public OSExcludeCasesAlreadyImported(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsExcludeCasesAI;
            if (prms == null || (prms.ValidationMethod == EnumValidationMethod.SP && prms.StoredProcedure == null) || (prms.ValidationMethod == EnumValidationMethod.CmdTxt && prms.CmdText == null))
            {
                throw new MemberAccessException("PrmsExcludeCasesAI parameters are missing/configured incorrectly for " + ftdt.DTAction.ActionName + ", please update the configuration and try again.");
            }
            if ((!string.IsNullOrEmpty(prms.EFPrefix)) && (string.IsNullOrEmpty(prms.CaseRefColName) || string.IsNullOrEmpty(prms.ExMessage)))
            {
                throw new MemberAccessException("PrmsExcludeCasesAI parameters are missing/configured incorrectly for " + ftdt.DTAction.ActionName + ", please update the configuration for exception file and try again.");
            }
        }
        public override bool Execute()
        {
            List<DataRow> exRows = new List<DataRow>();

            foreach (DataRow rw in ParentFTDT.FileTemplate.ProcessedDataTable.Rows)
            {
                foreach (var df in ParentFTDT.FileTemplate.BodyCols)
                {
                    df.OriginalValue = rw[df.Name]?.ToString();
                }

                switch (prms.ValidationMethod)
                {
                    case EnumValidationMethod.SP:
                        if (!prms.StoredProcedure.Validate())
                        {
                            exRows.Add(rw);
                        }
                        break;
                    case EnumValidationMethod.CmdTxt:
                        if (!prms.CmdText.Validate())
                        {
                            exRows.Add(rw);
                        }
                        break;
                    default:
                        break;
                }
            }

            if (exRows.Count > 0)
            {
                if (!string.IsNullOrEmpty(prms.EFPrefix))
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var rw in exRows)
                    {
                        if (!sb.ToString().Contains(rw[prms.CaseRefColName].ToString())) // This is to prevent repeating the logging for cases that has same case ref.
                        {

                            string ex = prms.ExMessage;
                            ex = ex.RenderTag(prms.CaseRefColName, rw[prms.CaseRefColName].ToString());
                            string filter = "[" + prms.CaseRefColName + "] = '" + rw[prms.CaseRefColName].ToString() + "'";
                            DataRow[] duplicatedCases = ParentFTDT.FileTemplate.BUDataTable.Select(filter);
                            foreach (var item in duplicatedCases)
                            {
                                sb.AppendLine(ex.RenderTag("LineNumber", (ParentFTDT.FileTemplate.BUDataTable.Rows.IndexOf(item) + 1).ToString()));
                            }
                        }
                    }
                    if (sb.Length > 0)
                    {
                        var ef = ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(prms.EFPrefix, EnumOutputFileCategory.Exception);
                        ef.Context = ef.Context + sb.ToString();
                    }
                }


                DataTable dt = string.IsNullOrEmpty(prms.OFPrefix) ? ParentFTDT.FileTemplate.ExcludedDataTable : ParentFTDT.FileTemplate.FileWriter.GetDataOutputFile(prms.OFPrefix, EnumOutputFileCategory.Ignored).Data;
                for (int i = 0; i < exRows.Count; i++)
                {
                    dt.ImportRow(exRows[i]);
                    ParentFTDT.FileTemplate.ProcessedDataTable.Rows.Remove(exRows[i]);
                }
            }

            return true;
        }
    }
}