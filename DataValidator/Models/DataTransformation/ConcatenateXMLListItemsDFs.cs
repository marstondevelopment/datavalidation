﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class ConcatenateXMLListItemsDFs : DTStep
    {
        private PrmsConcatenateXMLListItemsDFs prms;
        public ConcatenateXMLListItemsDFs(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsConcatenateXMLListItemsDFs;
            if (prms == null)
            {
                throw new MemberAccessException("ConcatenateXMLListItemsDFs parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            if (string.IsNullOrEmpty(prms.ListItemColName) || string.IsNullOrEmpty(prms.SubNodesColNames) || string.IsNullOrEmpty(prms.OutputFieldName) || string.IsNullOrEmpty(prms.FieldSeparator))
            {
                throw new MemberAccessException("ConcatenateXMLListItemsDFs - " + ftdt.DTAction.ActionName + " not configured correctly, please update the settings and try again.");
            }

        }
        public override bool Execute()
        {
            var rows = !string.IsNullOrEmpty(prms.Filter) ? ParentFTDT.FileTemplate.ProcessedDataTable.Select(prms.Filter) : ParentFTDT.FileTemplate.ProcessedDataTable.Select();

            if (SNColNames.Length > 0 && rows.Length >0)
            {
                List<string[]> dfGroupMatrix = new List<string[]>();
                for (int i = 1; i < 1000; i++)
                {
                    string parentNodeColName = prms.ListItemColName + i.ToString("00");
                    string firstNodeDC = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
                            .Where(y => y.ColumnName.StartsWith(parentNodeColName) && y.ColumnName.EndsWith(SNColNames[0]))
                             .Select(x => x.ColumnName).FirstOrDefault();
                    if (!string.IsNullOrEmpty(firstNodeDC))
                    {
                        List<string> subNodesColNames = new List<string>();
                        subNodesColNames.Add(firstNodeDC);
                        for (int j = 1; j < SNColNames.Length; j++)
                        {
                            string snColName = ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Cast<DataColumn>()
                                    .Where(y => y.ColumnName.StartsWith(parentNodeColName) && y.ColumnName.EndsWith(SNColNames[j]))
                                     .Select(x => x.ColumnName).FirstOrDefault();
                            if (!string.IsNullOrEmpty(snColName))
                                subNodesColNames.Add(snColName);
                        }
                        dfGroupMatrix.Add(subNodesColNames.ToArray());
                    }
                    else
                        break;
                }

                for (int i = 0; i < dfGroupMatrix.Count; i++)
                {
                    ConcatinateDFGroupsForOutputField(rows, dfGroupMatrix[i], prms.OutputFieldName + (i + 1).ToString("00"), prms.OutputFieldName);
                }

                ParentFTDT.FileTemplate.ProcessedDataTable.AcceptChanges();
            }

            return true;
        }

        private void ConcatinateDFGroupsForOutputField(DataRow[] rows, string[] dfGroup, string outputFieldName, string parentFieldName)
        {
            //Add the note column if it doesn't exist
            if (!ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Contains(outputFieldName))
            {
                ParentFTDT.FileTemplate.ProcessedDataTable.AddColumn(outputFieldName);
            }

            //var rows = !string.IsNullOrEmpty(prms.FieldValues) ? ParentFTDT.FileTemplate.ProcessedDataTable.Select(prms.FieldValues) : ParentFTDT.FileTemplate.ProcessedDataTable.Select();
            foreach (DataRow row in rows)
            {
                AppendFields(row, dfGroup, outputFieldName);
                ClearInputFields(row,dfGroup);
            }

            DCPostDT(rows, outputFieldName, parentFieldName);
            RemoveInputFields(dfGroup);
        }

        private string[] snColNames = null;

        public string[] SNColNames
        {
            get
            {
                if (snColNames == null)
                {
                    snColNames = prms.SubNodesColNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray());
                }
                return snColNames;
            }
        }

        private string GetText(DataRow row, string fieldName, int colIndex)
        {
            fieldName = fieldName.Trim();
            string conStr = string.Empty;
            DataColumn dc = (DataColumn)this.ParentFTDT.FileTemplate.BUDataTable.Columns[fieldName];
            try
            {
                if (row.Table.Columns.Contains(fieldName) && !string.IsNullOrEmpty(row[fieldName].ToString()))
                {
                    if (prms.RegularExpressionId.HasValue)
                    {
                        Regex regex = new Regex(prms.RegularExpression.RegExText);
                        Match match = null;
                        match = regex.Match(row[fieldName].ToString());
                        if (match.Length <= 0)
                        {
                            return conStr;
                        }
                    }

                    if (dc != null)
                    {
                        conStr = row[fieldName].ObjToString(dc) + prms.FieldSeparator;
                    }
                    else
                    {
                        conStr = row[fieldName].ToString() + prms.FieldSeparator;
                    }

                    if (prms.IncludeFieldName)
                        conStr = (prms.UsePColName ? SNColNames[colIndex] : fieldName) + ":" + conStr;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return conStr;
        }


        private void AppendFields(DataRow row, string[] dfGroup, string outputFieldName)
        {
            string conStr = string.Empty;
            int chunkLength = prms.SubStrMaxLength == 0 ? Utility.ONESTEP_MAX_LENGTH : prms.SubStrMaxLength;

            for (int i = 0; i < dfGroup.Length; i++)
            {
                conStr = conStr + GetText(row, dfGroup[i], i);
            }


            conStr = conStr.Trim(prms.FieldSeparator.ToCharArray());

            if (prms.SplitOutputString && conStr.Length > chunkLength)
                conStr = string.Join(Utility.ONESTEP_STRING_SEPARATOR, conStr.SmartSplit(chunkLength).ToArray());
            
            if (prms.OverwriteOutputField)
                row[outputFieldName] = conStr;
            else
                row[outputFieldName] = string.IsNullOrEmpty( row[outputFieldName].ToString()) ? conStr: row[outputFieldName].ToString() + prms.FieldSeparator + conStr;
        }

        private void RemoveInputFields(string[] dfGroup)
        {
            if (prms.HideInputField == true)
            {
                foreach (var fieldName in dfGroup)
                {
                        //Delete single column here
                        ParentFTDT.FileTemplate.ProcessedDataTable.Columns.Remove(fieldName);
                }
            }
        }

        private void ClearInputFields(DataRow row, string[] dfGroup)
        {
            if (prms.ClearInputFields == true)
            {
                foreach (var fieldName in dfGroup)
                {
                   row[fieldName]=null;
                }
            }
        }

        private void DCPostDT(DataRow[] rows, string outputFieldName, string parentFieldName)
        {
            DataField df = this.ParentFTDT.FileTemplate.BodyCols.Where(x => x.Name == parentFieldName).FirstOrDefault();
            if ( df != null &&  prms.DCPostDT == true && ((EnumFieldType) df.FieldType) == EnumFieldType.String)
            {
                foreach (DataRow row in rows)
                {
                    df.OriginalValue = row[outputFieldName].ToString();
                    foreach (var dc in df.DataFieldDataCleansings)
                    {
                        dc.DCAction.ExecuteAction();
                    }
                    row[outputFieldName] = df.OriginalValue;
                }
            }
        }
    }
}