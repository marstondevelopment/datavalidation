﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mgl.DataValidator.Models.ValidationRules;
using Mgl.DataValidator.Models;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data.SqlClient;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class StoredProcedure
    {
        private Dictionary<string, object> _paramCollection;
        private const int COOKIE_MAX_LENGTH = 1600;
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Dictionary<string, object> ParamCollection
        {
            get
            {
                if (!string.IsNullOrEmpty(Params))
                {
                    _paramCollection = new Dictionary<string, object>();
                    string[] cols = Params.Split(",".ToCharArray()).Select(x => x.Trim()).ToArray();
                    foreach (var item in cols)
                    {
                        try
                        {
                            DataField dataField = FileTemplate.BodyCols.Where(x => x.Name.ToUpper() == item.ToUpper()).FirstOrDefault();
                            if (dataField == null)
                            {
                                dataField  = FileTemplate.BodyCols.Where(x => x.OutputColumnName != null && x.OutputColumnName.ToUpper() == item.ToUpper()).FirstOrDefault();
                                if (dataField == null)
                                {
                                    dataField = FileTemplate.Columns.Where(x => x.Name.ToUpper() == item.ToUpper()).FirstOrDefault();
                                    if (dataField == null)
                                        throw new MissingFieldException("Data field not found for SP parameter - " + item);
                                }
                            }
                            _paramCollection.Add(item.Trim().Replace(" ",string.Empty), dataField.TypedField());
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                            throw;
                        }

                    }
                }

                return _paramCollection;
            }

            set
            {
                _paramCollection = value;
            }
        }

        public VRSPValidation ParentVR { get; set; }

        //public void SetPrmsCollection(List<DataField> columns)
        //{
        //    if (!string.IsNullOrEmpty(Params))
        //    {
        //        _paramCollection = new Dictionary<string, object>();
        //        string[] cols = Params.Split(",".ToCharArray()).Select(x=> x.Trim()).ToArray();
        //        foreach (var item in cols)
        //        {
        //            try
        //            {
        //                DataField dataField = columns.Where(x => x.Name.ToUpper() == item.ToUpper()).FirstOrDefault();
        //                if (dataField == null)
        //                {
        //                    throw new MissingFieldException("Data field not found for SP parameter - " + item);
        //                }
        //                _paramCollection.Add(item, dataField.TypedField());
        //            }
        //            catch (Exception ex)
        //            {
        //                logger.Error(ex);
        //                throw;
        //            }

        //        }
        //    }
        //}
        [Display(Name = "Referenced By")]
        public string AssociatedFileTemplates
        {
            get
            {
                string fileNames = string.Empty;
                List<FileTemplate> fileTemplates = FileTemplates.Where(x => x.Disabled == false).ToList();
                string appPath = System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/FileTemplate/Details/";
                if (fileTemplates.Count > 0)
                {
                    foreach(var fileTemplate in fileTemplates)
                    {
                        fileNames = fileNames + " | <a href='" +appPath + fileTemplate.Id.ToString() + "'>" + fileTemplate.Name + "</a>";
                    }
                    fileNames = fileNames.Trim().Trim("|".ToCharArray());
                }
                if (AssociatedDTActions!=null && AssociatedDTActions.Count>0)
                {
                    appPath = System.Web.HttpContext.Current.Request.ApplicationPath.TrimEnd("/".ToCharArray()) + "/DTAction/Details/";
                    foreach (var item in AssociatedDTActions)
                    {
                        fileNames = fileNames + " | <a href='" + appPath + item.Key.ToString() + "'>" + item.Value + "</a>";
                    }

                    fileNames = fileNames.Trim().Trim("|".ToCharArray());
                }

                return fileNames;
            }
        }

        public string AssociatedFileTemplatesForCookie
        {
            get
            {
                string assocItemNames = AssociatedFileTemplates;
                if (!string.IsNullOrEmpty(assocItemNames) && assocItemNames.Length > COOKIE_MAX_LENGTH)
                {
                    assocItemNames = assocItemNames.Substring(0, COOKIE_MAX_LENGTH);
                    assocItemNames = assocItemNames.Substring(0, assocItemNames.LastIndexOf("|")) + "...";
                }

                return assocItemNames;
            }
        }

        public FileTemplate FileTemplate;

        public bool Validate()
        {
            return FileTemplate.DataProvider.ValidateData(this);
        }

        public DataTable GetData()
        {
            return FileTemplate.DataProvider.GetData(this);
        }

        public string DescriptionDisplay
        {
            get { return !string.IsNullOrEmpty(Description) ? Description : "N/A"; }
        }

        public Dictionary<int,string> AssociatedDTActions { get; set; }

        public class Metadata
        {
            [Display(Name = "Stored Procedure")]
            [Required(ErrorMessage = "Stored Procedure name required.")]
            public string SPName;
            [Display(Name = "Connection")]
            [Required(ErrorMessage = "Database connection name required.")]
            public int DBConnectionId;
            [Display(Name = "SP Type")]
            [Required(ErrorMessage = "Stored Procedure type required.")]
            public int SPType;
        }
    }
}