﻿using Mgl.DataValidator.Models.ValidationRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{

    [MetadataType(typeof(MetaData))]
    public partial class DataFieldRule
    {
        private VRBase _validationRule = null;
        public VRBase ValidationRule
        {
            get
            {
                if (_validationRule == null)
                {
                    _validationRule = VRBase.New(this);
                }

                return _validationRule;
            }
        }

        public EnumValidationRule VR
        {
            get
            {
                return (EnumValidationRule)VRId;
            }
        }

        public DataFieldRule Clone
        {
            get
            {
                DataFieldRule dataFieldRule = new DataFieldRule
                {
                    DataFieldId = DataFieldId,
                    VRId = VRId,
                    StoredProcedureId = StoredProcedureId,
                    RegularExpressionId = RegularExpressionId,
                    MaxFieldLength = MaxFieldLength,
                    MinFieldLength = MinFieldLength,
                    ReferenceListId = ReferenceListId,
                    ErrorMessage = ErrorMessage,
                    TerminateIfInvalid = TerminateIfInvalid
                };
                return dataFieldRule;
            }
        }

        public class MetaData
        {
            [Display(Name = "Validation Rule")]
            public int VRId;
            [Display(Name = "Stored Procedure")]
            public Nullable<int> StoredProcedureId;
            [Display(Name = "Regular Expression")]
            public Nullable<int> RegularExpressionId;
            [Display(Name = "Max FL")]
            public Nullable<int> MaxFieldLength;
            [Display(Name = "Min FL")]
            public Nullable<int> MinFieldLength;
            [Display(Name = "Reference List")]
            public Nullable<int> ReferenceListId;
            [Display(Name = "Error Message")]
            public string ErrorMessage;
            [Display(Name = "Terminate If Invalid")]
            public bool TerminateIfInvalid;

        }
    }
}