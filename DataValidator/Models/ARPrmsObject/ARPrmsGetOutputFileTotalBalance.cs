﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Mgl.DataValidator.Models
{
    
    public class ARPrmsGetOutputFileTotalBalance : ARPrmsBase
    {
        public ARPrmsGetOutputFileTotalBalance() { }
        public ARPrmsGetOutputFileTotalBalance(ARAction ar) : base(ar) { }

        [Display(Name = "Input Column Name")]
        public string InputColumnName { get; set; }
    }
}