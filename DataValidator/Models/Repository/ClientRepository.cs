﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mgl.DataValidator.Models
{
    public class ClientRepository : RepositoryBase<DataIntegrationEntities, Client>
    {
        private UserRepository _ur;
        public ClientRepository(DataIntegrationEntities entity, UserRepository ur) : base(entity)
        {
            _ur = ur;
        }

        public override IQueryable<Client> GetAll()
        {
            return base.GetAll(c => c.Disabled == false);
        }

        public Client GetById(int id)
        {
            return GetAll(c => c.Id == id).FirstOrDefault();
        }

        public void Create(Client client)
        {
            client.CreatedBy = _ur.CurrentUser.Id;
            client.DateCreated = DateTime.Now;
            client.UpdatedBy = _ur.CurrentUser.Id;
            client.DateUpdated = DateTime.Now;
            this.Add(client);
            this.SubmitChanges();
        }

        public void Update(Client client)
        {          
            client.DateUpdated = DateTime.Now;
            client.UpdatedBy = _ur.CurrentUser.Id;
            base.SubmitChanges();
            
        }

        public void Delete(Client client)
        {                       
            client.Disabled = true;
            Update(client);           
        }

        public void Reinstate(Client client)
        {            
            client.Disabled = false;
            Update(client);            
        }     
    }
}