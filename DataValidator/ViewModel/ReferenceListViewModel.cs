﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.ViewModel
{
    public class ReferenceListViewModel
    {
        ReferenceListRepository _rlr;
        public ReferenceListViewModel(ReferenceListRepository rlr)
        {
            _rlr = rlr;
            ReferenceLists = _rlr.GetAll().Where(x => x.Disabled == false).ToList();
        }

        public List<ReferenceList> ReferenceLists { get; set; }


    }
}