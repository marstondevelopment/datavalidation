﻿using Mgl.DataValidator.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mgl.DataValidator.ViewModel
{
    public class VMFileUpload
    {
        public List<Client> Clients { get; set; }

        public int? SelectedClientId { get; set; }
        private List<SelectListItem> _clientList;
        public List<SelectListItem> ClientList
        {
            get
            {
                if (_clientList == null)
                {
                    _clientList = Clients.Select(c=> new SelectListItem() { Value = c.Id.ToString(), Text = c.Name, Selected = SelectedClientId.HasValue ? c.Id == SelectedClientId.Value : false}).ToList();
                }
                return _clientList;
            }
        }
        public int? SelectedFileTemplateId { get; set; }
        private List<SelectListItem> _fileTemplateList= new List<SelectListItem>();
        public List<SelectListItem> FileTemplateList
        {
            get
            {
                if (SelectedClientId.HasValue)
                {
                    _fileTemplateList = Clients.Where(c => c.Id == SelectedClientId.Value).FirstOrDefault().FileTemplates.Where(x=> x.Disabled == false).OrderBy(y => y.Name).Select(t => new SelectListItem() { Value = t.Id.ToString(), Text = t.Name + " ( " + t.MimeType.Extension + " )" }).ToList();
                }

                return _fileTemplateList;
            }
        }

        public List<SelectListItem> FileTemplateBulkCopyReadyList
        {
            get
            {
                if (SelectedClientId.HasValue)
                {
                    _fileTemplateList = Clients.Where(c => c.Id == SelectedClientId.Value).FirstOrDefault().FileTemplates.Where(x=> x.BulkUpload==true && x.Disabled == false).OrderBy(y => y.Name).Select(t => new SelectListItem() { Value = t.Id.ToString(), Text = t.Name + " ( " + t.MimeType.Extension + " )" }).ToList();
                }

                return _fileTemplateList;
            }
        }

        public List<SelectListItem> FileTemplateDEUploadList
        {
            get
            {
                if (SelectedClientId.HasValue)
                {
                    _fileTemplateList = Clients.Where(c => c.Id == SelectedClientId.Value).FirstOrDefault().FileTemplates.Where(x => x.DataEntry == true && x.Disabled == false).OrderBy(y => y.Name).Select(t => new SelectListItem() { Value = t.Id.ToString(), Text = t.Name }).ToList();
                }

                return _fileTemplateList;
            }
        }

        public List<SelectListItem> FileTemplateOutputFilesList
        {
            get
            {
                if (SelectedClientId.HasValue)
                {
                    _fileTemplateList = Clients.Where(c => c.Id == SelectedClientId.Value).FirstOrDefault().FileTemplates.Where(x => x.GenerateOutputFile == true && x.Disabled == false).OrderBy(y => y.Name).Select(t => new SelectListItem() { Value = t.Id.ToString(), Text = t.Name + " ( " + t.MimeType.Extension + " )" }).ToList();
                }

                return _fileTemplateList;
            }
        }

        public List<SelectListItem> UploadToRepositoryFilesList
        {
            get
            {
                if (SelectedClientId.HasValue)
                {
                    _fileTemplateList = Clients.Where(c => c.Id == SelectedClientId.Value).FirstOrDefault().FileTemplates.Where(x => x.UploadToFileRepository == true && x.Disabled == false).OrderBy(y => y.Name).Select(t => new SelectListItem() { Value = t.Id.ToString(), Text = t.Name + " ( " + t.MimeType.Extension + " )" }).ToList();
                }

                return _fileTemplateList;
            }
        }

        public List<SelectListItem> FileTemplateTransformReadyList
        {
            get
            {
                if (SelectedClientId.HasValue)
                {
                    _fileTemplateList = Clients.Where(c => c.Id == SelectedClientId.Value).FirstOrDefault().FileTemplates.Where(x=> x.TransformData == true && x.Disabled == false).OrderBy(y => y.Name).Select(t => new SelectListItem() { Value = t.Id.ToString(), Text = t.Name + " ( " + t.MimeType.Extension + " )" }).ToList();
                }

                return _fileTemplateList;
            }
        }

        public FileTemplate SelectedFT { get; set; }
    }
}
