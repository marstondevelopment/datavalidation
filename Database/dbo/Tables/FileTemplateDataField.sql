﻿CREATE TABLE [dbo].[FileTemplateDataField] (
    [Id]             INT IDENTITY (1, 1) NOT NULL,
    [FileTemplateId] INT NOT NULL,
    [DataFieldId]    INT NOT NULL,
    CONSTRAINT [PK_FileTemplateDataField] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_FileTemplateDataField_DataField] FOREIGN KEY ([DataFieldId]) REFERENCES [dbo].[DataField] ([Id]),
    CONSTRAINT [FK_FileTemplateDataField_FileTemplate] FOREIGN KEY ([FileTemplateId]) REFERENCES [dbo].[FileTemplate] ([Id])
);

