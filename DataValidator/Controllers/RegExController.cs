﻿using Mgl.DataValidator.Models;
using Security.Authorize;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mgl.DataValidator.Controllers
{
    [AdminOnlyAuthorizeUser]
    public class RegExController : ControllerBase
    {
        RegExRepository _rer;
        public RegExController(RegExRepository rer)
        {
            _rer = rer;
        }

        public ActionResult Index()
        {
            IEnumerable<RegularExpression> regexs = _rer.GetAll();
            return View(regexs);
        }

        public ActionResult Details(int id)
        {            
            return View(_rer.GetById(id));
        }

        public ActionResult Create()
        {            
            return View(new RegularExpression());
        }

        [HttpPost]
        public ActionResult Create(RegularExpression re)
        {
            
            if (ModelState.IsValid)
            {
                _rer.Create(re);
                return RedirectToAction("Index").Success(re.Description + " has been created successfully.");
            }
            return RedirectToAction("Create").Success("Failed to create Regular Expression - " + re.Description);
        }

        public ActionResult Edit(int id)
        {
            return View(_rer.GetById(id));
        }


        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            RegularExpression re = _rer.GetById(id);
            TryUpdateModel(re);
            if (ModelState.IsValid)
            {
                _rer.Update(re);
                return RedirectToAction("Index").Success(re.Description + " has been updated successfully.");
            }
            return View(re).Danger(" Failed to update the regular expression - " + re.Description);
        }

        public ActionResult Delete(int id)
        {


            if (ModelState.IsValid)
            {
                RegularExpression re = _rer.GetById(id);

                if (re.DataFieldRules.Count > 0 )
                {
                    return RedirectToAction("Index").Danger(re.Description + " cannot be deleted due to the reference of other entities.");
                }
                _rer.Delete(re);
                return RedirectToAction("Index").Success(re.Description + " has been deleted successfully.");

            }
            return RedirectToAction("Index").Danger("System failed to delete the regular expression.");
        }
    }
}
