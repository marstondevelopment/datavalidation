﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models
{
    public class PrmsExcludeCasesAI : PrmsBase,IDBCmd
    {
        public PrmsExcludeCasesAI() { }
        public PrmsExcludeCasesAI(DTAction dt) : base(dt) { }

        [Display(Name = "Validation Method")]
        public int ValidationMethodId { get; set; }

        [JsonIgnore]
        public EnumValidationMethod ValidationMethod => (EnumValidationMethod)ValidationMethodId;

        [Display(Name = "Command Text")]
        public int? CmdTextId { get; set; }
        [JsonIgnore]
        public CmdText CmdText { get; set; }

        [Display(Name = "Stored Procedure")]
        public int? StoredProcedureId { get; set; }
        [JsonIgnore]
        public StoredProcedure StoredProcedure { get; set; }

        [Display(Name = "Output File Prefix")]
        public string OFPrefix { get; set; }
        [Display(Name = "Exception File Prefix")]
        public string EFPrefix { get; set; }
        [Display(Name = "Exception Message")]
        public string ExMessage { get; set; }
        [Display(Name = "Case Ref. Col Name")]
        public string CaseRefColName { get; set; }

    }
}