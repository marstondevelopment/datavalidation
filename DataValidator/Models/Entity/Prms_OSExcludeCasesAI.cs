﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Mgl.DataValidator.Models
{
    [MetadataType(typeof(Metadata))]
    public partial class Prms_OSExcludeCasesAI
    {

        public class Metadata
        {
            [Display(Name = "Case Reference")]
            public string CaseRef;
            [Display(Name = "Stored Procedures")]
            public int StoredProcedureId;
        }
    }


}