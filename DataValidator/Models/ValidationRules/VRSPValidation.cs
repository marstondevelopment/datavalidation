﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.ValidationRules
{
    public class VRSPValidation : VRBase
    {
        public VRSPValidation(DataFieldRule df) : base(df)
        {
            if (this.ParentFieldRule.StoredProcedure != null)
            {
                ParentFieldRule.StoredProcedure.ParentVR = this;
            }
        }

        public override bool ValidateField()
        {
            if (!string.IsNullOrEmpty(ParentFieldRule.DataField.OriginalValue))
            {
                if (this.ParentFieldRule.StoredProcedure == null)
                {
                    throw new MemberAccessException("Stored Procedure is not set for validation rule - " + this.ParentFieldRule.DataField.Name);
                }
                if (!this.ParentFieldRule.StoredProcedure.Validate())
                {
                    AppendError();
                    return false;
                }
            }
            return true;
        }
    }
}