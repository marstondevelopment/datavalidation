﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataCleansing
{
    public abstract class DCAction
    {
        public DCAction(DataFieldDataCleansing dc)
        {
            DataFieldDataCleansing = dc;
        }
        protected DataFieldDataCleansing DataFieldDataCleansing { get; set; }

        public abstract void ExecuteAction();

        public static DCAction New(DataFieldDataCleansing dc)
        {

            switch ((EnumDCAction) dc.DCId)
            {
                case EnumDCAction.RemoveSpace:
                    return new DCRemoveSpace(dc);
                case EnumDCAction.RemoveComma:
                    return new DCRemoveComma(dc);
                case EnumDCAction.ReplaceText:
                    return new DCReplaceText(dc);
                case EnumDCAction.PadLeft:
                    return new DCPaddingLeft(dc);
                case EnumDCAction.PadRight:
                    return new DCPaddingRight(dc);
                case EnumDCAction.StringToDate:
                    return new DCStringToDate(dc);
                case EnumDCAction.Trim:
                    return new DCTrim(dc);
                case EnumDCAction.ToUppercase:
                    return new DCToUppercase(dc);
                case EnumDCAction.ToLowercase:
                    return new DCToLowerCase(dc);
                case EnumDCAction.FormatValidPhoneNO:
                    return new DCFormatValidPhoneNo(dc);
                case EnumDCAction.RegexRemoveTexts:
                    return new DCRegexRemoveTexts(dc);
                case EnumDCAction.RemoveInvalidPhoneNO:
                    return new DCRemoveInvalidPhoneNo(dc);
                case EnumDCAction.DCTrimNonAlphabetChars:
                    return new DCTrimNonAlphabetChars(dc);
                default:
                    throw new NotSupportedException("Action not supported");
            }
        }
    }
}