﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Mgl.DataValidator.Models.DataTransformation
{
    public class OSSplitNameColumnIntoThree : DTStep
    {
        private PrmsSplitNameColumnIntoThree prms;
        private List<string> outputFields;
        public OSSplitNameColumnIntoThree(FileTemplateDTAction ftdt) : base(ftdt)
        {
            prms = ftdt.DTAction.PrmsSplitNameColumnIntoThree;
            if (prms == null)
            {
                throw new MemberAccessException("OSSplitNameColumnIntoThree parameters are missing for " + ftdt.DTAction.ActionName + ", please update the configuration settings and try again.");
            }
            outputFields = prms.OutputColumnNames.Split(Utility.DEFAULT_DELIMITER.ToCharArray()).Select(x=>x.Trim()).ToList();

            if (outputFields.Count != 3)
            {
                throw new MemberAccessException("Number of output columns for " + ftdt.DTAction.ActionName + " is incorrectly, please update the configuration settings and try again.");
            }

        }


        public override bool Execute()
        {
            if (prms.ImplementInDTStage)
            {
                ExecuteDTRule(ParentFTDT.FileTemplate.ProcessedDataTable);
            }
            else
            {
                ParentFTDT.FileTemplate.FileWriter.AddDefaultOutputFiles();
                var dts = ParentFTDT.FileTemplate.FileWriter.OutputFiles.Where(x => x.FileCategory == EnumOutputFileCategory.Output).ToArray();
                for (int i = 0; i < dts.Length; i++)
                {
                    DataTable dt = dts[i].Data;
                    ExecuteDTRule(dt);
                }
            }

            return true;
        }

        private void ExecuteDTRule(DataTable dt)
        {
            int colIndex = dt.Columns.IndexOf(prms.InputColumnName);

            if (colIndex >= 0)
            {
                foreach (var field in outputFields)
                {
                    if (dt.Columns[field] == null)
                    {
                        DataColumn col = dt.AddColumn(field);
                        col.SetOrdinal(colIndex);
                        colIndex++;
                    }
                }

                dt.AcceptChanges();
                foreach (DataRow rw in dt.Rows)
                {
                    List<string> elements = rw[prms.InputColumnName].ToString().Split(" ".ToCharArray()).Select(x => x.Trim()).ToList();
                    if (elements.Count > 0)
                    {
                        //Check for tile from the first element
                        EnumNameTitle title;
                        if (Enum.TryParse(elements[0].RemoveNoneAlphabeticChars().ToUpper(), out title))
                        {
                            rw[outputFields[0]] = elements[0].RemoveNoneAlphabeticChars();
                            elements.RemoveAt(0);
                        }
                    }
                    if (elements.Count > 0)
                    {
                        //Pick the last item as Surname
                        rw[outputFields[2]] = elements[elements.Count - 1];
                        elements.RemoveAt(elements.Count - 1);
                    }
                    if (elements.Count > 0)
                    {
                        //Render the First Name
                        rw[outputFields[1]] = string.Join(" ", elements.ToArray());
                    }

                }

            }


        }
    }
}